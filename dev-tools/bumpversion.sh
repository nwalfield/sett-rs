#!/usr/bin/env bash

set -e

HELP="Bump package version and update CHANGELOG.

Note: run this script from the root of the repository.

Requirements: git-cliff >= 2.2.0 (install it with \`cargo install git-cliff\`)

Usage: bumpversion.sh [OPTIONS] <PACKAGE>

Arguments:
  <PACKAGE>         Package to update. Options: sett, sett-cli, sett-gui, sett-rs

Options:
  -n, --dry-run     Run script without making any changes.
  -c, --custom      Custom version number to use. If not specified, the script
                    will infer the version number based on the commits content
                    since the last release.
  -h, --help:       Display this help message.

Examples:
  bumpversion.sh sett-cli
  bumpversion.sh --dry-run sett-cli
"

dry_run=false
cliff_config="dev-tools/cliff.toml"
package_cargo_lock="Cargo.lock"
version_new=""

if [ $# -eq 0 ]; then
  echo "💥 No package name specified" >&2
  echo "$HELP"
  exit 1
fi

while [[ $# -gt 0 ]]; do
  case $1 in
    -n | --dry-run)
      dry_run=true
      shift
      ;;
    -h | --help)
      echo "$HELP"
      exit 0
      ;;
    -c | --custom)
      if [ -z "$2" ]; then
        echo "💥 Missing version number" >&2
        echo "$HELP"
        exit 1
      fi
      version_new="$2"
      shift
      shift
      ;;
    -? | --*)
      echo "💥 Unsupported flag: $1" >&2
      echo "$HELP"
      exit 1
      ;;
    sett)
      package="sett"
      package_cargo_toml="sett/Cargo.toml"
      shift
      ;;
    sett-cli)
      package="sett-cli"
      package_cargo_toml="sett-cli/Cargo.toml"
      shift
      ;;
    sett-gui)
      package="sett-gui"
      package_cargo_toml="sett-gui/src-tauri/Cargo.toml"
      package_cargo_lock="sett-gui/src-tauri/Cargo.lock"
      shift
      ;;
    sett-rs)
      package="sett-rs"
      package_cargo_toml="sett-rs/Cargo.toml"
      shift
      ;;
    *)
      echo "💥 Wrong crate name" >&2
      echo "$HELP"
      exit 1
      ;;
  esac
done

# The `sed` in-place command behaves differently between the BSD (MacOS) and GNU (Linux)
# implementations. This makes the command portable.
# Default case for Linux sed, just use "-i"
case "$(uname)" in
  Darwin*) sedi=(-i "") ;; # For MacOS, use two parameters
  *) sedi=(-i) ;;
esac

export GIT_CLIFF__GIT__TAG_PATTERN="${package}/[0-9]*"
changelog_file="${package}/CHANGELOG.md"
cargo_package_name="$(sed -n '/\[package\]/,/^\[/ s/^ *name = "\(.*\)"$/\1/p' "${package_cargo_toml}")"

version_current="$(sed -n 's/^ *version = "\([0-9a-z\.\-]*\)"$/\1/p' "${package_cargo_toml}")"
echo "📌 Current version: ${version_current}" >&2

# Infer the new version number if not provided by the user.
if [ -z "$version_new" ]; then
  version_new=$(git cliff -c "$cliff_config" --include-path "${package}/**/*" --bumped-version | cut -d'/' -f2)
fi

echo "📦 Bumping to version $version_new" >&2
if [ "$dry_run" = false ]; then
  sed "${sedi[@]}" "s/\(^ *version = \"\)[0-9\.]*[a-z0-9\.\-]*\"\$/\1$version_new\"/" "${package_cargo_toml}"
  cd "$(dirname "${package_cargo_lock}")"
  cargo update -p "${cargo_package_name}"
  cd -
fi

tag_msg="${package}/${version_new}"
commit_msg="chore(release): ${tag_msg}"
files_to_commit=("${changelog_file}" "${package_cargo_toml}" "${package_cargo_lock}")
echo "📜 Generating changelog" >&2
echo
# Print the changelog to stdout
git cliff -c "$cliff_config" --include-path "${package}/**/*" -u --tag "${tag_msg}" >&2
echo
if [ "$dry_run" = false ]; then
  # Update the changelog file
  git cliff -c "$cliff_config" --include-path "${package}/**/*" -u -p "${changelog_file}" --tag "${tag_msg}"
  echo
  git add "${files_to_commit[@]}"
  git commit -q -m "$commit_msg"
  git tag -a "$tag_msg" -m "$commit_msg"
else
  echo "🚧 Dry run: Would have commited: ${files_to_commit[*]}" >&2
fi
echo "🖊️  New commit: $commit_msg" >&2
echo "🏷️  New tag: $tag_msg" >&2
echo "🚀 Version bumped to: $version_new" >&2
if [ "$dry_run" = false ]; then
  echo "👷 You can now push the new version using: git push --follow-tags origin $(git branch --show-current)" >&2
else
  echo "🚧 Dry run: completed" >&2
fi

# Print the new version on stdout, in case the user wants to retrieve this
# value for further usage by the shell.
echo "$version_new"
