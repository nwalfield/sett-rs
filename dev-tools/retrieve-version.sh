#!/usr/bin/env sh

# Retrieves the current version of `sett` from either the CI_COMMIT_TAG
# environment variable (if the variable is set), or from the Cargo.toml file.
#
# Expects the path to the Cargo.toml file as argument $1.

set -e

if [ -n "$CI_COMMIT_TAG" ]; then
  echo "$CI_COMMIT_TAG" | cut -d'/' -f2
else
  grep '^version = ' "$1" | cut -d"=" -f2 | tr -d ' "'
fi
