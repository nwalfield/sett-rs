# sett - secure encryption and transfer tool

**sett** stands for `Secure Encryption and Transfer Tool`.

Harnessing the power of the Rust programming language, **sett** is meticulously crafted to streamline the packaging, encryption,
and seamless transfer of your critical data. At its core, **sett** integrates the formidable
[Sequoia-PGP](https://sequoia-pgp.org/) for encryption, and uses _S3_ and _SFTP_ as transfer protocols, ensuring unparalleled
security and efficiency in data handling.

Offering both a sleek GUI experience with [sett-gui](sett-gui) and a powerful command-line interface with [sett-cli](sett-cli),
we empower users of all backgrounds to effortlessly safeguard and transport their valuable information.

Born out of the innovative corridors of the [BioMedIT](https://www.biomedit.ch/) project, **sett** represents the pinnacle of collaborative
ingenuity and cutting-edge technology. Backed by a team of passionate developers committed to excellence, **sett** stands
as a testament to our unwavering dedication to advancing digital security in an ever-evolving landscape.

This repository is composed of four subprojects, each in its own directory:

- [sett](sett) - the main crate containing the main library.
- [sett-cli](sett-cli) - a _command line interface_ to the main library.
- [sett-gui](sett-gui) - a _graphical user interface_ to the main library.
- [sett-rs](sett-rs) - Python bindings to the main library.
