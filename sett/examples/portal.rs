use anyhow::Result;
use sett::portal::Portal;

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    let portal = Portal::new("http://localhost:8000")?;
    let fingerprints = portal
        .get_key_status(&["095F7F80127F704CDC0CA991B24CE13C32FCE9B4"])
        .await?;
    println!("Fingerprint status received: {:?}", fingerprints);
    Ok(())
}
