//! Simple command line interface to test an SFTP connection with 2FA.
//!
//! Usage:
//!    cargo run --example two_factor_auth -- <username> <host> <port> [<key>]

use std::env;
use std::path::Path;

use anyhow::{Context, Result};

fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let username = args.get(1).context("missing username")?.into();
    let host = args.get(2).context("missing host")?.into();
    let port = args
        .get(3)
        .context("missing port")?
        .parse::<u16>()
        .context("parse port")?;
    let key = args.get(4);
    println!("user = '{username}', host = '{host}', port = '{port}', key = '{key:?}'");
    let sftp_opts = sett::destination::sftp::SftpOpts {
        host,
        port,
        username,
        base_path: Path::new(".").into(),
        key_path: key.map(Into::into),
        two_factor_callback: Some(Box::new(two_factor_prompt)),
        ..Default::default()
    };
    sett::destination::sftp::connect(&sftp_opts)?;
    println!("Connection successful");
    Ok(())
}

fn two_factor_prompt() -> Result<String> {
    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .context("Failed to read input")?;
    Ok(input.replace('\n', ""))
}
