//! Decrypt workflow

use std::{
    collections::BTreeMap,
    io::{self, Write as _},
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

use anyhow::Context as _;
use flate2::read::GzDecoder;
use sequoia_openpgp::{
    crypto::Password,
    parse::{stream::DecryptorBuilder, Parse},
    policy::StandardPolicy,
    Cert,
};
use sha2::{Digest, Sha256};
use tracing::instrument;
use walkdir::WalkDir;

use crate::{
    certstore,
    checksum::{read_checksum_file, verify_checksums},
    dpkg::{self, CompressionAlgorithm, Package},
    filesystem::get_combined_file_size,
    task::{Mode, Status},
    utils::{Progress, ProgressReader},
};

const HEAP_BUFFER_SIZE: usize = 1 << 22;

/// Options required by the decrypt workflow
pub struct DecryptOpts<T: Progress> {
    /// Input file for decryption.
    pub package: Package,
    /// Private OpenPGP keys of recipients (used for decryption data).
    pub recipients: Vec<Vec<u8>>,
    /// Public OpenPGP key of data package signer (used for verifying signatures).
    pub signer: Vec<u8>,
    /// Password for decrypting recipients' keys.
    pub password: Option<Password>,
    /// Output path for the decrypted data.
    pub output: Option<PathBuf>,
    /// Decrypt data without unpacking it.
    pub decrypt_only: bool,
    /// Run the workflow or only perform a check.
    pub mode: Mode,
    /// Report decryption progress using this callback.
    pub progress: Option<T>,
}

impl<T: Progress> std::fmt::Debug for DecryptOpts<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DecryptOpts")
            .field("package", &self.package)
            .field("password", &self.password.as_ref().map(|_| "***"))
            .field("output", &format_args!("{:?}", self.output.as_ref()))
            .field("decrypt_only", &self.decrypt_only)
            .field("mode", &self.mode)
            .finish()
    }
}

/// Decrypts and (optionally) decompresses a data package.
///
/// While decrypting/decompressing signatures and checksums are verified.
#[instrument(level = "debug")]
pub async fn decrypt(
    opts: DecryptOpts<impl Progress + Send + Sync + 'static>,
) -> anyhow::Result<Status> {
    let output = get_output_path(opts.output, opts.package.path())?;
    let policy = StandardPolicy::new();
    let recipients = opts
        .recipients
        .iter()
        .map(Cert::from_bytes)
        .collect::<Result<Vec<_>, _>>()?;
    let signer = Cert::from_bytes(&opts.signer)?;
    let helper = crate::crypt::Helper::new(
        &recipients.iter().collect::<Vec<_>>(),
        &[&signer],
        &policy,
        opts.password.as_ref(),
    )?;
    let package = opts
        .package
        .verify(&certstore::CertBytes::from(&opts.signer[..]))?;
    let metadata = package.metadata()?;
    let (data_reader, data_size) = package.data().await?;
    let mut data_reader = tokio_util::io::SyncIoBridge::new(data_reader);
    let status = tokio::task::spawn_blocking(move || -> anyhow::Result<_> {
        let mut decryptor =
            DecryptorBuilder::from_reader(&mut data_reader)?.with_policy(&policy, None, helper)?;
        let status = if let Mode::Check = opts.mode {
            Status::Checked {
                destination: output.to_string_lossy().to_string(),
                source_size: data_size,
            }
        } else {
            std::fs::create_dir_all(&output)?;
            if let Some(pg) = opts.progress {
                pg.set_length(data_size);
                let mut p = ProgressReader::new(decryptor, |len| {
                    pg.inc(len.try_into()?);
                    Ok(())
                });
                if opts.decrypt_only {
                    write_to_file(&mut p, &output)?;
                } else {
                    unpack(&mut p, &output, metadata.compression_algorithm)?;
                }
                pg.finish();
            } else if opts.decrypt_only {
                write_to_file(&mut decryptor, &output)?;
            } else {
                unpack(&mut decryptor, &output, metadata.compression_algorithm)?;
            }

            let output_files = WalkDir::new(&output)
                .into_iter()
                .flatten()
                .filter(|entry| entry.file_type().is_file())
                .map(|entry| entry.into_path());

            Status::Completed {
                source_size: data_size,
                destination_size: get_combined_file_size(output_files)?,
                destination: output.to_string_lossy().to_string(),
            }
        };
        Ok(status)
    })
    .await??;
    status.log();
    Ok(status)
}

/// Returns output path based on the provided or default path and the package name.
fn get_output_path(output: Option<PathBuf>, pkg_path: &Path) -> anyhow::Result<PathBuf> {
    let base = if let Some(p) = output {
        p
    } else {
        std::env::current_dir()?
    };
    let pkg_name = pkg_path
        .file_name()
        .context("Unable to get package file name")?
        .to_string_lossy();
    let pkg_base_name = pkg_name
        .split('.')
        .next()
        .context("Package file has no extension")?;
    let mut output = base.join(pkg_base_name);
    let mut i = 1;
    while output.exists() {
        output = base.join(format!("{pkg_base_name}_{i}"));
        i += 1;
    }
    Ok(output)
}

/// Decompresses source while writing to destination.
fn unpack<R: io::Read + Send>(
    source: &mut R,
    output: &Path,
    compression_algorithm: CompressionAlgorithm,
) -> anyhow::Result<()> {
    match compression_algorithm {
        CompressionAlgorithm::Stored => unpack_tar(&mut tar::Archive::new(source), output),
        CompressionAlgorithm::Gzip(_) => {
            unpack_tar(&mut tar::Archive::new(GzDecoder::new(source)), output)
        }
        CompressionAlgorithm::Zstandard(_) => unpack_tar(
            &mut tar::Archive::new(zstd::stream::read::Decoder::new(source)?),
            output,
        ),
    }?;
    Ok(())
}

/// Returns the destination path for a file extracted from a tar archive.
///
/// It sanitizes the file path to prevent tar bombs and resolves symbolic links.
///
/// Note: the sanitization implementation is taken from the `tar` crate.
fn sanitize_path(dest: &Path, path: &Path) -> anyhow::Result<PathBuf> {
    use std::path::Component;
    let mut sanitized = PathBuf::new();

    for part in path.components() {
        match part {
            // Leading '/' characters, root paths, and '.'
            // components are just ignored and treated as "empty
            // components"
            Component::Prefix(_) | Component::RootDir | Component::CurDir => continue,

            // If any part of the filename is '..', then skip over
            // unpacking the file to prevent directory traversal
            // security issues.  See, e.g.: CVE-2001-1267,
            // CVE-2002-0399, CVE-2005-1918, CVE-2007-4131
            Component::ParentDir => anyhow::bail!("file path contains a relative part"),

            Component::Normal(part) => sanitized.push(part),
        }
    }
    anyhow::ensure!(sanitized.parent().is_some(), "empty file path");
    Ok(dest.join(&sanitized))
}

enum Message {
    Init(PathBuf),
    Payload(bytes::Bytes),
    Finalize,
}

fn unpack_tar(archive: &mut tar::Archive<impl io::Read>, dest: &Path) -> anyhow::Result<()> {
    let (tx_checksum, rx_checksum) = std::sync::mpsc::sync_channel(8);
    let (tx_write, rx_write) = std::sync::mpsc::sync_channel(8);
    let checksums = Arc::new(Mutex::new(BTreeMap::new()));
    let checksums_clone = Arc::clone(&checksums);

    let checksum_handle = std::thread::spawn(move || {
        let mut hasher = Sha256::new();
        let mut path = None;
        let mut checksums = checksums_clone.lock().unwrap();
        while let Ok(message) = rx_checksum.recv() {
            match message {
                Message::Init(p) => {
                    path = Some(p);
                }
                Message::Payload(buf) => hasher.update(&buf),
                Message::Finalize => {
                    checksums.insert(
                        std::mem::take(&mut path).expect("path is initialized"),
                        crate::checksum::to_hex_string(
                            &std::mem::replace(&mut hasher, Sha256::new()).finalize(),
                        ),
                    );
                }
            }
        }
    });

    let write_handle = std::thread::spawn(move || -> io::Result<()> {
        let mut writer = None;
        while let Ok(message) = rx_write.recv() {
            match message {
                Message::Init(p) => {
                    if let Some(parent) = p.parent() {
                        if !parent.exists() {
                            std::fs::create_dir_all(parent)?;
                        }
                    }
                    writer = Some(io::BufWriter::with_capacity(
                        HEAP_BUFFER_SIZE,
                        std::fs::File::create(&p)?,
                    ));
                }
                Message::Payload(buf) => writer
                    .as_mut()
                    .expect("writer is initialized")
                    .write_all(&buf)?,
                Message::Finalize => {
                    writer = None;
                }
            }
        }
        Ok(())
    });

    for entry in archive.entries()? {
        let mut entry = entry?;
        let archive_path = entry.path()?.into_owned();
        let output_path = match sanitize_path(dest, &archive_path) {
            Ok(p) => p,
            Err(e) => {
                tracing::warn!("{:?}: {}", archive_path, e);
                continue;
            }
        };
        tx_checksum.send(Message::Init(archive_path))?;
        tx_write.send(Message::Init(output_path))?;
        copy_to_channels(&mut entry, [&tx_checksum, &tx_write])?;
        tx_checksum.send(Message::Finalize)?;
        tx_write.send(Message::Finalize)?;
    }
    drop(tx_checksum);
    drop(tx_write);

    anyhow::ensure!(checksum_handle.join().is_ok(), "checksum thread panicked");
    anyhow::ensure!(write_handle.join().is_ok(), "write thread panicked");

    let mut checksums = checksums.lock().unwrap();
    checksums.remove(Path::new(dpkg::CHECKSUM_FILE));
    verify_checksums(
        &checksums,
        &read_checksum_file(dest.join(dpkg::CHECKSUM_FILE))?,
    )?;
    Ok(())
}

fn copy_to_channels<const N: usize>(
    reader: &mut impl io::Read,
    tx: [&std::sync::mpsc::SyncSender<Message>; N],
) -> anyhow::Result<()> {
    let mut buf = [0; 8192];
    let mut bigbuf = bytes::BytesMut::with_capacity(HEAP_BUFFER_SIZE);
    macro_rules! exchange {
        ($buffer:expr) => {{
            let b = std::mem::replace(&mut bigbuf, $buffer).freeze();
            for tx in tx {
                tx.send(Message::Payload(b.clone()))?;
            }
        }};
    }
    loop {
        let n = reader.read(&mut buf)?;
        if n == 0 {
            if !bigbuf.is_empty() {
                exchange!(bytes::BytesMut::new());
            }
            break;
        }
        if bigbuf.len() + n > bigbuf.capacity() {
            exchange!(bytes::BytesMut::with_capacity(HEAP_BUFFER_SIZE));
        }
        bigbuf.extend_from_slice(&buf[..n]);
    }
    Ok(())
}

/// Writes source to a file.
fn write_to_file<R: io::Read, P: AsRef<Path>>(source: &mut R, output: P) -> anyhow::Result<()> {
    let mut f = std::fs::File::create(output.as_ref().join(dpkg::DATA_FILE))?;
    io::copy(source, &mut f)?;
    Ok(())
}
