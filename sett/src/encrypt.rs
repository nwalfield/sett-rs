//! Encrypt workflow

use std::{
    io::{self, Write as _},
    path::{Path, PathBuf},
};

use anyhow::Context as _;
use bytes::BytesMut;
use chrono::{DateTime, Utc};
use flate2::{write::GzEncoder, Compression};
use sequoia_openpgp::{
    crypto::{KeyPair, Password},
    parse::Parse,
    policy::StandardPolicy,
    Cert, Fingerprint,
};
use tokio::io::AsyncWriteExt;
use tracing::{debug, instrument, trace};

use crate::{
    certstore,
    checksum::{generate_checksums_file_content, Sha256ChecksumWriter},
    crypt,
    destination::Destination,
    dpkg::{self, CompressionAlgorithm},
    filesystem::{check_space, check_writeable, get_common_path},
    task::{Mode, Status},
    utils::{get_max_cpu, Progress, ProgressReader},
    zip::ZipArchive,
};

/// Options required by the encrypt workflow.
#[derive(Default)]
pub struct EncryptOpts<T: Progress> {
    /// Input files for encryption.
    pub files: Vec<PathBuf>,
    /// Public OpenPGP keys of recipients.
    pub recipients: Vec<Vec<u8>>,
    /// Private OpenPGP key for signer (used for signing data).
    pub signer: Option<Vec<u8>>,
    /// Password for decrypting the signer's key.
    pub password: Option<Password>,
    /// Algorithm used for compressing data before encryption.
    pub compression_algorithm: dpkg::CompressionAlgorithm,
    /// Run the workflow or only perform a check.
    pub mode: Mode,
    /// The maximum number of CPU cores used by the workflow (minimum: 2).
    pub max_cpu: Option<usize>,
    /// Encryption progress handler.
    pub progress: Option<T>,
    /// Purpose, see [`dpkg::Purpose`] for more details.
    pub purpose: Option<dpkg::Purpose>,
    /// Data transfer ID, see [`dpkg::PkgMetadata::transfer_id`] for more details.
    pub transfer_id: Option<u32>,
    /// Timestamp of the data package.
    pub timestamp: DateTime<Utc>,
    /// Prefix for the data package name.
    pub prefix: Option<String>,
}

impl<T: Progress> std::fmt::Debug for EncryptOpts<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("EncryptOpts")
            .field("files", &format_args!("{:?}", self.files))
            .field("password", &self.password.as_ref().map(|_| "***"))
            .field("compression_algorithm", &self.compression_algorithm)
            .field("mode", &self.mode)
            .field("max_cpu", &self.max_cpu)
            .field("purpose", &self.purpose)
            .field("transfer_id", &self.transfer_id)
            .finish()
    }
}

/// Creates a data package with encrypted and optionally compressed data along
/// with checksums and metadata.
///
/// The data package can be stored in either the local filesystem or on a
/// remote server.
#[instrument(level = "debug")]
pub async fn encrypt(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    dest: Destination<'static>,
) -> anyhow::Result<Status> {
    trace!("Extract signing and encryption certificates");
    let policy = StandardPolicy::new();
    let signer_cert = opts.signer.as_ref().map(Cert::from_bytes).transpose()?;
    let signer_key = signer_cert
        .as_ref()
        .map(|cert| crypt::get_signing_key(cert, opts.password.as_ref(), &policy))
        .transpose()?;
    let recipients_certs = opts
        .recipients
        .iter()
        .map(Cert::from_bytes)
        .collect::<anyhow::Result<Vec<_>>>()?;
    let recipients_keys = crypt::get_encryption_keys(&recipients_certs, &policy)?;
    let keys = EncryptKeys {
        signer: match (signer_key, signer_cert) {
            (Some(key), Some(cert)) => Some((key, cert.fingerprint())),
            _ => None,
        },
        recipients: recipients_keys
            .into_iter()
            .zip(recipients_certs.iter().map(|cert| cert.fingerprint()))
            .collect(),
    };
    let status = match dest {
        Destination::Stdout => encrypt_stdout(opts, keys).await,
        Destination::Local(local_opts) => encrypt_local(opts, &local_opts, keys).await,
        Destination::Sftp(sftp_opts) => encrypt_sftp(opts, &sftp_opts, keys).await,
        Destination::S3(s3_opts) => encrypt_s3(opts, &s3_opts, keys).await,
    }?;
    status.log();
    Ok(status)
}

/// A collection of keys necessary for creating a data package.
///
/// In addition to the key pair, we also store the fingerprint of the
/// certificate, which is used to identify the signer and recipients
/// in the metadata file.
struct EncryptKeys {
    signer: Option<(KeyPair, Fingerprint)>,
    recipients: Vec<(
        sequoia_openpgp::packet::Key<
            sequoia_openpgp::packet::key::PublicParts,
            sequoia_openpgp::packet::key::UnspecifiedRole,
        >,
        Fingerprint,
    )>,
}

/// Returns paths where the common prefix (to all paths) is stripped.
fn get_archive_paths(files: &[PathBuf]) -> anyhow::Result<Vec<PathBuf>> {
    let parent_folders = files
        .iter()
        .map(|f| {
            f.parent()
                .with_context(|| format!("Unable to find parent folder of {f:?}"))
        })
        .collect::<Result<Vec<_>, _>>()?;
    let root_dir = get_common_path(&parent_folders)?;
    let archive_paths = files
        .iter()
        .map(|f| Ok(Path::new(dpkg::CONTENT_FOLDER).join(f.strip_prefix(&root_dir)?)))
        .collect::<anyhow::Result<_>>()?;
    Ok(archive_paths)
}

/// Initializes and return an OpenPGP message, which can be used for writing
/// data into it.
fn init_message<'a, W: io::Write + Send + Sync>(
    writer: &'a mut W,
    keys: &'a EncryptKeys,
) -> anyhow::Result<sequoia_openpgp::serialize::stream::Message<'a>> {
    use sequoia_openpgp::serialize::stream::{
        Encryptor2, LiteralWriter, Message, Recipient, Signer,
    };
    let message = Encryptor2::for_recipients(
        Message::new(writer),
        keys.recipients.iter().map(|(key, _)| Recipient::from(key)),
    )
    .build()?;
    let message = match keys.signer.as_ref() {
        Some((key, _)) => Signer::new(message, key.clone()).build()?,
        None => message,
    };
    LiteralWriter::new(message).build()
}

/// Adds a file to the inner tar archive containing checksums of the individual
/// data files.
fn add_checksum_file(
    archive: &mut tar::Builder<impl io::Write>,
    content: &[u8],
) -> anyhow::Result<()> {
    let mut header = tar::Header::new_gnu();
    header.set_entry_type(tar::EntryType::file());
    header.set_size(content.len().try_into()?);
    header.set_mtime(Utc::now().timestamp().try_into()?);
    header.set_mode(0o644);
    header.set_cksum();
    archive.append_data(&mut header, dpkg::CHECKSUM_FILE, content)?;
    Ok(())
}

/// Writes files into a tarball.
fn create_tarball<P: AsRef<Path>>(
    writer: &mut impl io::Write,
    file_path: &[(P, P)],
    max_cpu: usize,
    progress: Option<&impl Progress>,
) -> anyhow::Result<()> {
    let mut archive = tar::Builder::new(writer);
    if let Some(pg) = progress {
        for (f, p) in file_path {
            let f = std::fs::File::open(f)?;
            let mut header = tar::Header::new_gnu();
            let metadata = f.metadata()?;
            header.set_metadata(&metadata);
            header.set_cksum();
            let reader = ProgressReader::new(f, |len| {
                pg.inc(len.try_into()?);
                Ok(())
            });
            archive.append_data(&mut header, p, reader)?;
        }
    } else {
        for (f, p) in file_path {
            archive.append_path_with_name(f, p)?;
        }
    }
    add_checksum_file(
        &mut archive,
        generate_checksums_file_content(file_path, max_cpu)?.as_bytes(),
    )?;
    archive.finish()?;
    Ok(())
}

/// Adds the metadata file to the data package (outer zip file).
fn add_metadata_file(
    archive: &mut ZipArchive<impl io::Write>,
    metadata: &dpkg::PkgMetadata,
    signer_key: Option<KeyPair>,
) -> anyhow::Result<()> {
    let zip_file_opts = Default::default();
    archive.add(dpkg::METADATA_FILE, &zip_file_opts)?;
    let metadata_json = serde_json::to_string(&metadata)?.as_bytes().to_owned();
    archive.write_all(&metadata_json)?;
    if let Some(key) = signer_key {
        archive.add(dpkg::METADATA_SIG_FILE, &zip_file_opts)?;
        archive.write_all(&crypt::sign_detached(
            &metadata_json,
            key,
            certstore::SerializationFormat::AsciiArmored,
        )?)?;
    }
    Ok(())
}

/// Processes input data files.
///
/// Returns:
///
/// - A vector pairs where the first element is the absolute path to the file
///   and the second element is the path inside the data package, i.e. a path
///   where the common prefix (for all files) is stripped.
/// - The combined size of all files.
///
/// Processing steps:
///
/// - Walk directories and return individual files.
/// - Normalize paths (make them absolute).
/// - Remove duplicated paths.
/// - Verify that archive paths contain only valid UTF-8 characters
///   (later in the workflow it's necessary to write paths as strings).
/// - Calculate the combined size of the input.
fn process_files<P: AsRef<Path>>(files: &[P]) -> anyhow::Result<(Vec<(PathBuf, PathBuf)>, u64)> {
    let mut total_input_size = 0u64;
    let mut unique_files = std::collections::HashSet::new();
    for entry in files.iter().flat_map(walkdir::WalkDir::new) {
        let entry = entry?;
        if entry.file_type().is_file() {
            total_input_size += entry.metadata()?.len();
            unique_files.insert(entry.path().canonicalize()?.to_owned());
        }
    }
    let files = Vec::from_iter(unique_files);
    let archive_paths = get_archive_paths(&files)?;
    archive_paths.iter().try_for_each(|p| {
        p.to_str()
            .and(Some(()))
            .with_context(|| format!("Path contains non-UTF-8 characters: {p:?}"))
    })?;
    let file_archive_path = files.into_iter().zip(archive_paths).collect();
    Ok((file_archive_path, total_input_size))
}

/// Spawns a blocking task to handle the encryption and compression pipeline.
fn spawn_encryption_task(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    file_path: Vec<(PathBuf, PathBuf)>,
    total_input_size: u64,
    keys: EncryptKeys,
    source: crate::io::Source,
    sink: tokio::sync::mpsc::Sender<BytesMut>,
) -> tokio::task::JoinHandle<anyhow::Result<u64>> {
    tokio::task::spawn_blocking(move || -> anyhow::Result<u64> {
        let writer = crate::io::ChannelWriter::new(source, sink)?;
        let mut zip = ZipArchive::new(writer);
        zip.add(dpkg::DATA_FILE_ENCRYPTED, &Default::default())?;
        let mut checksum_writer = Sha256ChecksumWriter::new(&mut zip);
        let mut encrypted_message = init_message(&mut checksum_writer, &keys)?;

        let progress = opts.progress.as_ref();
        if let Some(p) = progress {
            p.set_length(total_input_size);
        }
        super::io::ThreadWriter::default().write(&mut encrypted_message, |w| {
            let max_cpu = opts.max_cpu.unwrap_or_else(get_max_cpu);
            match opts.compression_algorithm {
                CompressionAlgorithm::Stored => create_tarball(w, &file_path, max_cpu, progress),
                CompressionAlgorithm::Gzip(level) => {
                    let mut enc = GzEncoder::new(
                        w,
                        Compression::new(level.unwrap_or_else(|| Compression::default().level())),
                    );
                    create_tarball(&mut enc, &file_path, max_cpu, progress)
                }
                CompressionAlgorithm::Zstandard(level) => {
                    let mut enc = zstd::stream::write::Encoder::new(
                        w,
                        level.unwrap_or(zstd::DEFAULT_COMPRESSION_LEVEL),
                    )?;
                    enc.multithread(max_cpu as u32)?;
                    create_tarball(&mut enc, &file_path, max_cpu, progress)?;
                    enc.finish()?;
                    Ok(())
                }
            }
        })?;
        encrypted_message.finalize()?;

        let data_checksum = checksum_writer.get_hash();
        add_metadata_file(
            &mut zip,
            &dpkg::PkgMetadata {
                sender: keys
                    .signer
                    .as_ref()
                    .map_or_else(|| "".into(), |(_, fp)| fp.to_hex()),
                recipients: keys.recipients.iter().map(|(_, fp)| fp.to_hex()).collect(),
                checksum: data_checksum,
                timestamp: opts.timestamp,
                version: dpkg::default_version(),
                checksum_algorithm: Default::default(),
                compression_algorithm: opts.compression_algorithm.clone(),
                transfer_id: opts.transfer_id,
                purpose: opts.purpose,
            },
            keys.signer.as_ref().map(|(key, _)| key.clone()),
        )?;
        let size = zip.finish()?;

        if let Some(p) = progress {
            p.finish();
        }
        Ok(size)
    })
}

async fn encrypt_to_writer(
    output: impl tokio::io::AsyncWrite,
    opts: EncryptOpts<impl Progress + Send + 'static>,
    file_path: Vec<(PathBuf, PathBuf)>,
    total_input_size: u64,
    keys: EncryptKeys,
) -> anyhow::Result<u64> {
    let (result_tx, mut result_rx) = tokio::sync::mpsc::channel(3);
    let (buf_pool_tx, buf_pool_rx) = tokio::sync::mpsc::channel(3);
    for _ in 0..3 {
        buf_pool_tx.send(BytesMut::with_capacity(1 << 22)).await?;
    }
    let encrypt_task_handle = spawn_encryption_task(
        opts,
        file_path,
        total_input_size,
        keys,
        crate::io::Source::Channel(buf_pool_rx),
        result_tx,
    );
    tokio::pin!(output);
    while let Some(chunk) = result_rx.recv().await {
        output.write_all(&chunk).await?;
        if buf_pool_tx.send(chunk).await.is_err() {
            // Under "normal" conditions this can only happen once, when the encryption thread
            // is done and the receiver end of this channel is closed.
            trace!("Failed to send chunk back to the pool (channel closed)");
        }
    }
    encrypt_task_handle.await?
}

async fn encrypt_to_blocking_writer(
    output: &mut impl std::io::Write,
    opts: EncryptOpts<impl Progress + Send + 'static>,
    file_path: Vec<(PathBuf, PathBuf)>,
    total_input_size: u64,
    keys: EncryptKeys,
) -> anyhow::Result<u64> {
    let (result_tx, mut result_rx) = tokio::sync::mpsc::channel(3);
    let (buf_pool_tx, buf_pool_rx) = tokio::sync::mpsc::channel(3);
    for _ in 0..3 {
        buf_pool_tx.send(BytesMut::with_capacity(1 << 22)).await?;
    }
    let encrypt_task_handle = spawn_encryption_task(
        opts,
        file_path,
        total_input_size,
        keys,
        crate::io::Source::Channel(buf_pool_rx),
        result_tx,
    );
    while let Some(chunk) = result_rx.recv().await {
        output.write_all(&chunk)?;
        if buf_pool_tx.send(chunk).await.is_err() {
            // Under "normal" conditions this can only happen once, when the encryption thread
            // is done and the receiver end of this channel is closed.
            trace!("Failed to send chunk back to the pool (channel closed)");
        }
    }
    encrypt_task_handle.await?
}

/// Encrypt to a file on disk.
async fn encrypt_local(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    local_opts: &crate::destination::local::LocalOpts,
    keys: EncryptKeys,
) -> anyhow::Result<Status> {
    debug!("Verify files to encrypt");
    let (file_path, source_size) = process_files(&opts.files)?;
    let output_path = local_opts
        .path
        .join(local_opts.package_name(&opts.timestamp, opts.prefix.as_deref()));
    let destination = output_path.to_string_lossy().into_owned();
    let parent = output_path
        .parent()
        .context("Unable to find output directory")?;
    trace!("Verify available disk space");
    check_space(source_size, parent, opts.mode)?;
    trace!("Verify whether destination is writeable");
    check_writeable(parent, opts.mode)?;
    Ok(if let Mode::Check = opts.mode {
        Status::Checked {
            destination,
            source_size,
        }
    } else {
        let output = tokio::fs::File::create(&output_path).await?;
        let mut buffered_output = tokio::io::BufWriter::with_capacity(1 << 22, output);
        let package_size =
            encrypt_to_writer(&mut buffered_output, opts, file_path, source_size, keys)
                .await
                .with_context(|| {
                    // If an error occurs while creating the `.zip` file, delete
                    // the partial file.
                    let err_msg = "encryption failed";
                    if let Err(msg) = std::fs::remove_file(&output_path) {
                        anyhow::anyhow!(
                            "{}. Additionally, failed to clean partial file '{}', reason: {}",
                            err_msg,
                            &destination,
                            msg
                        )
                    } else {
                        anyhow::anyhow!(err_msg)
                    }
                })?;
        buffered_output.flush().await?;
        Status::Completed {
            destination,
            source_size,
            destination_size: package_size,
        }
    })
}

/// Encrypt to standard output.
async fn encrypt_stdout(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    keys: EncryptKeys,
) -> anyhow::Result<Status> {
    debug!("Verify files to encrypt");
    let (file_path, source_size) = process_files(&opts.files)?;
    let destination = "stdout".to_string();
    Ok(if let Mode::Check = opts.mode {
        Status::Checked {
            destination,
            source_size,
        }
    } else {
        let package_size =
            encrypt_to_writer(tokio::io::stdout(), opts, file_path, source_size, keys).await?;
        Status::Completed {
            destination,
            source_size,
            destination_size: package_size,
        }
    })
}

async fn encrypt_s3(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    s3_opts: &crate::destination::s3::S3Opts<'_>,
    keys: EncryptKeys,
) -> anyhow::Result<Status> {
    let object_name = dpkg::generate_package_name(&opts.timestamp, opts.prefix.as_deref());
    debug!("Verify files to encrypt");
    let (file_path, source_size) = process_files(&opts.files)?;
    let (client, endpoint_url) = crate::destination::s3::build_client(s3_opts).await?;
    let bucket = s3_opts.bucket.as_ref();
    let destination = [&endpoint_url, bucket, &object_name].join("/");
    if let Mode::Check = opts.mode {
        return Ok(Status::Checked {
            destination,
            source_size,
        });
    }
    let (result_tx, result_rx) = tokio::sync::mpsc::channel(3);
    let chunk_size = crate::destination::s3::compute_chunk_size(source_size);
    let encrypt_task_handle = spawn_encryption_task(
        opts,
        file_path,
        source_size,
        keys,
        crate::io::Source::New(chunk_size),
        result_tx,
    );
    crate::destination::s3::put_object(client, bucket, &object_name, result_rx).await?;
    let package_size = encrypt_task_handle.await??;
    Ok(Status::Completed {
        destination,
        source_size,
        destination_size: package_size,
    })
}

/// Encrypts to a file or a remote SFTP server.
async fn encrypt_sftp(
    opts: EncryptOpts<impl Progress + Send + 'static>,
    sftp_opts: &crate::destination::sftp::SftpOpts<'_>,
    keys: EncryptKeys,
) -> anyhow::Result<Status> {
    debug!("Verify files to encrypt");
    let (file_path, source_size) = process_files(&opts.files)?;
    let sftp = crate::destination::sftp::connect(sftp_opts)?;
    let upload_dir = crate::destination::sftp::UploadDir::new(
        &sftp_opts.base_path,
        sftp_opts.envelope_dir.as_deref(),
        &sftp,
    );
    let dpkg_path = crate::destination::sftp::DpkgPath::new(
        &upload_dir.path,
        dpkg::generate_package_name(&opts.timestamp, opts.prefix.as_deref()),
        &sftp,
    );
    let destination = sftp_opts.get_url(&dpkg_path.path);
    if let Mode::Check = opts.mode {
        return Ok(Status::Checked {
            destination,
            source_size,
        });
    }
    upload_dir.create(None)?;
    let mut buffered_output =
        std::io::BufWriter::with_capacity(1 << 22, sftp.create(&dpkg_path.tmp)?);
    let package_size =
        encrypt_to_blocking_writer(&mut buffered_output, opts, file_path, source_size, keys)
            .await
            .with_context(|| {
                // If an error occurs while creating the `.zip` file, delete
                // the partial file on the SFTP server.
                let err_msg = "encryption failed";
                if let Err(msg) = upload_dir.delete() {
                    anyhow::anyhow!(
                        "{}. Additionally, failed to clean partially uploaded data \
                    at '{}', reason: {}",
                        err_msg,
                        destination,
                        msg
                    )
                } else {
                    anyhow::anyhow!(err_msg)
                }
            })?;
    buffered_output.flush()?;
    dpkg_path.finalize()?;
    upload_dir.finalize()?;
    Ok(Status::Completed {
        source_size,
        destination_size: package_size,
        destination,
    })
}
