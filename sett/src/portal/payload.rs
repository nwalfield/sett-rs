use serde::Serialize;

#[derive(Serialize, Debug)]
pub(super) struct KeyStatusPayload<'a> {
    pub(super) fingerprints: &'a [SequoiaFingerprint],
}
/// `newtype` for being able to define a validator and serialization
#[derive(Debug)]
pub(super) struct SequoiaFingerprint(sequoia_openpgp::Fingerprint);

/// Fingerprint validator based on `sequoia_openpgp::Fingerprint`
impl SequoiaFingerprint {
    pub(super) fn new<S: AsRef<str>>(hex: S) -> anyhow::Result<SequoiaFingerprint> {
        match hex.as_ref().parse()? {
            parsed @ sequoia_openpgp::Fingerprint::V4(_) => Ok(SequoiaFingerprint(parsed)),
            _ => Err(anyhow::anyhow!("Invalid fingerprint '{}'", hex.as_ref())),
        }
    }
}

/// `Serialize` implementation for `sequoia_openpgp::Fingerprint`
impl Serialize for SequoiaFingerprint {
    fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        serializer.serialize_str(&self.0.to_hex())
    }
}

#[derive(Serialize, Debug)]
pub(super) struct CheckPackage<'a> {
    pub(super) metadata: &'a str,
    pub(super) file_name: &'a str,
}

#[cfg(feature = "auth")]
#[derive(Serialize, Debug)]
pub(super) struct Sts {
    pub(super) dtr: u32,
}

#[cfg(test)]
mod tests {

    use super::{super::ApprovalStatus, super::KeyStatus, KeyStatusPayload, SequoiaFingerprint};

    #[test]
    fn test_err_fingerprints() {
        for fingerprint in ["1234af", &"M".repeat(4), &"1234af".repeat(40)] {
            assert!(SequoiaFingerprint::new(fingerprint).is_err())
        }
    }

    #[test]
    fn test_ok_fingerprints() {
        for fingerprint in [
            "D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3",   // Normal
            "d99ad936fc83c9babde7c33e1cf8c1a2076818c3",   // all lowercase
            "0xd99ad936fc83c9babde7c33e1cf8c1a2076818c3", // starting with `0x`
            "01AB 4567 89AB CDEF 0123 4567 89AB CDEF 0123 4567", // with spaces
        ] {
            assert!(
                SequoiaFingerprint::new(fingerprint).is_ok(),
                "'{}' is NOT OK",
                fingerprint
            )
        }
    }

    #[test]
    fn test_serialize_keystatus() {
        let fingerprints = KeyStatusPayload {
            fingerprints: &[
                SequoiaFingerprint::new("D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3").unwrap(),
                SequoiaFingerprint::new("95D6F0110B446D6EABFEA10C6B52BA2D01E77CA5").unwrap(),
            ],
        };
        let serialized = serde_json::to_string(&fingerprints).unwrap();
        assert_eq!(
            &serialized,
            r#"{"fingerprints":["D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3","95D6F0110B446D6EABFEA10C6B52BA2D01E77CA5"]}"#
        );
    }

    #[test]
    fn test_deserialize_fingerprints() {
        let payload = r#"[{"fingerprint":"D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3","status":"APPROVED"},
        {"fingerprint":"95D6F0110B446D6EABFEA10C6B52BA2D01E77CA5","status":"APPROVAL_REVOKED"}]"#;
        let key_statuses = serde_json::from_str::<Vec<KeyStatus>>(payload).unwrap();
        let f1 = KeyStatus {
            fingerprint: String::from("D99AD936FC83C9BABDE7C33E1CF8C1A2076818C3"),
            status: ApprovalStatus::Approved,
        };
        let f2 = KeyStatus {
            fingerprint: String::from("95D6F0110B446D6EABFEA10C6B52BA2D01E77CA5"),
            status: ApprovalStatus::ApprovalRevoked,
        };
        assert!(&key_statuses
            .iter()
            .zip([f1, f2].iter())
            .all(|(a, b)| a == b));
    }
}
