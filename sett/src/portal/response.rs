//! Portal API response types.

use serde::Deserialize;

#[derive(Deserialize, Debug)]
/// Check package response from the Portal API.
pub struct CheckPackage {
    /// The project code of the package.
    ///
    /// It is derived from the `transfer_id` field of the package metadata.
    pub project_code: String,
}

#[derive(Deserialize, Debug)]
/// Error response from the Portal API when checking a package.
pub(super) struct PortalError {
    /// The error message.
    pub(super) detail: String,
}

#[cfg(feature = "auth")]
#[derive(Deserialize, Debug)]
pub(super) struct StsCredentials {
    pub(super) access_key_id: String,
    pub(super) secret_access_key: String,
    pub(super) session_token: String,
    pub(super) endpoint: String,
    pub(super) bucket: String,
}

#[cfg(feature = "auth")]
#[derive(Deserialize, Debug, PartialEq)]
#[serde(rename_all(deserialize = "SCREAMING_SNAKE_CASE"))]
/// Possible statuses for a data transfer request on Portal.
pub enum DataTransferStatus {
    /// The data transfer is undergoing the approval process.
    Initial,
    /// The data transfer has been approved by all parties and ready to be
    /// performed.
    Authorized,
    /// The data transfer has been rejected by at least one the parties and will
    /// not be performed.
    Unauthorized,
    /// The data transfer is expired and cannot be performed.
    Expired,
}

#[cfg(feature = "auth")]
#[derive(Deserialize, Debug)]
/// Portal's representation of data transfer request.
pub struct DataTransfer {
    /// Unique ID.
    pub id: u32,
    /// Name of the data provider sending the data.
    pub data_provider_name: String,
    /// Name of the project receiving the data.
    pub project_name: String,
    /// Current status of the data transfer request.
    pub status: DataTransferStatus,
}
