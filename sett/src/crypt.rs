//! Cryptography

use std::collections::HashMap;
use std::io::Write;

use anyhow::{ensure, Context, Result};
use sequoia_openpgp::{
    self as sequoia, armor,
    cert::{Cert, ValidCert},
    crypto::{KeyPair, Password, SessionKey, Signer},
    packet::{
        key::{self, PublicParts, UnspecifiedRole},
        Key, PKESK, SKESK,
    },
    parse::stream::{DecryptionHelper, MessageLayer, MessageStructure, VerificationHelper},
    policy::Policy,
    serialize::stream,
    types::{RevocationStatus, SymmetricAlgorithm},
    Fingerprint, KeyHandle, KeyID,
};
use tracing::debug;

use crate::certstore;

/// Returns signing-capable keys from a given certificate file.
///
/// Similarly to [`ValidCert::keys`] this function returns the primary key as
/// the first element. The order of subkeys in undefined.
fn get_signing_keys<T: Policy>(
    cert: &Cert,
    password: Option<&Password>,
    policy: &T,
) -> Result<Vec<KeyPair>> {
    let keys = cert
        .keys()
        .with_policy(policy, None)
        .alive()
        .revoked(false)
        .for_signing()
        .secret()
        .map(|ka| -> Result<_> {
            let mut key = ka.key().clone();
            if key.secret().is_encrypted() {
                let password = password.context("Password not provided for the encrypted key")?;
                let algo = key.pk_algo();
                key.secret_mut()
                    .decrypt_in_place(algo, password)
                    .context("Wrong password")?;
            }
            key.into_keypair()
        })
        .collect::<Result<_>>()?;
    Ok(keys)
}

/// Returns a signing-capable key from a given certificate file.
///
/// If the certificate contains multiple signing-capable keys, return the
/// first one, which is usually the primary key.
pub(super) fn get_signing_key<T: Policy>(
    cert: &Cert,
    password: Option<&Password>,
    policy: &T,
) -> Result<KeyPair> {
    get_signing_keys(cert, password, policy)?
        .into_iter()
        .next()
        .context("No signing key found")
}

/// Creates a detached OpenPGP signature.
pub(super) fn sign_detached<T: Signer + Send + Sync>(
    data: &[u8],
    cert: T,
    format: certstore::SerializationFormat,
) -> Result<Vec<u8>> {
    let mut sink = vec![];
    let message = if let certstore::SerializationFormat::Binary = format {
        stream::Message::new(&mut sink)
    } else {
        stream::Armorer::new(stream::Message::new(&mut sink))
            .kind(armor::Kind::Signature)
            .build()?
    };
    let mut message = stream::Signer::new(message, cert).detached().build()?;
    message.write_all(data)?;
    message.finalize()?;

    Ok(sink)
}

/// Returns encryption-capable keys.
pub(super) fn get_encryption_keys<T: Policy>(
    certs: &[Cert],
    policy: &T,
) -> Result<Vec<Key<PublicParts, UnspecifiedRole>>> {
    let mut recipient_subkeys = Vec::new();
    for cert in certs {
        // Make sure we add at least one subkey from every
        // certificate.
        let mut found_one = false;
        for ka in cert
            .keys()
            .with_policy(policy, None)
            .supported()
            .alive()
            .revoked(false)
            .for_transport_encryption()
        {
            let key = ka.key().clone();
            recipient_subkeys.push(key);
            found_one = true;
        }

        ensure!(found_one, "No suitable encryption subkey for {}", cert);
    }
    Ok(recipient_subkeys)
}

/// Checks that a certificate is valid, i.e. is neither expired nor revoked.
fn is_valid_cert(cert: &ValidCert) -> bool {
    matches!(
        (cert.alive(), cert.revocation_status()),
        (Ok(()), RevocationStatus::NotAsFarAsWeKnow)
    )
}

/// Private OpenPGP key.
#[derive(Clone)]
struct PrivateKey {
    key: Key<key::SecretParts, key::UnspecifiedRole>,
}

impl PrivateKey {
    /// Returns the secret key. If the key is encrypted, first try decrypting
    /// it with the provided password.
    fn get_secret(&self, password: Option<&Password>) -> Result<KeyPair> {
        match (self.key.secret().is_encrypted(), password) {
            (false, _) => self.key.clone().into_keypair(),
            (true, None) => Err(anyhow::anyhow!(
                "no password provided (failed to unlock the private key)"
            )),
            (true, Some(p)) => self
                .key
                .clone()
                .decrypt_secret(p)
                .context("wrong password (failed to unlock the private key)")?
                .into_keypair(),
        }
    }
}

/// Helper for decrypting data and validating signatures.
///
/// Helper structs can be used to either decrypt data, check signatures made
/// to data, or both.
/// If a Helper is only used for checking signatures, it does not need any
/// `secret_keys` nor `password`.
#[derive(Clone)]
pub(crate) struct Helper {
    secret_keys: HashMap<KeyID, PrivateKey>,
    certs: Vec<Cert>,
    password: Option<Password>,
}

impl Helper {
    /// Constructor method for struct `Helper`.
    ///
    /// To instantiate a `Helper` that will only be used for signature
    /// verification (and not for data decryption), `secrets` can be set to
    /// an empty array `&[]`, and `password` can be set to `None`.
    pub fn new<P: Policy>(
        secrets: &[&Cert],
        certs: &[&Cert],
        policy: &P,
        password: Option<&Password>,
    ) -> Result<Self> {
        let secret_keys = secrets
            .iter()
            .flat_map(|cert| {
                cert.keys()
                    .with_policy(policy, None)
                    .for_transport_encryption()
                    .for_storage_encryption()
            })
            .map(|ka| {
                Ok((
                    KeyID::from(ka.key().fingerprint()),
                    PrivateKey {
                        key: ka.key().parts_as_secret()?.clone(),
                    },
                ))
            })
            .collect::<Result<_>>()?;

        let certs_vec: Vec<Cert> = certs
            .iter()
            .filter(|cert| {
                cert.with_policy(policy, None)
                    .as_ref()
                    .map_or(false, is_valid_cert)
            })
            .map(|c| (*c).to_owned())
            .collect();

        Ok(Self {
            secret_keys,
            certs: certs_vec,
            password: password.cloned(),
        })
    }

    /// Tries decrypting data for the given private keys.
    fn try_decrypt<D>(
        &self,
        pkesk: &PKESK,
        sym_algo: Option<SymmetricAlgorithm>,
        keypair: &mut KeyPair,
        decrypt: &mut D,
    ) -> Option<Fingerprint>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        pkesk
            .decrypt(keypair, sym_algo)
            .and_then(|(algo, sk)| decrypt(algo, &sk).then(|| keypair.public().fingerprint()))
    }
}

impl VerificationHelper for Helper {
    fn get_certs(&mut self, ids: &[KeyHandle]) -> sequoia::Result<Vec<Cert>> {
        Ok(self
            .certs
            .iter()
            .filter(|c| c.keys().key_handles(ids.iter()).next().is_some())
            .cloned()
            .collect())
    }

    fn check(&mut self, structure: MessageStructure) -> sequoia::Result<()> {
        for layer in structure.into_iter() {
            match layer {
                MessageLayer::Compression { algo } => debug!("Compressed using {}", algo),
                MessageLayer::Encryption {
                    sym_algo,
                    aead_algo,
                } => match aead_algo {
                    Some(aead_algo) => {
                        debug!("Encrypted and protected using {}/{}", sym_algo, aead_algo)
                    }
                    None => debug!("Encrypted using {}", sym_algo),
                },
                MessageLayer::SignatureGroup { ref results } => {
                    // TODO: do we want to check for anything else?
                    ensure!(results.iter().any(|r| r.is_ok()), "No valid signature");
                }
            }
        }
        Ok(())
    }
}

impl DecryptionHelper for Helper {
    fn decrypt<D>(
        &mut self,
        pkesks: &[PKESK],
        _skesks: &[SKESK],
        sym_algo: Option<SymmetricAlgorithm>,
        mut decrypt: D,
    ) -> sequoia::Result<Option<Fingerprint>>
    where
        D: FnMut(SymmetricAlgorithm, &SessionKey) -> bool,
    {
        // Loop through all keys for which the data was encrypted.
        let mut error_message = String::new();
        for pkesk in pkesks {
            let keyid = pkesk.recipient();

            // If the key is present in the user's keyring, check whether the
            // provided password can unlock it.
            if let Some(key) = self.secret_keys.get(keyid) {
                match key.get_secret(self.password.as_ref()) {
                    Ok(mut keypair) => {
                        if let Some(fp) =
                            self.try_decrypt(pkesk, sym_algo, &mut keypair, &mut decrypt)
                        {
                            return Ok(Some(fp));
                        }
                    }
                    Err(e) => error_message = e.to_string(),
                }
            }
        }

        // If this point is reached, no key to decrypt the data could be found
        // or successfully unlocked with the provided password.
        let prefix = "Data decryption failed:";
        if error_message.is_empty() {
            Err(anyhow::anyhow!(
                "{prefix} could not find a private key able to decrypt the \
                data. Make sure that the data was encrypted for you, and that \
                your private key is present in your local keystore. \
                In addition (if using command line mode) make sure that the \
                correct recipient key was passed as argument."
            ))
        } else {
            Err(anyhow::anyhow!("{} {}.", prefix, error_message))
        }
    }
}
