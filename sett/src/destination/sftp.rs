//! SFTP data transfers
//!
//! This module provides support for establishing SSH connections and
//! transferring data with SFTP.

use std::{
    borrow::Cow,
    fs::File,
    io,
    net::TcpStream,
    path::{Path, PathBuf},
};

use anyhow::{bail, ensure, Context, Result};
use chrono::Utc;
use ssh2::{RenameFlags, Session, Sftp};
use tracing::debug;

use crate::{
    task::Status,
    utils::{Progress, ProgressReader},
};

const UPLOAD_TMP_SUFFIX: &str = ".part";

fn make_session(host: &str, port: u16) -> Result<Session> {
    let tcp = TcpStream::connect(format!("{}:{}", host, port))?;
    let mut session = Session::new()?;
    session.set_tcp_stream(tcp);
    session.handshake()?;
    Ok(session)
}

/// Establish an SSH connection.
///
/// Connect using the SSH key or SSH Agent (including support for 2 factor authentication).
pub fn connect(sftp_opts: &SftpOpts) -> Result<Sftp> {
    let mut session = make_session(sftp_opts.host.as_ref(), sftp_opts.port)?;
    // TODO should we check known hosts?
    if let Some(key) = sftp_opts.key_path.as_deref() {
        session.userauth_pubkey_file(
            sftp_opts.username.as_ref(),
            None,
            Path::new(key),
            sftp_opts.key_password.as_deref(),
        )?;
    } else {
        debug!("No SSH key used. Using SSH Agent");
        connect_with_agent(sftp_opts.username.as_ref(), &mut session)?;
    }
    if !session.authenticated() {
        let methods = session
            .auth_methods(sftp_opts.username.as_ref())
            .unwrap_or("none");
        ensure!(
            methods == "keyboard-interactive",
            "The following method(s) are not supported (client side) during multi factor authentication: {}",
            methods);
        debug!(
            "Partially connected. Trying second factor. Allowed methods: {}",
            methods
        );
        if let Some(cb) = &sftp_opts.two_factor_callback {
            let mut prompt = Prompt { cb };
            session.userauth_keyboard_interactive(sftp_opts.username.as_ref(), &mut prompt)?;
        } else {
            bail!("A second factor was requested but no two_factor_callback available");
        }
    }

    Ok(session.sftp()?)
}

/// Establishes an SSH connection using SSH Agent.
fn connect_with_agent(username: &str, session: &mut Session) -> Result<()> {
    let mut agent = session.agent()?;
    agent.connect()?;
    agent.list_identities()?;
    let identities = agent.identities()?;
    let key = &identities.iter().find(|i| {
        agent
            .userauth(username, i)
            .or_else(|e| {
                // For some reason, ssh2 returns code -19: "Invalid signature
                // for supplied public key, or bad username/public key combination",
                // where the server receives a "Partial publickey"
                if e.code() == ssh2::ErrorCode::Session(-19) {
                    Ok(())
                } else {
                    Err(e)
                }
            })
            .map_err(|e| {
                debug!("{:?}", e);
                e
            })
            .is_ok()
    });
    agent.disconnect()?;
    ensure!(key.is_some(), "Agent authentication failed");
    Ok(())
}

/// Holds parameters necessary for establishing an SSH connection.
#[derive(Default)]
pub struct SftpOpts<'a> {
    /// Domain name (or IP address) of the SFTP server.
    pub host: Cow<'a, str>,
    /// SFTP server port number.
    pub port: u16,
    /// User name for authentication with the SFTP server.
    pub username: Cow<'a, str>,
    /// Path on the remote SFTP host where encrypted files will be stored.
    pub base_path: Cow<'a, Path>,
    /// Name of the envelope directory in which to store the file.
    pub envelope_dir: Option<Cow<'a, str>>,
    /// Private SSH key path.
    pub key_path: Option<Cow<'a, str>>,
    /// Private SSH key password.
    pub key_password: Option<Cow<'a, str>>,
    /// Two factor authentication callback function.
    pub two_factor_callback: Option<Box<dyn Fn() -> Result<String> + Send + Sync + 'a>>,
}

impl std::fmt::Debug for SftpOpts<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("SftpOpts")
            .field("host", &self.host)
            .field("port", &self.port)
            .field("username", &self.username)
            .field("base_path", &self.base_path)
            .field("envelope_dir", &self.envelope_dir)
            .field("key_path", &self.key_path)
            .field(
                "key_password",
                &self.key_password.as_ref().map(|_| "******"),
            )
            .finish()
    }
}

impl SftpOpts<'_> {
    pub(crate) fn get_url(&self, path: &Path) -> String {
        format!(
            "sftp://{}:{}/{}",
            self.host,
            self.port,
            path.to_string_lossy()
        )
    }
}

/// A directory for uploading data packages on an SFTP server.
pub(crate) struct UploadDir<'a> {
    pub(crate) path: PathBuf,
    sftp: &'a Sftp,
}

impl<'a> UploadDir<'a> {
    pub(crate) fn new(
        base_path: impl AsRef<Path>,
        envelope_dir: Option<&str>,
        sftp: &'a Sftp,
    ) -> Self {
        const DATETIME_FORMAT: &str = "%Y%m%dT%H%M%S_%f";
        let default_envelope_dir = Utc::now().format(DATETIME_FORMAT).to_string();
        Self {
            path: base_path
                .as_ref()
                .join(envelope_dir.unwrap_or(&default_envelope_dir)),
            sftp,
        }
    }

    /// Creates an empty directory on an SFTP server.
    pub(crate) fn create(&self, mode: Option<i32>) -> Result<()> {
        // TODO mkdir will fail if parent is missing, should it be recursive?
        self.sftp.mkdir(&self.path, mode.unwrap_or(0o755))?;
        Ok(())
    }

    /// Creates a marker file indicating that no new packages will be uploaded
    /// to this directory.
    pub(crate) fn finalize(self) -> Result<()> {
        const UPLOAD_FINISHED_MARKER_NAME: &str = "done.txt";
        self.sftp
            .create(&self.path.join(UPLOAD_FINISHED_MARKER_NAME))?;
        Ok(())
    }

    /// Deletes an upload directory and all its content in case of a failure
    /// during upload. The directory is expected to contain only `.part` files,
    /// the partially uploaded files.
    pub(crate) fn delete(&self) -> Result<()> {
        // Delete all files with a `.part` extension inside the directory.
        // This should in principle delete all files in the directory.
        for (file, _) in self.sftp.readdir(&self.path)? {
            if file.extension().is_some_and(|e| e != "part") {
                bail!(
                    "Cannot delete '{}'. Only '*.part' files can be deleted",
                    file.to_string_lossy(),
                )
            }
            self.sftp.unlink(&file)?
        }

        // Delete the directory itself, which should be empty at this point.
        self.sftp.rmdir(&self.path)?;
        Ok(())
    }
}

/// A path to a data package on an SFTP server.
pub(crate) struct DpkgPath<'a> {
    pub(crate) tmp: PathBuf,
    pub(crate) path: PathBuf,
    sftp: &'a Sftp,
}

impl<'a> DpkgPath<'a> {
    pub(crate) fn new<P: AsRef<Path>, S: AsRef<str>>(base: P, name: S, sftp: &'a Sftp) -> Self {
        let p: PathBuf = base.as_ref().into();
        Self {
            tmp: p.join(format!("{}.{}", name.as_ref(), UPLOAD_TMP_SUFFIX)),
            path: p.join(name.as_ref()),
            sftp,
        }
    }

    /// Renames the package name to its final version.
    pub(crate) fn finalize(&self) -> Result<()> {
        self.sftp.rename(
            &self.tmp,
            &self.path,
            Some(RenameFlags::ATOMIC | RenameFlags::NATIVE),
        )?;
        Ok(())
    }
}

/// Uploads files with SFTP.
///
/// This function establishes a connection to a remove SSH server and transfers
/// the provided files sequentially.
pub fn upload(
    path: &impl AsRef<Path>,
    sftp_opts: &SftpOpts,
    progress: Option<&impl Progress>,
    buf_size: Option<usize>, // Buffer size for uploading files
    mode: crate::task::Mode,
) -> Result<Status> {
    let package_size = path.as_ref().metadata()?.len();
    let sftp = connect(sftp_opts)?;
    let upload_dir = UploadDir::new(
        &sftp_opts.base_path,
        sftp_opts.envelope_dir.as_deref(),
        &sftp,
    );
    let dpkg_path = DpkgPath::new(
        &upload_dir.path,
        path.as_ref()
            .file_name()
            // Since the file has already been opened this error should never occur
            .context("This is not a file")?
            .to_string_lossy(),
        &sftp,
    );
    let destination = sftp_opts.get_url(&dpkg_path.path);
    if let crate::task::Mode::Check = mode {
        let status = Status::Checked {
            destination,
            source_size: package_size,
        };
        status.log();
        return Ok(status);
    }
    upload_dir.create(None)?;
    let buf_size = buf_size.unwrap_or(1 << 22);
    let mut reader = io::BufReader::with_capacity(buf_size, File::open(path)?);
    let mut fout = io::BufWriter::with_capacity(buf_size, sftp.create(&dpkg_path.tmp)?);
    if let Some(p) = progress {
        p.set_length(package_size);
        let mut reader = ProgressReader::new(reader, |len| {
            p.inc(len.try_into()?);
            Ok(())
        });
        io::copy(&mut reader, &mut fout)?;
        p.finish();
    } else {
        io::copy(&mut reader, &mut fout)?;
    }
    dpkg_path.finalize()?;
    upload_dir.finalize()?;
    let status = Status::Completed {
        destination,
        source_size: package_size,
        destination_size: package_size,
    };
    status.log();
    Ok(status)
}

/// 2FA prompt.
struct Prompt<Cb: Fn() -> Result<String>> {
    cb: Cb,
}
impl<Cb: Fn() -> Result<String>> ssh2::KeyboardInteractivePrompt for Prompt<Cb> {
    fn prompt(
        &mut self,
        username: &str,
        instructions: &str,
        prompts: &[ssh2::Prompt],
    ) -> Vec<String> {
        debug!(
            "prompt: username='{}', instructions='{}', prompts={:?}",
            username, instructions, prompts
        );
        prompts
            .iter()
            .map(|p| {
                debug!("prompting for '{}'", p.text);
                let response = (self.cb)().ok().unwrap_or_default();
                debug!("Returning '{}'", response);
                response
            })
            .collect()
    }
}
