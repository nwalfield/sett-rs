//! Destination-specific logic for destination type S3.

use std::{borrow::Cow, path::Path, sync::Arc};

use anyhow::{Context, Result};
use aws_config::meta::{credentials::CredentialsProviderChain, region::RegionProviderChain};
use aws_sdk_s3::{
    config::Credentials,
    primitives::ByteStream,
    types::{CompletedMultipartUpload, CompletedPart},
    Client,
};
use aws_types::region::Region;
use bytes::BytesMut;
use tokio::{
    io::AsyncReadExt,
    sync::{mpsc, Semaphore},
};
use tracing::{debug, info};

use crate::{
    task::{Mode, Status},
    utils::{to_human_readable_size, Progress},
};

// 4 * 8MB (4 * 8_388_608) = 32 MB (33_554_432)
const MINIMUM_CHUNK_SIZE: usize = 4 << 23;
const MAX_CHUNKS: u32 = 10000;

/// Options required for transferring data packages to an S3 host
#[derive(Clone)]
pub struct S3Opts<'a> {
    /// Endpoint URL
    pub endpoint: Option<Cow<'a, str>>,
    /// Bucket name
    pub bucket: Cow<'a, str>,
    /// Region
    pub region: Option<Cow<'a, str>>,
    /// Profile
    pub profile: Option<Cow<'a, str>>,
    /// Access key ID
    pub access_key: Option<Cow<'a, str>>,
    /// Secret access key
    pub secret_key: Option<Cow<'a, str>>,
    /// Session token applications use for authentication
    pub session_token: Option<Cow<'a, str>>,
}

impl std::fmt::Debug for S3Opts<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("S3Opts")
            .field("endpoint", &self.endpoint)
            .field("bucket", &self.bucket)
            .field("region", &self.region)
            .field("profile", &self.profile)
            .field("access_key", &self.access_key.as_ref().map(|_| "***"))
            .field("secret_key", &self.secret_key.as_ref().map(|_| "***"))
            .field("session_token", &self.session_token.as_ref().map(|_| "***"))
            .finish()
    }
}

/// Upload a package to an S3 object store.
pub async fn upload(
    opts: &S3Opts<'_>,
    path: &impl AsRef<Path>,
    mode: Mode,
    progress: Option<impl Progress + Send + 'static>,
) -> Result<Status> {
    let (client, endpoint_url) = build_client(opts).await?;
    let bucket = opts.bucket.as_ref();
    let input_size = tokio::fs::metadata(path).await?.len();
    let key = path
        .as_ref()
        .file_name()
        .context("Unable to extract file name from path")?
        .to_str()
        .context("File name contains non-UTF8 characters")?;
    let location = [&endpoint_url, bucket, key].join("/");
    let status = if let crate::task::Mode::Check = mode {
        Status::Checked {
            destination: location,
            source_size: input_size,
        }
    } else {
        let path = path.as_ref().to_path_buf();
        let (tx, rx) = mpsc::channel(2);
        let handle =
            tokio::task::spawn(async move { read_chunks_from_file(&path, tx, progress).await });
        put_object(client, bucket, key, rx).await?;
        handle.await??;
        Status::Completed {
            destination: location,
            source_size: input_size,
            destination_size: input_size,
        }
    };
    status.log();
    Ok(status)
}

async fn read_chunks_from_file(
    path: &Path,
    sink: mpsc::Sender<BytesMut>,
    progress: Option<impl Progress + Send>,
) -> anyhow::Result<()> {
    let mut file = tokio::fs::File::open(path).await?;
    let input_size = file.metadata().await?.len();
    let chunk_size = compute_chunk_size(input_size);
    let mut buffer = BytesMut::with_capacity(chunk_size);
    if let Some(p) = &progress {
        p.set_length(input_size);
    }
    loop {
        let n = file.read_buf(&mut buffer).await?;
        if n == 0 || buffer.len() >= chunk_size {
            sink.send(std::mem::replace(
                &mut buffer,
                BytesMut::with_capacity(chunk_size),
            ))
            .await?;
        }
        if n == 0 {
            break;
        }
        if let Some(p) = &progress {
            p.inc(n as u64);
        }
    }
    if let Some(p) = progress {
        p.finish();
    }
    Ok(())
}

mod proxy {
    use aws_sdk_s3::config::SharedHttpClient;
    use aws_smithy_runtime::client::http::hyper_014::HyperClientBuilder;
    use hyper_proxy::{Intercept, Proxy, ProxyConnector};
    use hyper_rustls::ConfigBuilderExt;

    pub(super) fn get_url_from_env() -> Option<String> {
        let mut proxy_url = None;
        for name in [
            "HTTP_PROXY",
            "HTTPS_PROXY",
            "ALL_PROXY",
            "http_proxy",
            "https_proxy",
            "all_proxy",
        ] {
            if let Ok(val) = std::env::var(name) {
                proxy_url = Some(val);
                break;
            }
        }
        proxy_url
    }

    pub(super) fn build_http_client(url: &str) -> anyhow::Result<SharedHttpClient> {
        // Connector code is taken from the AWS SDK.
        // https://github.com/awslabs/aws-sdk-rust/blob/45ba2a808bb01f4875229acae73ca994bd75177e
        // /sdk/aws-smithy-runtime/src/client/http/hyper_014.rs#L53
        let connector = hyper_rustls::HttpsConnectorBuilder::new()
            .with_tls_config(
                rustls::ClientConfig::builder()
                    .with_cipher_suites(&[
                        // TLS1.3 suites
                        rustls::cipher_suite::TLS13_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS13_AES_128_GCM_SHA256,
                        // TLS1.2 suites
                        rustls::cipher_suite::TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        rustls::cipher_suite::TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256,
                    ])
                    .with_safe_default_kx_groups()
                    .with_safe_default_protocol_versions()
                    .expect("Error with the TLS configuration")
                    .with_native_roots()
                    .with_no_client_auth(),
            )
            .https_or_http()
            .enable_http1()
            .enable_http2()
            .build();
        let proxy_url = url.parse()?;
        let proxy = ProxyConnector::from_proxy(connector, Proxy::new(Intercept::All, proxy_url))?;
        Ok(HyperClientBuilder::new().build(proxy))
    }
}

pub(crate) async fn build_client(opts: &S3Opts<'_>) -> Result<(Client, String)> {
    let region = opts.region.as_deref().map(|s| Region::new(s.to_string()));
    let region_provider = RegionProviderChain::first_try(region)
        .or_default_provider()
        .or_else("us-east-1");
    let credentials_provider = if let (Some(access_key), Some(secret_key)) =
        (opts.access_key.as_deref(), opts.secret_key.as_deref())
    {
        CredentialsProviderChain::first_try(
            "opts",
            Credentials::from_keys(
                access_key,
                secret_key,
                opts.session_token.as_deref().map(Into::into),
            ),
        )
    } else {
        CredentialsProviderChain::default_provider().await
    };
    let mut config_loader = aws_config::from_env();
    config_loader = if let Some(profile) = opts.profile.as_deref() {
        config_loader.profile_name(profile)
    } else {
        config_loader
            .region(region_provider)
            .credentials_provider(credentials_provider)
    };
    if let Some(endpoint) = opts.endpoint.as_deref() {
        config_loader = config_loader.endpoint_url(endpoint);
    }
    let sdk_config = config_loader.load().await;
    let s3_config_builder = aws_sdk_s3::config::Builder::from(&sdk_config).force_path_style(true);
    let s3_config = if let Some(proxy_url) = proxy::get_url_from_env() {
        debug!(proxy_url, "Proxy URL found");
        s3_config_builder.http_client(proxy::build_http_client(&proxy_url)?)
    } else {
        s3_config_builder
    }
    .build();
    Ok((
        Client::from_conf(s3_config),
        sdk_config.endpoint_url().unwrap_or_default().into(),
    ))
}

pub(crate) async fn put_object(
    client: Client,
    bucket: &str,
    key: &str,
    mut channel: mpsc::Receiver<BytesMut>,
) -> Result<()> {
    debug!("Transferring '{}' to bucket '{}'", key, bucket);
    macro_rules! info_log {
        ($size:expr) => {
            info!(
                "Successfully transferred '{}' to bucket '{}' (size: {})",
                key,
                bucket,
                to_human_readable_size($size as u64)
            );
        };
    }

    let mut counter: u64 = 0;
    let mut part_number: i32 = 0;
    let mut upload_id = String::new();
    // Limit the number of concurrent uploads to 4.
    let semaphore = Arc::new(Semaphore::new(4));
    let mut join_handles = Vec::new();

    while let Some(chunk) = channel.recv().await {
        if part_number == 0 {
            // It's a single part upload.
            // We assume that the first chunk is completely filled for multipart
            // uploads.
            let chunk_len = chunk.len();
            if chunk_len < chunk.capacity() {
                client
                    .put_object()
                    .bucket(bucket)
                    .key(key)
                    .body(ByteStream::from(chunk.freeze()))
                    .send()
                    .await?;
                info_log!(chunk_len);
                return Ok(());
            // It's a multipart upload.
            } else {
                client
                    .create_multipart_upload()
                    .bucket(bucket)
                    .key(key)
                    .send()
                    .await?
                    .upload_id()
                    .context("Multipart upload ID could not be fetched")?
                    .clone_into(&mut upload_id);
            }
        }
        // Part numbering starts at 1
        part_number += 1;
        counter += chunk.len() as u64;

        let permit = semaphore
            .clone()
            .acquire_owned()
            .await
            .context("semaphore error");
        let upload_id = upload_id.clone();
        let key = key.to_string();
        let bucket = bucket.to_string();
        let client = client.clone();
        let join_handle = tokio::task::spawn(async move {
            let upload_part_res = client
                .upload_part()
                .key(key)
                .bucket(bucket)
                .upload_id(upload_id)
                .body(ByteStream::from(chunk.freeze()))
                .part_number(part_number)
                .send()
                .await?;
            let completed_part = CompletedPart::builder()
                .e_tag(
                    upload_part_res
                        .e_tag
                        .context("Could not retrieve the entity tag for the uploaded object")?,
                )
                .part_number(part_number)
                .build();
            drop(permit);
            Ok(completed_part)
        }) as tokio::task::JoinHandle<anyhow::Result<_>>;
        join_handles.push(join_handle);
    }

    let mut upload_parts = Vec::with_capacity(join_handles.len());
    for join_handle in join_handles {
        upload_parts.push(join_handle.await??);
    }
    anyhow::ensure!(!upload_parts.is_empty(), "No parts have been uploaded");

    let completed_multipart_upload = CompletedMultipartUpload::builder()
        .set_parts(Some(upload_parts))
        .build();
    client
        .complete_multipart_upload()
        .bucket(bucket)
        .key(key)
        .multipart_upload(completed_multipart_upload)
        .upload_id(upload_id)
        .send()
        .await?;

    info_log!(counter);
    Ok(())
}

pub(crate) fn compute_chunk_size(total_size: u64) -> usize {
    std::cmp::max(
        (total_size as f64 / MAX_CHUNKS as f64).ceil() as usize,
        MINIMUM_CHUNK_SIZE,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_compute_chunk_size() {
        let max_size_before_chunk_size = MAX_CHUNKS as u64 * MINIMUM_CHUNK_SIZE as u64;
        assert_eq!(compute_chunk_size(0), MINIMUM_CHUNK_SIZE);
        assert_eq!(compute_chunk_size(1), MINIMUM_CHUNK_SIZE);
        assert_eq!(
            compute_chunk_size(max_size_before_chunk_size),
            MINIMUM_CHUNK_SIZE
        );
        assert_eq!(
            compute_chunk_size(max_size_before_chunk_size + 1),
            MINIMUM_CHUNK_SIZE + 1
        );
    }
}
