//! Destination-specific logic for destination type local.

use std::path::PathBuf;

use chrono::{DateTime, Utc};

use crate::dpkg;

#[derive(Debug, Clone)]
/// Options required for destination type `local`.
pub struct LocalOpts {
    /// Path to the directory where the data package will be stored.
    pub path: PathBuf,
    /// Name of the encrypted data package.
    pub name: Option<String>,
}

impl LocalOpts {
    pub(crate) fn package_name(&self, timestamp: &DateTime<Utc>, prefix: Option<&str>) -> String {
        self.name.as_ref().map_or_else(
            || dpkg::generate_package_name(timestamp, prefix),
            Into::into,
        )
    }
}
