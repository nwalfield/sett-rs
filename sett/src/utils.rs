//! Miscellaneous utilities

use anyhow::Result;
use std::io::{self, Read};

/// Formats sizes in a human-readable format.
pub(super) fn to_human_readable_size(size: u64) -> String {
    let prefix = ["", "k", "M", "G", "T", "P", "E"];
    let exp = 1024.0;
    let log_value = if size > 0 {
        (size as f32).log(exp) as usize
    } else {
        0
    };
    format!(
        "{:.prec$} {}B",
        size as f32 / exp.powi(log_value as i32),
        prefix[log_value],
        prec = if log_value > 0 { 2 } else { 0 }
    )
}

/// Keep track of a current progress.
pub trait Progress {
    /// Sets the length of the progress bar.
    fn set_length(&self, len: u64);
    /// Increments the current progress completion.
    fn inc(&self, delta: u64);
    /// Finalizes the progress bar.
    fn finish(&self);
}

/// Runs a callback function while reading through a reader to
/// report the current progress.
pub(super) struct ProgressReader<R: Read, C: FnMut(usize) -> Result<()>> {
    reader: R,
    cb: C,
}

impl<R: Read, C: FnMut(usize) -> Result<()>> ProgressReader<R, C> {
    pub(super) fn new(reader: R, cb: C) -> Self {
        Self { reader, cb }
    }
}

impl<R: Read, C: FnMut(usize) -> Result<()>> Read for ProgressReader<R, C> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let read = self.reader.read(buf)?;
        (self.cb)(read).map_err(|e| io::Error::new(io::ErrorKind::Other, format!("{e:?}")))?;
        Ok(read)
    }
}

/// Returns the number of available cores divided by two, but at least 2.
pub(super) fn get_max_cpu() -> usize {
    std::cmp::max(
        std::thread::available_parallelism()
            // unwrap is ok, NonZeroUsize returns None only when 0 is passed
            .unwrap_or(std::num::NonZeroUsize::new(4).unwrap())
            .get()
            / 2,
        2,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_human_readable_size() {
        let sizes = [
            (0u64, "0 B"),
            (1023u64, "1023 B"),
            (9253u64, "9.04 kB"),
            (3771286u64, "3.60 MB"),
            (8363220129, "7.79 GB"),
            (7856731783569, "7.15 TB"),
            (4799178968842384, "4.26 PB"),
            (3424799178968842384, "2.97 EB"),
        ];
        for (size, expected) in sizes {
            assert_eq!(to_human_readable_size(size), expected);
        }
    }
}
