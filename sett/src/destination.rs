//! Destination-specific logic for all supported destination types.

pub mod local;
pub mod s3;
pub mod sftp;

/// Destination type for data packages
#[derive(Debug)]
pub enum Destination<'a> {
    /// A location in the local filesystem.
    Local(local::LocalOpts),
    /// Standard output
    Stdout,
    /// A location on a remote SFTP server.
    Sftp(sftp::SftpOpts<'a>),
    /// A location on a remote S3 compatible object store.
    S3(s3::S3Opts<'a>),
}

impl<'a> From<local::LocalOpts> for Destination<'a> {
    fn from(val: local::LocalOpts) -> Self {
        Destination::Local(val)
    }
}

impl<'a> From<sftp::SftpOpts<'a>> for Destination<'a> {
    fn from(val: sftp::SftpOpts<'a>) -> Self {
        Destination::Sftp(val)
    }
}

impl<'a> From<s3::S3Opts<'a>> for Destination<'a> {
    fn from(val: s3::S3Opts<'a>) -> Self {
        Destination::S3(val)
    }
}
