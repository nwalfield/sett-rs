use std::{
    borrow::Cow,
    fs::File,
    io::{self, BufReader, Read, Seek, Write},
    path::PathBuf,
};

use anyhow::{Context, Result};
use chrono::{DateTime, Utc};
use crc32fast::Hasher;
use tokio::io::{AsyncReadExt, AsyncSeekExt};

mod spec;

#[derive(Debug, Default, Clone)]
struct ZipFile {
    name: String,
    offset: u64,
    size: u64,
    hasher: Hasher,
    crc32: u32,
    flags: u16,
    external_attributes: u32,
    timestamp: DateTime<Utc>,
}

impl ZipFile {
    fn new(name: String, offset: u64, opts: &ZipFileOpts) -> Self {
        Self {
            name,
            offset,
            timestamp: opts.timestamp,
            flags: 1u16 << 3 | 1u16 << 11, // Streaming + UTF-8
            external_attributes: opts.permissions << 16,
            ..Default::default()
        }
    }
    fn to_central_directory_header(&self) -> spec::CentralDirectoryHeader {
        spec::CentralDirectoryHeader {
            crc32: self.crc32,
            external_attributes: self.external_attributes,
            flags: self.flags,
            modified: self.timestamp,
            name: Cow::Borrowed(&self.name),
            offset: self.offset,
            size: self.size,
            ..Default::default()
        }
    }
    fn to_local_file_header(&self) -> spec::LocalFileHeader {
        spec::LocalFileHeader {
            flags: self.flags,
            modified: self.timestamp,
            name: Cow::Borrowed(&self.name),
            ..Default::default()
        }
    }
    fn to_data_descriptor(&self) -> spec::DataDescriptor {
        spec::DataDescriptor {
            crc32: self.crc32,
            size: self.size,
        }
    }
    fn finish(mut self) -> Self {
        self.crc32 = self.hasher.clone().finalize();
        self
    }
}

pub(super) struct ZipFileOpts {
    timestamp: DateTime<Utc>,
    permissions: u32,
}

impl Default for ZipFileOpts {
    fn default() -> Self {
        Self {
            timestamp: Utc::now(),
            // Default permissions are rw-r--r--
            permissions: 0o100644,
        }
    }
}

enum ZipArchiveState {
    Start,
    /// Only write file data
    FileOpen(ZipFile),
    /// Only write metadata, not files
    FileClosed,
}

impl ZipArchiveState {
    fn unwrap(self) -> ZipFile {
        if let ZipArchiveState::FileOpen(f) = self {
            f
        } else {
            panic!("Only `FileOpen` can be unwrapped");
        }
    }
}

pub(super) struct ZipArchive<W: Write> {
    inner: W,
    files: Vec<ZipFile>,
    state: ZipArchiveState,
    size: u64,
}

impl<W: io::Write> ZipArchive<W> {
    pub(super) fn new(writer: W) -> Self {
        Self {
            inner: writer,
            files: Vec::new(),
            state: ZipArchiveState::Start,
            size: 0,
        }
    }

    fn finish_file(&mut self) -> Result<()> {
        if let ZipArchiveState::FileOpen(_) = &self.state {
            let closed = std::mem::replace(&mut self.state, ZipArchiveState::FileClosed)
                .unwrap()
                .finish();
            self.write_all(&closed.to_data_descriptor().build())?;
            self.files.push(closed);
        }
        Ok(())
    }

    pub(super) fn add(&mut self, file: &str, opts: &ZipFileOpts) -> Result<()> {
        self.finish_file()?;
        let f = ZipFile::new(file.into(), self.size, opts);
        self.write_all(&f.to_local_file_header().build())?;
        self.state = ZipArchiveState::FileOpen(f);
        Ok(())
    }

    pub(super) fn finish(&mut self) -> Result<u64> {
        self.finish_file()?;
        let central_directory_offset = self.size;
        let headers = self
            .files
            .iter()
            .map(|f| f.to_central_directory_header().build())
            .collect::<Vec<_>>();
        for header in &headers {
            self.write_all(header)?;
        }

        let central_directory_size = self.size - central_directory_offset;
        let zip64_central_directory_offset = self.size;

        if self.size > spec::ZIP64_THRESHOLD_BYTES
            || self.files.len() as u64 > spec::ZIP64_THRESHOLD_FILES
        {
            let zip64_end = spec::Zip64CentralDirectoryEnd {
                disk_number_of_records: self.files.len() as u64,
                total_number_of_records: self.files.len() as u64,
                central_directory_size,
                central_directory_offset,
                ..Default::default()
            };
            self.write_all(&zip64_end.build())?;
            let zip64_end_locator = spec::Zip64CentralDirectoryEndLocator {
                disk_with_zip64_central_directory_end: 0,
                zip64_central_directory_end_offset: zip64_central_directory_offset,
                total_number_of_disks: 1,
            };
            self.write_all(&zip64_end_locator.build())?;
        }

        let end = spec::CentralDirectoryEnd {
            disk_number: 0,
            disk_with_central_directory: 0,
            disk_number_of_records: self.files.len() as u16,
            total_number_of_records: self.files.len() as u16,
            central_directory_size: central_directory_size.min(spec::ZIP64_THRESHOLD_BYTES) as u32,
            central_directory_offset: central_directory_offset.min(spec::ZIP64_THRESHOLD_BYTES)
                as u32,
            comment: Vec::with_capacity(0),
        };
        self.write_all(&end.build())?;
        self.flush()?;
        Ok(self.size)
    }
}

impl<W: Write> Write for ZipArchive<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let written = self.inner.write(buf)?;
        if let ZipArchiveState::FileOpen(f) = &mut self.state {
            f.hasher.update(&buf[..written]);
            f.size += written as u64;
        }
        self.size += written as u64;
        Ok(written)
    }
    fn flush(&mut self) -> io::Result<()> {
        self.inner.flush()
    }
}

#[derive(Clone, Debug)]
pub(super) struct ZipReader {
    path: PathBuf,
    files: Vec<ZipFile>,
    /// A position where the ZIP archive starts.
    ///
    /// It can be different from 0 if the ZIP archive is appended to another file.
    zip_offset: u64,
}

impl ZipReader {
    pub(super) fn open(path: PathBuf) -> Result<Self> {
        let mut reader = BufReader::new(File::open(&path)?);
        spec::CentralDirectoryEnd::find(&mut reader).context("invalid zip archive")?;
        let cde = spec::CentralDirectoryEnd::parse(&mut reader).context("invalid zip archive")?;
        anyhow::ensure!(
            cde.disk_number == cde.disk_with_central_directory,
            "multi-disk zip archives are not supported"
        );
        // If present the Zip64 central directory end locator is at 20 bytes before the
        // cental directory end.
        reader.seek(io::SeekFrom::End(
            -((cde.size() + spec::Zip64CentralDirectoryEndLocator::SIZE) as i64),
        ))?;
        let (number_of_records, central_directory_offset, zip_offset) = if let Some(zip64_end) =
            spec::Zip64CentralDirectoryEndLocator::parse(&mut reader)
                .context("invalid zip archive")?
        {
            reader.seek(io::SeekFrom::Start(
                zip64_end.zip64_central_directory_end_offset,
            ))?;
            let zip64cde_actual_position = spec::Zip64CentralDirectoryEnd::find(&mut reader)?;
            let zip64cde = spec::Zip64CentralDirectoryEnd::parse(&mut reader)?;
            anyhow::ensure!(
                zip64cde.disk_number_of_records == zip64cde.total_number_of_records,
                "multi-disk zip archives are not supported"
            );
            (
                zip64cde.total_number_of_records,
                zip64cde.central_directory_offset,
                zip64cde_actual_position - zip64_end.zip64_central_directory_end_offset,
            )
        } else {
            reader.seek(io::SeekFrom::Start(cde.central_directory_offset as u64))?;
            let central_directory_actual_offset = spec::CentralDirectoryHeader::find(&mut reader)?;
            (
                cde.total_number_of_records as u64,
                cde.central_directory_offset as u64,
                central_directory_actual_offset - (cde.central_directory_offset as u64),
            )
        };

        reader.seek(io::SeekFrom::Start(zip_offset + central_directory_offset))?;
        let mut files = Vec::with_capacity(number_of_records as usize);
        for _ in 0..number_of_records {
            let header = spec::CentralDirectoryHeader::parse(&mut reader)?;
            files.push(ZipFile {
                name: header.name.into_owned(),
                offset: header.offset,
                size: header.size,
                hasher: Hasher::new(),
                crc32: header.crc32,
                flags: header.flags,
                external_attributes: header.external_attributes,
                timestamp: header.modified,
            });
        }

        Ok(Self {
            path,
            files,
            zip_offset,
        })
    }

    /// Finds file bounds in the ZIP archive.
    ///
    /// Returns a tuple of start offset and file size.
    fn file_bounds(&self, name: &str) -> Result<(u64, u64)> {
        let file = self
            .files
            .iter()
            .find(|f| f.name == name)
            .context("file not found")?;
        let mut reader = BufReader::new(File::open(&self.path)?);
        reader.seek(io::SeekFrom::Start(self.zip_offset + file.offset))?;
        spec::LocalFileHeader::parse(&mut reader)?;
        Ok((reader.stream_position()?, file.size))
    }

    /// Reads a file from the ZIP archive.
    ///
    /// Returns a tuple of a reader and a file size.
    pub(super) fn read(&self, name: &str) -> Result<(impl Read, u64)> {
        let (offset, size) = self.file_bounds(name)?;
        let mut reader = BufReader::new(File::open(&self.path)?);
        reader.seek(io::SeekFrom::Start(offset))?;
        Ok((reader.take(size), size))
    }

    /// Reads a file from the ZIP archive.
    ///
    /// Returns a tuple of a reader and a file size.
    pub(super) async fn async_read(&self, name: &str) -> Result<(impl tokio::io::AsyncRead, u64)> {
        let (offset, size) = self.file_bounds(name)?;
        let mut reader = tokio::io::BufReader::new(tokio::fs::File::open(&self.path).await?);
        reader.seek(io::SeekFrom::Start(offset)).await?;
        Ok((reader.take(size), size))
    }

    // Return the list of files present in the Zip archive.
    pub(super) fn file_names(&self) -> impl Iterator<Item = &str> {
        self.files.iter().map(|f| f.name.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_zip() -> anyhow::Result<()> {
        use std::io::Cursor;
        let mut zip = ZipArchive::new(Cursor::new(Vec::new()));
        let opts = Default::default();
        zip.add("test.txt", &opts)?;
        zip.write_all(b"First file")?;
        zip.add("test2.txt", &opts)?;
        zip.write_all(b"Second file")?;
        zip.finish()?;
        Ok(())
    }
}
