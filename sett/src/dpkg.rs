//! Types and constants related to the data package format

use std::{
    io::Read,
    path::{Path, PathBuf},
    str::FromStr,
};

use anyhow::Context;
use chrono::{DateTime, Utc};
use sequoia_openpgp::parse::{stream::DetachedVerifierBuilder, Parse};
use serde::{de::Visitor, Deserialize, Serialize, Serializer};

use crate::certstore::{self, CertLoad};

/// Date format used for generating the default data package file name.
pub const DATETIME_FORMAT: &str = "%Y%m%dT%H%M%S";
/// Directory where decrypted and decompressed files are stored.
pub const CONTENT_FOLDER: &str = "content";
/// File containing checksums of individual input files.
pub const CHECKSUM_FILE: &str = "checksum.sha256";
/// Archive file containing all input files.
pub const DATA_FILE: &str = "data.tar.gz";
/// Encrypted archive file.
pub const DATA_FILE_ENCRYPTED: &str = "data.tar.gz.gpg";
/// File containing package metadata.
pub const METADATA_FILE: &str = "metadata.json";
/// Detached signature of the metadata file.
pub const METADATA_SIG_FILE: &str = "metadata.json.sig";

/// Data package verification state.
pub mod state {
    /// A marker type for a data package that has been verified.
    #[derive(Debug, Clone)]
    pub struct Verified;
    /// A marker type for a data package that has not been verified.
    #[derive(Debug, Clone)]
    pub struct Unverified;

    /// This is a sealed trait, it cannot be implemented outside of this crate.
    pub trait State: sealed::Sealed {}
    impl State for Unverified {}
    impl State for Verified {}

    mod sealed {
        pub trait Sealed {}
        impl Sealed for super::Unverified {}
        impl Sealed for super::Verified {}
    }
}

/// A type for securely working with the data package format.
///
/// A data package is a ZIP file containing the following files:
/// - `metadata.json`: metadata about the package
/// - `metadata.json.sig`: detached signature of the metadata file
/// - `data.tar.gz.gpg`: encrypted archive of the input files
///
/// This type is generic over the verification state of the package.  A
/// package that has been verified is in the [`Verified`] state, and a
/// package that has not been verified is in the [`Unverified`] state.
/// [`Package::verify`] can be used to transition a package from the
/// [`Unverified`] state to the [`Verified`] state.
///
/// [`Verified`]: state::Verified
/// [`Unverified`]: state::Unverified
#[derive(Debug, Clone)]
pub struct Package<State: state::State = state::Unverified> {
    state: std::marker::PhantomData<State>,
    location: std::path::PathBuf,
    zip_reader: crate::zip::ZipReader,
}

impl<State: state::State> Package<State> {
    pub(crate) fn path(&self) -> &Path {
        &self.location
    }
}

impl Package<state::Unverified> {
    /// Open a data package file.
    pub fn open(path: impl AsRef<Path>) -> anyhow::Result<Self> {
        anyhow::ensure!(path.as_ref().exists(), "package does not exist");
        let path = PathBuf::from(path.as_ref());
        Ok(Package {
            state: Default::default(),
            location: path.clone(),
            zip_reader: crate::zip::ZipReader::open(path)?,
        })
    }

    /// Verify the Zip archive has the correct structure for a data package,
    /// i.e. check that it contains exactly the expected files.
    fn verify_format(&self) -> anyhow::Result<()> {
        const EXPECTED_FILES: [&str; 3] = [DATA_FILE_ENCRYPTED, METADATA_FILE, METADATA_SIG_FILE];
        let error_msg_base = format!(
            "A valid data package must contain exactly the following {} files: {}",
            EXPECTED_FILES.len(),
            EXPECTED_FILES.join(", ")
        );

        // Search the .zip archive for the expected files.
        let mut actual_files = Vec::new();
        for file_name in self.zip_reader.file_names() {
            if EXPECTED_FILES.contains(&file_name) {
                actual_files.push(file_name);
            } else {
                anyhow::bail!(
                    "invalid data package. Zip archive contains unexpected \
                    files. {error_msg_base}."
                );
            }
        }

        // If exactly all expected files are present, verification is complete.
        if actual_files.len() == EXPECTED_FILES.len() {
            return Ok(());
        }

        Err(anyhow::anyhow!(
            "invalid data package. Zip archive is missing the following \
            files: {}. {}.",
            EXPECTED_FILES
                .into_iter()
                .filter(|f| !actual_files.contains(f))
                .collect::<Vec<&str>>()
                .join(", "),
            error_msg_base
        ))
    }

    /// Verify the signature of a data package.
    ///
    /// Verifies the signature of the metadata file contained in the data
    /// package. If the signature is valid, the function returns a [`Package`]
    /// in [`Verified`] state. Otherwise, the function returns an error.
    ///
    /// [`Verified`]: state::Verified
    pub fn verify(self, cert_source: &impl CertLoad) -> anyhow::Result<Package<state::Verified>> {
        macro_rules! read_inner {
            ($file:expr) => {{
                let mut buffer = Vec::new();
                self.zip_reader
                    .read($file)
                    .with_context(|| format!("{} not found", $file))?
                    .0
                    .read_to_end(&mut buffer)?;
                buffer
            }};
        }

        self.verify_format()?;
        let signature_raw = read_inner!(METADATA_SIG_FILE);
        let signature = sequoia_openpgp::Packet::from_bytes(&signature_raw)?;
        let fingerprint = if let sequoia_openpgp::Packet::Signature(sig) = &signature {
            let mut fingerprint_iter = sig.issuer_fingerprints();
            let fingerprint = fingerprint_iter.next().context("no fingerprints")?;
            anyhow::ensure!(
                fingerprint_iter.next().is_none(),
                "signature contains multiple fingerprints"
            );
            fingerprint
        } else {
            anyhow::bail!("invalid signature packet");
        };
        let cert = cert_source
            .load(&(fingerprint.clone().into()), certstore::CertType::Public)?
            .with_context(|| {
                format!(
                    "unable to verify the data sender's signature. No \
                    certificate found for fingerprint '{fingerprint}' \
                    (used for package signature). \
                    Possible causes for this error are: \
                    i) the sender's key is not in your local keystore, \
                    ii) the sender is not who you think they are, \
                    iii) (only if using command line mode) the key passed as \
                    sender argument is not the correct one."
                )
            })?;
        let policy = sequoia_openpgp::policy::StandardPolicy::new();
        DetachedVerifierBuilder::from_bytes(&signature_raw)?
            .with_policy(
                &policy,
                None,
                crate::crypt::Helper::new(&[], &[&cert.into()], &policy, None)?,
            )?
            .verify_bytes(read_inner!(METADATA_FILE))?;
        Ok(Package {
            state: Default::default(),
            location: self.location,
            zip_reader: self.zip_reader,
        })
    }
}

impl Package<state::Verified> {
    /// Reads metadata from a data package file.
    pub fn metadata(&self) -> anyhow::Result<PkgMetadata> {
        Ok(serde_json::from_reader(
            self.zip_reader.read(METADATA_FILE)?.0,
        )?)
    }

    /// Reads the encrypted data file from a data package.
    ///
    /// Returns a reader for the encrypted data file and its size.
    pub(crate) async fn data(&self) -> anyhow::Result<(impl tokio::io::AsyncRead, u64)> {
        self.zip_reader.async_read(DATA_FILE_ENCRYPTED).await
    }
}

/// Package Metadata struct
#[derive(Deserialize, Serialize, Debug, Default)]
pub struct PkgMetadata {
    /// Fingerprint of the data package sender.
    pub sender: String,
    /// Data package recipients fingerprints.
    pub recipients: Vec<String>,
    /// Checksum of the encrypted data file.
    pub checksum: String,
    /// Creation time of the data package.
    ///
    /// (De)serialized using the RFC 3339 format.
    pub timestamp: DateTime<Utc>,
    /// Metadata version.
    #[serde(default = "default_version")]
    pub version: String,
    /// Algorithm used to compute the checksum of the encrypted data file.
    #[serde(default)]
    pub checksum_algorithm: ChecksumAlgorithm,
    #[serde(default)]
    /// Algorithm used to compress input data files.
    pub compression_algorithm: CompressionAlgorithm,
    #[serde(default)]
    /// Data transfer ID (DTR).
    pub transfer_id: Option<u32>,
    #[serde(default)]
    /// Data package purpose.
    pub purpose: Option<Purpose>,
}

/// Possible checksum algorithms for the encrypted data file.
#[derive(Deserialize, Serialize, Debug, Default)]
pub enum ChecksumAlgorithm {
    /// sha256
    #[default]
    SHA256,
}

/// Possible compression algorithms for data.
///
/// Note: compression is applied before encryption.
#[derive(Clone, Debug)]
pub enum CompressionAlgorithm {
    /// No compression
    Stored,
    /// Use gzip compression (level 1-9)
    Gzip(Option<u32>),
    /// Use zstandard compression (level 1-21)
    Zstandard(Option<i32>),
}

impl Default for CompressionAlgorithm {
    fn default() -> Self {
        Self::Zstandard(None)
    }
}

impl Serialize for CompressionAlgorithm {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match *self {
            CompressionAlgorithm::Stored => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 0, "stored")
            }
            CompressionAlgorithm::Gzip(_) => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 1, "gzip")
            }
            CompressionAlgorithm::Zstandard(_) => {
                serializer.serialize_unit_variant("CompressionAlgorithm", 2, "zstandard")
            }
        }
    }
}

impl<'de> Deserialize<'de> for CompressionAlgorithm {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        struct CompressionAlgorithmVisitor;

        impl<'de> Visitor<'de> for CompressionAlgorithmVisitor {
            type Value = CompressionAlgorithm;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("one of `stored`, `gzip`, `zstandard`")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                match v.to_lowercase().as_str() {
                    "stored" => Ok(CompressionAlgorithm::Stored),
                    "gzip" => Ok(CompressionAlgorithm::Gzip(None)),
                    "zstandard" => Ok(CompressionAlgorithm::Zstandard(None)),
                    _ => Err(E::custom(format!("unknown variant `{}`", v))),
                }
            }
        }
        deserializer.deserialize_str(CompressionAlgorithmVisitor {})
    }
}

/// Returns the current default metadata version.
pub fn default_version() -> String {
    "0.7.1".into()
}

/// Data package purpose determines if data is meant for testing or production.
#[derive(Copy, Clone, Deserialize, Serialize, Debug)]
pub enum Purpose {
    /// For sensitive data
    PRODUCTION,
    /// Only for testing
    TEST,
}

impl FromStr for Purpose {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "production" => Ok(Self::PRODUCTION),
            "test" => Ok(Self::TEST),
            _ => Err(anyhow::anyhow!("Invalid purpose: {}", s)),
        }
    }
}

/// Generates the default name for the data package based on a timestamp.
pub(crate) fn generate_package_name(timestamp: &DateTime<Utc>, prefix: Option<&str>) -> String {
    let ts = timestamp.format(DATETIME_FORMAT);
    if let Some(prefix) = prefix {
        format!("{prefix}_{ts}.zip")
    } else {
        format!("{ts}.zip")
    }
}
