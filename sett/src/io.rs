//!  Low-level I/O utilities.

use std::{
    io::{self, Write as _},
    mem, thread,
};

use anyhow::Context as _;
use bytes::{BufMut as _, BytesMut};

fn to_io_error<E: std::fmt::Debug>(e: E) -> io::Error {
    io::Error::new(io::ErrorKind::Other, format!("{e:?}"))
}

pub(crate) enum Source {
    Channel(tokio::sync::mpsc::Receiver<BytesMut>),
    New(usize),
}

impl Source {
    fn get(&mut self) -> anyhow::Result<BytesMut> {
        match self {
            Source::Channel(receiver) => receiver.blocking_recv().context("channel closed"),
            Source::New(size) => Ok(BytesMut::with_capacity(*size)),
        }
    }
}

pub(crate) struct ChannelWriter {
    source: Source,
    sink: tokio::sync::mpsc::Sender<BytesMut>,
    buffer: BytesMut,
}

impl ChannelWriter {
    pub(crate) fn new(
        mut source: Source,
        sink: tokio::sync::mpsc::Sender<BytesMut>,
    ) -> anyhow::Result<Self> {
        let buffer = source.get()?;
        Ok(Self {
            source,
            sink,
            buffer,
        })
    }
}

impl io::Write for ChannelWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        // NOTE: `BufMut::remaining_mut` doesn't consider the capacity of the buffer, so we need to
        // calculate the remaining space manually.
        let remaining_capacity = self.buffer.capacity() - self.buffer.len();
        let bytes_to_copy = std::cmp::min(buf.len(), remaining_capacity);
        self.buffer.put_slice(&buf[..bytes_to_copy]);
        if remaining_capacity - bytes_to_copy == 0 {
            self.sink
                .blocking_send(mem::replace(
                    &mut self.buffer,
                    self.source.get().map_err(to_io_error)?,
                ))
                .context("channel closed while sending (write)")
                .map_err(to_io_error)?;
            self.buffer.clear();
        }
        Ok(bytes_to_copy)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.sink
            .blocking_send(mem::replace(
                &mut self.buffer,
                self.source.get().map_err(to_io_error)?,
            ))
            .context("channel closed while sending (flush)")
            .map_err(to_io_error)?;
        Ok(())
    }
}

enum Message {
    Payload(BytesMut),
    Flush,
    Finalize,
}

pub(super) struct FgWriter {
    sender: std::sync::mpsc::Sender<Message>,
    receiver: std::sync::mpsc::Receiver<io::Result<BytesMut>>,
    buffer: BytesMut,
}

impl FgWriter {
    fn new(
        sender: std::sync::mpsc::Sender<Message>,
        receiver: std::sync::mpsc::Receiver<io::Result<BytesMut>>,
        buffer_size: usize,
    ) -> Self {
        Self {
            sender,
            receiver,
            buffer: BytesMut::with_capacity(buffer_size),
        }
    }

    fn exchange_buffer(&mut self) -> io::Result<()> {
        let buffer = std::mem::replace(
            &mut self.buffer,
            self.receiver.recv().map_err(to_io_error)??,
        );
        self.buffer.clear();
        self.sender
            .send(Message::Payload(buffer))
            .map_err(to_io_error)
    }

    fn finalize(&mut self) -> io::Result<()> {
        self.exchange_buffer()?;
        self.sender.send(Message::Finalize).map_err(to_io_error)
    }
}

impl io::Write for FgWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let remaining_capacity = self.buffer.capacity() - self.buffer.len();
        let bytes_to_copy = std::cmp::min(buf.len(), remaining_capacity);
        self.buffer.put_slice(&buf[..bytes_to_copy]);
        if remaining_capacity - bytes_to_copy == 0 {
            self.exchange_buffer()?;
        }
        Ok(bytes_to_copy)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.exchange_buffer()?;
        self.sender.send(Message::Flush).map_err(to_io_error)
    }
}

struct BgWriter {
    sender: std::sync::mpsc::Sender<io::Result<BytesMut>>,
    receiver: std::sync::mpsc::Receiver<Message>,
}

impl BgWriter {
    fn new(
        sender: std::sync::mpsc::Sender<io::Result<BytesMut>>,
        receiver: std::sync::mpsc::Receiver<Message>,
        buffer_size: usize,
        queue_length: usize,
    ) -> io::Result<Self> {
        for _ in 0..queue_length {
            sender
                .send(Ok(BytesMut::with_capacity(buffer_size)))
                .map_err(to_io_error)?;
        }
        Ok(Self { sender, receiver })
    }

    fn listen<W: io::Write>(&mut self, writer: &mut W) -> io::Result<()> {
        loop {
            match self.receiver.recv().map_err(to_io_error)? {
                Message::Payload(buffer) => {
                    if buffer.is_empty() {
                        self.sender.send(Ok(buffer)).map_err(to_io_error)?;
                        continue;
                    }
                    if let Err(e) = writer.write_all(&buffer) {
                        self.sender
                            .send(Err(to_io_error("error occurred while writing")))
                            .map_err(to_io_error)?;
                        return Err(e);
                    }
                    self.sender.send(Ok(buffer)).map_err(to_io_error)?;
                }
                Message::Flush => {
                    if let Err(e) = writer.flush() {
                        self.sender
                            .send(Err(to_io_error("error occurred while flushing")))
                            .map_err(to_io_error)?;
                        return Err(e);
                    }
                }
                Message::Finalize => break,
            }
        }
        Ok(())
    }
}

pub(super) struct ThreadWriter {
    buffer_size: usize,
    queue_length: usize,
}

impl Default for ThreadWriter {
    fn default() -> Self {
        Self::new(1 << 22, 3)
    }
}

impl ThreadWriter {
    pub(super) fn new(buffer_size: usize, queue_length: usize) -> Self {
        Self {
            buffer_size,
            queue_length,
        }
    }

    pub(super) fn write<W, F, O, E>(&self, writer: &mut W, f: F) -> Result<O, E>
    where
        W: io::Write + Send,
        E: From<io::Error>,
        F: FnOnce(&mut FgWriter) -> Result<O, E>,
    {
        let (sender, receiver) = std::sync::mpsc::channel();
        let (sender_back, receiver_back) = std::sync::mpsc::channel();
        let mut fg_writer = FgWriter::new(sender, receiver_back, self.buffer_size);
        let mut bg_writer =
            BgWriter::new(sender_back, receiver, self.buffer_size, self.queue_length)?;
        thread::scope(move |s| {
            let handle = s.spawn(move || bg_writer.listen(writer));
            let output = f(&mut fg_writer);
            fg_writer.flush()?;
            fg_writer.finalize()?;
            handle.join().map_err(to_io_error)??;
            output
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn write_to_buffer() {
        let thread_writer = ThreadWriter::new(8, 1);
        let mut output = Vec::new();
        let text = "We want a shrubbery!".as_bytes();
        for b in text {
            thread_writer
                .write(&mut output, |w| -> Result<(), io::Error> {
                    assert_eq!(w.write(&[*b]).unwrap(), 1);
                    Ok(())
                })
                .unwrap();
        }
        assert_eq!(&output, text);
    }
}
