//! OIDC authentication and token management.

use openidconnect::OAuth2TokenResponse;
use serde::{Deserialize, Serialize};
use tracing::debug;

#[derive(Clone, Debug, Deserialize, Serialize)]
struct DeviceEndpointProviderMetadata {
    device_authorization_endpoint: openidconnect::DeviceAuthorizationUrl,
}

impl openidconnect::AdditionalProviderMetadata for DeviceEndpointProviderMetadata {}

type DeviceProviderMetadata = openidconnect::ProviderMetadata<
    DeviceEndpointProviderMetadata,
    openidconnect::core::CoreAuthDisplay,
    openidconnect::core::CoreClientAuthMethod,
    openidconnect::core::CoreClaimName,
    openidconnect::core::CoreClaimType,
    openidconnect::core::CoreGrantType,
    openidconnect::core::CoreJweContentEncryptionAlgorithm,
    openidconnect::core::CoreJweKeyManagementAlgorithm,
    openidconnect::core::CoreJwsSigningAlgorithm,
    openidconnect::core::CoreJsonWebKeyType,
    openidconnect::core::CoreJsonWebKeyUse,
    openidconnect::core::CoreJsonWebKey,
    openidconnect::core::CoreResponseMode,
    openidconnect::core::CoreResponseType,
    openidconnect::core::CoreSubjectIdentifierType,
>;

/// OAuth2 access token
pub struct AccessToken(pub(crate) String);

/// OAuth2 refresh token
pub struct RefreshToken(String);

/// OAuth2 tokens.
pub struct Token {
    /// Access token.
    pub access_token: AccessToken,
    /// Refresh token.
    pub refresh_token: Option<RefreshToken>,
}

/// Openid Connect client.
///
/// This client can be used to authenticate with an OpenID Connect provider using device
/// authorization and refresh tokens.
pub struct Oidc {
    client_id: openidconnect::ClientId,
    issuer_url: openidconnect::IssuerUrl,
}

/// Details for the authentication handler.
pub struct VerificationDetails {
    /// User Code.
    pub user_code: String,
    /// Verification URI.
    pub verification_uri: String,
    /// Verification URI with User Code as query parameter.
    pub verification_uri_complete: Option<String>,
}

type AuthHandler = dyn FnOnce(&VerificationDetails) -> anyhow::Result<()>;

impl Oidc {
    /// Create a new OpenID Connect client.
    pub fn new(client_id: &str, issuer_url: &str) -> anyhow::Result<Self> {
        Ok(Self {
            client_id: openidconnect::ClientId::new(client_id.into()),
            issuer_url: openidconnect::IssuerUrl::new(issuer_url.into())?,
        })
    }

    /// Authenticate with the OpenID Connect provider using device authorization.
    ///
    /// The `auth_handler` is a callback for returning the verification URL that the user should
    /// open in their browser to authenticate.
    pub async fn authenticate(&self, auth_handler: Box<AuthHandler>) -> anyhow::Result<Token> {
        let issuer_metadata =
            DeviceProviderMetadata::discover_async(self.issuer_url.clone(), http_client).await?;
        debug!(?issuer_metadata, "provider metadata");
        let device_authorization_uri = issuer_metadata
            .additional_metadata()
            .device_authorization_endpoint
            .clone();
        let client = openidconnect::core::CoreClient::from_provider_metadata(
            issuer_metadata,
            self.client_id.clone(),
            None,
        )
        .set_device_authorization_uri(device_authorization_uri)
        .set_auth_type(openidconnect::AuthType::RequestBody);
        let (pkce_challenge, pkce_verifier) = openidconnect::PkceCodeChallenge::new_random_sha256();

        let details: openidconnect::core::CoreDeviceAuthorizationResponse = client
            .exchange_device_code()?
            .add_scope(openidconnect::Scope::new("profile".to_string()))
            // .set_pkce_challenge(pkce_challenge.clone())
            .add_extra_param("code_challenge", pkce_challenge.as_str())
            .add_extra_param("code_challenge_method", pkce_challenge.method().as_str())
            .request_async(http_client)
            .await?;
        debug!(?details, "device authorization response");
        auth_handler(&VerificationDetails {
            user_code: details.user_code().secret().to_string(),
            verification_uri: details.verification_uri().to_string(),
            verification_uri_complete: details
                .verification_uri_complete()
                .map(|uri| uri.secret().to_string()),
        })?;
        let token = client
            .exchange_device_access_token(&details)
            .add_extra_param("code_verifier", pkce_verifier.secret())
            .request_async(http_client, tokio::time::sleep, None)
            .await?;
        debug!(?token, "token response");

        Ok(Token {
            access_token: AccessToken(token.access_token().secret().to_string()),
            refresh_token: token
                .refresh_token()
                .map(|t| RefreshToken(t.secret().to_string())),
        })
    }

    /// Refresh the access token using a refresh token.
    pub async fn refresh(&self, refresh_token: &RefreshToken) -> anyhow::Result<Token> {
        let issuer_metadata = openidconnect::core::CoreProviderMetadata::discover_async(
            self.issuer_url.clone(),
            http_client,
        )
        .await?;
        let client = openidconnect::core::CoreClient::from_provider_metadata(
            issuer_metadata,
            self.client_id.clone(),
            None,
        );
        let token = client
            .exchange_refresh_token(&openidconnect::RefreshToken::new(refresh_token.0.clone()))
            .request_async(http_client)
            .await?;
        debug!(?token, "refreshed token");
        Ok(Token {
            access_token: AccessToken(token.access_token().secret().to_string()),
            refresh_token: token
                .refresh_token()
                .map(|t| RefreshToken(t.secret().to_string())),
        })
    }
}

/// HTTP client for OpenID Connect requests.
async fn http_client(
    request: openidconnect::HttpRequest,
) -> Result<openidconnect::HttpResponse, reqwest::Error> {
    let client = reqwest::Client::builder()
        .use_rustls_tls()
        // Prevent SSRF (Server-side request forgery) vulnerabilities.
        .redirect(reqwest::redirect::Policy::none())
        .build()?;
    let mut request_builder = client
        .request(
            reqwest::Method::from_bytes(request.method.as_str().as_bytes()).expect("valid method"),
            request.url.as_str(),
        )
        .body(request.body);
    for (name, value) in &request.headers {
        request_builder = request_builder.header(name.as_str(), value.as_bytes());
    }
    let request = request_builder.build()?;
    let response = client.execute(request).await?;
    let status_code = openidconnect::http::StatusCode::from_u16(response.status().as_u16())
        .expect("valid status code");
    let headers = response
        .headers()
        .iter()
        .map(|(key, value)| {
            (
                key.as_str()
                    .as_bytes()
                    .try_into()
                    .expect("valid header name"),
                value.as_bytes().try_into().expect("valid header value"),
            )
        })
        .collect();
    let body = response.bytes().await?.to_vec();
    Ok(openidconnect::HttpResponse {
        status_code,
        headers,
        body,
    })
}
