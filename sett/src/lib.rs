//! Library for Secure Encryption and Transfer Tool (sett).
#![warn(missing_docs)]

#[cfg(feature = "auth")]
pub mod auth;
pub mod certstore;
mod checksum;
mod crypt;
pub mod decrypt;
pub mod destination;
pub mod dpkg;
pub mod encrypt;
mod filesystem;
pub mod gnupg;
mod io;
pub mod keyserver;
pub mod portal;
pub mod task;
pub mod utils;
mod zip;
