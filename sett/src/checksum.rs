//! Checksum generation and verification

use std::{
    collections::BTreeMap,
    fmt::Write as _,
    fs::File,
    io::{self, BufRead as _},
    path::{Path, PathBuf},
};

use anyhow::{Context as _, Result};
use rayon::prelude::*;
use sha2::{Digest, Sha256};

/// Converts bytes into a hexadecimal value.
pub(crate) fn to_hex_string(bytes: &[u8]) -> String {
    bytes.iter().fold(String::new(), |mut output, b| {
        // `write!` to a String can never fail so it's safe to ignore the result.
        let _ = write!(output, "{b:02x}");
        output
    })
}

/// Computes Sha256 checksum.
fn compute_checksum(source: &mut impl io::Read) -> Result<String> {
    let mut hasher = Sha256::new();
    io::copy(source, &mut hasher)?;
    let hash = hasher.finalize();

    Ok(to_hex_string(&hash))
}

/// Generates the checksum file content for the data package.
///
/// Given a list of files, in the form of pairs (path to file, path inside of
/// the data package), compute the Sha256 checksum for each file and return
/// output as:
///
/// ```text
/// <checksum1> <file 1 path inside the data package>
/// <checksum2> <file 2 path inside the data package>
/// ...
/// ```
///
/// The computation is performed in parallel.
pub(super) fn generate_checksums_file_content(
    file_path: &[(impl AsRef<Path>, impl AsRef<Path>)],
    max_workers: usize,
) -> Result<String> {
    let fp_ref = file_path
        .iter()
        .map(|(f, p)| (f.as_ref(), p.as_ref()))
        .collect::<Vec<_>>();
    let mut checksums = rayon::ThreadPoolBuilder::new()
        .num_threads(max_workers)
        .build()?
        .install(|| {
            fp_ref.par_iter().map(|(f, p)| {
                let cs = match File::open(f) {
                    Ok(mut source) => compute_checksum(&mut source),
                    Err(e) => Err(e.into()),
                };
                (cs, p)
            })
        })
        .map(|(sha, path)| Ok((sha?, path)))
        .collect::<Result<Vec<_>>>()?;
    checksums.sort_by_key(|x| x.1);
    Ok(checksums
        .iter()
        .map(|(sha, path)| -> Result<_> {
            let posix_path = crate::filesystem::to_posix_path(path)
                .with_context(|| format!("Path contains non-UTF-8 characters: {path:?}"))?;
            Ok(format!("{sha} {posix_path}"))
        })
        .collect::<Result<Vec<_>>>()?
        .join("\n"))
}

/// Returns the content of the data package checksum file.
///
/// The checksum file has the following structure:
///
/// ```text
/// <checksum1> <file 1 path inside the data package>
/// <checksum2> <file 2 path inside the data package>
/// ...
/// ```
pub(super) fn read_checksum_file(path: impl AsRef<Path>) -> Result<BTreeMap<PathBuf, String>> {
    let mut reader = io::BufReader::new(File::open(path)?);
    let mut parsed = BTreeMap::new();
    let mut buf = String::new();
    while reader.read_line(&mut buf)? > 0 {
        let (checksum, path) = buf
            .trim()
            .split_once(char::is_whitespace)
            .context("Unable to parse the checksum file")?;
        parsed.insert(PathBuf::from(path), checksum.to_string());
        buf.clear();
    }
    Ok(parsed)
}

pub(super) fn verify_checksums(
    source: &BTreeMap<PathBuf, String>,
    reference: &BTreeMap<PathBuf, String>,
) -> Result<()> {
    for (path, checksum) in source {
        let expected = reference
            .get(path)
            .with_context(|| format!("unable to find checksum for file: {path:?}"))?;
        if !checksum.eq_ignore_ascii_case(expected) {
            anyhow::bail!("wrong checksum for {path:?} (expected {expected}, computed {checksum})");
        }
    }
    Ok(())
}

/// Computes Sha256 checksum while passing input into the underlying writer.
pub(super) struct Sha256ChecksumWriter<W> {
    writer: W,
    hasher: Sha256,
}

impl<W> Sha256ChecksumWriter<W> {
    /// Initializes a new writer.
    pub(super) fn new(writer: W) -> Self {
        let hasher = Sha256::new();
        Self { writer, hasher }
    }

    /// Returns the computed checksum.
    pub(super) fn get_hash(self) -> String {
        to_hex_string(&self.hasher.finalize())
    }
}

impl<W: io::Write> io::Write for Sha256ChecksumWriter<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let written = self.writer.write(buf)?;
        self.hasher.update(buf);
        Ok(written)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.writer.flush()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::{Cursor, Write as _};

    #[test]
    fn test_to_hex_string() {
        assert_eq!(&to_hex_string(b"chucknorris"), "636875636b6e6f72726973");
    }

    #[test]
    fn test_compute_checksum() -> Result<()> {
        let mut source = Cursor::new(b"chucknorris\n");
        assert_eq!(
            &compute_checksum(&mut source)?,
            "79d6d443ff65b164a0ee9bf4cc5150f1e5434e147c1f33dc593d4c33ff55b935"
        );
        Ok(())
    }

    #[test]
    fn test_sha256_writer() {
        let mut output = Vec::new();
        let mut writer = Sha256ChecksumWriter::new(&mut output);
        writer.write_all(b"chuck").unwrap();
        let cs = writer.get_hash();
        assert_eq!(
            &cs,
            "632db65f4b5accae489fa673e5687fa8e62a9b105ee330bc041ebb4a8bb5750f"
        );
        assert_eq!(&output, b"chuck");
    }
}
