//! Interface to an OpenPGP keyserver.

use crate::certstore::{Cert, CertType, SerializationFormat};

#[derive(serde::Serialize, Debug)]
struct UploadRequest<'a> {
    keytext: &'a str,
}

#[derive(serde::Serialize, Debug)]
struct VerifyRequest<'a> {
    token: &'a str,
    addresses: Vec<&'a str>,
}

#[derive(serde::Deserialize, Debug)]
/// Standard response from an OpenPGP keyserver.
pub struct KeyserverResponse {
    /// Status of the email verification process for this key.
    pub status: std::collections::BTreeMap<String, KeyserverEmailStatus>,
    /// Opaque token, to be used to initiate the verification process after
    /// uploading the key.
    pub token: String,
}

#[derive(serde::Deserialize, Clone, Debug)]
#[serde(rename_all = "lowercase")]
/// Email verification process statuses.
pub enum KeyserverEmailStatus {
    /// No verification process has been triggered for this email.
    Unpublished,
    /// The key associated to this email has been already verified.
    Published,
    /// The key associated to this email has been revoked and cannot be used.
    Revoked,
    /// The verification process has been triggered but is still pending.
    Pending,
}

/// A client for interacting with the Keyserver API
#[derive(Debug)]
pub struct Keyserver {
    client: reqwest::Client,
}

impl Keyserver {
    /// Return a new Keyserver client.
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {
            client: reqwest::Client::builder().use_rustls_tls().build()?,
        })
    }

    /// Request key verification (email ownership verification) from keyserver
    ///
    /// Send a request to keyserver to initiate the key verification process
    /// for the certificate associated with the given `token` and `email`.
    pub async fn verify_cert(&self, token: &str, email: &str) -> anyhow::Result<KeyserverResponse> {
        const VERIFY_URL: &str = "https://keys.openpgp.org/vks/v1/request-verify";
        let response = self
            .client
            .post(VERIFY_URL)
            .json(&VerifyRequest {
                token,
                addresses: vec![email],
            })
            .send()
            .await?;
        Ok(response.json().await?)
    }

    /// Upload a certificate to this keyserver.
    pub async fn upload_cert(&self, cert: &Cert) -> anyhow::Result<KeyserverResponse> {
        const UPLOAD_URL: &str = "https://keys.openpgp.org/vks/v1/upload";
        let bytes = cert.serialize(CertType::Public, SerializationFormat::AsciiArmored)?;
        let keytext = std::str::from_utf8(&bytes)?;
        let response = self
            .client
            .post(UPLOAD_URL)
            .json(&UploadRequest { keytext })
            .send()
            .await?;
        Ok(response.json().await?)
    }
}
