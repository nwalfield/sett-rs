//! Private certificate store implementation and interface to
//! sequoia_cert_store.

use std::borrow::Cow;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::{fmt, fs, io, time};

use anyhow::{anyhow, bail, ensure, Context, Result};
use openpgp_cert_d::{CertD, MergeResult};
use regex::Regex;
use sequoia_cert_store::store::keyserver::KEYS_OPENPGP_ORG_URL;
use sequoia_cert_store::{CertStore as SequoiaCertStore, LazyCert, Store, StoreUpdate};
use sequoia_openpgp::parse::{PacketParserBuilder, PacketParserResult};
use sequoia_openpgp::{
    cert::{amalgamation::ValidateAmalgamation, CertParser},
    parse::{PacketParser, Parse},
    policy::StandardPolicy,
    serialize::{Serialize, SerializeInto},
};

const PUB_STORE_DEFAULT_DIR: &str = "pgp.cert.d";
const PUB_STORE_ENV_VARIABLE: &str = "PGP_CERT_D";
const SEC_STORE_DEFAULT_DIR: &str = "pgp.cert.d.sec";
const SEC_STORE_ENV_VARIABLE: &str = "PGP_CERT_D_SEC";

/// OpenPGP certificate.
#[derive(Debug, PartialEq)]
pub struct Cert(sequoia_openpgp::cert::Cert);

impl Cert {
    /// Returns the fingerprint of the certificate.
    pub fn fingerprint(&self) -> sequoia_openpgp::Fingerprint {
        self.0.fingerprint()
    }

    /// Returns the user IDs of the certificate as strings.
    pub fn userids(&self) -> Vec<String> {
        self.0
            .userids()
            .map(|uid| uid.userid().to_string())
            .collect()
    }

    /// Convert/Export the specified OpenPGP certificate to bytes.
    ///
    /// # Arguments
    ///
    /// * `cert_type`: if `cert_type` is set to `CertType::Secret`, the secret
    ///   material of the certificate is included in the export, otherwise it is
    ///   stripped. If `CertType::Secret` is passed and the certificate does not
    ///   contain any secret material, an error is returned.
    /// * `format`: one of `SerializationFormat::Binary` or `AsciiArmored`,
    ///   indicating whether the export format should be binary or ASCII-armored.
    pub fn serialize(&self, cert_type: CertType, format: SerializationFormat) -> Result<Vec<u8>> {
        // Raise an error if the export format requests secret material to be
        // included but the actual certificate does not contain secret material.
        if matches!(cert_type, CertType::Secret) && !self.0.is_tsk() {
            bail!(
                "The OpenPGP key '{}' contains no private material to serialize",
                userid_as_string(self)
            );
        }

        // Serialize the certificate to bytes, either in binary or ASCII-armored
        // format.
        match (format, cert_type) {
            (SerializationFormat::Binary, CertType::Public) => self.0.to_vec(),
            (SerializationFormat::Binary, CertType::Secret) => self.0.as_tsk().to_vec(),
            (SerializationFormat::AsciiArmored, CertType::Public) => self.0.armored().to_vec(),
            (SerializationFormat::AsciiArmored, CertType::Secret) => {
                self.0.as_tsk().armored().to_vec()
            }
        }
    }

    fn validate(&self) -> Result<()> {
        if let Err(msg) = self.0.with_policy(&StandardPolicy::new(), None) {
            bail!("Invalid key: no valid self-signature was found. {msg}")
        }
        Ok(())
    }

    /// Return an Option containing the certificate's expiration date, or
    /// `None` if the certificate has no expiration date.
    pub fn expiration_time(&self) -> Result<Option<time::SystemTime>> {
        Ok(self
            .0
            .primary_key()
            .with_policy(&StandardPolicy::new(), None)?
            .key_expiration_time())
    }

    /// Return the first certificate found in the input bytes.
    pub fn from_bytes(data: impl AsRef<[u8]>) -> Result<Self> {
        Ok(Self(sequoia_openpgp::cert::Cert::from_bytes(
            data.as_ref(),
        )?))
    }

    /// Return the certificate type.
    pub fn cert_type(&self) -> CertType {
        if self.0.is_tsk() {
            CertType::Secret
        } else {
            CertType::Public
        }
    }

    /// Check if the certificate's primary key is encrypted.
    pub fn is_encrypted(&self) -> Result<bool> {
        Ok(self
            .0
            .primary_key()
            .key()
            .clone()
            .parts_into_secret()?
            .secret()
            .is_encrypted())
    }

    /// Return the revocation status of the certificate.
    pub fn revocation_status(&self) -> RevocationStatus {
        self.0
            .revocation_status(&StandardPolicy::new(), None)
            .into()
    }

    /// Generate revocation signature.
    ///
    /// Use the primary key of the certificate to generate a revocation signature.
    pub fn generate_rev_sig(
        &self,
        code: ReasonForRevocation,
        reason: &[u8],
        password: Option<&[u8]>,
    ) -> Result<Vec<u8>> {
        let secret = self.0.primary_key().key().clone().parts_into_secret()?;
        let mut signer = if secret.secret().is_encrypted() {
            if let Some(password) = password {
                secret
                    .decrypt_secret(&password.into())
                    .context("Wrong password")?
                    .into_keypair()?
            } else {
                bail!("The OpenPGP key is encrypted, but no password was provided.")
            }
        } else {
            secret.into_keypair()?
        };
        serialize_revocation_signature(&self.0, self.0.revoke(&mut signer, code.into(), reason)?)
    }

    /// Revoke the certificate.
    pub fn revoke<R: std::io::Read + Send + Sync>(&self, signature: R) -> Result<Self> {
        let mut revocations = Vec::new();
        let mut ppr = PacketParserBuilder::from_reader(signature)?
            .buffer_unread_content()
            .build()?;
        while let PacketParserResult::Some(pp) = ppr {
            let (packet, next_ppr) = pp.recurse()?;
            ppr = next_ppr;

            if let sequoia_openpgp::Packet::Signature(sig) = packet {
                if matches!(
                    sig.typ(),
                    sequoia_openpgp::types::SignatureType::CertificationRevocation
                        | sequoia_openpgp::types::SignatureType::KeyRevocation
                        | sequoia_openpgp::types::SignatureType::SubkeyRevocation
                ) {
                    revocations.push(sig);
                }
            }
        }
        ensure!(!revocations.is_empty(), "No revocation signature found");
        Ok(Self(self.0.clone().insert_packets(revocations)?))
    }
}

impl From<sequoia_openpgp::cert::Cert> for Cert {
    fn from(val: sequoia_openpgp::cert::Cert) -> Self {
        Self(val)
    }
}

impl From<Cert> for sequoia_openpgp::cert::Cert {
    fn from(val: Cert) -> Self {
        val.0
    }
}

/// Trait for loading OpenPGP certificates.
pub trait CertLoad {
    /// Loads an OpenPGP certificate matching the specified fingerprint.
    ///
    /// Returns `None` if no certificate matching the specified fingerprint
    /// is found.
    fn load(&self, fingerprint: &Fingerprint, cert_type: CertType) -> Result<Option<Cert>>;
}

impl CertLoad for CertStore<'_> {
    fn load(&self, fingerprint: &Fingerprint, cert_type: CertType) -> Result<Option<Cert>> {
        Ok(self
            .get_cert_by_fingerprint(&fingerprint.clone().into(), cert_type)
            .ok())
    }
}

/// Holds the bytes of OpenPGP certificate(s).
pub struct CertBytes<'a>(&'a [u8]);

impl<'a> From<&'a [u8]> for CertBytes<'a> {
    fn from(val: &'a [u8]) -> Self {
        Self(val)
    }
}

impl CertLoad for CertBytes<'_> {
    fn load(&self, fingerprint: &Fingerprint, cert_type: CertType) -> Result<Option<Cert>> {
        let cert = sequoia_openpgp::Cert::from_bytes(self.0)?;
        let sq_fingerprint = fingerprint.clone().into();
        if let CertType::Secret = cert_type {
            anyhow::ensure!(cert.is_tsk(), "key does not contain private material");
        }
        if !cert.keys().any(|key| key.fingerprint() == sq_fingerprint) {
            return Ok(None);
        }
        Ok(Some(cert.into()))
    }
}

#[derive(Default, Copy, Clone)]
/// Internal representation of the supported cipher suites.
pub enum CipherSuite {
    #[default]
    /// Elliptic-curve cryptography with 256-bit keys.
    Cv25519,
    /// RSA cryptography with 4096-bit keys.
    RSA4k,
}

impl From<CipherSuite> for sequoia_openpgp::cert::CipherSuite {
    fn from(val: CipherSuite) -> Self {
        match val {
            CipherSuite::Cv25519 => Self::Cv25519,
            CipherSuite::RSA4k => Self::RSA4k,
        }
    }
}

#[derive(Debug, Clone)]
/// Internal representation of an OpenPGP signature.
pub struct Signature(sequoia_openpgp::packet::Signature);

impl Signature {
    /// Return the reason for the revocation of the signature.
    pub fn reason_for_revocation(&self) -> Option<(ReasonForRevocation, &[u8])> {
        self.0
            .reason_for_revocation()
            .map(|(code, msg)| (code.into(), msg))
    }
}

impl From<sequoia_openpgp::packet::Signature> for Signature {
    fn from(val: sequoia_openpgp::packet::Signature) -> Self {
        Self(val)
    }
}

impl From<&sequoia_openpgp::packet::Signature> for Signature {
    fn from(val: &sequoia_openpgp::packet::Signature) -> Self {
        Self(val.clone())
    }
}

#[derive(Debug, Clone, PartialEq)]
#[non_exhaustive]
/// Describes the reason for a revocation.
/// This enum cannot be exhaustively matched to allow future extensions.
pub enum ReasonForRevocation {
    /// None of the other reasons apply. This can be used can be used when
    /// creating a revocation signature in advance.
    Unspecified,
    /// The private key has been replaced by a new one.
    KeySuperseded,
    /// The private key material (may) have been compromised.
    KeyCompromised,
    /// This public key should not be used anymore and there is no
    /// replacement key.
    KeyRetired,
    /// User ID information is no longer valid (cert revocations).
    UIDRetired,
    /// Private reason identifier.
    Private(u8),
    /// Unknown reason identifier.
    Unknown(u8),
}

impl std::fmt::Display for ReasonForRevocation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let reason: sequoia_openpgp::types::ReasonForRevocation = self.clone().into();
        write!(f, "{}", reason)
    }
}

impl From<ReasonForRevocation> for sequoia_openpgp::types::ReasonForRevocation {
    fn from(val: ReasonForRevocation) -> Self {
        match val {
            ReasonForRevocation::Unspecified => Self::Unspecified,
            ReasonForRevocation::KeySuperseded => Self::KeySuperseded,
            ReasonForRevocation::KeyCompromised => Self::KeyCompromised,
            ReasonForRevocation::KeyRetired => Self::KeyRetired,
            ReasonForRevocation::UIDRetired => Self::UIDRetired,
            ReasonForRevocation::Private(v) => Self::Private(v),
            ReasonForRevocation::Unknown(v) => Self::Unknown(v),
        }
    }
}

impl From<sequoia_openpgp::types::ReasonForRevocation> for ReasonForRevocation {
    fn from(val: sequoia_openpgp::types::ReasonForRevocation) -> Self {
        match val {
            sequoia_openpgp::types::ReasonForRevocation::Unspecified => Self::Unspecified,
            sequoia_openpgp::types::ReasonForRevocation::KeySuperseded => Self::KeySuperseded,
            sequoia_openpgp::types::ReasonForRevocation::KeyCompromised => Self::KeyCompromised,
            sequoia_openpgp::types::ReasonForRevocation::KeyRetired => Self::KeyRetired,
            sequoia_openpgp::types::ReasonForRevocation::UIDRetired => Self::UIDRetired,
            sequoia_openpgp::types::ReasonForRevocation::Private(v) => Self::Private(v),
            sequoia_openpgp::types::ReasonForRevocation::Unknown(v) => Self::Unknown(v),
            // TODO: currently it's unreachable, but it might change in the future if
            // Sequoia extends the enum.
            _ => todo!(),
        }
    }
}

#[derive(Debug, Clone)]
/// Internal representation of an OpenPGP key revocation status.
pub enum RevocationStatus {
    /// Key is revoked.
    Revoked(Vec<Signature>),
    /// There is a revocation certificate from a possible designated
    /// revoker.
    CouldBe(Vec<Signature>),
    /// Key does not appear to be revoked.
    NotAsFarAsWeKnow,
}

impl From<sequoia_openpgp::types::RevocationStatus<'_>> for RevocationStatus {
    fn from(val: sequoia_openpgp::types::RevocationStatus) -> Self {
        match val {
            sequoia_openpgp::types::RevocationStatus::Revoked(s) => {
                RevocationStatus::Revoked(s.into_iter().map(Into::into).collect())
            }
            sequoia_openpgp::types::RevocationStatus::CouldBe(s) => {
                RevocationStatus::CouldBe(s.into_iter().map(Into::into).collect())
            }
            sequoia_openpgp::types::RevocationStatus::NotAsFarAsWeKnow => {
                RevocationStatus::NotAsFarAsWeKnow
            }
        }
    }
}

fn serialize_revocation_signature(
    cert: &sequoia_openpgp::Cert,
    rev: sequoia_openpgp::packet::Signature,
) -> Result<Vec<u8>> {
    let mut rev_serialized = Vec::new();
    let mut writer = sequoia_openpgp::armor::Writer::with_headers(
        &mut rev_serialized,
        sequoia_openpgp::armor::Kind::Signature,
        std::iter::once(("Comment", "Revocation certificate for")).chain(
            cert.armor_headers()
                .iter()
                .map(|value| ("Comment", value.as_str())),
        ),
    )?;
    sequoia_openpgp::packet::Packet::Signature(rev).serialize(&mut writer)?;
    writer.finalize()?;
    Ok(rev_serialized)
}

/// Generate an OpenPGP certificate with the given information.
pub fn generate_cert(uid: &str, password: &[u8], cipher: CipherSuite) -> Result<(Cert, Vec<u8>)> {
    let builder = sequoia_openpgp::cert::CertBuilder::new()
        .add_authentication_subkey()
        .add_signing_subkey()
        .add_subkey(
            sequoia_openpgp::types::KeyFlags::empty()
                .set_transport_encryption()
                .set_storage_encryption(),
            None,
            None,
        )
        .add_userid(uid)
        .set_cipher_suite(cipher.into())
        .set_password(Some(password.into()));
    let (cert, rev) = builder.generate()?;
    let rev_sig = serialize_revocation_signature(&cert, rev)?;
    Ok((cert.into(), rev_sig))
}

/// Reads OpenPGP certificates from bytes.
///
/// Input data may contain multiple certificates. The parser will return
/// an error if the data contains invalid certificates.
pub fn read_cert<R: io::Read + Send + Sync>(data: R) -> Result<Vec<Cert>> {
    CertParser::from(PacketParser::from_reader(data)?)
        .map(|c| -> Result<_> { Ok(Cert(c?)) })
        .collect()
}

/// Enum indicating whether an OpenPGP certificate stores only public material,
/// or if it also contains secret material.
#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub enum CertType {
    /// Certificate contains only public material.
    #[default]
    Public,
    /// Certificate contains both public and secret material.
    Secret,
}

// Allows to print a `CertType` variant as a string.
impl fmt::Display for CertType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CertType::Public => write!(f, "public"),
            CertType::Secret => write!(f, "private"),
        }
    }
}

/// Enum indicating whether OpenPGP data should be serialized (i.e.
/// exported as bytes) to a binary format or to ASCII-armored format.
#[derive(Clone, Copy, Default)]
pub enum SerializationFormat {
    /// Certificate to be serialized to binary format.
    #[default]
    Binary,
    /// Certificate to be serialized to ASCII-armored format.
    AsciiArmored,
}

impl std::str::FromStr for SerializationFormat {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_lowercase().as_str() {
            "binary" => Ok(Self::Binary),
            "ascii" => Ok(Self::AsciiArmored),
            _ => Err(anyhow!("Invalid SerializationFormat: {s}")),
        }
    }
}

/// A unique identifier for OpenPGP certificates and keys.
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fingerprint(sequoia_openpgp::Fingerprint);

impl From<sequoia_openpgp::Fingerprint> for Fingerprint {
    fn from(val: sequoia_openpgp::Fingerprint) -> Self {
        Self(val)
    }
}

impl From<Fingerprint> for sequoia_openpgp::Fingerprint {
    fn from(val: Fingerprint) -> Self {
        val.0
    }
}

/// Search term to use when retrieving an OpenPGP certificate from a
/// `CertStore`.
pub enum QueryTerm<'a> {
    /// Fingerprint associated with the certificate to retrieve.
    Fingerprint(Cow<'a, sequoia_openpgp::Fingerprint>),
    /// Email associated with the user ID of the certificate to retrieve.
    Email(Cow<'a, str>),
}

impl<'a> std::str::FromStr for QueryTerm<'a> {
    type Err = anyhow::Error;

    /// Converts the input string `s` to a `QueryTerm` Enum.
    /// * If `s` is a valid OpenPGP Fingerprint, return the `Fingerprint` variant.
    /// * If `s` is a valid email, return the `Email` variant.
    /// * Otherwise return an error.
    fn from_str(s: &str) -> Result<Self> {
        if let Ok(fingerprint) = sequoia_openpgp::Fingerprint::from_hex(s) {
            Ok(Self::Fingerprint(Cow::Owned(fingerprint)))
        } else if is_valid_email(s) {
            Ok(Self::Email(Cow::Owned(s.into())))
        } else {
            Err(anyhow!(
                "Invalid query term: '{s}' is not a valid fingerprint or email."
            ))
        }
    }
}

#[derive(Debug, Clone)]
/// Options required to instantiate a new certificate store.
pub struct CertStoreOpts<'a> {
    /// Path of public certificate sub-store. If None, the
    /// default location is used instead (`<data dir>/pgp.cert.d`).
    pub pub_store_path: Option<&'a Path>,
    /// Path of secret certificate sub-store. If None, the
    /// default location is used instead (`<data dir>/pgp.cert.d.sec`).
    pub sec_store_path: Option<&'a Path>,
    /// Open the store in read-only mode.
    pub read_only: bool,
    /// If `true`, a new store is created if none is found
    /// at the specified or default path. If `false`, an error is raised if
    /// the specified or default path does not exits.
    pub allow_create: bool,
    /// If `true`, an OpenPGP keystore will be added as a backend for the
    /// certificate store.
    pub use_keyserver: bool,
    /// URL of the OpenPGP keyserver to add as a backend for the certificate
    /// store. Default is keys.openpgp.org.
    pub keyserver_url: Option<&'a str>,
}

impl Default for CertStoreOpts<'_> {
    fn default() -> Self {
        Self {
            pub_store_path: None,
            sec_store_path: None,
            read_only: false,
            allow_create: true,
            use_keyserver: false,
            keyserver_url: None,
        }
    }
}

impl CertStoreOpts<'_> {
    /// Verify that the specified `path` exists or create it if allowed.
    ///
    /// By default try retrieving from the `env_variable` environment variable,
    /// or use the path `<data dir>/<sub_dir_name>`.
    fn try_get_store_path_or_default(&self, cert_store_type: CertType) -> Result<PathBuf> {
        let (initial_path, env_variable, default_dir) = match cert_store_type {
            CertType::Public => (
                self.pub_store_path,
                PUB_STORE_ENV_VARIABLE,
                PUB_STORE_DEFAULT_DIR,
            ),
            CertType::Secret => (
                self.sec_store_path,
                SEC_STORE_ENV_VARIABLE,
                SEC_STORE_DEFAULT_DIR,
            ),
        };
        let path = if let Some(p) = initial_path {
            p.into()
        } else {
            path_from_env_or_data_dir(env_variable, default_dir)?
        };
        if !path.is_dir() {
            if self.allow_create {
                fs::create_dir_all(&path)?;
            } else {
                bail!(
                    "No keystore found at '{}'. This can be because \
                    i) you have no keystore yet, or \
                    ii) your keystore is located elsewhere and your \
                    configuration setting is incorrect.",
                    path.to_string_lossy()
                )
            }
        }
        Ok(path)
    }
}

/// Return the path given by the specified environment variable or use the
/// specified directory as a subdirectory of the system's data dir.
/// * If the environmental variable "env_variable" is present, the path is
///   taken from it.
/// * Otherwise, the returned path is `<data dir>/<sub_dir_name>`, e.g.
///   `~/.local/share/<sub_dir_name>` on a Linux system.
fn path_from_env_or_data_dir(env_variable: &str, sub_dir_name: &str) -> Result<PathBuf> {
    std::env::var_os(env_variable).map_or_else(
        // Case 1: The default data directory of the operating system is used.
        || {
            dirs::data_dir()
                .map(|p| p.join(sub_dir_name))
                .context("Unsupported platform: 'data dir' is not defined.")
        },
        // Case 2: an environmental variable containing the path to use as
        //         store location is set
        |p| Ok(p.into()),
    )
}

/// Store for OpenPGP certificates with two sub-stores for public and secret
/// certificates respectively.
pub struct CertStore<'a> {
    /// Sub-store for certificates that only contain public material.
    pub_store: SequoiaCertStore<'a>,
    /// Sub-store for certificates that contain some secret material.
    sec_store: CertD,

    // TODO: these properties can be replaced by methods that return the path
    // of the CertD certificate store after the next release of pgp-cert-d.
    pub_store_path: PathBuf,
    sec_store_path: PathBuf,
}

impl<'a> CertStore<'a> {
    /// Create or open a new certificate store on disk, at either the
    /// specified path locations, or at the default locations.
    pub fn open(opts: CertStoreOpts) -> Result<Self> {
        // Check (or create) the paths for the public and secret stores.
        let sec_store_path = opts.try_get_store_path_or_default(CertType::Secret)?;
        let pub_store_path = opts.try_get_store_path_or_default(CertType::Public)?;

        let mut pub_store = if opts.read_only {
            SequoiaCertStore::open_readonly(&pub_store_path)?
        } else {
            SequoiaCertStore::open(&pub_store_path)?
        };

        if opts.use_keyserver {
            pub_store.add_keyserver(opts.keyserver_url.unwrap_or(KEYS_OPENPGP_ORG_URL))?;
        }

        // Return a new certificate store.
        Ok(Self {
            pub_store,
            sec_store: CertD::with_base_dir(&sec_store_path)?,
            pub_store_path,
            sec_store_path,
        })
    }

    /// Return the path of the public or secret store of the `CertStore`.
    /// * If `CertType` is `Public` the public store path is returned.
    /// * If `CertType` is `Secret` the secret store path is returned.
    pub fn get_store_path(&self, cert_type: CertType) -> &PathBuf {
        if let CertType::Public = cert_type {
            &self.pub_store_path
        } else {
            &self.sec_store_path
        }
    }

    /// Return the path of an OpenPGP certificate file location in the
    /// `CertStore`.
    /// * The certificate is specified via its `fingerprint`.
    /// * The type of the certificate `Public` or `Secret` is specified
    ///   via the `cert_type` argument.
    /// * If the certificate is not present in the store, an error is returned.
    ///
    /// Certificates are stored as files under the path:
    ///  * `/path/of/store/<first 2 chars of fingerprint>/<remainder of fingerprint>`.
    ///  * Example: `~/.local/share/pgp.cert.d/1e/a0292ecbf2457cadae20e2b94fa6a56d9fa1fb`.
    pub fn get_cert_path(
        &self,
        fingerprint: &sequoia_openpgp::Fingerprint,
        cert_type: CertType,
    ) -> Result<PathBuf> {
        // Make sure the certificate is present in the store.
        if let Err(err_msg) = self.get_cert_by_fingerprint(fingerprint, cert_type) {
            bail!("Cannot get key path: {err_msg}")
        }

        // Return path of certificate.
        let fingerprint_as_string = fingerprint.to_hex().to_ascii_lowercase();
        Ok(self
            .get_store_path(cert_type)
            .join(&fingerprint_as_string[..2])
            .join(&fingerprint_as_string[2..]))
    }

    /// Add a public certificate (i.e. with no secret material) to the public
    /// store. If a certificate with secret material is passed, the secret
    /// material is stripped away and only the public part of the certificate
    /// is stored.
    /// If a certificate with the same fingerprint is already present, it gets
    /// updated with the new info (if any) present in the certificate being
    /// added.
    fn add_pub_cert(&mut self, cert: &Cert) -> Result<Cert> {
        let imported_cert = self
            .pub_store
            .update_by(
                Arc::new(LazyCert::from_cert(
                    cert.0.clone().strip_secret_key_material(),
                )),
                &(),
            )?
            .to_cert()?
            .clone();

        // Make sure any secret material has been stripped from the certificate.
        assert!(!imported_cert.is_tsk());
        Ok(imported_cert.into())
    }

    /// Add a secret certificate to the secret store. If a certificate with the
    /// same fingerprint is already present, it gets updated with the new info
    /// (if any) present in the certificate being added.
    fn add_sec_cert(&self, cert: &Cert) -> Result<Cert> {
        let mut updated_cert_result = Err(anyhow::anyhow!("merge callback not invoked"));
        // Note: `certd.insert()` does not work with secret keys, because it
        // somehow cannot extract a fingerprint from it (needed to name the
        // file under which the certificate is saved in the store). The
        // workaround is to use `.insert_special()` to which we pass the
        // fingerprint of the certificate as `name` argument.
        let (_, _updated_bytes) = self.sec_store.insert_special(
            &cert.fingerprint().to_hex(),
            cert.0.as_tsk().to_vec()?,
            true,
            // Merge two versions of a same certificate together. Any data
            // contained in `new` but not present in `old` (e.g. a new
            // signature or subkey) is added to the certificate.
            |new, old| {
                let new_cert = sequoia_openpgp::cert::Cert::from_bytes(&new);
                let updated_cert = if let Some(old_bytes) = old {
                    sequoia_openpgp::cert::Cert::from_bytes(old_bytes)?
                        .merge_public_and_secret(new_cert?)
                } else {
                    new_cert
                }?;
                let updated_bytes = updated_cert.as_tsk().to_vec()?;
                updated_cert_result = Ok(updated_cert);
                Ok(MergeResult::Data(updated_bytes))
            },
        )?;
        Ok(updated_cert_result?.into())
    }

    /// Add an OpenPGP certificate `cert` to the `CertStore`.
    /// * If `cert_type` is `Public`, only the public material of the
    ///   certificate - which might be the entire certificate - is added to
    ///   the store.
    /// * If `cert_type` is `Secret`:
    ///   * If the certificate contains secret material, the certificate
    ///     (secret and public material) is added to the store.
    ///   * If no secret material is present, an error is returned.
    /// * If a certificate with the same fingerprint is already present, it
    ///   gets updated with the new material (if any) present in the
    ///   certificate being added.
    ///
    /// # Arguments
    ///
    /// * `cert`: the certificate to add to the CertStore.
    /// * `cert_type`: the type of certificate (`Public` or `Secret`) to be
    ///   added.
    pub fn add_cert(&mut self, cert: &Cert, cert_type: CertType) -> Result<Cert> {
        // Verify that the certificate has at least one valid self-signature.
        cert.validate()?;

        match cert_type {
            CertType::Public => self.add_pub_cert(cert),
            CertType::Secret => {
                // Verify the certificate contains secret material.
                if !cert.0.is_tsk() {
                    bail!(
                        "The OpenPGP key '{}' does not contain any private \
                        material to import.",
                        userid_as_string(cert)
                    )
                }
                // Add public and secret material of the certificate to store.
                self.add_pub_cert(cert)?;
                self.add_sec_cert(cert)
            }
        }
    }

    /// Retrieve an OpenPGP certificate from the `CertStore` based on its
    /// `fingerprint`.
    /// If no certificates matching the specified `fingerprint` is found,
    /// an error is returned.
    ///
    /// # Arguments
    ///
    /// * `fingerprint`: `Fingerprint` of the certificate (or any of its keys)
    ///    to retrieve.
    /// * `cert_type`: type of certificate to retrieve, `Public` or `Secret`.
    fn get_cert_by_fingerprint(
        &self,
        fingerprint: &sequoia_openpgp::Fingerprint,
        cert_type: CertType,
    ) -> Result<Cert> {
        macro_rules! return_not_found_error {
            () => {
                bail!(
                    "No {cert_type} key matching the specified fingerprint \
                    '{fingerprint}' was found."
                )
            };
        }

        if let CertType::Public = cert_type {
            if let Ok(certs) = self.pub_store.lookup_by_cert_or_subkey(&fingerprint.into()) {
                let cert: Cert = match certs.len() {
                    0 => return_not_found_error!(),
                    1 => certs[0].to_cert()?.clone().into(),
                    _ => bail!(
                        "Multiple keys with the same fingerprint \
                        '{fingerprint}' were found."
                    ),
                };
                Ok(cert)
            } else {
                return_not_found_error!()
            }
        } else if let Some((_, cert_data)) = self.sec_store.get(&fingerprint.to_hex())? {
            Ok(Cert::from_bytes(cert_data)?)
        } else {
            return_not_found_error!()
        }
    }

    /// Retrieve an OpenPGP certificate from the `CertStore` based on its
    /// `email`. If no certificates matching the specified `email` is found,
    /// an error is returned.
    ///
    /// # Arguments
    ///
    /// * `email`: email of the certificate to retrieve.
    /// * `cert_type`: type of certificate to retrieve, `Public` or `Secret`.
    fn get_cert_by_email(&self, email: &str, cert_type: CertType) -> Result<Vec<Cert>> {
        if let CertType::Public = cert_type {
            self.pub_store
                .lookup_by_email(email)
                .context(format!(
                    "No public key matching the email '{email}' was found"
                ))? // returns an error if no matching certificate is found.
                .iter()
                .map(|lazy_cert| -> Result<Cert> { Ok(lazy_cert.to_cert()?.clone().into()) })
                .collect()
        } else {
            // Loop through all certificates present in the CertStore, and
            // retrieve those that match the requested email.
            let matching_certs: Vec<Cert> = self
                .get_all_certs(cert_type)?
                .into_iter()
                .filter(|cert| {
                    get_primary_email(cert)
                        .unwrap_or(None)
                        .unwrap_or_default()
                        .to_lowercase()
                        == email.to_lowercase()
                })
                .collect();

            if matching_certs.is_empty() {
                bail!("No private key matching the email '{email}' was found")
            }
            Ok(matching_certs)
        }
    }

    /// Retrieve OpenPGP certificates from the `CertStore` by searching for a
    /// `query_term` that can be either a fingerprint or an email address.
    /// * The function returns a `Vec` of certificates, because there can be
    ///   multiple certificates with the same email in the store.
    /// * If no certificates matching the specified `query_term` is found,
    ///   an error is returned.
    ///
    /// # Arguments
    ///
    /// * `query_term`: fingerprint or an email address of the certificate to
    ///   retrieve. Must be passed as a `QueryTerm` variant.
    /// * `cert_type`: type of certificate to retrieve, `Public` or `Secret`.
    pub fn get_cert(&self, query_term: &QueryTerm, cert_type: CertType) -> Result<Vec<Cert>> {
        match query_term {
            QueryTerm::Fingerprint(f) => Ok(vec![self.get_cert_by_fingerprint(f, cert_type)?]),
            QueryTerm::Email(email) => self.get_cert_by_email(email, cert_type),
        }
    }

    /// Retrieve all public or secret certificates from the `CertStore`.
    pub fn get_all_certs(&self, cert_type: CertType) -> Result<Vec<Cert>> {
        if let CertType::Public = cert_type {
            self.pub_store
                .certs()
                .map(|lazy_cert| Ok(lazy_cert.to_cert()?.clone().into()))
                .collect()
        } else {
            self.sec_store
                .iter()
                .map(|c| {
                    c.map_or_else(
                        |e| Err(anyhow!(e)),
                        |(_, _, cert_data)| Cert::from_bytes(cert_data),
                    )
                })
                .collect()
        }
    }
}

/// Retrieve the email of the primary userID from the certificate `cert`.
pub fn get_primary_email(cert: &Cert) -> Result<Option<String>> {
    cert.0
        .with_policy(&StandardPolicy::new(), None)?
        .primary_userid()?
        .email2()
        .map(|s| s.map(String::from))
}

/// Format the primary user ID of an OpenPGP certificate as a string of
/// type: `<user ID> <email> [fingerprint]`
pub fn userid_as_string(cert: &Cert) -> String {
    let user_id = &cert
        .0
        .with_policy(&StandardPolicy::new(), None)
        .map_or_else(
            |_| String::from("Invalid key: no valid self-signature was found"),
            |valid_cert| {
                valid_cert.primary_userid().map_or_else(
                    |_| String::from("Missing or no valid user ID"),
                    |user_id| user_id.userid().to_string(),
                )
            },
        );
    format!("{} [{}]", user_id, cert.fingerprint().to_hex())
}

/// Test if the specified string `s` is a valid email.
fn is_valid_email(s: &str) -> bool {
    // Note: the "local-part" of email addresses allows a lot of special
    // characters and edge-cases. As such, it's better to keep the check
    // minimal.
    let email_regex = Regex::new(r"^[^.-].*[^.]@([[:alnum:]]+([\-\.]{1}[[:alnum:]]+)*\.[a-zA-Z]+)")
        .expect("regexp pattern should never fail because it's hard-coded");
    email_regex.is_match(s)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;
    use tempfile::tempdir;

    fn generate_tmp_certstore<'a>(allow_create: bool) -> Result<CertStore<'a>> {
        let tmp_dir = tempdir().unwrap().into_path();
        CertStore::open(CertStoreOpts {
            pub_store_path: Some(&tmp_dir.join(PUB_STORE_DEFAULT_DIR)),
            sec_store_path: Some(&tmp_dir.join(SEC_STORE_DEFAULT_DIR)),
            allow_create,
            ..Default::default()
        })
    }

    #[test]
    fn test_path_from_env_or_data_dir() {
        // If the specified environmental variable is not present, the
        // returned directory should be a subdirectory of `<data dir>`.
        let default_dir = "test_dir";
        assert_eq!(
            path_from_env_or_data_dir("DOES_NOT_EXIST", default_dir).unwrap(),
            dirs::data_dir().unwrap().join(default_dir)
        );

        // If the specified environmental variable exists, the returned
        // directory should be the one given in the variable.
        std::env::set_var("DOES_EXIST", dirs::home_dir().unwrap().as_os_str());
        assert_eq!(
            path_from_env_or_data_dir("DOES_EXIST", default_dir).unwrap(),
            dirs::home_dir().unwrap()
        );
    }

    // Test that creating a new certstore is working.
    #[test]
    fn test_open_certstore() {
        let store = generate_tmp_certstore(true).unwrap();
        assert!(store.pub_store_path.is_dir());
        assert!(store.sec_store_path.is_dir());
    }

    // Opening a non-existing CertStore with `allow_create=false` should fail.
    #[test]
    fn test_open_certstore_without_existing_path() {
        assert!(generate_tmp_certstore(false).is_err());
    }

    // Test adding certificates to the store.
    #[test]
    fn test_add_cert() -> Result<()> {
        let mut store = generate_tmp_certstore(true)?;

        macro_rules! assert_add_to_store {
            ($file_name:expr, $cert_type:expr) => {
                // Add cert to store.
                let cert = store.add_cert(
                    &Cert::from_bytes(include_bytes!(concat!("../tests/data/", $file_name)))?,
                    $cert_type,
                )?;
                // Make sure secret certificates added as public certs are
                // stripped from their secret material.
                assert!(
                    cert.0.is_tsk()
                        == match $cert_type {
                            CertType::Secret => true,
                            CertType::Public => false,
                        }
                );

                // Make sure the certificate file is stored at the expected
                // place.
                assert!(store
                    .get_cert_path(&cert.fingerprint(), $cert_type)?
                    .is_file());

                // Make sure cert can be retrieved from the store.
                let fingerprint = &$file_name[..40];
                assert_eq!(
                    store
                        .get_cert_by_fingerprint(
                            &sequoia_openpgp::Fingerprint::from_hex(fingerprint)?,
                            $cert_type
                        )?
                        .fingerprint()
                        .to_string(),
                    fingerprint,
                );
            };
        }

        // Adding a certificate with only public material as a public cert
        // should pass.
        assert_add_to_store!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub",
            CertType::Public
        );
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 1);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 0);

        // Adding a certificate with only public material as a secret cert
        // should fail.
        assert!(store
            .add_cert(
                &Cert::from_bytes(include_bytes!(
                    "../tests/data/C0621CB3669020CC31050A361956EC38A96CA852.pub"
                ))?,
                CertType::Secret,
            )
            .is_err());
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 1);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 0);

        // Adding a certificate with secret material as a public cert should
        // pass.
        assert_add_to_store!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec",
            CertType::Public
        );
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 2);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 0);

        // Adding a certificate with secret material as a secret cert should
        // pass.
        assert_add_to_store!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec",
            CertType::Secret
        );
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 2);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 1);
        // Adding a secret cert should add the certificate to both the public
        // and the secret stores.
        assert_add_to_store!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec",
            CertType::Secret
        );
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 2);
        Ok(())
    }

    // Test that a secret certificate can be revoked. This also tests that
    // adding content to a secret/public certificate (in this case the
    // revocation signature) is done properly.
    #[test]
    fn test_revoke_cert() -> Result<()> {
        let mut store = generate_tmp_certstore(true)?;

        macro_rules! assert_revoke {
            ($fingerprint:expr, $cert_type:expr, $extension:expr, $revocation_message:expr) => {
                // Add the certificate to the store.
                store.add_cert(
                    &Cert::from_bytes(include_bytes!(concat!(
                        "../tests/data/",
                        $fingerprint,
                        $extension
                    )))?,
                    $cert_type,
                )?;
                // Make sure the certificate is originally not revoked.
                let cert = store.get_cert_by_fingerprint(
                    &sequoia_openpgp::Fingerprint::from_hex($fingerprint)?,
                    $cert_type,
                )?;
                assert!(matches!(
                    cert.revocation_status(),
                    RevocationStatus::NotAsFarAsWeKnow
                ));

                // Revoke the certificate and add the revoked version of the
                // certificate to the store.
                store.add_cert(
                    &cert.revoke(std::io::Cursor::new(&include_bytes!(concat!(
                        "../tests/data/",
                        $fingerprint,
                        ".rev"
                    ))))?,
                    $cert_type,
                )?;
                // Make sure the certificate in the store is now revoked.
                let revoked_cert = store.get_cert_by_fingerprint(
                    &sequoia_openpgp::Fingerprint::from_hex($fingerprint)?,
                    $cert_type,
                )?;
                assert!(matches!(
                    revoked_cert.revocation_status(),
                    RevocationStatus::Revoked(_)
                ));
                if let RevocationStatus::Revoked(sig) = revoked_cert.revocation_status() {
                    assert_eq!(sig.len(), 1);
                    let (reason, message) = sig[0]
                        .reason_for_revocation()
                        .expect("This signature has a revocation reason.");
                    assert_eq!(reason, ReasonForRevocation::KeyCompromised);
                    assert_eq!(message, $revocation_message.as_bytes())
                }
            };
        }

        // Test with public certificates.
        assert_revoke!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB",
            CertType::Public,
            ".pub",
            "CN does not compromise"
        );
        assert_revoke!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF",
            CertType::Public,
            ".pub",
            "CN does not remember passwords, passwords remember him!"
        );
        // Test with secret certificates.
        assert_revoke!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB",
            CertType::Secret,
            ".sec",
            "CN does not compromise"
        );
        assert_revoke!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF",
            CertType::Secret,
            ".sec",
            "CN does not remember passwords, passwords remember him!"
        );

        Ok(())
    }

    // Test that certificates can be retrieved from the CertStore.
    #[test]
    fn test_retrieve_cert() -> Result<()> {
        let mut store = generate_tmp_certstore(true)?;

        // cert_1 => only public material. cert_2 => contains secret material.
        let cert_1 = store.add_cert(
            &Cert::from_bytes(include_bytes!(
                "../tests/data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
            ))?,
            CertType::Public,
        )?;
        let cert_2 = store.add_cert(
            &Cert::from_bytes(include_bytes!(
                "../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec"
            ))?,
            CertType::Secret,
        )?;

        // Retrieving all certificates should yield the correct number of certs.
        assert_eq!(store.get_all_certs(CertType::Public)?.len(), 2);
        assert_eq!(store.get_all_certs(CertType::Secret)?.len(), 1);

        // Retrieving certificates by their fingerprint should work.
        assert_eq!(
            store.get_cert_by_fingerprint(&cert_1.fingerprint(), CertType::Public)?,
            cert_1
        );
        assert_eq!(
            store.get_cert_by_fingerprint(&cert_2.fingerprint(), CertType::Secret)?,
            cert_2
        );
        let cert1_vec = vec![cert_1];
        let cert2_vec = vec![cert_2];

        // Retrieving certificates by their email should work.
        assert_eq!(
            &store.get_cert_by_email("chuck.norris@roundhouse.org", CertType::Public)?,
            &cert1_vec
        );
        assert_eq!(
            &store.get_cert_by_email("chuck.norris@roundhouse.swiss", CertType::Public)?,
            &cert2_vec
        );
        assert_eq!(
            &store.get_cert_by_email("chuck.norris@roundhouse.swiss", CertType::Secret)?,
            &cert2_vec
        );

        // Retrieving certificates by QueryTerm (fingerprint or email) should
        // work.
        assert_eq!(
            &store.get_cert(
                &QueryTerm::Email("chuck.norris@roundhouse.org".into()),
                CertType::Public
            )?,
            &cert1_vec
        );
        let fingerprint =
            sequoia_openpgp::Fingerprint::from_hex("B2E961753ECE0B345E718E74BA6F29C998DDD9BF")?;
        assert_eq!(
            &store.get_cert(
                &QueryTerm::Fingerprint(Cow::Owned(fingerprint)),
                CertType::Secret
            )?,
            &cert2_vec
        );

        // Trying to retrieve a non-existing certificate by its fingerprint or
        // email should fail.
        assert!(store
            .get_cert_by_email("chuck.norris@missing.com", CertType::Public)
            .is_err());
        Ok(())
    }

    #[test]
    fn test_queryterm_from_str() {
        // Passing non-valid fingerprints or emails should fail.
        assert!(QueryTerm::from_str("not_a_fingerprint_nor_an_email").is_err());
    }

    #[test]
    fn test_serialize_cert() -> Result<()> {
        // Import certificates from test data.
        let cert = Cert::from_bytes(include_bytes!(
            "../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub"
        ))?;
        let cert_with_secret = Cert::from_bytes(include_bytes!(
            "../tests/data/B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec"
        ))?;

        for format in [
            SerializationFormat::Binary,
            SerializationFormat::AsciiArmored,
        ] {
            // Serializing a public certificate should work.
            let serialized_cert = cert.serialize(CertType::Public, format)?;
            let cert_from_serialized_data = Cert::from_bytes(&serialized_cert)?;
            assert_eq!(cert, cert_from_serialized_data);

            // Serializing a secret certificate should work.
            let serialized_cert = cert_with_secret.serialize(CertType::Secret, format)?;
            let cert_from_serialized_data = Cert::from_bytes(&serialized_cert)?;
            assert_eq!(cert_with_secret, cert_from_serialized_data);

            // Make sure that serializing as a public certificates strips-away any
            // secret material.
            let serialized_cert = cert_with_secret.serialize(CertType::Public, format)?;
            let cert_from_serialized_data = Cert::from_bytes(&serialized_cert)?;
            assert_eq!(cert, cert_from_serialized_data);

            // Attempting to serialize a certificate with no secret material as
            // `CertType::Secret` should fail.
            assert!(cert.serialize(CertType::Secret, format).is_err());
        }
        Ok(())
    }

    #[test]
    fn test_is_valid_email() -> Result<()> {
        // Test valid values.
        assert!(is_valid_email("chuck.norris@roundhouse.org"));
        assert!(is_valid_email("chuck@roundhouse.org"));
        assert!(is_valid_email("chuck_norris+@round-house.org.com"));
        assert!(is_valid_email("chûéèà_k.ñörrís+@example.org"));
        assert!(is_valid_email("CHUCK.NORRIS@EXAMPLE.ORG"));

        // Test invalid values.
        assert!(!is_valid_email(".chuck.norris@roundhouse.org"));
        assert!(!is_valid_email("-chuck.norris@roundhouse.org"));
        assert!(!is_valid_email("chuck.@roundhouse.org"));
        assert!(!is_valid_email("chuck@roundhouse."));
        assert!(!is_valid_email("chuck"));
        assert!(!is_valid_email("chuck.norris"));
        assert!(!is_valid_email("chuck@"));
        assert!(!is_valid_email("chuck@.org"));
        assert!(!is_valid_email("chuck.@roundhouse.oo7"));
        Ok(())
    }

    #[test]
    fn test_cert_type_to_string() -> Result<()> {
        // Verify that variants of CertType are correctly converted to
        // string.
        assert_eq!(format!("{}", CertType::Public), "public");
        assert_eq!(format!("{}", CertType::Secret), "private");
        Ok(())
    }
}
