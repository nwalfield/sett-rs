# Changelog

All notable changes to this project will be documented in this file.

### [0.2.2](https://gitlab.com/biomedit/sett-rs/compare/0.2.1...0.2.2) (2022-11-23)

### Features

- add python submodule to provide improved python bindings ([f1d3223](https://gitlab.com/biomedit/sett-rs/commit/f1d3223c08b93b1d0b1b25097ff12caddb0c34c1)), closes [#23](https://gitlab.com/biomedit/sett-rs/issues/23)
- **encrypt:** allow encrypting directly to a remote destination ([d88a214](https://gitlab.com/biomedit/sett-rs/commit/d88a214b0789d3418d2c0f004b5fe0912cccdfc4)), closes [#22](https://gitlab.com/biomedit/sett-rs/issues/22)
- **sett-rs:** add OpenPGP cert/key reader ([3bf27dd](https://gitlab.com/biomedit/sett-rs/commit/3bf27dd02776321b64527875be67f3ed3ee16988)), closes [#21](https://gitlab.com/biomedit/sett-rs/issues/21)

### Bug Fixes

- **encrypt:** use the first/primary key for signing if multiple are provided ([c2c7300](https://gitlab.com/biomedit/sett-rs/commit/c2c73000426fe26430369f16255b35f61a176e59)), closes [#24](https://gitlab.com/biomedit/sett-rs/issues/24)
- **filesystem:** available space computation should work on each OS ([287bcfe](https://gitlab.com/biomedit/sett-rs/commit/287bcfe3267eaf770c858596039db47c3343d67f))

### [0.2.1](https://gitlab.com/biomedit/sett-rs/compare/0.2.0...0.2.1) (2022-09-30)

### Features

- add decrypt workflow ([01dc4a8](https://gitlab.com/biomedit/sett-rs/commit/01dc4a805fd52f14b2d937fd9ce90b972f9c286a)), closes [#14](https://gitlab.com/biomedit/sett-rs/issues/14)
- add encryption workflow ([320be33](https://gitlab.com/biomedit/sett-rs/commit/320be33ad56485adbfc6d57685de3be05e4b8e0c))
- **sett/encrypt:** use in-memory encryption for passwords ([643ad39](https://gitlab.com/biomedit/sett-rs/commit/643ad3939b493f2c947dbd19d2e0ddb8d0be9b69))

### Bug Fixes

- rename functions to remove clippy warnings ([dacb8e6](https://gitlab.com/biomedit/sett-rs/commit/dacb8e6268d54fe0bcafc0fb9775b1fe902d08d3))

## [0.2.0](https://gitlab.com/biomedit/sett-rs/compare/0.1.3...0.2.0) (2021-12-09)

### ⚠ BREAKING CHANGES

- **lib:** sftp_upload now requires progress argument to be callable

### Features

- Add library stub ([7560ab4](https://gitlab.com/biomedit/sett-rs/commit/7560ab4add87db737089731b418f722a46fdb811))
- **sftp:** Support for 2 factor authentication ([d83b160](https://gitlab.com/biomedit/sett-rs/commit/d83b16060811b179d9d0f4c1d524ee3567c23f98))

- **lib:** Change progress callback from python object to python callable ([a2a8e38](https://gitlab.com/biomedit/sett-rs/commit/a2a8e3863d83a209004f8187e2aeb87998382999))

### [0.1.3](https://gitlab.com/biomedit/sett-rs/compare/0.1.2...0.1.3) (2021-10-13)

### [0.1.2](https://gitlab.com/biomedit/sett-rs/compare/0.1.1...0.1.2) (2021-08-11)

### Features

- **sftp:** make upload buffer size configurable ([07e594b](https://gitlab.com/biomedit/sett-rs/commit/07e594b95452d0097f4ce780bd5d83577c230b7b))

### [0.1.1](https://gitlab.com/biomedit/sett-rs/compare/0.1.0...0.1.1) (2021-08-04)

### Features

- **sftp:** log uploaded files size ([2cd9d11](https://gitlab.com/biomedit/sett-rs/commit/2cd9d11b9167a03646b8c2097f5ffe5cbd873141))

### Bug Fixes

- remove needless_borrow reported by clippy ([3236462](https://gitlab.com/biomedit/sett-rs/commit/32364628a498a928f06ff6513d92882ad8772e82))
- replace deprecated macro text_signature with new pyo3(text_signature) ([fabefb0](https://gitlab.com/biomedit/sett-rs/commit/fabefb03cacb423c11dc7cdb0ec4424031266bf4))

## 0.1.0 (2021-08-03)

### Bug Fixes

- **sftp:** add upload progress ([d2576e4](https://gitlab.com/biomedit/sett-rs/commit/d2576e412daffb4ea76e1be0bde6142dcba79ea6))
