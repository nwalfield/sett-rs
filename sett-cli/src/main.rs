use anyhow::Result;
use clap::Parser;

use crate::cmd::Command;

mod cmd;
mod log;
mod util;

/// Declaration of the main commands of the sett CLI.
/// The names of the variants of the enum become the top-level commands of
/// the CLI.
#[derive(Parser)]
#[command(about, version, propagate_version = true)]
enum Base {
    /// Encrypt data package
    #[command(subcommand)]
    Encrypt(cmd::encrypt::Encrypt),
    /// Decrypt data package
    Decrypt(cmd::decrypt::Decrypt),
    /// Transfer data package
    #[command(subcommand)]
    Transfer(cmd::transfer::Transfer),
    /// OpenPGP key management.
    #[command(subcommand)]
    Keys(cmd::certstore::Keys),
    /// Interaction with Portal.
    #[cfg(feature = "auth")]
    #[command(subcommand)]
    Portal(cmd::portal::Portal),
}

impl Command for Base {
    fn run(&self) -> Result<()> {
        match self {
            Self::Encrypt(c) => c.run(),
            Self::Decrypt(c) => c.run(),
            Self::Transfer(c) => c.run(),
            Self::Keys(c) => c.run(),
            #[cfg(feature = "auth")]
            Self::Portal(c) => c.run(),
        }
    }
}

fn main() -> Result<()> {
    Base::parse().run()
}
