use anyhow::{Context, Result};
use indicatif::{ProgressBar, ProgressDrawTarget, ProgressStyle};
use sett::utils::Progress;

#[derive(Clone)]
pub(crate) struct CliProgress {
    pb: ProgressBar,
}

impl CliProgress {
    pub(crate) fn new() -> Self {
        let style = ProgressStyle::with_template("{spinner} [{elapsed_precise}] {wide_bar} {bytes}/{total_bytes} ({bytes_per_sec}) ETA: {eta}")
            .expect("Failed to set progress bar style");
        let pb = ProgressBar::with_draw_target(None, ProgressDrawTarget::stderr_with_hz(2))
            .with_style(style);
        Self { pb }
    }
}

impl Progress for CliProgress {
    fn set_length(&self, len: u64) {
        self.pb.set_length(len)
    }
    fn inc(&self, delta: u64) {
        self.pb.inc(delta)
    }
    fn finish(&self) {
        self.pb.finish();
        eprintln!();
    }
}

pub(crate) fn two_factor_prompt() -> Result<String> {
    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .context("Failed to read input")?;
    Ok(input.replace('\n', ""))
}
