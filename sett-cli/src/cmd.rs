use anyhow::Result;

pub(super) mod certstore;
mod common;
pub(super) mod decrypt;
pub(super) mod encrypt;
#[cfg(feature = "auth")]
pub(super) mod portal;
pub(super) mod transfer;

pub(crate) trait Command {
    fn run(&self) -> Result<()>;
}
