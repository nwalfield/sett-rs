use std::io::{self, Write};

use anyhow::{Context, Result};
use clap::{Args, Subcommand};
use sett::portal::response::DataTransferStatus;

#[derive(Subcommand)]
pub(crate) enum Portal {
    /// List the data transfer requests present in the Portal.
    Dtr(Dtr),
}

#[derive(Args)]
struct PortalArgs {
    /// Log verbosity level.
    #[arg(short, action=clap::ArgAction::Count)]
    pub(super) verbosity: u8,
}

#[derive(Args)]
pub(crate) struct Dtr {
    #[command(flatten)]
    portal: PortalArgs,
}

impl crate::cmd::Command for Portal {
    fn run(&self) -> Result<()> {
        match self {
            Self::Dtr(c) => c.run(),
        }
    }
}

impl crate::cmd::Command for Dtr {
    fn run(&self) -> Result<()> {
        let _log_guard = crate::log::init_log(self.portal.verbosity)?;
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .context("Failed to create tokio runtime")?;
        let portal_client = super::common::get_portal_client()?;
        let oidc_client = super::common::get_oidc_client()?;
        let token =
            runtime.block_on(oidc_client.authenticate(Box::new(super::common::auth_handler)))?;
        let mut data_transfers =
            runtime.block_on(portal_client.get_data_transfers(&token.access_token))?;
        data_transfers.sort_by(|a, b| a.id.cmp(&b.id).reverse());
        let mut std_lock = io::stdout();
        let data_provider_max_length = data_transfers
            .iter()
            .map(|dtr| dtr.data_provider_name.len())
            .max()
            .unwrap_or(0);
        for (status, emoji) in [
            (DataTransferStatus::Authorized, "✅ "),
            (DataTransferStatus::Initial, "⏱️ "),
            (DataTransferStatus::Unauthorized, "❌ "),
            (DataTransferStatus::Expired, "🗑️  "),
        ] {
            let mut data_transfers_by_status = data_transfers
                .iter()
                .filter(|dtr| dtr.status == status)
                .peekable();
            if data_transfers_by_status.peek().is_some() {
                writeln!(&mut std_lock, "{emoji}{status:?} Data Transfers")?;
                for dtr in data_transfers_by_status {
                    writeln!(
                        &mut std_lock,
                        "DTR {:>3} from {:<data_provider_max_length$} to {}",
                        dtr.id, dtr.data_provider_name, dtr.project_name
                    )?;
                }
                writeln!(&mut std_lock)?;
            }
        }
        Ok(())
    }
}
