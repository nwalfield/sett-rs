use std::path::{Path, PathBuf};

use anyhow::{ensure, Context, Result};
use clap::{Args, Subcommand};
use sett::{
    certstore::CertStore,
    dpkg::{Package, DATETIME_FORMAT},
    task::Mode,
};

#[cfg(feature = "auth")]
use super::common::get_oidc_client;
use super::common::get_portal_client;

#[derive(Subcommand)]
pub(crate) enum Transfer {
    Sftp(Sftp),
    S3(S3),
    #[cfg(feature = "auth")]
    S3Portal(S3Portal),
}

/// Transfer an encrypted package to an SFTP server
#[derive(Args)]
pub(crate) struct Sftp {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Perform an additional data package verification using Portal.
    #[arg(long)]
    verify: bool,

    #[command(flatten)]
    sftp: super::common::SftpArgs,

    file: PathBuf,
}

/// Transfer an encrypted package to an S3 server
///
/// S3 authentication credentials can be provider by the following means
/// (in order of precedence): cli arguments, environmental variables, credentials
/// file. For more information about the last two options refer to the official
/// AWS documentation
/// <https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html>.
#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Perform an additional data package verification using Portal.
    #[arg(long)]
    verify: bool,

    #[command(flatten)]
    s3: super::common::S3Args,

    file: PathBuf,
}

#[cfg(feature = "auth")]
/// Transfer an encrypted package to an S3 server with credentials fetched from Portal.
#[derive(Args)]
pub(crate) struct S3Portal {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    file: PathBuf,
}

impl crate::cmd::Command for Transfer {
    fn run(&self) -> Result<()> {
        match self {
            Self::Sftp(c) => c.run(),
            Self::S3(c) => {
                let runtime = tokio::runtime::Builder::new_multi_thread()
                    .enable_all()
                    .build()
                    .context("Failed to create tokio runtime")?;
                runtime.block_on(c.run())
            }
            #[cfg(feature = "auth")]
            Self::S3Portal(c) => {
                let runtime = tokio::runtime::Builder::new_multi_thread()
                    .enable_all()
                    .build()
                    .context("Failed to create tokio runtime")?;
                runtime.block_on(c.run())
            }
        }
    }
}

impl crate::cmd::Command for Sftp {
    fn run(&self) -> Result<()> {
        let _log_guard = crate::log::init_log(self.display.verbosity)?;
        if self.verify {
            let runtime = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .context("Failed to create tokio runtime")?;
            runtime.block_on(verify(&self.file, &get_portal_client()?))?;
        }
        let status = sett::destination::sftp::upload(
            &self.file,
            &self.sftp.clone().into(),
            (!self.display.quiet)
                .then(crate::util::CliProgress::new)
                .as_ref(),
            None,
            if self.mode.check {
                Mode::Check
            } else {
                Mode::Run
            },
        )?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

impl S3 {
    async fn run(&self) -> Result<()> {
        let _log_guard = crate::log::init_log(self.display.verbosity)?;
        if self.verify {
            verify(&self.file, &get_portal_client()?).await?;
        }
        let status = sett::destination::s3::upload(
            &self.s3.clone().into(),
            &self.file,
            if self.mode.check {
                Mode::Check
            } else {
                Mode::Run
            },
            (!self.display.quiet).then(crate::util::CliProgress::new),
        )
        .await?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

#[cfg(feature = "auth")]
impl S3Portal {
    async fn run(&self) -> Result<()> {
        let _log_guard = crate::log::init_log(self.display.verbosity)?;
        let portal_client = get_portal_client()?;
        let dtr = verify(&self.file, &portal_client)
            .await?
            .transfer_id
            .expect("missing transfer id");
        let token = get_oidc_client()?
            .authenticate(Box::new(super::common::auth_handler))
            .await?;
        let s3opts = portal_client
            .get_s3_credentials(dtr, &token.access_token)
            .await?;
        let status = sett::destination::s3::upload(
            &s3opts,
            &self.file,
            if self.mode.check {
                Mode::Check
            } else {
                Mode::Run
            },
            (!self.display.quiet).then(crate::util::CliProgress::new),
        )
        .await?;
        super::common::Task::Transfer.print_status(&status);
        Ok(())
    }
}

async fn verify(
    file: &Path,
    portal_client: &sett::portal::Portal,
) -> Result<sett::dpkg::PkgMetadata> {
    ensure!(
        file.is_file(),
        "File '{}' does not exist or is not a regular file",
        file.display()
    );
    let file_name = file
        .file_name()
        .context("Failed to get file name")?
        .to_string_lossy();
    let certstore = CertStore::open(sett::certstore::CertStoreOpts {
        read_only: true,
        ..Default::default()
    })?;
    let metadata = Package::open(file)?.verify(&certstore)?.metadata()?;
    let response = portal_client.check_package(&metadata, &file_name).await?;
    let file_name_expected = format!(
        "{}_{}.zip",
        response.project_code,
        metadata.timestamp.format(DATETIME_FORMAT)
    );
    ensure!(
        file_name == file_name_expected,
        "Package verification failed: invalid file name '{}' (expected '{}')",
        &file_name,
        file_name_expected
    );
    Ok(metadata)
}
