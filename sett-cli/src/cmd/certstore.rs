use std::borrow::Cow;
use std::fs::File;
use std::io::{self, Cursor, Read, Write};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use anyhow::{anyhow, bail, ensure, Context, Result};
use chrono::{DateTime, Local};
use clap::{ArgAction, Args, Subcommand, ValueEnum};
use console::Emoji;

use sett::certstore::{
    self, get_primary_email, userid_as_string, Cert, CertStore, CertStoreOpts, CertType, QueryTerm,
    ReasonForRevocation, RevocationStatus, SerializationFormat,
};
use sett::gnupg::{export_key, list_keys};
use sett::keyserver::{Keyserver, KeyserverEmailStatus};

const TIME_FORMAT: &str = "%d.%m.%Y %H:%M";

enum CertExpirationTime {
    Valid(Option<DateTime<Local>>),
    Expired(DateTime<Local>),
}

/// Declaration of the sub-commands of the `sett keys` CLI command. The names
/// of the Enum's variants become the sub-commands of the `sett keys` CLI
/// command.
#[derive(Subcommand)]
pub(crate) enum Keys {
    /// Import public or private OpenPGP keys into the local keystore.
    /// SECURITY WARNING: only import private keys that you trust, such as
    /// your own key.
    #[command(subcommand)]
    Import(Import),
    /// Export an OpenPGP key from the local store to a file in binary
    /// or ASCII-armored (plain text) format.
    Export(Export),
    /// List the OpenPGP keys present in the local store.
    List(List),
    /// Generate a new public/private OpenPGP key pair.
    Generate(Generate),
    /// Revoke an OpenPGP key.
    ///
    /// A public/private key pair can be revoked using the corresponding
    /// private key and providing a `REASON` and `MESSAGE`, or by using an
    /// existing revocation signature (`--rev-sig`).
    Revoke(Revoke),
    /// Upload an OpenPGP public key to the keyserver.
    Upload(Upload),
}

#[derive(Args)]
pub struct PrivateFlagArgs {
    /// Import/export an OpenPGP key containing private material.
    ///
    /// This option must be added when importing or exporting keys that contain
    /// private material. Passing this flag when importing/exporting public
    /// keys will generate an error.
    #[arg(short = 'p', long = "private", action = ArgAction::SetTrue, value_parser = parse_cert_type_flag )]
    cert_type: CertType,
}

#[derive(Subcommand)]
#[allow(clippy::enum_variant_names)]
pub(crate) enum Import {
    /// Import an OpenPGP key from a file (or multiple files) on disk.
    FromFile(FromFile),
    /// Download public OpenPGP keys from the keys.openpgp.org keyserver.
    FromKeyserver(FromKeyserver),
    /// Import OpenPGP keys from a local GnuPG keyring.
    FromGpg(FromGpg),
    /// Import OpenPGP keys from a stream passed to standard input.
    FromStdin(FromStdin),
}

#[derive(Args)]
pub(crate) struct FromFile {
    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Path(s) of the file(s) containing the OpenPGP key(s) to import.
    ///
    /// Multiple file paths can be passed at a time. Binary and ASCII-armored
    /// formats are supported.
    files: Vec<PathBuf>,
}

impl crate::cmd::Command for FromFile {
    fn run(&self) -> Result<()> {
        // Display a warning when importing secret keys.
        if let CertType::Secret = self.private.cert_type {
            print_secrets_warning_msg()
        }

        // Open the local Sequoia CertStore.
        let mut store = CertStore::open(Default::default())?;

        for file in &self.files {
            import_certs(File::open(file)?, &mut store, self.private.cert_type)
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromKeyserver {
    /// Identifier of OpenPGP keys to import.
    ///
    /// Fingerprint(s) or email(s) of the key(s) to download from the
    /// keyserver.
    identifiers: Vec<String>,
}

impl crate::cmd::Command for FromKeyserver {
    fn run(&self) -> Result<()> {
        // Open the local Sequoia CertStore.
        let store = CertStore::open(CertStoreOpts {
            use_keyserver: true,
            ..Default::default()
        })?;

        for identifier in &self.identifiers {
            match QueryTerm::from_str(identifier)
                .and_then(|query_term| store.get_cert(&query_term, CertType::Public))
            {
                Ok(imported_certs) => {
                    for imported_cert in imported_certs {
                        print_successful_import_msg(&imported_cert, false)
                    }
                }
                Err(error) => eprintln!(
                    "{} Import from keyserver failed: {error}",
                    Emoji("💥", "ERROR:")
                ),
            }
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromGpg {
    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Import all OpenPGP keys (public OR private) present in the local GnuPG
    /// store.
    #[arg(short = 'a', long, action = ArgAction::SetTrue, required_unless_present = "identifiers", conflicts_with = "identifiers")]
    all: bool,

    /// GnuPG home directory of GnuPG (Optional).
    ///
    /// By default, the default GnuPG directory is used (e.g. ~/.gnupg on
    /// Linux). This option allows overriding the default value by passing a
    /// custom directory where to look for a GnuPG keyring.
    #[arg(long = "gpghome")]
    gpg_home: Option<PathBuf>,

    /// Identifiers of OpenPGP keys to import.
    ///
    /// Fingerprint or email of the key(s) to extract from the GnuPG store. If
    /// a key with the same fingerprint is already present in the local sett
    /// keystore, the content of the existing and imported keys is merged.
    identifiers: Vec<String>,
}

impl crate::cmd::Command for FromGpg {
    fn run(&self) -> Result<()> {
        let mut store = CertStore::open(Default::default())?;
        let identifiers = if self.all {
            list_keys(self.private.cert_type, self.gpg_home.as_ref())?
                .into_iter()
                .map(|c| c.fingerprint)
                .collect()
        } else {
            self.identifiers.to_owned()
        };
        for identifier in identifiers {
            let password = match self.private.cert_type {
                CertType::Secret => Some(
                    rpassword::prompt_password(format!(
                        "{}Enter the password of the private key {identifier}: ",
                        Emoji("🔑 ", "")
                    ))?
                    .into_bytes(),
                ),
                CertType::Public => None,
            };
            match export_key(
                &identifier,
                self.private.cert_type,
                password.as_deref(),
                self.gpg_home.as_deref(),
            ) {
                Ok(exported) => {
                    import_certs(Cursor::new(exported), &mut store, self.private.cert_type)
                }
                Err(error) => print_failed_import_msg(error),
            }
        }
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct FromStdin {
    #[command(flatten)]
    private: PrivateFlagArgs,
}

impl crate::cmd::Command for FromStdin {
    fn run(&self) -> Result<()> {
        if let CertType::Secret = self.private.cert_type {
            print_secrets_warning_msg()
        }

        // Open the local Sequoia CertStore.
        let mut store = CertStore::open(Default::default())?;

        let mut data = Vec::new();
        io::stdin().lock().read_to_end(&mut data)?;
        import_certs(Cursor::new(data), &mut store, self.private.cert_type);
        Ok(())
    }
}

#[derive(Args)]
pub(crate) struct Export {
    /// Identified of OpenPGP key to export.
    ///
    /// Fingerprint or email of the key to export. Only a single key can be
    /// exported at a time.
    key_identifier: String,

    #[command(flatten)]
    private: PrivateFlagArgs,

    /// Export the OpenPGP key(s) in ASCII-armored format (i.e. plain text).
    ///
    /// By default keys are exported in binary format.
    #[arg(short = 'a', long = "armor", action = ArgAction::SetTrue, value_parser = parse_ascii_armor_flag )]
    output_format: SerializationFormat, // Is set to variant `AsciiArmored` if the flag is passed.

    /// Output path for the exported OpenPGP key.
    ///
    /// Export the OpenPGP key to a file on disk (instead of writing to stdout
    /// as is done by default). If this option is passed but no file name is
    /// provided, the key's fingerprint is used as default name.
    /// A ".pub"/".sec" extension for public/private keys exported in binary
    /// format and a ".pub.asc"/".sec.asc" extension for public/private keys
    /// exported in ASCII-armored format is added.
    /// Note: to pass this option without specifying a file name, it must be
    /// passed at the end of the command line.
    #[arg(short, long)]
    output_file: Option<Option<PathBuf>>,
}

#[derive(Args)]
pub(crate) struct List {
    /// Display the expiration date of OpenPGP keys.
    ///
    /// Expired keys always display this information, even if this option is
    /// not passed.
    #[arg(short = 'e', long)]
    show_expiration: bool,

    /// Display the path of OpenPGP keys (location on disk).
    #[arg(short = 'p', long)]
    show_path: bool,
}

#[derive(Debug, Clone, ValueEnum)]
enum CipherSuite {
    /// EdDSA and ECDH over Curve25519 with SHA512 and AES256
    Cv25519,
    /// 4096 bit RSA with SHA512 and AES256
    Rsa4k,
}

impl From<&CipherSuite> for certstore::CipherSuite {
    fn from(val: &CipherSuite) -> Self {
        match val {
            CipherSuite::Cv25519 => Self::Cv25519,
            CipherSuite::Rsa4k => Self::RSA4k,
        }
    }
}

#[derive(Args)]
pub(crate) struct Generate {
    /// User ID to be associated with the newly generated key.
    ///
    /// A User ID is a string that is usually composed of the following
    /// elements: "First name Last name (optional comment) <email>".
    ///
    /// Examples: "Alice Smith <alice@example.org>",
    /// "John Doe () <john@example.org>".
    userid: String,

    /// The cryptographic algorithm to be used for the new key.
    #[arg(short, long, default_value_t = CipherSuite::Cv25519, value_enum)]
    cipher_suite: CipherSuite,

    /// Export the newly generated key to a file or the standard output ("-")
    #[arg(short, long, required_if_eq("no_cert_store", "true"))]
    export: Option<PathBuf>,

    /// Revocation signature file path (Optional).
    ///
    /// Export the revocation signature of the newly generated key to a file
    /// instead of standard output (default, "-").
    #[arg(short, long, default_value = "-")]
    rev_sig: PathBuf,

    /// Do not add the newly generate key to the local keystore.
    #[arg(long)]
    no_store: bool,
}

impl crate::cmd::Command for Generate {
    fn run(&self) -> Result<()> {
        let p1 = rpassword::prompt_password(format!(
            "{}Enter a password to protect the private key: ",
            Emoji("🔑 ", "")
        ))?;
        let p2 = rpassword::prompt_password(format!(
            "{}Repeat the password for the private key: ",
            Emoji("🔑 ", "")
        ))?;
        ensure!(p1 == p2, "{}Passwords do not match", Emoji("💥 ", ""));
        let (cert, rev) = certstore::generate_cert(
            self.userid.as_str(),
            p1.as_bytes(),
            (&self.cipher_suite).into(),
        )?;

        if let Some(p) = &self.export {
            let cert_serialized =
                cert.serialize(CertType::Secret, SerializationFormat::AsciiArmored)?;
            create_file_or_std_writer(Some(p))?.write_all(&cert_serialized)?;
        }

        eprintln!(
            "{} This is a revocation signature for the newly created \
            OpenPGP key: {}. Revocation signatures should be kept in a \
            safe place, such as a password manager.",
            Emoji("ℹ️ ", "INFO:"),
            userid_as_string(&cert)
        );
        create_file_or_std_writer(Some(&self.rev_sig))?.write_all(&rev)?;

        if self.no_store {
            eprintln!(
                "{}The following public/private OpenPGP key pair has been \
                successfully created: {}.",
                Emoji("🔏 ", ""),
                userid_as_string(&cert)
            );
        } else {
            CertStore::open(Default::default())?.add_cert(&cert, CertType::Secret)?;
            eprintln!(
                "{}The following public/private OpenPGP key pair has been \
                successfully created and added to the local keystore: {}.",
                Emoji("📥 ", ""),
                userid_as_string(&cert)
            );
        }

        Ok(())
    }
}

#[derive(Clone, Copy, ValueEnum)]
enum RevocationReason {
    /// The private key is (or may have been) compromised.
    Compromised,
    /// The key has been replaced by a new one. The message should contain
    /// the fingerprint of the new key.
    Superseded,
    /// The key should not be used anymore and there is no replacement key.
    /// This reason is appropriate e.g. when leaving an organization.
    Retired,
    /// None of the above reasons apply.
    /// Note: It is preferable to use a specific reason.
    Unspecified,
}

impl From<RevocationReason> for ReasonForRevocation {
    fn from(reason: RevocationReason) -> Self {
        match reason {
            RevocationReason::Compromised => ReasonForRevocation::KeyCompromised,
            RevocationReason::Superseded => ReasonForRevocation::KeySuperseded,
            RevocationReason::Retired => ReasonForRevocation::KeyRetired,
            RevocationReason::Unspecified => ReasonForRevocation::Unspecified,
        }
    }
}

#[derive(Args)]
pub(crate) struct Revoke {
    /// Identified of OpenPGP key to revoke.
    ///
    /// Fingerprint or email of the key to revoke. Only a single key can be
    /// revoked at a time.
    cert_identifier: String,

    /// Revocation reason.
    #[arg(value_enum, required_unless_present = "rev_sig")]
    reason: Option<RevocationReason>,

    /// Revocation reason details (Optional).
    ///
    /// A short text explaining the reason for the key revocation.
    #[arg(required_unless_present = "rev_sig")]
    message: Option<String>,

    /// Revocation signature file path.
    ///
    /// Use an existing revocation signature, instead of generating a new one
    /// from the private key. If access to the private key was lost or the
    /// password was forgotten, this is the only way to revoke a key.
    #[arg(long, conflicts_with_all = ["reason", "message"])]
    rev_sig: Option<PathBuf>,

    /// Generate revocation signature.
    ///
    /// Generate a revocation signature without revoking the original key in
    /// the keystore.
    #[arg(long, conflicts_with = "rev_sig")]
    gen_rev: bool,

    /// After revoking the key, print it to standard output.
    #[arg(long)]
    show_revoked_key: bool,
}

impl crate::cmd::Command for Revoke {
    fn run(&self) -> Result<()> {
        let mut store = CertStore::open(CertStoreOpts {
            allow_create: false,
            ..Default::default()
        })?;

        // Load the secret certificate to revoke. If not present, also try to
        // load the public certificate. This is done for the edge case where
        // a user would have lost their secret certificate, but somehow still
        // have their public certificate and the revocation signature, so that
        // they can still revoke their public certificate and e.g. upload it to
        // a keyserver.
        let cert = get_single_cert(
            &QueryTerm::from_str(&self.cert_identifier)?,
            CertType::Secret,
            &store,
        )
        .unwrap_or(
            get_single_cert(
                &QueryTerm::from_str(&self.cert_identifier)?,
                CertType::Public,
                &store,
            )
            .context(format!(
                "{} No private or public key matching '{}' \
                was found in the local keystore.",
                Emoji("💥", "ERROR:"),
                &self.cert_identifier,
            ))?,
        );

        // Load the revocation signature from a file provided by the user or
        // generate a new one.
        let revocation_signature = if let Some(p) = &self.rev_sig {
            std::fs::read(p)?
        } else {
            ensure!(
                cert.cert_type() == CertType::Secret,
                "{} A private key matching '{}' must be present in the local \
                keystore when no revocation signature is passed to this \
                command. \
                The reason for this is that creating a revocation signature \
                requires the private key.",
                Emoji("💥", "ERROR:"),
                &self.cert_identifier
            );
            // Create a revocation signature using the secret key of
            // the certificate.
            cert.generate_rev_sig(
                self.reason
                    .expect("`reason` is required when `rev_sig` is absent")
                    .into(),
                self.message
                    .as_ref()
                    .expect("`message` is required when `rev_sig` is absent")
                    .as_bytes(),
                cert.is_encrypted()?.then_some(
                    rpassword::prompt_password(format!(
                        "{}Enter the password for \"{}\": ",
                        Emoji("🔑 ", ""),
                        userid_as_string(&cert)
                    ))?
                    .as_bytes(),
                ),
            )?
        };

        // If the user requested to create a revocation signature but not to
        // revoke the certificate, print that signature to a file/stdout and
        // exit.
        if self.gen_rev {
            create_file_or_std_writer(None::<&Path>)?.write_all(&revocation_signature)?;
            return Ok(());
        }

        // Revoke public and secret certificates in the local store (on disk).
        let revoked = cert.revoke(std::io::Cursor::new(&revocation_signature))?;
        if self.show_revoked_key {
            eprintln!("{} Below is your revoked public key:", Emoji("ℹ️ ", "INFO:"));
            io::stdout().write_all(
                &revoked.serialize(CertType::Public, SerializationFormat::AsciiArmored)?,
            )?;
        }
        store.add_cert(&revoked, CertType::Public)?;
        if cert.cert_type() == CertType::Secret {
            store.add_cert(&revoked, CertType::Secret)?;
        }
        eprintln!(
            "{}Successfully revoked key: {} [{}]",
            Emoji("✅ ", ""),
            userid_as_string(&revoked),
            &revoked.fingerprint().to_string()
        );
        Ok(())
    }
}

fn create_file_or_std_writer(path: Option<impl AsRef<Path>>) -> Result<Box<dyn Write>> {
    match path {
        None => Ok(Box::new(io::stdout())),
        Some(p) if p.as_ref() == Path::new("-") => Ok(Box::new(io::stdout())),
        Some(p) => {
            if p.as_ref().exists() {
                bail!(
                    "File {:?} already exists. Remove the existing file or \
                    use a different path.",
                    p.as_ref()
                )
            }
            Ok(Box::new(File::create(p)?))
        }
    }
}

impl crate::cmd::Command for Keys {
    fn run(&self) -> Result<()> {
        match self {
            Self::Import(cmd) => cmd.run(),
            Self::Export(cmd) => cmd.run(),
            Self::List(cmd) => cmd.run(),
            Self::Generate(cmd) => cmd.run(),
            Self::Revoke(cmd) => cmd.run(),
            Self::Upload(cmd) => cmd.run(),
        }
    }
}

impl crate::cmd::Command for Import {
    fn run(&self) -> Result<()> {
        match self {
            Self::FromFile(cmd) => cmd.run(),
            Self::FromKeyserver(cmd) => cmd.run(),
            Self::FromGpg(cmd) => cmd.run(),
            Self::FromStdin(cmd) => cmd.run(),
        }
    }
}

/// Import OpenPGP certificate(s) into the local `CertStore` OpenPGP
/// certificate store. The input data can contain a single or multiple
/// certificates.
///
/// If a certificate with the same fingerprint is already present in the
/// `CertStore`, the content of the existing and imported certificates are
/// merged.
///
/// # Arguments
///
/// * `data`: the certificate(s) to import as a vector of bytes, e.g. read from
///   a file.
/// * `store`: the `CertStore` instance into which the certificate(s) should
///    be imported.
/// * `cert_type`: type of certificate (`Public` or `Secret`) to import.
fn import_certs<R: io::Read + Send + Sync>(data: R, store: &mut CertStore, cert_type: CertType) {
    if let Ok(certs) = certstore::read_cert(data) {
        for cert in certs {
            store
                .add_cert(&cert, cert_type)
                .map_or_else(print_failed_import_msg, |imported_cert| {
                    print_successful_import_msg(
                        &imported_cert,
                        matches!(cert.cert_type(), CertType::Secret)
                            && matches!(imported_cert.cert_type(), CertType::Public),
                    )
                })
        }
    } else {
        eprintln!(
            "{} Import failed: input does not contain valid OpenPGP key data",
            Emoji("💥", "ERROR:")
        )
    }
}

/// Print a summary of the successfully imported certificate.
fn print_successful_import_msg(imported_cert: &Cert, with_warning: bool) {
    eprintln!(
        "{}Imported {}",
        Emoji("📥 ", ""),
        userid_as_string_with_type(imported_cert)
    );

    // If a secret certificate was imported as a public certificate,
    // display a warning to the user.
    if with_warning {
        eprintln!(
            "{} The input data for '{}' contains both a private and a public \
            key, but only the public key was imported. To also import \
            the private key, pass the `-p/--private` option to this command.",
            Emoji("⚠️ ", "WARNING:"),
            userid_as_string(imported_cert)
        );
    }
}

/// Print an error message when importing an OpenPGP certificate fails.
fn print_failed_import_msg(error: anyhow::Error) {
    eprintln!(
        "{} Failed to import OpenPGP key: {error}",
        Emoji("💥", "ERROR:")
    );
}

/// Print a warning message when importing certificates containing
/// secret material.
fn print_secrets_warning_msg() {
    eprintln!(
        "{} Only import private keys that you trust, such as your own key.",
        Emoji("⚠️ ", "SECURITY WARNING:")
    )
}

#[derive(Args)]
pub(crate) struct Upload {
    /// Fingerprint or email of the public OpenPGP key to upload.
    identifier: String,

    /// Request the keyserver to send a verification email for the
    /// uploaded public key.
    #[arg(long, short = 'v', action = ArgAction::SetTrue)]
    verify: bool,
}

fn handle_failed_verification(error: anyhow::Error) {
    eprintln!(
        "{} Rejected verification request: {error}",
        Emoji("💥", "ERROR:")
    )
}

impl crate::cmd::Command for Upload {
    fn run(&self) -> Result<()> {
        let imported_cert = get_single_cert(
            &QueryTerm::from_str(&self.identifier)?,
            CertType::Public,
            &CertStore::open(CertStoreOpts {
                read_only: true,
                allow_create: false,
                ..Default::default()
            })?,
        )?;
        if let Some(email) = get_primary_email(&imported_cert)? {
            let keyserver = Keyserver::new().context("Failed to create keyserver client")?;
            let runtime = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .context("Failed to create tokio runtime")?;
            match runtime.block_on(keyserver.upload_cert(&imported_cert)) {
                Ok(response) => {
                    eprintln!(
                        "{}Uploaded {}",
                        Emoji("📤 ", ""),
                        userid_as_string_with_type(&imported_cert)
                    );
                    if self.verify {
                        match response.status.get(&email) {
                            Some(KeyserverEmailStatus::Unpublished)
                            | Some(KeyserverEmailStatus::Pending) => {
                                match runtime
                                    .block_on(keyserver.verify_cert(&response.token, &email))
                                {
                                    Ok(_) => eprintln!(
                                        "{}Verification request submitted.",
                                        Emoji("🔎 ", ""),
                                    ),
                                    Err(error) => handle_failed_verification(error),
                                }
                            }
                            Some(KeyserverEmailStatus::Revoked) => handle_failed_verification(
                                anyhow!("This key is revoked and cannot be used."),
                            ),
                            Some(KeyserverEmailStatus::Published) => {
                                handle_failed_verification(anyhow!("This key is already verified."))
                            }
                            None => handle_failed_verification(anyhow!(format!(
                                "Couldn't find {} in keyserver's response.",
                                email
                            ))),
                        }
                    }
                }
                Err(error) => eprintln!("{} Rejected upload: {error}", Emoji("💥", "ERROR:")),
            };
        } else {
            eprintln!(
                "{} This key does not contain an email.",
                Emoji("💥", "ERROR:"),
            )
        }
        Ok(())
    }
}

impl crate::cmd::Command for Export {
    fn run(&self) -> Result<()> {
        // Open the local certificate store and retrieve the requested
        // certificate based on the identifier provided by the user. This
        // can be either a fingerprint or an email address.
        let store = CertStore::open(CertStoreOpts {
            read_only: true,
            allow_create: false,
            ..Default::default()
        })?;
        let cert = get_single_cert(
            &QueryTerm::from_str(&self.key_identifier)?,
            self.private.cert_type,
            &store,
        )?;
        let serialized_cert = cert
            .serialize(self.private.cert_type, self.output_format)
            .context(
                "If you wish to export a public key, remove the \
                '-p/--private' option from this command",
            )?;

        // Write the serialized certificate to either a file or stdout.
        if let Some(optional_file_name) = &self.output_file {
            // Case 1: the certificate should be written to file.
            let file_name = optional_file_name.clone().unwrap_or_else(|| {
                generate_file_name(&cert, self.private.cert_type, self.output_format)
            });
            File::create(&file_name)?.write_all(&serialized_cert)?;
            eprintln!(
                "{}Exported {} to file '{}'.",
                Emoji("📤 ", ""),
                userid_as_string_with_type(&cert),
                file_name.to_string_lossy(),
            );
        } else {
            // Case 2: the certificate should be written to stdout.
            io::stdout().write_all(&serialized_cert)?;
        }
        Ok(())
    }
}

fn cert_expiration_time(cert: &Cert) -> Result<CertExpirationTime> {
    Ok(match cert.expiration_time()? {
        // Certificate has an expiration date.
        Some(expiration_time) => {
            if expiration_time > std::time::SystemTime::now() {
                CertExpirationTime::Valid(Some(DateTime::<Local>::from(expiration_time)))
            } else {
                CertExpirationTime::Expired(DateTime::<Local>::from(expiration_time))
            }
        }
        // Certificate never expires.
        None => CertExpirationTime::Valid(None),
    })
}

impl crate::cmd::Command for List {
    fn run(&self) -> Result<()> {
        let store = CertStore::open(CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
        let mut std_lock = io::stdout();
        for (cert_type, emoji) in [
            (CertType::Secret, Emoji("🔒 ", "")),
            (CertType::Public, Emoji("📨 ", "")),
        ] {
            let path = store.get_store_path(cert_type).to_string_lossy();
            writeln!(&mut std_lock, "{emoji}{cert_type} keys [{path}]")?;
            let certs = store.get_all_certs(cert_type)?;
            if certs.is_empty() {
                write!(
                    &std_lock,
                    "{}",
                    console::style(format!("{cert_type} keystore is empty.")).red()
                )?;
            }
            for cert in certs {
                write!(&mut std_lock, "{}", userid_as_string(&cert))?;
                if let RevocationStatus::Revoked(revs) = cert.revocation_status() {
                    for r in revs {
                        let (code, msg) = r.reason_for_revocation().map_or(
                            (ReasonForRevocation::Unspecified, Cow::Borrowed("")),
                            |(code, msg)| (code, String::from_utf8_lossy(msg)),
                        );
                        let (emoji, style) = match code {
                            ReasonForRevocation::KeyRetired
                            | ReasonForRevocation::UIDRetired
                            | ReasonForRevocation::KeySuperseded => {
                                (Emoji("⚠️ ", ""), console::Style::new().yellow())
                            }
                            _ => (Emoji("⛔", ""), console::Style::new().red()),
                        };
                        write!(
                            &mut std_lock,
                            " {}",
                            style.apply_to(format!(
                                "{} REVOKED! {}{}",
                                emoji,
                                code,
                                if msg.is_empty() {
                                    String::from("")
                                } else {
                                    format!(": \"{msg}\"")
                                }
                            ))
                        )?;
                    }
                }

                // Display the certificate's expiry date:
                // * Always, for certificates that have expired or whose
                //   expiry date could not be retrieved.
                // * Only if requested by the user otherwise.
                match cert_expiration_time(&cert) {
                    Ok(CertExpirationTime::Valid(Some(expiration_time))) => {
                        if self.show_expiration {
                            write!(
                                &mut std_lock,
                                " {}",
                                console::style(format!(
                                    "[Expires on {}]",
                                    expiration_time.format(TIME_FORMAT)
                                ))
                                .green()
                            )?
                        }
                    }
                    Ok(CertExpirationTime::Valid(None)) => {
                        if self.show_expiration {
                            write!(
                                &mut std_lock,
                                " {}",
                                console::style("[Never expires]").green()
                            )?
                        }
                    }
                    Ok(CertExpirationTime::Expired(expiration_time)) => write!(
                        &mut std_lock,
                        " {}{}",
                        Emoji("⛔ ", ""),
                        console::style(format!(
                            "Expired on {}",
                            expiration_time.format(TIME_FORMAT)
                        ))
                        .red()
                    )?,
                    Err(e) => write!(
                        &mut std_lock,
                        " {}{}",
                        Emoji("⛔ ", ""),
                        console::style(format!("Expiration date could not be retrieved: {}", e))
                            .red()
                    )?,
                };

                // Display certificate's path on disk, if the user asked for it.
                if self.show_path {
                    write!(
                        &mut std_lock,
                        "{}",
                        store
                            .get_cert_path(&cert.fingerprint(), cert_type)
                            .map_or_else(
                                |e| console::style(format!(" [{e}]")).red(),
                                |p| console::style(format!(" [{}]", p.to_string_lossy())).dim(),
                            )
                    )?
                }
                writeln!(&mut std_lock)?;
            }
            writeln!(&mut std_lock)?;
        }
        Ok(())
    }
}

/// Return `CertType::Secret` if `with_secret` is `true`.
fn parse_cert_type_flag(with_secret: &str) -> Result<CertType> {
    if with_secret == "true" {
        Ok(CertType::Secret)
    } else {
        Ok(CertType::Public)
    }
}

/// Return `SerializationFormat::AsciiArmored` if `ascii_armor` is `true`.
fn parse_ascii_armor_flag(ascii_armor: &str) -> Result<SerializationFormat> {
    if ascii_armor == "true" {
        Ok(SerializationFormat::AsciiArmored)
    } else {
        Ok(SerializationFormat::Binary)
    }
}

/// Generate an output file name based on the fingerprint of the specified
/// OpenPGP certificate.
fn generate_file_name(cert: &Cert, cert_type: CertType, format: SerializationFormat) -> PathBuf {
    PathBuf::from(cert.fingerprint().to_hex()).with_extension(match (format, cert_type) {
        (SerializationFormat::Binary, CertType::Public) => "pub",
        (SerializationFormat::Binary, CertType::Secret) => "sec",
        (SerializationFormat::AsciiArmored, CertType::Public) => "pub.asc",
        (SerializationFormat::AsciiArmored, CertType::Secret) => "sec.asc",
    })
}

/// Formats the primary user ID of an OpenPGP certificate as a string of type:
///  -> public/private key: <user ID> <email> [fingerprint]
fn userid_as_string_with_type(cert: &Cert) -> String {
    format!("{} key: {}", cert.cert_type(), userid_as_string(cert))
}

/// Retrieve an OpenPGP certificate matching the specified `query_term`.
/// If the query returns 0 or more than 1 certificates, an error is returned.
pub(super) fn get_single_cert(
    query_term: &QueryTerm,
    cert_type: CertType,
    store: &CertStore,
) -> Result<Cert> {
    // Search for certificates matching the specified query term. If no
    // certificate is found, an error is returned.
    let mut certs = store.get_cert(query_term, cert_type)?;

    // Return an error if more than one certificates matches the query.
    // Note that this can only happen if the query term is an email.
    if certs.len() > 1 {
        bail!(
            "More than one key is matching the specified email: {}",
            certs
                .into_iter()
                .map(|cert| userid_as_string(&cert))
                .collect::<Vec<String>>()
                .join(", ")
        );
    }

    // Return the (single) matching certificate - at this point we are
    // in principle guaranteed that `certs` contains exactly 1 certificate.
    Ok(certs
        .pop()
        .expect("`certs` should contain exactly 1 element at this point."))
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;
    use std::io::Cursor;
    use tempfile::tempdir;

    fn generate_tmp_certstore<'a>() -> CertStore<'a> {
        let tmp_dir = tempdir().unwrap().into_path();
        CertStore::open(CertStoreOpts {
            pub_store_path: Some(&tmp_dir.join("pgp.cert.d")),
            sec_store_path: Some(&tmp_dir.join("pgp.cert.d.sec")),
            ..Default::default()
        })
        .unwrap()
    }

    // Delete all certificates in the specified `store`.
    fn clear_store(store: &CertStore) -> Result<()> {
        for dir in [
            &store.get_store_path(CertType::Public),
            &store.get_store_path(CertType::Secret),
        ] {
            for path in fs::read_dir(dir)? {
                let path = path?;
                if path.file_type()?.is_dir() {
                    // Recursively delete the sub-directory.
                    let subdir_path = path.path();
                    if subdir_path.file_name().unwrap().len() != 2 {
                        bail!(
                            "subdirectory names of CertStore have length 2: {:?}",
                            subdir_path
                        )
                    }
                    fs::remove_dir_all(subdir_path)?;
                }
            }
        }
        Ok(())
    }

    // Test that importing a certificate from data is working.
    #[test]
    fn test_import_cert() -> Result<()> {
        let mut store = generate_tmp_certstore();

        macro_rules! assert_import {
            ($file_name:expr, $cert_type:expr) => {
                // Import certificate.
                _ = import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut store,
                    $cert_type,
                );
                // Make sure certificate is now present in the CertStore.
                let fingerprint = &$file_name[..40];
                assert_eq!(
                    get_single_cert(&QueryTerm::from_str(fingerprint)?, $cert_type, &store,)
                        .unwrap()
                        .fingerprint()
                        .to_string(),
                    fingerprint,
                )
            };
        }
        // Importing a public or secret certificate in ASCII format should work.
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 0);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub.asc",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub.asc",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.pub.asc",
            CertType::Public
        );
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec.asc",
            CertType::Secret
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec.asc",
            CertType::Secret
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec.asc",
            CertType::Secret
        );
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 3);

        // Importing public or secret certificates in binary format should work.
        clear_store(&store)?;
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.pub",
            CertType::Public
        );
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec",
            CertType::Secret
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec",
            CertType::Secret
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec",
            CertType::Secret
        );
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 3);

        // Importing secret certificates as public should pass.
        clear_store(&store)?;
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!(
            "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.sec.asc",
            CertType::Public
        );
        assert_import!(
            "B2E961753ECE0B345E718E74BA6F29C998DDD9BF.sec.asc",
            CertType::Public
        );
        assert_import!(
            "C0621CB3669020CC31050A361956EC38A96CA852.sec.asc",
            CertType::Public
        );
        assert_eq!(store.get_all_certs(CertType::Public).unwrap().len(), 3);
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);

        Ok(())
    }

    // Importing a public certificate as a secret certificate should not add
    // the secret certificate to the store.
    #[test]
    fn test_import_cert_failure() {
        let mut store = generate_tmp_certstore();

        macro_rules! assert_import_fails {
            ($file_name:expr) => {
                // Import certificate.
                _ = import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut store,
                    CertType::Secret,
                );
                // Make sure certificate is *not* present in the CertStore.
                for cert_type in [CertType::Secret, CertType::Public] {
                    assert!(get_single_cert(
                        &QueryTerm::from_str(&$file_name[..40]).unwrap(),
                        cert_type,
                        &store,
                    )
                    .is_err())
                }
            };
        }

        // Importing a public certificate as secret should return an error.
        assert_import_fails!("1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub");
        assert_import_fails!("1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub.asc");
        assert_import_fails!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub");
        assert_import_fails!("B2E961753ECE0B345E718E74BA6F29C998DDD9BF.pub.asc");
        assert_import_fails!("C0621CB3669020CC31050A361956EC38A96CA852.pub");
        assert_import_fails!("C0621CB3669020CC31050A361956EC38A96CA852.pub.asc");
    }

    #[test]
    fn test_get_single_cert() -> Result<()> {
        let mut store = generate_tmp_certstore();
        let fingerprint_cert_1 = "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB";
        let fingerprint_cert_2 = "C0621CB3669020CC31050A361956EC38A96CA852";

        macro_rules! assert_pass {
            ($query_term:expr, $cert:expr, $cert_type:expr) => {
                assert_eq!(
                    get_single_cert(&QueryTerm::from_str($query_term)?, $cert_type, &store)?,
                    $cert
                )
            };
        }

        macro_rules! assert_fails {
            ($query_term:expr, $cert_type:expr) => {
                assert!(
                    get_single_cert(&QueryTerm::from_str($query_term)?, $cert_type, &store)
                        .is_err()
                );
            };
        }

        // cert_1 => contains only public material.
        let cert_1 = store.add_cert(
            &Cert::from_bytes(include_bytes!(
                "../../../sett/tests/data/1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB.pub"
            ))?,
            CertType::Public,
        )?;

        // Retrieving certificate by email or fingerprint should work.
        assert_pass!(fingerprint_cert_1, cert_1, CertType::Public);
        assert_pass!("chuck.norris@roundhouse.org", cert_1, CertType::Public);

        // Trying to retrieve a non-existing certificate should fail.
        assert_fails!("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", CertType::Public);
        assert_fails!("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", CertType::Secret);
        assert_fails!("missing@example.org", CertType::Public);
        assert_fails!("missing@example.org", CertType::Secret);

        // When adding a second certificate to the store, with the same email
        // address as the first one, the function should now return an error
        // if we query by email.
        let cert_2 = store.add_cert(
            &Cert::from_bytes(include_bytes!(
                "../../../sett/tests/data/C0621CB3669020CC31050A361956EC38A96CA852.sec"
            ))?,
            CertType::Secret,
        )?;
        assert_fails!("chuck.norris@roundhouse.org", CertType::Public);

        // But querying by fingerprint should still work, since fingerprints
        // are unique.
        assert_pass!(fingerprint_cert_1, cert_1, CertType::Public);
        assert_pass!(fingerprint_cert_2, cert_2, CertType::Public);
        assert_pass!(fingerprint_cert_2, cert_2, CertType::Secret);

        Ok(())
    }

    // Test that importing multiple certificate from data is working.
    #[test]
    fn test_import_multiple_certs() -> Result<()> {
        let mut store = generate_tmp_certstore();

        macro_rules! assert_import {
            ($file_name:expr, $cert_type:expr) => {
                // Import certificate.
                _ = import_certs(
                    Cursor::new(include_bytes!(concat!(
                        "../../../sett/tests/data/",
                        $file_name
                    ))),
                    &mut store,
                    $cert_type,
                );
                // Make sure the 3 certificates that are part of the keyring
                // file are now present in the CertStore.
                for fingerprint in [
                    "1EA0292ECBF2457CADAE20E2B94FA6A56D9FA1FB",
                    "B2E961753ECE0B345E718E74BA6F29C998DDD9BF",
                    "C0621CB3669020CC31050A361956EC38A96CA852",
                ] {
                    assert_eq!(
                        get_single_cert(&QueryTerm::from_str(fingerprint)?, $cert_type, &store,)
                            .unwrap()
                            .fingerprint()
                            .to_string(),
                        fingerprint,
                    )
                }
            };
        }

        // Importing multiple public or secret certificates should work.
        assert_eq!(store.get_all_certs(CertType::Secret).unwrap().len(), 0);
        assert_import!("keyring.pub", CertType::Public);
        assert_import!("keyring.sec", CertType::Secret);

        Ok(())
    }
}
