#[cfg(feature = "auth")]
use std::io::{stdout, Write};
use std::{fs, path::PathBuf, str::FromStr};

use anyhow::{Context, Result};
use clap::Args;
use console::Emoji;
use indicatif::HumanBytes;
use sett::{
    certstore::{Cert, CertStore, CertStoreOpts, CertType, QueryTerm},
    destination::{s3::S3Opts, sftp::SftpOpts},
    task::Status,
};

use super::certstore::get_single_cert;

#[cfg(feature = "auth")]
const BIOMEDIT_KEYCLOAK_URL: &str = "https://login.biomedit.ch/realms/biomedit";
#[cfg(feature = "auth")]
const BIOMEDIT_WEBSITE_URL: &str = "https://biomedit.ch";

#[derive(Args)]
pub(super) struct DisplayArgs {
    /// Don't show progress bar.
    #[arg(short, long)]
    pub(super) quiet: bool,

    /// Log verbosity level.
    #[arg(short, action=clap::ArgAction::Count)]
    pub(super) verbosity: u8,
}

#[derive(Args)]
pub(super) struct ModeArgs {
    /// Check the input without running the full task. No data will be modified
    /// or stored at the destination.
    #[arg(long)]
    pub(super) check: bool,
}

#[derive(Args, Clone)]
pub(super) struct SftpArgs {
    /// User name for authentication with the SFTP server.
    #[arg(short, long)]
    pub(super) username: String,

    /// Domain name (or IP address) of the SFTP server.
    #[arg(short = 'H', long)]
    pub(super) host: String,

    /// SFTP server port number.
    #[arg(short = 'P', long, default_value_t = 22)]
    pub(super) port: u16,

    /// Path on the remote SFTP host where encrypted files will be stored.
    #[arg(long)]
    pub(super) base_path: PathBuf,

    /// Private SSH key path.
    #[arg(long)]
    pub(super) key_path: Option<String>,

    /// Private SSH key password.
    #[arg(long)]
    pub(super) key_pwd: Option<String>,
}

impl From<SftpArgs> for SftpOpts<'_> {
    fn from(val: SftpArgs) -> Self {
        Self {
            host: val.host.into(),
            port: val.port,
            username: val.username.into(),
            base_path: val.base_path.into(),
            envelope_dir: None,
            key_path: val.key_path.map(Into::into),
            key_password: val.key_pwd.map(Into::into),
            two_factor_callback: Some(Box::new(crate::util::two_factor_prompt)),
        }
    }
}

#[derive(Args, Clone)]
pub(super) struct S3Args {
    /// S3 endpoint URL.
    ///
    /// The URL of the S3 server where the encrypted data should
    /// be uploaded. By default, the official AWS S3 endpoint
    /// is used.
    #[arg(short, long)]
    pub(super) endpoint: Option<String>,

    /// Name of the S3 bucket to which the data encrypted should be uploaded.
    #[arg(short, long)]
    pub(super) bucket: String,

    /// Region, e.g. `us-east-1`, `eu-central-1`, etc. Required in AWS world,
    /// but might be optional if using a S3-compatible server like MinIO.
    #[arg(long)]
    pub(super) region: Option<String>,

    /// S3 credential profile to use. This option can be used if multiple
    /// credential profiles are defined in the login credential file, and
    /// allows to select a profile that is not the default one.
    #[arg(long)]
    pub(super) profile: Option<String>,

    /// Access key ID for authentication with the S3 server.
    #[arg(long)]
    pub(super) access_key: Option<String>,

    /// Secret access key for authentication with the S3 server.
    #[arg(long)]
    pub(super) secret_key: Option<String>,

    /// Session token.
    #[arg(long)]
    pub(super) session_token: Option<String>,
}

impl From<S3Args> for S3Opts<'_> {
    fn from(val: S3Args) -> Self {
        Self {
            bucket: val.bucket.into(),
            endpoint: val.endpoint.map(Into::into),
            region: val.region.map(Into::into),
            profile: val.profile.map(Into::into),
            access_key: val.access_key.map(Into::into),
            secret_key: val.secret_key.map(Into::into),
            session_token: val.session_token.map(Into::into),
        }
    }
}

pub(super) enum Task {
    Encrypt,
    Decrypt,
    Transfer,
}

impl Task {
    pub(super) fn print_status(&self, status: &Status) {
        match status {
        Status::Checked {
            destination,
            source_size,
        } => {
            eprintln!(
                "{}Check completed successfully: {destination} (source: {})",
                Emoji("✅ ", ""),
                HumanBytes(*source_size)
            );
        }
        Status::Completed {
            destination,
            source_size,
            destination_size,
        } => {
            match self {
                Task::Encrypt => eprintln!(
                    "{}Completed data encryption: {destination} (source: {}, destination: {}, ratio: {:.2})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                    HumanBytes(*destination_size),
                    *source_size as f32 / *destination_size as f32
                ),
                Task::Decrypt => eprintln!(
                    "{}Completed data decryption: {destination} (source: {}, destination: {}, ratio: {:.2})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                    HumanBytes(*destination_size),
                    *destination_size as f32 / *source_size as f32
                ),
                Task::Transfer => eprintln!(
                    "{}Completed data transfer: {destination} (source: {})",
                    Emoji("✅ ", ""),
                    HumanBytes(*source_size),
                ),
            }
        }
    }
    }
}

pub(super) fn get_portal_client() -> Result<sett::portal::Portal> {
    sett::portal::Portal::new(
        std::env::var("SETT_PORTAL_URL")
            .as_deref()
            .unwrap_or("https://portal.dcc.sib.swiss"),
    )
}

#[cfg(feature = "auth")]
pub(super) fn get_oidc_client() -> Result<sett::auth::Oidc> {
    sett::auth::Oidc::new(
        std::env::var("SETT_OIDC_CLIENT_ID")
            .as_deref()
            .unwrap_or("sett"),
        std::env::var("SETT_OIDC_ISSUER_URL")
            .as_deref()
            .unwrap_or(BIOMEDIT_KEYCLOAK_URL),
    )
}

#[cfg(feature = "auth")]
pub(super) fn auth_handler(details: &sett::auth::VerificationDetails) -> Result<()> {
    let mut lock = stdout().lock();
    writeln!(lock, "To authorize sett, using a browser, visit:")?;
    if let Some(uri) = &details.verification_uri_complete {
        writeln!(
            lock,
            "{}",
            // We set up a redirect to make the URL easier to type.
            uri.replace(BIOMEDIT_KEYCLOAK_URL, BIOMEDIT_WEBSITE_URL)
        )?;
    } else {
        writeln!(
            lock,
            "{}",
            details
                .verification_uri
                // We set up a redirect to make the URL easier to type.
                .replace(BIOMEDIT_KEYCLOAK_URL, BIOMEDIT_WEBSITE_URL)
        )?;
        writeln!(lock)?;
        writeln!(lock, "And enter the code:")?;
        writeln!(lock, "{}", details.user_code)?;
    };
    Result::Ok(())
}

/// Read OpenPGP certificates from files.
///
/// If a file contains multiple certificates, only the first one will be read.
pub(super) fn get_certs_from_fs(paths: &[PathBuf]) -> Result<Vec<Cert>> {
    paths
        .iter()
        .map(|path| {
            Cert::from_bytes(
                fs::read(path)
                    .context(format!("failed to read file {}", path.to_string_lossy()))?,
            )
        })
        .collect()
}

/// Instruction of how a missing certificate should be handled when searching
/// for certificates by `QueryTerm` in the local keystore.
pub(super) enum NoMatchPolicy {
    /// Silently ignore any QueryTerm for which no matching certificate is
    /// found in the local keystore.
    SkipQueryTerm,
    /// Raise an error as soon as a QueryTerm returns no matching certificate.
    RaiseError,
}

/// Retrieves the OpenPGP certificates matching the specified `query_terms`
/// from the local CertStore.
///
/// Each query term must match exactly one cert, or an error will be raised.
fn get_certs_from_store(
    query_terms: &[String],
    cert_type: CertType,
    no_match_policy: NoMatchPolicy,
) -> Result<Vec<Cert>> {
    let store = CertStore::open(CertStoreOpts {
        read_only: true,
        allow_create: false,
        ..Default::default()
    })?;
    match no_match_policy {
        // Case 1: all query terms must return exactly 1 matching certificate.
        NoMatchPolicy::RaiseError => query_terms
            .iter()
            .map(|query_term| get_single_cert(&QueryTerm::from_str(query_term)?, cert_type, &store))
            .collect(),
        // Case 2: QueryTerms for which no matching certificate is found are
        // silently ignored.
        NoMatchPolicy::SkipQueryTerm => query_terms
            .iter()
            .filter_map(|query_term| {
                let query_term = match QueryTerm::from_str(query_term) {
                    Err(e) => return Some(Err(e)),
                    Ok(q) => q,
                };
                let r = get_single_cert(&query_term, cert_type, &store).ok()?;
                Some(Ok(r))
            })
            .collect::<Result<Vec<_>>>(),
    }
}

/// Retrieves OpenPGP certificates from files and/or from the local certstore.
///
/// Passing references to empty lists to the `paths` or `query_terms` arguments
/// has the effect of ignoring them.
///
/// # Arguments
///
/// * `paths`: list of paths to files containing OpenPGP certificates to load.
/// * `query_terms`: list of fingerprints/emails of the certificates to load
///   from the local CertStore.
/// * `cert_type`: the type of certificate to be retrieved.
/// * `no_match_policy`:
pub(super) fn get_certs(
    paths: &[PathBuf],
    query_terms: &[String],
    cert_type: CertType,
    no_match_policy: NoMatchPolicy,
) -> Result<Vec<Cert>> {
    let mut certs = get_certs_from_fs(paths)?;
    certs.append(&mut get_certs_from_store(
        query_terms,
        cert_type,
        no_match_policy,
    )?);
    Ok(certs)
}

/// Returns password for a OpenPGP key.
///
/// If not provided, the password can be loaded from an environment
/// variable, or interactively.
pub(super) fn get_pgp_password(password: Option<&String>) -> Result<Option<String>> {
    let password = match password {
        Some(password) => password.clone(),
        None => {
            if let Ok(password) = std::env::var("SETT_OPENPGP_KEY_PWD") {
                password
            } else if let Ok(password_file) = std::env::var("SETT_OPENPGP_KEY_PWD_FILE") {
                fs::read_to_string(&password_file)
                    .with_context(|| format!("password file '{password_file}' not found"))?
                    .trim()
                    .to_string()
            } else {
                rpassword::prompt_password("Please enter your OpenPGP private key password:")?
            }
        }
    };
    Ok((!password.is_empty()).then_some(password))
}
