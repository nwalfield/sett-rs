use std::path::PathBuf;

use anyhow::{Context, Result};
use clap::Args;
use sett::{
    certstore::{CertBytes, CertStore, CertStoreOpts, CertType, SerializationFormat},
    dpkg,
    task::Mode,
};

use super::common::{get_certs, get_certs_from_fs, get_pgp_password, NoMatchPolicy};

#[derive(Args)]
pub(crate) struct Decrypt {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Signer OpenPGP key file path (Optional).
    ///
    /// Path of file containing the signer's public key, used to verify the
    /// authenticity of the data package to decrypt. Only needed when the
    /// signer's key is not present in the local keystore.
    #[arg(short = 'S', long)]
    pub(super) signer_path: Option<PathBuf>,

    /// Recipient OpenPGP key file path (Optional).
    ///
    /// Path of file containing the recipient's private key, used to decrypt
    /// the data package. Only needed when the recipient's key is not present
    /// in the local keystore.
    #[arg(short = 'R', long)]
    recipient_path: Option<PathBuf>,

    /// Password of the recipient OpenPGP key.
    ///
    /// Password of the key to use for decrypting the data. If the key is not
    /// password protected, an empty string should be passed as password value.
    #[arg(short, long)]
    pub(super) password: Option<String>,

    /// Output directory.
    #[arg(short, long)]
    output: Option<PathBuf>,

    /// Decrypt the package without unpacking files.
    #[arg(short, long)]
    decrypt_only: bool,

    /// Path to the data package to decrypt.
    file: PathBuf,
}

impl crate::cmd::Command for Decrypt {
    fn run(&self) -> Result<()> {
        let _log_guard = crate::log::init_log(self.display.verbosity)?;
        let progress = (!self.display.quiet).then(crate::util::CliProgress::new);

        // Load the signer's OpenPGP certificate from file, if specified. This
        // has to be done first, as the certificate might be needed to verify
        // the metadata's signature.
        let signer_cert_from_path = self
            .signer_path
            .as_ref()
            .map(|path| {
                get_certs_from_fs(std::slice::from_ref(path))?
                    .pop()
                    .expect("Exactly 1 cert here, or get_certs_from_fs() returns error")
                    .serialize(CertType::Public, SerializationFormat::Binary)
            })
            .transpose()?;

        let metadata = read_metadata_from_file(&self.file, signer_cert_from_path.as_deref())?;

        // If the signer's OpenPGP certificate was not provided via a file,
        // load it from the local certstore by searching for a certificate
        // matching the fingerprint provided in the metadata.
        let signer = match signer_cert_from_path {
            Some(cert_from_path) => cert_from_path,
            None => get_certs(
                &[],
                std::slice::from_ref(&metadata.sender),
                CertType::Public,
                NoMatchPolicy::RaiseError,
            )?
            .pop()
            .expect("Exactly 1 cert at this point, or get_certs() returns error")
            .serialize(CertType::Public, SerializationFormat::Binary)?,
        };

        // Load the recipient(s) OpenPGP certificate(s), either from the local
        // certstore (the default), or from a user-specified file.
        let recipients = get_certs(
            self.recipient_path
                .as_ref()
                .map(std::slice::from_ref)
                .unwrap_or_default(),
            match self.recipient_path {
                Some(_) => None,
                None => Some(metadata.recipients),
            }
            .as_deref()
            .unwrap_or_default(),
            CertType::Secret,
            NoMatchPolicy::SkipQueryTerm,
        )?
        .iter()
        .map(|cert| cert.serialize(CertType::Secret, SerializationFormat::Binary))
        .collect::<Result<Vec<_>>>()?;

        let runtime = tokio::runtime::Builder::new_multi_thread()
            .enable_all()
            .build()
            .context("Failed to create tokio runtime")?;

        let status = runtime.block_on(sett::decrypt::decrypt(sett::decrypt::DecryptOpts {
            package: dpkg::Package::open(&self.file)?,
            recipients,
            signer,
            output: self.output.clone(),
            decrypt_only: self.decrypt_only,
            mode: if self.mode.check {
                Mode::Check
            } else {
                Mode::Run
            },
            password: get_pgp_password(self.password.as_ref())?.map(Into::into),
            progress,
        }))?;
        super::common::Task::Decrypt.print_status(&status);
        Ok(())
    }
}

/// Verifies that a file is a valid data package.
///
/// A valid package must have the expected structure and a valid metadata
/// signature.
///
/// If not provided via the `signer_cert` argument, the signer's public OpenPGP
/// is loaded from a local certstore.
fn read_metadata_from_file(
    path: &PathBuf,
    signer_cert: Option<&[u8]>,
) -> Result<dpkg::PkgMetadata> {
    let package = dpkg::Package::open(path)?;
    let verified = if let Some(signer_cert) = signer_cert {
        package.verify(&CertBytes::from(signer_cert))
    } else {
        let certstore = CertStore::open(CertStoreOpts {
            read_only: true,
            ..Default::default()
        })?;
        package.verify(&certstore)
    };
    verified
        .context("failed to verify package")?
        .metadata()
        .context("failed to read package metadata")
}
