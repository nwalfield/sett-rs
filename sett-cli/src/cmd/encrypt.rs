use std::path::{Path, PathBuf};

use anyhow::{ensure, Context, Ok, Result};
use chrono::Utc;
use clap::{Args, Subcommand, ValueEnum};
use sett::{
    certstore::{CertType, SerializationFormat},
    destination::{local::LocalOpts, s3::S3Opts, sftp::SftpOpts, Destination},
    task::Mode,
};

use super::common::{get_certs, get_pgp_password, get_portal_client, NoMatchPolicy};

#[derive(Subcommand)]
pub(crate) enum Encrypt {
    /// Encrypt and store data in the local file system.
    Local(Local),
    /// Encrypt and send data directly to an S3 bucket.
    S3(S3),
    #[cfg(feature = "auth")]
    /// Encrypt and send data directly to an S3 bucket with credentials fetched from Portal.
    S3Portal(S3Portal),
    /// Encrypt and send data directly to an SFTP server.
    Sftp(Sftp),
}

#[derive(Clone, Copy, ValueEnum, Debug)]
pub enum CompressionAlgorithm {
    /// No compression.
    Stored,
    /// Use gzip compression.
    Gzip,
    /// Use zstandard compression.
    Zstandard,
}

#[derive(Args)]
pub(crate) struct Local {
    #[command(flatten)]
    encryption: EncryptionArgs,

    /// Path or name of the encrypted data package.
    #[arg(short, long)]
    output: Option<PathBuf>,
}

#[derive(Args)]
pub(crate) struct S3 {
    #[command(flatten)]
    encryption: EncryptionArgs,

    #[command(flatten)]
    s3: super::common::S3Args,
}

#[cfg(feature = "auth")]
#[derive(Args)]
pub(crate) struct S3Portal {
    #[command(flatten)]
    encryption: EncryptionArgs,
}

#[derive(Args)]
pub(crate) struct Sftp {
    #[command(flatten)]
    encryption: EncryptionArgs,

    #[command(flatten)]
    sftp: super::common::SftpArgs,
}

#[derive(Args)]
struct EncryptionArgs {
    #[command(flatten)]
    display: super::common::DisplayArgs,

    #[command(flatten)]
    mode: super::common::ModeArgs,

    /// Signer OpenPGP key identifier.
    ///
    /// Fingerprint or email address of the private key with which to sign the
    /// data (usually this is the key of the data sender). The signer's key
    /// must be present in the local keystore. Mutually exclusive with
    /// `--signer-path`.
    #[arg(
        short,
        long,
        conflicts_with = "signer_path",
        required_unless_present = "signer_path",
        value_name = "SIGNER_KEY"
    )]
    signer: Option<String>,

    /// Signer OpenPGP key file path.
    ///
    /// Path of file containing the data signer's private key. This argument
    /// is an alternative to `--signer`, allowing to specify the signer's key
    /// as a file on disk. Mutually exclusive with `--signer`.
    #[arg(short = 'S', long, required_unless_present = "signer")]
    pub(super) signer_path: Option<PathBuf>,

    /// Recipient OpenPGP key identifier.
    ///
    /// Fingerprint or email of the public key for which to encrypt the data.
    /// More than one recipient can be specified by passing this argument
    /// multiple times. The recipient's key must be present in the local
    /// keystore. At least one of `--recipient` or `--recipient-path` must
    /// be given.
    #[arg(
        short,
        long,
        required_unless_present = "recipient_path",
        value_name = "RECIPIENT_KEY"
    )]
    recipient: Option<Vec<String>>,

    /// Recipient OpenPGP key file path.
    ///
    /// Path of file containing a public key for which to encrypt the data.
    /// This argument is an alternative to `--recipient`, allowing to specify a
    /// recipient's key as a file on disk.
    /// More than one recipient can be specified by passing this argument
    /// multiple times. At least one of `--recipient` or `--recipient-path`
    /// must be given.
    #[arg(short = 'R', long, required_unless_present = "recipient")]
    recipient_path: Option<Vec<PathBuf>>,

    /// Password of the signer OpenPGP key (Optional).
    ///
    /// Password of the key to use for signing the encrypted data. If the key
    /// is not password protected, an empty string should be passed as
    /// password value.
    #[arg(short, long)]
    pub(super) password: Option<String>,

    /// Compression algorithm.
    #[arg(long, default_value = "zstandard")]
    compression_algorithm: CompressionAlgorithm,

    /// Compression level.
    ///
    /// Possible values depend on the selected compression
    /// algorithm:
    /// - zstandard: 1 (lowest compression, fastest) - 21 (highest compression,
    ///   slowest), default: 3.
    /// - gzip: 1 (lowest compression, fastest) - 9 (highest compression,
    ///   slowest), default: 6.
    /// - stored: not applicable.
    #[arg(long)]
    compression_level: Option<u8>,

    /// Data Transfer Request (DTR) identifier (required for s3-portal).
    #[arg(long)]
    dtr: Option<u32>,

    /// Perform an additional data package verification using Portal.
    #[arg(long, requires = "dtr")]
    verify: bool,

    /// File(s) and/or directory(ies) to encrypt and package.
    file: Vec<PathBuf>,
}

/// Converts a `CompressionAlgorithm` enum that has no associated compression
/// level, to a `sett::dpkg::CompressionAlgorithm` that has the specified
/// compression `level` associated to it.
fn compression_algo_with_level(
    algo: CompressionAlgorithm,
    level: Option<u8>,
) -> Result<sett::dpkg::CompressionAlgorithm> {
    macro_rules! assert_in_range {
        ($level:expr, $max:expr) => {
            $level
                .map(|l| {
                    ensure!(
                        (1..$max + 1).contains(&l),
                        format!(
                            "Compression level is outside of the allowed range (1-{})",
                            $max
                        )
                    );
                    Ok(l)
                })
                .transpose()?
        };
    }
    Ok(match algo {
        CompressionAlgorithm::Stored => sett::dpkg::CompressionAlgorithm::Stored,
        CompressionAlgorithm::Gzip => {
            sett::dpkg::CompressionAlgorithm::Gzip(assert_in_range!(level, 9).map(|l| l as u32))
        }
        CompressionAlgorithm::Zstandard => sett::dpkg::CompressionAlgorithm::Zstandard(
            assert_in_range!(level, 21).map(|l| l as i32),
        ),
    })
}

impl crate::cmd::Command for Encrypt {
    fn run(&self) -> Result<()> {
        match self {
            Self::Local(c) => c.run(),
            Self::S3(c) => c.run(),
            #[cfg(feature = "auth")]
            Self::S3Portal(c) => c.run(),
            Self::Sftp(c) => c.run(),
        }
    }
}

trait EncryptSubcommand {
    fn destination(&self) -> Result<Destination<'static>>;
    fn args(&self) -> &EncryptionArgs;
}

impl EncryptSubcommand for Local {
    fn destination(&self) -> Result<Destination<'static>> {
        Ok(if let Some(output) = &self.output {
            {
                let (path, name) = parse_output_path(output)?;
                ensure!(
                    !self.encryption.verify || name.is_none(),
                    "Custom package name is not allowed with --verify"
                );
                let opts = LocalOpts { path, name };
                Destination::Local(opts)
            }
        } else {
            Destination::Stdout
        })
    }

    fn args(&self) -> &EncryptionArgs {
        &self.encryption
    }
}

impl EncryptSubcommand for S3 {
    fn destination(&self) -> Result<Destination<'static>> {
        Ok(Destination::from(S3Opts::from(self.s3.clone())))
    }

    fn args(&self) -> &EncryptionArgs {
        &self.encryption
    }
}

#[cfg(feature = "auth")]
impl EncryptSubcommand for S3Portal {
    fn destination(&self) -> Result<Destination<'static>> {
        let dtr = self.encryption.dtr.context("missing DTR")?;
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .context("Failed to create tokio runtime")?;
        let portal_client = get_portal_client()?;
        let oidc_client = super::common::get_oidc_client()?;
        let token =
            runtime.block_on(oidc_client.authenticate(Box::new(super::common::auth_handler)))?;
        let s3opts =
            runtime.block_on(portal_client.get_s3_credentials(dtr, &token.access_token))?;
        Ok(Destination::S3(s3opts))
    }

    fn args(&self) -> &EncryptionArgs {
        &self.encryption
    }
}

impl EncryptSubcommand for Sftp {
    fn destination(&self) -> Result<Destination<'static>> {
        Ok(Destination::from(SftpOpts::from(self.sftp.clone())))
    }

    fn args(&self) -> &EncryptionArgs {
        &self.encryption
    }
}

fn run_encrypt_subcommand(subcommand: &impl EncryptSubcommand) -> Result<()> {
    let args = subcommand.args();
    let _log_guard = crate::log::init_log(args.display.verbosity)?;
    let timestamp = Utc::now();
    let compression_algorithm =
        compression_algo_with_level(args.compression_algorithm, args.compression_level)?;

    // Load sender and recipient(s) OpenPGP certificates, either from the
    // local certstore (the default), or from a user-specified file.
    let signer = get_certs(
        args.signer_path
            .as_ref()
            .map(std::slice::from_ref)
            .unwrap_or_default(),
        args.signer
            .as_ref()
            .map(std::slice::from_ref)
            .unwrap_or_default(),
        CertType::Secret,
        NoMatchPolicy::RaiseError,
    )?
    .pop()
    .expect("Exactly 1 cert at this point, or get_certs() returns error");
    let signer_fingerprint = signer.fingerprint().to_hex();
    let signer_serialized = signer.serialize(CertType::Secret, SerializationFormat::Binary)?;

    let recipients = get_certs(
        args.recipient_path.as_deref().unwrap_or_default(),
        args.recipient.as_deref().unwrap_or_default(),
        CertType::Public,
        NoMatchPolicy::RaiseError,
    )?;
    let recipients_fingerprints = recipients
        .iter()
        .map(|r| r.fingerprint().to_hex())
        .collect::<Vec<_>>();
    let recipients_serialized = recipients
        .iter()
        .map(|cert| cert.serialize(CertType::Public, SerializationFormat::Binary))
        .collect::<Result<Vec<_>>>()?;

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("Failed to create tokio runtime")?;

    // If the user requested verification from Portal (this requires that a
    // DTR ID was provided), check the data package's metadata and use the
    // projects "code" as prefix for the name of the generated data package.
    let prefix = args
        .verify
        .then(|| {
            let metadata = sett::dpkg::PkgMetadata {
                sender: signer_fingerprint,
                recipients: recipients_fingerprints,
                timestamp,
                compression_algorithm: compression_algorithm.clone(),
                transfer_id: args.dtr,
                ..Default::default()
            };
            Ok(runtime
                .block_on(get_portal_client()?.check_package(&metadata, "missing"))?
                .project_code)
        })
        .transpose()?;

    // Package, encrypt, and optionally transfer, the data.
    let encrypt_opts = sett::encrypt::EncryptOpts {
        files: args.file.clone(),
        recipients: recipients_serialized,
        signer: Some(signer_serialized),
        max_cpu: None,
        mode: if args.mode.check {
            Mode::Check
        } else {
            Mode::Run
        },
        purpose: None,
        transfer_id: args.dtr,
        compression_algorithm,
        password: get_pgp_password(args.password.as_ref())?.map(Into::into),
        progress: (!args.display.quiet).then(crate::util::CliProgress::new),
        timestamp,
        prefix,
    };
    let dest = subcommand.destination()?;
    let status = runtime.block_on(sett::encrypt::encrypt(encrypt_opts, dest))?;
    super::common::Task::Encrypt.print_status(&status);
    Ok(())
}

impl crate::cmd::Command for Local {
    fn run(&self) -> Result<()> {
        run_encrypt_subcommand(self)
    }
}

impl crate::cmd::Command for S3 {
    fn run(&self) -> Result<()> {
        run_encrypt_subcommand(self)
    }
}

#[cfg(feature = "auth")]
impl crate::cmd::Command for S3Portal {
    fn run(&self) -> Result<()> {
        run_encrypt_subcommand(self)
    }
}

impl crate::cmd::Command for Sftp {
    fn run(&self) -> Result<()> {
        run_encrypt_subcommand(self)
    }
}

fn to_absolute_path(path: &Path) -> Result<PathBuf> {
    Ok(if path.is_relative() {
        std::env::current_dir()?.join(path)
    } else {
        path.to_path_buf()
    })
}

fn parse_output_path(path: &Path) -> Result<(PathBuf, Option<String>)> {
    let path = to_absolute_path(path)?;
    Ok(if path.is_dir() {
        (path.clone(), None)
    } else {
        let d = path
            .parent()
            .context("Unable to find directory for the given output")?
            .to_path_buf();
        ensure!(d.exists(), "Output directory does not exist {:?})", d);
        let mut f = path
            .file_name()
            .expect("There is always a file at this point")
            .to_string_lossy()
            .to_string();
        if !f.ends_with(".zip") {
            f.push_str(".zip");
        }
        (d, Some(f))
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_output_path() -> Result<()> {
        assert!(parse_output_path(Path::new("does/not/exist")).is_err());
        let tmp = std::env::temp_dir();
        assert_eq!(parse_output_path(&tmp)?, (tmp.clone(), None));
        assert_eq!(
            parse_output_path(&tmp.join("package.zip"))?,
            (tmp.clone(), Some("package.zip".to_owned()))
        );
        assert_eq!(
            parse_output_path(&tmp.join("package.foo"))?,
            (tmp, Some("package.foo.zip".to_owned()))
        );
        Ok(())
    }
}
