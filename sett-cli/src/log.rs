use anyhow::Context;
use tracing::{level_filters::LevelFilter, Event, Subscriber};
use tracing_appender::non_blocking::WorkerGuard;
use tracing_subscriber::{filter::filter_fn, fmt, prelude::*, EnvFilter, Layer, Registry};

pub(crate) fn init_log(verbosity: u8) -> anyhow::Result<WorkerGuard> {
    let stderr_log = fmt::layer()
        .with_writer(std::io::stderr)
        .event_format(ConsoleLogFormatter)
        .with_filter(match verbosity {
            0 => LevelFilter::WARN,
            1 => LevelFilter::INFO,
            2 => LevelFilter::DEBUG,
            _ => LevelFilter::TRACE,
        })
        .with_filter(filter_fn(move |m| {
            if let Some(path) = m.module_path() {
                // The `aws_smithy_runtime` crate is too verbose at INFO level
                return !(path.starts_with("aws_smithy_runtime") && verbosity < 2);
            }
            true
        }));

    let (non_blocking_file_writer, non_blocking_file_writer_guard) =
        tracing_appender::non_blocking(tracing_appender::rolling::daily(
            dirs::data_dir()
                .context("failed to get data directory")?
                .join("ch.biomedit.sett")
                .join("log"),
            "sett.log",
        ));
    let file_log = fmt::layer()
        .with_writer(non_blocking_file_writer)
        .json()
        .with_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::DEBUG.into())
                .with_regex(false)
                .from_env()
                .context("failed to parse log filter")?,
        );

    let subscriber = Registry::default().with(stderr_log).with(file_log);
    tracing::subscriber::set_global_default(subscriber)?;
    Ok(non_blocking_file_writer_guard)
}

// TODO: currently `tracing_subscriber::fmt::format::Format` formatters don't
// allow hiding event scope arguments. Once they do, we should be able to get
// rid of this custom formatter (see, e.g.,
// https://github.com/tokio-rs/tracing/issues/2435)
struct ConsoleLogFormatter;

impl<S, N> fmt::FormatEvent<S, N> for ConsoleLogFormatter
where
    S: Subscriber + for<'a> tracing_subscriber::registry::LookupSpan<'a>,
    N: for<'a> fmt::FormatFields<'a> + 'static,
{
    fn format_event(
        &self,
        ctx: &fmt::FmtContext<'_, S, N>,
        mut writer: fmt::format::Writer<'_>,
        event: &Event<'_>,
    ) -> std::fmt::Result {
        let level = event.metadata().level();
        let style = match *level {
            tracing::Level::ERROR => console::Style::new().red(),
            tracing::Level::WARN => console::Style::new().yellow(),
            tracing::Level::INFO => console::Style::new().green(),
            tracing::Level::DEBUG => console::Style::new().blue(),
            tracing::Level::TRACE => console::Style::new().cyan(),
        };
        write!(&mut writer, "{:>5} ", style.apply_to(level))?;
        if let Some(scope) = ctx.event_scope() {
            for span in scope.from_root() {
                write!(writer, "{}: ", span.name())?;
            }
        }
        ctx.field_format().format_fields(writer.by_ref(), event)?;
        writeln!(writer)
    }
}
