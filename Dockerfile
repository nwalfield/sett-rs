FROM docker.io/library/ubuntu:24.04

ARG binary=/target/release/sett

COPY ${binary} /usr/local/bin/sett
RUN chmod +x /usr/local/bin/sett

RUN apt-get update \
  && apt-get install -y ca-certificates \
  && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["sett"]
CMD ["--help"]
