# sett-rs Python bindings

Building sett-rs Python bindings requires the
[Rust toolchain](https://www.rust-lang.org/tools/install) and
[maturin](https://github.com/PyO3/maturin).

## Development

Build and install directly in the current virtual env
(run `maturin` in the directory where this `README.md` file is located).

```shell
maturin develop --release
```

Windows builds require non-default features.

```shell
maturin develop --release --no-default-features --features=crypto-cng
```

## Production

See the GitLab CI file for the platform-specific instructions on building
production-ready Python wheels.
