//! Python bindings for a subset of the sett library

use pyo3::{prelude::*, wrap_pyfunction};

mod cert;
mod certstore;
mod gnupg;

/// Verify the (detached) signature of the metadata file in a `.zip` data
/// package.
/// Returns `None` if a valid signature matching `signer_cert` is found, and
/// raises an error otherwise.
///
/// # Arguments
///
/// * `data_pkg_path`: path of the `.zip` data package to check.
/// * `signer_cert`: public certificate of the data signer.
#[pyfunction]
pub(crate) fn verify_metadata_signature(
    _py: Python,
    data_pkg_path: &str,
    signer_cert: &[u8],
) -> PyResult<()> {
    sett::dpkg::Package::open(data_pkg_path)?
        .verify(&Into::<sett::certstore::CertBytes>::into(signer_cert))?;
    Ok(())
}

#[pymodule]
fn _sett_rs(py: Python, m: &Bound<'_, PyModule>) -> PyResult<()> {
    pyo3_log::init();
    m.add(
        "CertParsingError",
        py.get_type_bound::<cert::CertParsingError>(),
    )?;
    m.add_class::<cert::CertInfo>()?;
    m.add_class::<cert::Key>()?;
    m.add_class::<cert::KeyType>()?;
    m.add_class::<cert::UserID>()?;
    m.add_class::<cert::Validity>()?;
    m.add_class::<certstore::CertStore>()?;
    m.add_class::<certstore::CertType>()?;
    m.add_class::<certstore::RevocationReason>()?;
    m.add_function(wrap_pyfunction!(certstore::create_revocation_signature, m)?)?;
    m.add_function(wrap_pyfunction!(certstore::generate_cert, m)?)?;
    m.add_function(wrap_pyfunction!(gnupg::gnupg_extract_cert, m)?)?;
    m.add_function(wrap_pyfunction!(gnupg::gnupg_list_certs, m)?)?;
    m.add_function(wrap_pyfunction!(gnupg::gnupg_list_certs_with_userid, m)?)?;
    m.add_function(wrap_pyfunction!(verify_metadata_signature, m)?)?;
    Ok(())
}
