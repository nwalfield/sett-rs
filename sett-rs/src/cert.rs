use anyhow::Context;
use chrono::{DateTime, Utc};
use pyo3::{create_exception, prelude::*, types::PyType};
use sequoia_openpgp::{
    cert::{
        amalgamation::{
            ComponentAmalgamation, ValidAmalgamation, ValidComponentAmalgamation,
            ValidateAmalgamation,
        },
        prelude::{ErasedKeyAmalgamation, ValidErasedKeyAmalgamation},
        ValidCert,
    },
    packet::key::PublicParts,
    parse::Parse,
    policy::{Policy, StandardPolicy},
    types::RevocationStatus,
};

create_exception!(_sett_rs, CertParsingError, pyo3::exceptions::PyException);

#[pyclass(frozen)]
#[derive(Debug, Clone)]
pub(crate) enum KeyType {
    Public,
    Secret,
}

#[pyclass(frozen)]
#[derive(Debug, Clone)]
pub(crate) enum Validity {
    Expired,
    Invalid,
    Revoked,
    Unknown,
    Valid,
}

#[pyclass(frozen, get_all)]
#[derive(Debug, Clone)]
pub(crate) struct UserID {
    value: Vec<u8>,
    name: Option<String>,
    email: Option<String>,
    comment: Option<String>,
    validity: Validity,
    validity_info: Option<String>,
}

#[pymethods]
impl UserID {
    fn __str__(&self) -> String {
        String::from_utf8_lossy(&self.value).into_owned()
    }
}

#[pyclass(frozen, get_all)]
#[derive(Debug, Clone)]
pub(crate) struct Key {
    key_id: String,
    key_type: KeyType,
    fingerprint: String,
    length: Option<usize>,
    creation_date: DateTime<Utc>,
    expiration_date: Option<DateTime<Utc>>,
    pub_key_algorithm: u8,
    validity: Validity,
    validity_info: Option<String>,
}

#[pymethods]
impl Key {
    fn __str__(&self) -> String {
        format!("{:?}", self)
    }
}

#[pyclass(frozen, get_all)]
#[derive(Clone)]
pub(crate) struct CertInfo {
    key_id: String,
    fingerprint: String,
    uids: Vec<UserID>,
    validity: Validity,
    validity_info: Option<String>,
    keys: Vec<Key>,
}

#[pymethods]
impl CertInfo {
    #[getter]
    fn primary_key(&self) -> PyResult<Key> {
        Ok(self
            .keys
            .first()
            .context("Unable to find any keys in the certificate")?
            .clone())
    }

    #[getter]
    fn subkeys(&self) -> Vec<Key> {
        if self.keys.len() > 1 {
            self.keys[1..].to_vec()
        } else {
            Vec::with_capacity(0)
        }
    }

    #[getter]
    fn uid(&self) -> Option<UserID> {
        self.uids.first().cloned()
    }

    #[getter]
    fn email(&self) -> Option<String> {
        self.uid()?.email
    }

    fn __str__(&self) -> String {
        format!(
            "{} - {}",
            self.uids
                .first()
                .map(|uid| String::from_utf8_lossy(&uid.value))
                .unwrap_or(std::borrow::Cow::Borrowed("No user ID")),
            self.fingerprint
        )
    }

    /// Parse an input certificate and return its metadata as `CertInfo` object.
    #[classmethod]
    fn from_bytes(_cls: &Bound<'_, PyType>, data: &[u8]) -> PyResult<Self> {
        Ok(sequoia_openpgp::cert::Cert::from_bytes(data)
            .map_err(|e| {
                CertParsingError::new_err(format!(
                    "OpenPGP certificate parsing failed. Details: {e}"
                ))
            })?
            .into())
    }
}

impl From<sett::certstore::Cert> for CertInfo {
    fn from(cert: sett::certstore::Cert) -> Self {
        sequoia_openpgp::Cert::from(cert).into()
    }
}

impl From<sequoia_openpgp::cert::Cert> for CertInfo {
    fn from(cert: sequoia_openpgp::cert::Cert) -> Self {
        let policy = StandardPolicy::new();
        let (validity, validity_info) = validity(&cert.with_policy(&policy, None));
        CertInfo {
            fingerprint: cert.fingerprint().to_hex(),
            key_id: cert.keyid().to_hex(),
            validity,
            validity_info,
            uids: cert
                .userids()
                .map(|user_id| build_user_id(user_id, &policy))
                .collect(),
            keys: cert
                .keys()
                .map(|amalgamation| build_key(&amalgamation, &policy))
                .collect(),
        }
    }
}

trait ValidityHelper {
    fn alive(&self) -> anyhow::Result<()>;
    fn revocation(&self) -> RevocationStatus;
}

impl ValidityHelper for ValidCert<'_> {
    fn alive(&self) -> anyhow::Result<()> {
        self.alive()
    }

    fn revocation(&self) -> RevocationStatus {
        self.revocation_status()
    }
}

impl ValidityHelper for ValidErasedKeyAmalgamation<'_, PublicParts> {
    fn alive(&self) -> anyhow::Result<()> {
        self.alive()
    }

    fn revocation(&self) -> RevocationStatus {
        self.revocation_status()
    }
}

impl ValidityHelper for ValidComponentAmalgamation<'_, sequoia_openpgp::packet::UserID> {
    fn alive(&self) -> anyhow::Result<()> {
        Ok(())
    }

    fn revocation(&self) -> RevocationStatus {
        self.revocation_status()
    }
}

fn validity<T: ValidityHelper>(
    with_policy_result: &anyhow::Result<T>,
) -> (Validity, Option<String>) {
    match &with_policy_result {
        Ok(valid_cert) => {
            if let RevocationStatus::Revoked(revs) = valid_cert.revocation() {
                (
                    Validity::Revoked,
                    Some(
                        revs.iter()
                            .map(|r| {
                                r.reason_for_revocation().map_or_else(
                                    || String::from("Unspecified reason"),
                                    |(code, msg)| {
                                        format!("{code}: {}", String::from_utf8_lossy(msg))
                                    },
                                )
                            })
                            .collect(),
                    ),
                )
            } else if valid_cert.alive().is_ok() {
                (Validity::Valid, None)
            } else {
                (Validity::Expired, None)
            }
        }
        Err(e) => (Validity::Invalid, Some(format!("{e:#}"))),
    }
}

fn build_key(amalgamation: &ErasedKeyAmalgamation<PublicParts>, policy: &impl Policy) -> Key {
    let with_policy = amalgamation.clone().with_policy(policy, None);
    let (validity, validity_info) = validity(&with_policy);
    Key {
        key_id: amalgamation.keyid().to_hex(),
        key_type: if amalgamation.has_secret() {
            KeyType::Secret
        } else {
            KeyType::Public
        },
        fingerprint: amalgamation.fingerprint().to_hex(),
        length: amalgamation.mpis().bits(),
        creation_date: amalgamation.creation_time().into(),
        expiration_date: with_policy
            .as_ref()
            .map_or(None, |k| k.key_expiration_time().map(DateTime::<Utc>::from)),
        pub_key_algorithm: amalgamation.pk_algo().into(),
        validity,
        validity_info,
    }
}

fn build_user_id(
    user_id: ComponentAmalgamation<'_, sequoia_openpgp::packet::UserID>,
    policy: &impl Policy,
) -> UserID {
    let value = Vec::from(user_id.value());
    let name = user_id.name2().unwrap_or(None).map(String::from);
    let email = user_id.email2().unwrap_or(None).map(String::from);
    let comment = user_id.comment2().unwrap_or(None).map(String::from);
    let (validity, validity_info) = validity(&user_id.with_policy(policy, None));
    UserID {
        value,
        name,
        email,
        comment,
        validity,
        validity_info,
    }
}
