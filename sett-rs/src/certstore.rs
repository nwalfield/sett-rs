use std::{borrow::Cow, path::PathBuf};

use pyo3::{prelude::*, types::PyBytes};
use sequoia_openpgp::{
    packet::Packet,
    parse::{PacketParserBuilder, PacketParserResult, Parse},
    types::SignatureType,
    Fingerprint,
};
use sett::certstore::{
    self, Cert, CertStoreOpts, CipherSuite, QueryTerm, ReasonForRevocation, SerializationFormat,
};

#[pyfunction]
pub(crate) fn generate_cert(
    py: Python,
    uid: &str,
    password: &[u8],
) -> PyResult<(PyObject, PyObject)> {
    let (cert, rev) = certstore::generate_cert(uid, password, CipherSuite::Cv25519)?;
    Ok((
        PyBytes::new_bound(
            py,
            &cert.serialize(
                sett::certstore::CertType::Secret,
                SerializationFormat::AsciiArmored,
            )?,
        )
        .into(),
        PyBytes::new_bound(py, &rev).into(),
    ))
}

#[pyfunction]
pub(crate) fn create_revocation_signature(
    py: Python,
    cert: &[u8],
    reason: RevocationReason,
    message: &[u8],
    password: &[u8],
) -> PyResult<PyObject> {
    let cert = Cert::from_bytes(cert)?;
    let rev_sig = cert.generate_rev_sig(reason.into(), message, Some(password))?;
    Ok(PyBytes::new_bound(py, &rev_sig).into())
}

#[pyclass]
#[derive(Copy, Clone)]
pub(crate) enum CertType {
    Public,
    Secret,
}

impl From<CertType> for sett::certstore::CertType {
    fn from(cert_type: CertType) -> Self {
        match cert_type {
            CertType::Public => Self::Public,
            CertType::Secret => Self::Secret,
        }
    }
}

#[pyclass]
#[derive(Clone)]
pub(crate) enum RevocationReason {
    /// Private key material (may) have been compromised.
    Compromised,
    /// The key has been replaced by a new one. The message should
    /// contain the fingerprint of the new key.
    Superseded,
    /// This key should not be used anymore and there is no replacement
    /// key. This reason is appropriate e.g. when leaving an
    /// organization.
    Retired,
    /// None of the above reasons apply.
    /// Note: It is preferable to use a specific reason when possible.
    Unspecified,
}

impl From<RevocationReason> for ReasonForRevocation {
    fn from(reason: RevocationReason) -> Self {
        match reason {
            RevocationReason::Compromised => Self::KeyCompromised,
            RevocationReason::Superseded => Self::KeySuperseded,
            RevocationReason::Retired => Self::KeyRetired,
            RevocationReason::Unspecified => Self::Unspecified,
        }
    }
}

#[pyclass]
pub(crate) struct CertStore {
    pub_store_path: Option<PathBuf>,
    sec_store_path: Option<PathBuf>,
}

#[pymethods]
impl CertStore {
    #[new]
    fn new() -> Self {
        Self {
            pub_store_path: None,
            sec_store_path: None,
        }
    }

    pub(crate) fn import_cert(
        &self,
        data: &[u8],
        cert_type: CertType,
    ) -> PyResult<crate::cert::CertInfo> {
        // Open (or create) the local Certificate Store.
        let mut store = sett::certstore::CertStore::open(CertStoreOpts {
            pub_store_path: self.pub_store_path.as_deref(),
            sec_store_path: self.sec_store_path.as_deref(),
            ..Default::default()
        })?;

        // Add certificate to CertStore.
        Ok(Cert::from_bytes(data)
            .and_then(|cert| store.add_cert(&cert, cert_type.into()))?
            .into())
    }

    pub(crate) fn export_cert(
        &self,
        py: Python,
        fingerprint: &str,
        cert_type: CertType,
    ) -> PyResult<PyObject> {
        let store = sett::certstore::CertStore::open(CertStoreOpts {
            pub_store_path: self.pub_store_path.as_deref(),
            sec_store_path: self.sec_store_path.as_deref(),
            read_only: true,
            ..Default::default()
        })?;
        let cert = get_single_cert(&store, cert_type, &Fingerprint::from_hex(fingerprint)?)?;
        Ok(PyBytes::new_bound(
            py,
            &cert.serialize(cert_type.into(), SerializationFormat::AsciiArmored)?,
        )
        .into())
    }

    pub(crate) fn list_certs(&self, cert_type: CertType) -> PyResult<Vec<crate::cert::CertInfo>> {
        let store = sett::certstore::CertStore::open(CertStoreOpts {
            pub_store_path: self.pub_store_path.as_deref(),
            sec_store_path: self.sec_store_path.as_deref(),
            read_only: true,
            ..Default::default()
        })?;
        Ok(store
            .get_all_certs(cert_type.into())?
            .into_iter()
            .map(Into::into)
            .collect())
    }

    pub(crate) fn revoke(&self, rev_sig: &[u8]) -> PyResult<()> {
        let mut revocations = Vec::new();
        let mut ppr = PacketParserBuilder::from_bytes(rev_sig)?.build()?;
        while let PacketParserResult::Some(pp) = ppr {
            let (packet, next_ppr) = pp.recurse()?;
            ppr = next_ppr;

            if let Packet::Signature(sig) = packet {
                if matches!(
                    sig.typ(),
                    SignatureType::CertificationRevocation
                        | SignatureType::KeyRevocation
                        | SignatureType::SubkeyRevocation
                ) {
                    revocations.push(sig);
                }
            }
        }
        let signature = match revocations.len() {
            0 => Err(anyhow::anyhow!("No revocation signature found")),
            1 => Ok(revocations.pop().expect("There is one signature")),
            _ => Err(anyhow::anyhow!("More than one revocation signature found")),
        }?;
        let mut fps = signature.issuer_fingerprints().collect::<Vec<_>>();
        fps.sort();
        fps.dedup();
        let issuer_fp = match fps.len() {
            0 => Err(anyhow::anyhow!("No issuer fingerprint found")),
            1 => Ok(fps.pop().expect("There is one fingerprint")),
            _ => Err(anyhow::anyhow!("More than one issuer fingerprint found")),
        }?;
        let mut store = sett::certstore::CertStore::open(CertStoreOpts {
            pub_store_path: self.pub_store_path.as_deref(),
            sec_store_path: self.sec_store_path.as_deref(),
            allow_create: false,
            ..Default::default()
        })?;
        let cert = get_single_cert(&store, CertType::Public, issuer_fp)?;
        let revoked_cert = cert.revoke(rev_sig)?;
        store.add_cert(&revoked_cert, sett::certstore::CertType::Public)?;
        Ok(())
    }
}

fn get_single_cert(
    store: &sett::certstore::CertStore,
    cert_type: CertType,
    fingerprint: &Fingerprint,
) -> anyhow::Result<Cert> {
    Ok(store
        .get_cert(
            &QueryTerm::Fingerprint(Cow::Borrowed(fingerprint)),
            cert_type.into(),
        )?
        .pop()
        .expect("`get_store` returns an error if no certificate has been found"))
}
