//! Python bindings for the sett::gnupg functions.

use pyo3::{prelude::*, types::PyBytes};

use crate::certstore::CertType;

/// Extract the specified key from a GnuPG keyring.
#[pyfunction]
pub(crate) fn gnupg_extract_cert(
    py: Python,
    identifier: &str,
    cert_type: CertType,
    gpg_home: Option<String>,
) -> PyResult<PyObject> {
    Ok(PyBytes::new_bound(
        py,
        &sett::gnupg::export_key(identifier, cert_type.into(), None, gpg_home)?,
    )
    .into())
}

/// List fingerprints of keys present in the local GnuPG keyring.
#[pyfunction]
pub(crate) fn gnupg_list_certs(
    _py: Python,
    cert_type: CertType,
    gpg_home: Option<String>,
) -> PyResult<Vec<String>> {
    Ok(sett::gnupg::list_keys(cert_type.into(), gpg_home)?
        .into_iter()
        .map(|cert| cert.fingerprint)
        .collect())
}

/// List fingerprints and userID of keys present in the local GnuPG keyring.
#[pyfunction]
pub(crate) fn gnupg_list_certs_with_userid(
    _py: Python,
    cert_type: CertType,
    gpg_home: Option<String>,
) -> PyResult<Vec<String>> {
    Ok(sett::gnupg::list_keys(cert_type.into(), gpg_home)?
        .iter()
        .map(|cert| {
            format!(
                "{}{}{}",
                cert.fingerprint,
                if cert.userid.is_some() { " " } else { "" },
                cert.userid.as_deref().unwrap_or("")
            )
        })
        .collect())
}
