# Changelog

All notable changes to this project will be documented in this file.

## [0.7.1](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E7%2E1) - 2024-02-29

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E7%2E0...sett%2Drs%2F0%2E7%2E1)


### 🐞 Bug Fixes

- `upload` method not working for S3 destination ([ec4c75e](https://gitlab.com/biomedit/sett-rs/commit/ec4c75e7c9ef3cb4ebaaefe60d03ba2c73de9c63)), Closes #179

### 🧱 Build system and dependencies

- Increase MSRV from 1.70 to 1.72 ([694f68a](https://gitlab.com/biomedit/sett-rs/commit/694f68ab85d29b7256f2a378afa5c36b901d025f))

## [0.7.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E7%2E0) - 2024-01-31

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E7%2E0...sett%2Drs%2F0%2E7%2E0)

### ⚠ BREAKING CHANGES

- `region` and `endpoint` are now optional.
  When `endpoint` is not specified, then the official public AWS endpoint will be used.
- remove `end-relax`

### ✨ Features

- Make `region` and `endpoint` optional ([97174a8](https://gitlab.com/biomedit/sett-rs/commit/97174a864a4dcc42ed224f2df275ef2c1c7f867b)), ⚠ BREAKING CHANGE
- **cert:** Remove `end-relax` ([b209392](https://gitlab.com/biomedit/sett-rs/commit/b209392f9bc6f3e35f889ad568d5972f2c4f7bef)), Close #137

### 🐞 Bug Fixes

- **CertInfo:** Fix type annotations ([1ef22a3](https://gitlab.com/biomedit/sett-rs/commit/1ef22a3fdbee0db77ad48810bd7d62758492918d))
- **cert:** Fix type conversion ([df75077](https://gitlab.com/biomedit/sett-rs/commit/df75077df46ca7e64bca00d40cb95b5029aacccd))

### 🧱 Build system and dependencies

- Define `log` crate version in crate dependencies ([c25b12f](https://gitlab.com/biomedit/sett-rs/commit/c25b12f39191fcb7c163f6cf89bdac4ba4565671))
- Specify patch version for `pyo3` to fix CI/CD ([7c8f216](https://gitlab.com/biomedit/sett-rs/commit/7c8f21649670f76c3c47ccf3ee3a41c75df592dd))
- Update maturin ([d3f8083](https://gitlab.com/biomedit/sett-rs/commit/d3f8083fd4105358fa10d14f321f86770fb8f196))

### 👷 CI

- **ruff:** Include examples ([f3ca5fe](https://gitlab.com/biomedit/sett-rs/commit/f3ca5feccd45ba673d4942f25275bddc7b44ca46))
- Replace `black` with `ruff format` for Python autoformatting ([06cf121](https://gitlab.com/biomedit/sett-rs/commit/06cf121e43c65bd8d94899c3d8c4746bbf8d27a1)), Close #101

### 🧹 Refactoring

- Change the terms certificate to key and secret to private ([c77fed5](https://gitlab.com/biomedit/sett-rs/commit/c77fed50f5e4a1f9f0d0cc73c5e0fc918429e458)), Closes #156
- Use PyO3 `anyhow` feature for easy error handling ([21ddfdf](https://gitlab.com/biomedit/sett-rs/commit/21ddfdf41011d5a2e9122fb6cbb8952f55af0258))
- Use `sett::dpgk::Package` for data package verification ([298ce2d](https://gitlab.com/biomedit/sett-rs/commit/298ce2d8a418a2ea6b26e0abf1aa46816833fc82))
- **certstore:** Use the new sett revocation types ([b470270](https://gitlab.com/biomedit/sett-rs/commit/b4702707bb4ed20a3b97c1317b0bc213db363298))
- **sett:** Update code for changes in sequoia-openpgp 1.17 ([90d8dd4](https://gitlab.com/biomedit/sett-rs/commit/90d8dd4eab6e3d3f065757457ebdd9031659381b))
- Use the new sett API ([60df5a4](https://gitlab.com/biomedit/sett-rs/commit/60df5a4045efa4647a91e8f36313b73ddc1cea7b))
- **destination:** Use the new `Cow`-based fields ([9951d99](https://gitlab.com/biomedit/sett-rs/commit/9951d99cffc6adf18e033be50fadc5e3fc2c7102))

### ✅ Test

- Add extra Python lint rules ([049c1a7](https://gitlab.com/biomedit/sett-rs/commit/049c1a7b5d26a66a2898ba4050e1bee586921bf8))
- Use default_factory instead of a mutable variable ([2b38f54](https://gitlab.com/biomedit/sett-rs/commit/2b38f54c69f59c7be482cd4aa259926ee6dc8758))

## [0.6.1](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E6%2E1) - 2023-10-04

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E6%2E1...sett%2Drs%2F0%2E6%2E1)


### ✨ Features

- **python-bindings:** Add verify_metadata_signature function to python bindings ([c059434](https://gitlab.com/biomedit/sett-rs/commit/c0594345f9aff0f96596305fedf66f568a1a17bc)), Closes #92

## [0.6.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E6%2E0) - 2023-09-11

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E6%2E0...sett%2Drs%2F0%2E6%2E0)

### ⚠ BREAKING CHANGES

- the following functions now take a CertType instead
    of a boolean as input argument: CertStore.import_cert(), and
    in the python bindings: gnupg_extract_cert(), gnupg_list_certs()
    and gnupg_list_certs_with_userid().

### 🐛 Bug Fixes

- **certstore:** Use CertType enum for import_cert argument ([41f7b2b](https://gitlab.com/biomedit/sett-rs/commit/41f7b2b861e06c77d95707bc4ad8efff702191ed)), ⚠ BREAKING CHANGE, Closes #86

## [0.5.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E5%2E0) - 2023-09-06

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2F0%2E2%2E2...sett%2Drs%2F0%2E5%2E0)

### ⚠ BREAKING CHANGES

- Changed package structure.
  The package is organized into two main modules: `workflow` and `cert`.
  Additionally some functions/classes have been changed and/or renamed,
  for example `Cert` -> `CertInfo.`
- heavily refactor parameters for encryption and destination.

  - CLI: Rename `Sftp` property `path` to `base_path`.
  - CLI: Remove `Encrypt` property `output`. You should now use destination
    specific properties.
  - Python bindings: Rename `SftpOpts` property `destination_dir` to
    `base_path`.
  - Python bindings: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - Python bindings: Remove `S3Opts` from `encrypt` argument `remote`'s typing
    alternatives.
  - encrypt: Move `Destination` to a separate `destination` module.
  - encrypt: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - transfer: Rename module to `destination`.
  - transfer/s3: Remove `S3Opts` generic `P`.
  - transfer/s3: Remove `S3Opts` property `files`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `S3Opts` property `progress`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `upload` generic `P`.
  - transfer/sftp: Rename `Remote` to `SftpOpts`.
  - transfer/sftp: Remove `SftpOpts` generic `Cb`.
  - transfer/sftp: Rename `SftpOpts` property `path` to `base_path`.
  - transfer/sftp: Change the type of `SftpOpts` property `base_path` from `str`
    to `PathBuf`.
  - transfer/sftp: Remove `SftpOpts` implementation for `connect`. You should
    now use the module's level `connect` function.
- group encryption and decryption option into objects.

  - `encrypt` function signature has changed.
  - `decrypt` function signature has changed.
- compression is now defined by both an algorithm and a level
- - `Cert.uid` used to be of type `UserID` and is now of type `Optional[UserID]`
- `EncryptOpts` field types changed
- `sftp_upload` replaced by more general `transfer`
- `utils::Progress` is now a trait

Progress bar implementations should now implement the `Progress` trait.
- `anyhow::Error` replaces `sett::error::Error`

### ♻️ Refactoring

- **sett-rs:** Fix typo in get_single_cert error msg ([56dabc6](https://gitlab.com/biomedit/sett-rs/commit/56dabc6bf1e62913f8997cbed63917e61c9b6f5e))
- **certstore:** Removed error handling based on error message ([952ee85](https://gitlab.com/biomedit/sett-rs/commit/952ee85e10a0075bf2f0220cd96f2a03d504b076)), Closes #56
- **sett-rs:** Use pyclass get_all parameter where possible ([0b1d9dd](https://gitlab.com/biomedit/sett-rs/commit/0b1d9dd9bfdb0073d82045ae68bc4bf12cfbf48a))
- Improve naming, separation of concerns and add local as a destination variant ([fee95f7](https://gitlab.com/biomedit/sett-rs/commit/fee95f79c4cb883d41a01e8774f2d4c071d437a6)), ⚠ BREAKING CHANGE
- **sett-rs:** Group encrypt and decrypt parameters into opts objects ([d61858c](https://gitlab.com/biomedit/sett-rs/commit/d61858cdc2d90a1bbee9447f1bf7f8fcf1a36e11)), Close #43, ⚠ BREAKING CHANGE

### ⚡️ Performance

- Add zstandard (zstd) as a new compression algorithm ([fcac89d](https://gitlab.com/biomedit/sett-rs/commit/fcac89d1623eea441187c482f8030b7c7736e952)), ⚠ BREAKING CHANGE, Close #58

### ✨ Features

- **sett-rs:** Add gnupg list and extract keys functions to python package ([7d798cd](https://gitlab.com/biomedit/sett-rs/commit/7d798cd8eadbd14183f4c2731d598fda67373a41))
- Add support for encryption streaming directly to S3 object ([2c9a3a1](https://gitlab.com/biomedit/sett-rs/commit/2c9a3a1b6e79551fa77335c35cdcfb6491b65e3c)), Closes #62
- **sett/certstore:** Support adding a keyserver as one of the certstore backends ([402a0bc](https://gitlab.com/biomedit/sett-rs/commit/402a0bc167f5eec6b377a7fd093ca21064a5367c))
- Add certstore bindings ([7c46b5b](https://gitlab.com/biomedit/sett-rs/commit/7c46b5b03fc20d9657312d5643da0591306fb638)), Close #59, ⚠ BREAKING CHANGE
- **pgp:** Support SHA-1 keys until given relax date ([4031efb](https://gitlab.com/biomedit/sett-rs/commit/4031efbce6548c4ecbf00022ab02fda679d9235b)), Closes #54
- **s3:** Add support for STS `session_token` ([86d5927](https://gitlab.com/biomedit/sett-rs/commit/86d5927f9348d3fbe28ffea9fff77589a2cc5963)), Closes #47
- **sett-rs:** Improve python bindings ([9371b3a](https://gitlab.com/biomedit/sett-rs/commit/9371b3a33c4cba7f1555b1ff0aa35d445e22dc38)), ⚠ BREAKING CHANGE, Close #32
- **cli:** Make the CLI arguments more consistent ([0dcdb84](https://gitlab.com/biomedit/sett-rs/commit/0dcdb84f8e4ff1d568c4ad12fd4249a009f4b5e1)), ⚠ BREAKING CHANGE
- **s3:** Add support for s3 data transfers ([987ad22](https://gitlab.com/biomedit/sett-rs/commit/987ad22893949604152af1742eacc4f1bac27f71)), ⚠ BREAKING CHANGE
- Replace a concrete implementation of progress with a trait ([9879642](https://gitlab.com/biomedit/sett-rs/commit/9879642e6488a4cc7701c03cb87610b1b9d4a562)), ⚠ BREAKING CHANGE
- **error:** Use `anyhow` for error handling ([71124fd](https://gitlab.com/biomedit/sett-rs/commit/71124fdc15c426c05d7c5fed104a292dd194ef77)), ⚠ BREAKING CHANGE

### 🎨 Style

- Fix clippy warnings (rust 1.67.0) ([39409bc](https://gitlab.com/biomedit/sett-rs/commit/39409bcb2593c475659995b9e746cbe8aa7cc178))

### 🐛 Bug Fixes

- **sett-rs:** Fix pyo3 0.18.0 deprecation warnings ([ffb0140](https://gitlab.com/biomedit/sett-rs/commit/ffb01402318078666cea2a44e87525a445b9ee39))

### 👷 CI

- Pin python dev tool versions ([2fe974a](https://gitlab.com/biomedit/sett-rs/commit/2fe974a6a4f91a314702355f0d84bc396b077190))

### 🧱 Build system and dependencies

- **python/lint:** Replace pylint with ruff ([b2bc820](https://gitlab.com/biomedit/sett-rs/commit/b2bc820b6d9f75cbbff394e127c5794c5622eb40))
- Use openssl as the default backend ([0c3d3da](https://gitlab.com/biomedit/sett-rs/commit/0c3d3dad7740a7ec2d1b7bba757ea23ddf67e649)), Close #57
- Decouple sett and sett-rs release schedule ([0a24dc2](https://gitlab.com/biomedit/sett-rs/commit/0a24dc2c26566207c75657633c8c96648977cf30)), Close #48
- **sequoia:** Add optional openssl backend ([68c8c76](https://gitlab.com/biomedit/sett-rs/commit/68c8c76ed5eb4c4dc923afa980449c3f03b2ad66))

## [0.4.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E4%2E0) - 2023-06-07

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E3%2E0...sett%2Drs%2F0%2E4%2E0)

### ⚠ BREAKING CHANGES

- Changed package structure.
  The package is organized into two main modules: `workflow` and `cert`.
  Additionally some functions/classes have been changed and/or renamed,
  for example `Cert` -> `CertInfo.`
- heavily refactor parameters for encryption and destination.

  - CLI: Rename `Sftp` property `path` to `base_path`.
  - CLI: Remove `Encrypt` property `output`. You should now use destination
    specific properties.
  - Python bindings: Rename `SftpOpts` property `destination_dir` to
    `base_path`.
  - Python bindings: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - Python bindings: Remove `S3Opts` from `encrypt` argument `remote`'s typing
    alternatives.
  - encrypt: Move `Destination` to a separate `destination` module.
  - encrypt: Remove `EncryptOpts` property `output`. You should now use
    destination specific properties.
  - transfer: Rename module to `destination`.
  - transfer/s3: Remove `S3Opts` generic `P`.
  - transfer/s3: Remove `S3Opts` property `files`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `S3Opts` property `progress`. You should now pass it
    directly to the `upload` function.
  - transfer/s3: Remove `upload` generic `P`.
  - transfer/sftp: Rename `Remote` to `SftpOpts`.
  - transfer/sftp: Remove `SftpOpts` generic `Cb`.
  - transfer/sftp: Rename `SftpOpts` property `path` to `base_path`.
  - transfer/sftp: Change the type of `SftpOpts` property `base_path` from `str`
    to `PathBuf`.
  - transfer/sftp: Remove `SftpOpts` implementation for `connect`. You should
    now use the module's level `connect` function.
- group encryption and decryption option into objects.

  - `encrypt` function signature has changed.
  - `decrypt` function signature has changed.
- compression is now defined by both an algorithm and a level

### Features

- Add certstore bindings ([7c46b5b](https://gitlab.com/biomedit/sett-rs/commit/7c46b5b03fc20d9657312d5643da0591306fb638)), Close #59, ⚠ BREAKING CHANGE

### Performance

- Add zstandard (zstd) as a new compression algorithm ([fcac89d](https://gitlab.com/biomedit/sett-rs/commit/fcac89d1623eea441187c482f8030b7c7736e952)), ⚠ BREAKING CHANGE, Close #58

### Refactor

- Improve naming, separation of concerns and add local as a destination variant ([fee95f7](https://gitlab.com/biomedit/sett-rs/commit/fee95f79c4cb883d41a01e8774f2d4c071d437a6)), ⚠ BREAKING CHANGE
- **sett-rs:** Group encrypt and decrypt parameters into opts objects ([d61858c](https://gitlab.com/biomedit/sett-rs/commit/d61858cdc2d90a1bbee9447f1bf7f8fcf1a36e11)), Close #43, ⚠ BREAKING CHANGE

## [0.3.0](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Drs%2F0%2E3%2E0) - 2023-04-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Drs%2F0%2E2%2E2...sett%2Drs%2F0%2E3%2E0)

### ⚠ BREAKING CHANGES

- - `Cert.uid` used to be of type `UserID` and is now of type `Optional[UserID]`
- `EncryptOpts` field types changed
- `sftp_upload` replaced by more general `transfer`
- `utils::Progress` is now a trait

Progress bar implementations should now implement the `Progress` trait.
- `anyhow::Error` replaces `sett::error::Error`

### Bug Fixes

- **sett-rs:** Fix pyo3 0.18.0 deprecation warnings ([ffb0140](https://gitlab.com/biomedit/sett-rs/commit/ffb01402318078666cea2a44e87525a445b9ee39))

### Features

- **pgp:** Support SHA-1 keys until given relax date ([4031efb](https://gitlab.com/biomedit/sett-rs/commit/4031efbce6548c4ecbf00022ab02fda679d9235b)), Closes #54
- **s3:** Add support for STS `session_token` ([86d5927](https://gitlab.com/biomedit/sett-rs/commit/86d5927f9348d3fbe28ffea9fff77589a2cc5963)), Closes #47
- **sett-rs:** Improve python bindings ([9371b3a](https://gitlab.com/biomedit/sett-rs/commit/9371b3a33c4cba7f1555b1ff0aa35d445e22dc38)), ⚠ BREAKING CHANGE: - `Cert.uid` used to be of type `UserID` and is now of type `Optional[UserID]`, Close #32
- **cli:** Make the CLI arguments more consistent ([0dcdb84](https://gitlab.com/biomedit/sett-rs/commit/0dcdb84f8e4ff1d568c4ad12fd4249a009f4b5e1)), ⚠ BREAKING CHANGE: `EncryptOpts` field types changed
- **s3:** Add support for s3 data transfers ([987ad22](https://gitlab.com/biomedit/sett-rs/commit/987ad22893949604152af1742eacc4f1bac27f71)), ⚠ BREAKING CHANGE: `sftp_upload` replaced by more general `transfer`
- Replace a concrete implementation of progress with a trait ([9879642](https://gitlab.com/biomedit/sett-rs/commit/9879642e6488a4cc7701c03cb87610b1b9d4a562)), ⚠ BREAKING CHANGE: `utils::Progress` is now a trait  Progress bar implementations should now implement the `Progress` trait.
- **error:** Use `anyhow` for error handling ([71124fd](https://gitlab.com/biomedit/sett-rs/commit/71124fdc15c426c05d7c5fed104a292dd194ef77)), ⚠ BREAKING CHANGE: `anyhow::Error` replaces `sett::error::Error`

### [0.2.2](https://gitlab.com/biomedit/sett-rs/compare/0.2.1...0.2.2) (2022-11-23)

### Features

- add python submodule to provide improved python bindings ([f1d3223](https://gitlab.com/biomedit/sett-rs/commit/f1d3223c08b93b1d0b1b25097ff12caddb0c34c1)), closes [#23](https://gitlab.com/biomedit/sett-rs/issues/23)
- **encrypt:** allow encrypting directly to a remote destination ([d88a214](https://gitlab.com/biomedit/sett-rs/commit/d88a214b0789d3418d2c0f004b5fe0912cccdfc4)), closes [#22](https://gitlab.com/biomedit/sett-rs/issues/22)
- **sett-rs:** add OpenPGP cert/key reader ([3bf27dd](https://gitlab.com/biomedit/sett-rs/commit/3bf27dd02776321b64527875be67f3ed3ee16988)), closes [#21](https://gitlab.com/biomedit/sett-rs/issues/21)

### Bug Fixes

- **encrypt:** use the first/primary key for signing if multiple are provided ([c2c7300](https://gitlab.com/biomedit/sett-rs/commit/c2c73000426fe26430369f16255b35f61a176e59)), closes [#24](https://gitlab.com/biomedit/sett-rs/issues/24)
- **filesystem:** available space computation should work on each OS ([287bcfe](https://gitlab.com/biomedit/sett-rs/commit/287bcfe3267eaf770c858596039db47c3343d67f))

### [0.2.1](https://gitlab.com/biomedit/sett-rs/compare/0.2.0...0.2.1) (2022-09-30)

### Features

- add decrypt workflow ([01dc4a8](https://gitlab.com/biomedit/sett-rs/commit/01dc4a805fd52f14b2d937fd9ce90b972f9c286a)), closes [#14](https://gitlab.com/biomedit/sett-rs/issues/14)
- add encryption workflow ([320be33](https://gitlab.com/biomedit/sett-rs/commit/320be33ad56485adbfc6d57685de3be05e4b8e0c))
- **sett/encrypt:** use in-memory encryption for passwords ([643ad39](https://gitlab.com/biomedit/sett-rs/commit/643ad3939b493f2c947dbd19d2e0ddb8d0be9b69))

### Bug Fixes

- rename functions to remove clippy warnings ([dacb8e6](https://gitlab.com/biomedit/sett-rs/commit/dacb8e6268d54fe0bcafc0fb9775b1fe902d08d3))

## [0.2.0](https://gitlab.com/biomedit/sett-rs/compare/0.1.3...0.2.0) (2021-12-09)

### ⚠ BREAKING CHANGES

- **lib:** sftp_upload now requires progress argument to be callable

### Features

- Add library stub ([7560ab4](https://gitlab.com/biomedit/sett-rs/commit/7560ab4add87db737089731b418f722a46fdb811))
- **sftp:** Support for 2 factor authentication ([d83b160](https://gitlab.com/biomedit/sett-rs/commit/d83b16060811b179d9d0f4c1d524ee3567c23f98))

- **lib:** Change progress callback from python object to python callable ([a2a8e38](https://gitlab.com/biomedit/sett-rs/commit/a2a8e3863d83a209004f8187e2aeb87998382999))

### [0.1.3](https://gitlab.com/biomedit/sett-rs/compare/0.1.2...0.1.3) (2021-10-13)

### [0.1.2](https://gitlab.com/biomedit/sett-rs/compare/0.1.1...0.1.2) (2021-08-11)

### Features

- **sftp:** make upload buffer size configurable ([07e594b](https://gitlab.com/biomedit/sett-rs/commit/07e594b95452d0097f4ce780bd5d83577c230b7b))

### [0.1.1](https://gitlab.com/biomedit/sett-rs/compare/0.1.0...0.1.1) (2021-08-04)

### Features

- **sftp:** log uploaded files size ([2cd9d11](https://gitlab.com/biomedit/sett-rs/commit/2cd9d11b9167a03646b8c2097f5ffe5cbd873141))

### Bug Fixes

- remove needless_borrow reported by clippy ([3236462](https://gitlab.com/biomedit/sett-rs/commit/32364628a498a928f06ff6513d92882ad8772e82))
- replace deprecated macro text_signature with new pyo3(text_signature) ([fabefb0](https://gitlab.com/biomedit/sett-rs/commit/fabefb03cacb423c11dc7cdb0ec4424031266bf4))

## 0.1.0 (2021-08-03)

### Bug Fixes

- **sftp:** add upload progress ([d2576e4](https://gitlab.com/biomedit/sett-rs/commit/d2576e412daffb4ea76e1be0bde6142dcba79ea6))
