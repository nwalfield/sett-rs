from datetime import datetime
from enum import Enum
from typing import Any, List, Optional, Tuple

class KeyType(Enum):
    Public: Any
    Secret: Any

class CertType(Enum):
    Public: Any
    Secret: Any

class Validity(Enum):
    Expired: Any
    Invalid: Any
    Revoked: Any
    Unknown: Any
    Valid: Any

class RevocationReason(Enum):
    Compromised: Any
    Superseded: Any
    Retired: Any
    Unspecified: Any

class UserID:
    value: bytes
    name: Optional[str]
    email: Optional[str]
    comment: Optional[str]
    validity: Validity
    validity_info: Optional[str]

class Key:
    key_id: str
    key_type: KeyType
    fingerprint: str
    length: Optional[int]
    creation_date: datetime
    expiration_date: Optional[datetime]
    pub_key_algorithm: int
    validity: Validity
    validity_info: Optional[str]

class CertInfo:
    email: Optional[str]
    fingerprint: str
    key_id: str
    keys: List[Key]
    primary_key: Key
    subkeys: List[Key]
    uid: Optional[UserID]
    uids: List[UserID]
    validity: Validity
    validity_info: Optional[str]

    @classmethod
    def from_bytes(cls, data: bytes, end_relax: Optional[int] = None) -> CertInfo: ...

def create_revocation_signature(
    cert: bytes, reason: RevocationReason, message: bytes, password: bytes
) -> bytes: ...
def generate_cert(uid: str, password: bytes) -> Tuple[bytes, bytes]: ...

class CertStore:
    def import_cert(self, cert: bytes, cert_type: CertType) -> CertInfo: ...
    def export_cert(self, fingerprint: str, cert_type: CertType) -> bytes: ...
    def list_certs(self, cert_type: CertType) -> List[CertInfo]: ...
    def revoke(self, rev_sig: bytes) -> None: ...

class CertParsingError(Exception): ...

def gnupg_extract_cert(
    identifier: str, cert_type: CertType, gpg_home: Optional[str]
) -> bytes: ...
def gnupg_list_certs(cert_type: CertType, gpg_home: Optional[str]) -> List[str]: ...
def gnupg_list_certs_with_userid(
    cert_type: CertType, gpg_home: Optional[str]
) -> List[str]: ...
def verify_metadata_signature(data_pkg_path: str, signer_cert: bytes) -> None: ...
