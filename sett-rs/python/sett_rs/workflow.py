from ._sett_rs import (
    verify_metadata_signature as verify_metadata_signature,
)

__all__ = [
    "verify_metadata_signature",
]
