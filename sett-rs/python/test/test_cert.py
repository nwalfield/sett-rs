import itertools

import pytest

from sett_rs.cert import CertInfo, CertParsingError, Key, KeyType, Validity

from .data_cert import (
    KEY_4096_RSA_NO_EXPIRY_METADATA,
    KEY_2048_DSA_EXPIRES_METADATA,
    KEY_256_ECC_NO_EXPIRY_METADATA,
    KEY_256_ECC_REVOKED_METADATA,
    KEY_WITH_NO_UID_METADATA,
    KEY_WITH_SUBKEYS_METADATA,
    KeyMetadata,
    CertMetadata,
    KeyFileType,
    KEY_4096_RSA_NO_EXPIRY_SHA1_METADATA,
)


def assert_cert_matches(cert: CertInfo, expected_cert: CertMetadata) -> None:
    """Check that the specified certificate has the expected metadata."""

    # Verify direct attributes of Certificate.
    assert cert.fingerprint == expected_cert.primary_key.fingerprint
    assert cert.key_id == expected_cert.primary_key.fingerprint[-16:]
    assert cert.validity == expected_cert.primary_key.validity
    assert len(cert.keys) == len(expected_cert.subkeys) + 1
    assert len(cert.keys[1:]) == len(expected_cert.subkeys)

    expected_uid = next(iter(expected_cert.uids), None)

    # Verify attributes of primary user ID, if it exists.
    if expected_uid is not None:
        assert cert.email == expected_uid.email
        assert cert.uid
        assert cert.uid.name == expected_uid.name
        assert cert.uid.comment == expected_uid.comment
        assert (
            str(cert.uid)
            == f"{expected_uid.name} ({expected_uid.comment}) <{expected_uid.email}>"
        )
    else:
        assert cert.uid is None

    # Verify attributes of all user ID.
    assert len(cert.uids) == len(expected_cert.uids)
    for expected_uid in expected_cert.uids:
        uid = next(u for u in cert.uids if u.email == expected_uid.email)
        assert uid.name == expected_uid.name
        assert uid.email == expected_uid.email
        assert uid.comment == expected_uid.comment
        assert uid.validity == expected_uid.validity


def assert_key_matches(key: Key, expected_key: KeyMetadata) -> None:
    """Check that the specified key has the expected metadata."""

    assert key.fingerprint == expected_key.fingerprint
    assert key.key_id == expected_key.fingerprint[-16:]
    assert key.length == expected_key.length
    assert key.validity == expected_key.validity
    assert key.pub_key_algorithm == expected_key.key_algo.value
    assert key.creation_date.isoformat().startswith(f"{expected_key.creation_date}T")
    if expected_key.expiration_date:
        assert key.expiration_date  # Added for mypy
        key.expiration_date.isoformat().startswith(f"{expected_key.expiration_date}T")
    else:
        assert key.expiration_date is None


def assert_key_type(cert: CertInfo, expected_key_type: KeyType) -> None:
    """Verify that all keys in the specified certificate are of the correct type."""

    assert all(k.key_type == expected_key_type for k in cert.keys)


@pytest.mark.parametrize(
    "cert_metadata, key_file_type",
    tuple(
        itertools.product(
            (
                # Keys with a single user ID and single subkey.
                KEY_4096_RSA_NO_EXPIRY_METADATA,
                KEY_2048_DSA_EXPIRES_METADATA,
                KEY_256_ECC_NO_EXPIRY_METADATA,
                KEY_256_ECC_REVOKED_METADATA,
                #
                # Key with multiple user IDs and subkeys.
                KEY_WITH_SUBKEYS_METADATA,
            ),
            tuple(KeyFileType),
        )
    )
    + (
        (  # Key with no user ID
            KEY_WITH_NO_UID_METADATA,
            KeyFileType.PUB_ASCII_ARMORED,
        ),
        (
            # SHA-1 key
            KEY_4096_RSA_NO_EXPIRY_SHA1_METADATA,
            KeyFileType.PUB_ASCII_ARMORED,
        ),
    ),
)
def test_cert_from_bytes(
    cert_metadata: CertMetadata,
    key_file_type: KeyFileType,
) -> None:
    """Test loading a certificate with a single user ID and a single subkey
    from either ASCII armored or binary PGP key files.
    """

    # Load certificate from file.
    with open(cert_metadata.key_file(key_file_type), "rb") as f:
        cert_info = CertInfo.from_bytes(f.read())
    cert_type = (
        KeyType.Public
        if key_file_type in (KeyFileType.PUB_BINARY, KeyFileType.PUB_ASCII_ARMORED)
        else KeyType.Secret
    )

    # Verify attributes of the certificate. This includes a check of all
    # user IDs associated with the certificate.
    assert_cert_matches(cert_info, cert_metadata)
    assert_key_type(cert_info, cert_type)

    # Verify the primary key and subkeys.
    assert_key_matches(cert_info.primary_key, cert_metadata.primary_key)
    for expected in [cert_metadata.primary_key] + cert_metadata.subkeys:
        key = next(k for k in cert_info.keys if k.fingerprint == expected.fingerprint)
        assert_key_matches(key, expected)

    # Verify the certificate's string representation.
    uid = next(iter(cert_metadata.uids), None)
    user_id = f"{uid.name} ({uid.comment}) <{uid.email}>" if uid else "No user ID"
    assert str(cert_info) == f"{user_id} - {cert_metadata.primary_key.fingerprint}"


def test_load_invalid_file() -> None:
    """Load a file that is not a proper PGP key raises an error"""

    invalid_key = b"""-----BEGIN PGP PRIVATE KEY BLOCK-----
    Invalid key
    -----END PGP PRIVATE KEY BLOCK-----
    """

    with pytest.raises(CertParsingError, match="OpenPGP certificate parsing failed."):
        CertInfo.from_bytes(invalid_key)


def test_sha1_key_without_relax_date() -> None:
    """Loading an SHA-1 key without relax date returns `Validity.Invalid`"""

    with open(
        KEY_4096_RSA_NO_EXPIRY_SHA1_METADATA.key_file(KeyFileType.PUB_ASCII_ARMORED),
        "rb",
    ) as f:
        cert = CertInfo.from_bytes(f.read())
    assert cert.validity == Validity.Invalid
