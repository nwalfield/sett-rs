import os
from dataclasses import dataclass, field
from enum import Enum
from typing import Optional, List

from sett_rs._sett_rs import Validity

CN_NAME = "Chuck Norris"
CN_EMAIL = "chuck.norris@roundhouse.org"

# Directory where test PGP keys are stored in binary format.
TESTKEY_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "pgp_test_keys")


class KeyAlgo(Enum):
    """Codes for public key algorithms.
    See https://www.rfc-editor.org/rfc/rfc4880#section-9.1 for details.
    """

    RSA = 1
    Elgamal = 16
    DSA = 17
    ECC_Encrypt_Only = 18
    ECC = 22


class KeyFileType(Enum):
    """File extensions by type of PGP key file type."""

    PUB_BINARY = ".pub"
    SEC_BINARY = ".sec"
    PUB_ASCII_ARMORED = ".pub.asc"
    SEC_ASCII_ARMORED = ".sec.asc"


@dataclass
class UserIdMetadata:
    """Holds metadata of a User ID used in unit tests."""

    name: str = CN_NAME
    email: str = CN_EMAIL
    comment: Optional[str] = None
    validity: Validity = field(default_factory=lambda: Validity.Valid)


@dataclass
class KeyMetadata:
    """Holds metadata of a PGP key used in unit tests."""

    fingerprint: str
    key_algo: KeyAlgo
    creation_date: str
    length: int = 4096
    validity: Validity = field(default_factory=lambda: Validity.Valid)
    expiration_date: Optional[str] = None


@dataclass
class CertMetadata:
    """Holds metadata of a PGP certificate used in unit tests."""

    uids: List[UserIdMetadata]
    primary_key: KeyMetadata
    subkeys: List[KeyMetadata]
    key_files_basename: str
    key_files_dir: str = TESTKEY_DIR

    def key_file(self, file_type: KeyFileType) -> str:
        file_path = os.path.join(
            self.key_files_dir, self.key_files_basename + str(file_type.value)
        )
        if os.path.isfile(file_path):
            return file_path
        raise ValueError(f"Unable to find PGP key file {file_path}")


# Key Sha One (SHA1 Key) <jenoye8590@fectode.com>, password: "sha1"
KEY_4096_RSA_NO_EXPIRY_SHA1_METADATA = CertMetadata(
    uids=[
        UserIdMetadata(
            "Sha One",
            "jenoye8590@fectode.com",
            comment="SHA1 Key",
            validity=Validity.Invalid,
        )
    ],
    primary_key=(
        KeyMetadata(
            fingerprint="F090ECF41E97BC2EB3BF0239A73639286B772B20",
            length=4096,
            validity=Validity.Invalid,
            key_algo=KeyAlgo.RSA,
            creation_date="2023-04-09",
            expiration_date=None,
        )
    ),
    subkeys=[
        (
            KeyMetadata(
                fingerprint="F090ECF41E97BC2EB3BF0239A73639286B772B20",
                length=4096,
                validity=Validity.Invalid,
                key_algo=KeyAlgo.RSA,
                creation_date="2023-04-09",
                expiration_date=None,
            )
        )
    ],
    key_files_basename="testkey_rsa_no_expiry_sha1",
)

# Key Chuck Norris (4096 DSA no expiry) <chuck.norris@roundhouse.org>
KEY_4096_RSA_NO_EXPIRY_METADATA = CertMetadata(
    uids=[UserIdMetadata(CN_NAME, CN_EMAIL, comment="4096 RSA no expiry")],
    primary_key=KeyMetadata(
        fingerprint="1F322ECF81911787A815E192800F7DF30CE12F1B",
        length=4096,
        validity=Validity.Valid,
        key_algo=KeyAlgo.RSA,
        creation_date="2022-11-14",
        expiration_date=None,
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="50448235BFC3B8CCF838808894DDE0D02E79F43E",
            length=4096,
            validity=Validity.Valid,
            key_algo=KeyAlgo.RSA,
            creation_date="2022-11-14",
            expiration_date=None,
        )
    ],
    key_files_basename="testkey_4096_RSA_no_expiry",
)

# Key Chuck Norris (2048 DSA expires 2022-11-16) <chuck.norris@roundhouse.org>
KEY_2048_DSA_EXPIRES_METADATA = CertMetadata(
    uids=[UserIdMetadata(CN_NAME, CN_EMAIL, comment="2048 DSA expires 2022-11-16")],
    primary_key=KeyMetadata(
        fingerprint="32F80E29E3FACF069CB715F7DFD457B214181A35",
        length=2048,
        validity=Validity.Expired,
        key_algo=KeyAlgo.DSA,
        creation_date="2022-11-14",
        expiration_date="2022-11-16",
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="E8D9BB5BA6CB0D491FC757F0B78B0E724E4EF35F",
            length=2048,
            validity=Validity.Expired,
            key_algo=KeyAlgo.Elgamal,
            creation_date="2022-11-14",
            expiration_date="2022-11-16",
        )
    ],
    key_files_basename="testkey_2048_DSA_expired",
)

# Key Chuck Norris (256 ECC no expiry) <chuck.norris@roundhouse.org>
KEY_256_ECC_NO_EXPIRY_METADATA = CertMetadata(
    uids=[UserIdMetadata(CN_NAME, CN_EMAIL, comment="256 ECC no expiry")],
    primary_key=KeyMetadata(
        fingerprint="C0621CB3669020CC31050A361956EC38A96CA852",
        length=256,
        validity=Validity.Valid,
        key_algo=KeyAlgo.ECC,
        creation_date="2022-11-14",
        expiration_date=None,
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="A53F97AE03B3C4C1CB1585BCEA4EB7C0D6279DF3",
            length=256,
            validity=Validity.Valid,
            key_algo=KeyAlgo.ECC_Encrypt_Only,
            creation_date="2022-11-14",
            expiration_date=None,
        )
    ],
    key_files_basename="testkey_ECC_no_expiry",
)

# Key Chuck Norris (256 ECC expires 2022-11-16 revoked) <chuck.norris@roundhouse.org>
KEY_256_ECC_REVOKED_METADATA = CertMetadata(
    uids=[
        UserIdMetadata(CN_NAME, CN_EMAIL, comment="256 ECC expires 2022-11-16 revoked")
    ],
    primary_key=KeyMetadata(
        fingerprint="022E40EE680CEF47E5A8A1AF106AAA3D81B19A86",
        length=256,
        validity=Validity.Revoked,
        key_algo=KeyAlgo.ECC,
        creation_date="2022-11-14",
        expiration_date="2022-11-16",
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="54F1B58BAFDB7C36E2F3A003BCEB68996CE07311",
            length=256,
            validity=Validity.Expired,
            key_algo=KeyAlgo.ECC_Encrypt_Only,
            creation_date="2022-11-14",
            expiration_date="2022-11-16",
        )
    ],
    key_files_basename="testkey_ECC_revoked",
)

# Test key with multiple user IDs and subkeys.
# Fingerprints:
#  * Primary: 9E6A150E6CDA0288E8DC473FA500E42FE09ADA86
#
# User IDs:
#  * [ultimate] (1). Chuck N. (user ID 3) <chuck.n@roundhouse.org>
#  * [ revoked] (2)  Chuck Norris (4096 RSA with_subkeys) <chuck.norris@roundhouse.org>
#  * [ultimate] (3)  Roundhouse (user ID 2) <cn@roundhouse.org>
#
# Key has 5 subkeys:
#  * C71696B7B366037BC3A16EB2786C3772B3875BE1 [E] [REVOKED]
#    Subkey RSA encrypt-only, 4096, does not expire.
#  * 7FBC4D74755A9BA83BA3360122C2C48DA9FC7605 [S] [REVOKED]
#    Subkey RSA sign-only, 4096, Expires: 16 Nov 2022.
#  * 4E1686938C13544BFC23548DCCCA9AB7CC7931A3 [E]
#    subkey ECC encrypt-only, 256, does not expire.
#  * B41C398BE951B147EB746615D1FFCAF39E369B52 [E]
#    subkey ECC encrypt-only, 256, Expires: 16 Nov 2022
#  * 982C0A0FA2460C87B395E49F726AB1CE8493BB7F [S]
#    Subkey RSA sign-only, 4096, does not expire
KEY_WITH_SUBKEYS_METADATA = CertMetadata(
    uids=[
        UserIdMetadata("Chuck N.", "chuck.n@roundhouse.org", "user ID 3"),
        UserIdMetadata(
            "Chuck Norris",
            "chuck.norris@roundhouse.org",
            "4096 RSA with_subkeys",
            validity=Validity.Revoked,
        ),
        UserIdMetadata("Roundhouse", "cn@roundhouse.org", "user ID 2"),
    ],
    primary_key=KeyMetadata(
        fingerprint="9E6A150E6CDA0288E8DC473FA500E42FE09ADA86",
        length=4096,
        validity=Validity.Valid,
        key_algo=KeyAlgo.RSA,
        creation_date="2022-11-15",
        expiration_date=None,
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="C71696B7B366037BC3A16EB2786C3772B3875BE1",
            length=4096,
            validity=Validity.Revoked,
            key_algo=KeyAlgo.RSA,
            creation_date="2022-11-15",
            expiration_date=None,
        ),
        KeyMetadata(
            fingerprint="7FBC4D74755A9BA83BA3360122C2C48DA9FC7605",
            length=4096,
            validity=Validity.Revoked,
            key_algo=KeyAlgo.RSA,
            creation_date="2022-11-15",
            expiration_date="2022-11-16",
        ),
        KeyMetadata(
            fingerprint="4E1686938C13544BFC23548DCCCA9AB7CC7931A3",
            length=256,
            validity=Validity.Valid,
            key_algo=KeyAlgo.ECC_Encrypt_Only,
            creation_date="2022-11-15",
            expiration_date=None,
        ),
        KeyMetadata(
            fingerprint="B41C398BE951B147EB746615D1FFCAF39E369B52",
            length=256,
            validity=Validity.Expired,
            key_algo=KeyAlgo.ECC_Encrypt_Only,
            creation_date="2022-11-15",
            expiration_date="2022-11-16",
        ),
        KeyMetadata(
            fingerprint="982C0A0FA2460C87B395E49F726AB1CE8493BB7F",
            length=4096,
            validity=Validity.Valid,
            key_algo=KeyAlgo.RSA,
            creation_date="2022-11-15",
            expiration_date=None,
        ),
    ],
    key_files_basename="testkey_with_subkeys",
)

# Key with no UserID
KEY_WITH_NO_UID_METADATA = CertMetadata(
    uids=[],
    primary_key=KeyMetadata(
        fingerprint="FC39A4524E8A9172E6A61AE90190F25D4C32FFCD",
        length=256,
        validity=Validity.Invalid,
        key_algo=KeyAlgo.ECC,
        creation_date="2023-01-31",
        expiration_date=None,
    ),
    subkeys=[
        KeyMetadata(
            fingerprint="FC39A4524E8A9172E6A61AE90190F25D4C32FFCD",
            length=256,
            validity=Validity.Invalid,
            key_algo=KeyAlgo.ECC,
            creation_date="2023-01-31",
            expiration_date=None,
        )
    ],
    key_files_basename="testkey_with_no_uid",
)
