# Changelog

All notable changes to this project will be documented in this file.

## [5.0.0-rc.3](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E3) - 2024-04-18

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E2...sett%2Dgui%2F5%2E0%2E0%2Drc%2E3)


### ✨ Features

- Add tooltips to interactive components ([bb7e9b3](https://gitlab.com/biomedit/sett-rs/commit/bb7e9b3a1ede3d751a0358bce2908eea8b4aba1f)), Closes #229
- Add and harmonize button icons in keys tab ([a05bb55](https://gitlab.com/biomedit/sett-rs/commit/a05bb557d6f937461f0e21a3006f761989377bd1))
- Require pwd to be ge 10 chars when generating new PGP key ([0de6c14](https://gitlab.com/biomedit/sett-rs/commit/0de6c14dcaa54924ebc30e462bd42b98654438bb))
- Add input verification to ImportFromKeyserverDialog ([8d33323](https://gitlab.com/biomedit/sett-rs/commit/8d33323b1034b70b312429f8e53b330b03f95015))
- Add relevant content to the about section ([14a419a](https://gitlab.com/biomedit/sett-rs/commit/14a419acc53bdb26cd1f2bfa711ccacfe1be70b7)), Close #226
- **settings:** Display location of log file and keyserver in settings tab ([0846fe1](https://gitlab.com/biomedit/sett-rs/commit/0846fe177ee536bddb74d84ad52a5bfc752ca322)), Closes #222
- Make button text and size configurable in CopyToClipboard component ([7130013](https://gitlab.com/biomedit/sett-rs/commit/71300138dff08dc3fc3a1391455d0f0caed83c5d))
- Add optional comment field to generate key dialog ([4a0a9cf](https://gitlab.com/biomedit/sett-rs/commit/4a0a9cf283a683e0baab45de78944af751cb38bc)), Closes #215
- **cert:** Sort certificates alphabetically ([5c4203a](https://gitlab.com/biomedit/sett-rs/commit/5c4203a1fbd3ee5338363a45b510469a7b9bf493))
- **cert:** Use badge for representing Portal approval ([9ba28e3](https://gitlab.com/biomedit/sett-rs/commit/9ba28e31ccac8276c261b701a8b92d44e85ed0cd)), Close #153
- **about:** Add version number ([9e5862b](https://gitlab.com/biomedit/sett-rs/commit/9e5862bb719ae8f72e70b770a98256ef80fe51ec)), Close #191
- Verify package with Portal when the corresponding setting is enabled ([de605c0](https://gitlab.com/biomedit/sett-rs/commit/de605c0b66fc74b39eb41d77419e14c0190a4464))

### 🐞 Bug Fixes

- **sett-gui/button:** Change background color when button is disabled ([277f310](https://gitlab.com/biomedit/sett-rs/commit/277f3108153a081cdf0ea7f5c1ddbfa863f494e4)), Close #199
- **sett-gui/task:** Handle division by zero when updating progress bar ([eda5aff](https://gitlab.com/biomedit/sett-rs/commit/eda5affe4665307f248bfd395d212432188ae820)), Close #224
- Make email verification checkbox in UploadCertDialog work ([405874a](https://gitlab.com/biomedit/sett-rs/commit/405874a6b9c84e8569f94df63d0975ce2c160b5f))
- Show a complete destination url for all tasks ([937371e](https://gitlab.com/biomedit/sett-rs/commit/937371eb8fcba524ebabd8c6ae15bba43815d5c8)), Close #223
- **keyserver:** Remove option to import further keys after success import ([b7eaeb0](https://gitlab.com/biomedit/sett-rs/commit/b7eaeb03868c0b3ee654b241b8627083d069477d)), Close #218
- **revocation-dialog:** Improve message for unspecified reason ([0a90a04](https://gitlab.com/biomedit/sett-rs/commit/0a90a04ca3ab28dd34e60a20601959043628f964)), Close #220
- **cert:** Show upload to keyserver button only for private keys ([09a94a3](https://gitlab.com/biomedit/sett-rs/commit/09a94a3b99634b730f66003648082c01cf2a1552)), Close #219
- **css:** Switch warning color from yellow to orange ([73b7793](https://gitlab.com/biomedit/sett-rs/commit/73b77930bb712cf65a1a0a5199be57e8dfd1e56a))
- Correctly display key type in DeleteCertDialog ([ed84ba1](https://gitlab.com/biomedit/sett-rs/commit/ed84ba1b73e4709f4d00b97ae22ce9e3d1202c0d)), Closes #203
- **task:** Forget password after submitting a task ([55d4a38](https://gitlab.com/biomedit/sett-rs/commit/55d4a38f1768167e0ee19dec090e51b716fb95a9))
- **Chip:** Use the correct color for the chip button ([8877384](https://gitlab.com/biomedit/sett-rs/commit/88773844dd7a176da6b0788f45b508e222cb8100))
- **QuickSearch:** Make magnification glass icon visible also in the production build ([351b095](https://gitlab.com/biomedit/sett-rs/commit/351b095878ed8ed5ce2e68a12447de83fe01d043)), Close #216
- **settings:** Hide keyserver url ([cb8c498](https://gitlab.com/biomedit/sett-rs/commit/cb8c4980f8b6bebf8ef973137a24e0dad954b132))
- **Windows:** Enable overlay scrollbars ([845c096](https://gitlab.com/biomedit/sett-rs/commit/845c096b25b3a0681c45a7f3ef80db2adefe69da))
- **encrypt:** Require transfer id when verify package is enabled ([2d5345b](https://gitlab.com/biomedit/sett-rs/commit/2d5345b4907509b5c63e10c9269f57538bbe5d73))

### 🧱 Build system and dependencies

- Update Cargo lock ([fa43d0d](https://gitlab.com/biomedit/sett-rs/commit/fa43d0dfffc48e54c069b85a1934d6c8e8d3ab60))
- Add licenses for each crate ([be2ad30](https://gitlab.com/biomedit/sett-rs/commit/be2ad30accc4693eed5136be70892866aea682f0)), Close #217

### 👷 CI

- Switch to flat config file for eslint ([fa83820](https://gitlab.com/biomedit/sett-rs/commit/fa83820f82d3fcd0216321ffcf6ea68b8fefbdc1)), Closes #234
- **build/linux:** Bump base image to ubuntu 20.04 for gui .Appimage builds ([e85d3ed](https://gitlab.com/biomedit/sett-rs/commit/e85d3edffd10a9dfa7a845115951c2bb54755652)), Close #205
- Fix new security vulnerability ([2381703](https://gitlab.com/biomedit/sett-rs/commit/238170307a0446e71e14f812e4b246499bb8883b))

### 📝 Documentation

- Improve all project's `README.md` ([365085f](https://gitlab.com/biomedit/sett-rs/commit/365085f285cf23a85727a54977241c8f3970887f)), Close #177
- Use OpenPGP instead of PGP in functions documentation ([3b2e78a](https://gitlab.com/biomedit/sett-rs/commit/3b2e78ab765b580078abfc103091f9ab8dc15893))

### 🧹 Refactoring

- Sort imports in .ts and .svelte files ([280103f](https://gitlab.com/biomedit/sett-rs/commit/280103fb8b055ebfb4a716ee021ebf8022949e68))
- **cert:** Improve error message from read_pkg_metadata() ([88bc2d5](https://gitlab.com/biomedit/sett-rs/commit/88bc2d559da602ae6d0ce8bb0ffd7a43b273b780))
- **keyserver:** Use the new keyserver API ([03e1701](https://gitlab.com/biomedit/sett-rs/commit/03e1701d2e6b8c626d70d7b188d474f112abbfe0))
- **settings:** Use Svelte store for managing settings ([9c1c39e](https://gitlab.com/biomedit/sett-rs/commit/9c1c39e7cae1e6d9178b8bd91c3bfb5afcc17550))

## [5.0.0-rc.2](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E2) - 2024-03-05

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1...sett%2Dgui%2F5%2E0%2E0%2Drc%2E2)


### ✨ Features

- Icrease default App size to 900 x 1040 px ([1bfee0e](https://gitlab.com/biomedit/sett-rs/commit/1bfee0e9a1856d62acfc2b9221618ac3de73110f))
- Add preflight checks ([43a364a](https://gitlab.com/biomedit/sett-rs/commit/43a364af31d905262756766dd0ea35691f0af583)), Close #113
- **gui:** Rename certificate(s) to key(s) when interacting with the user ([a534d6f](https://gitlab.com/biomedit/sett-rs/commit/a534d6fcbe13a8e98290a63e67ab42b58af50442)), Close #170
- Refactor pkg summary table to show user IDs, not fingerprints ([b45b7e0](https://gitlab.com/biomedit/sett-rs/commit/b45b7e08cc5c8e2a38c43a0e18ec7ba5909ac7e4)), Closes #133
- **settings:** Add reset settings button to GUI ([604e481](https://gitlab.com/biomedit/sett-rs/commit/604e481ce05abcacee7e3050fcaa905b038dff24)), Closes #180
- Use chips to improve UX in GUI ([e20a26d](https://gitlab.com/biomedit/sett-rs/commit/e20a26d93ce31d41b2087b2a34cd9530f84fd61e)), Closes #142
- **settings:** Add default output directory setting to GUI ([303246f](https://gitlab.com/biomedit/sett-rs/commit/303246fbe9f5e318d987f92724c05c89fa139d83))
- Specify `started`/`updated|finished` timestamps for each task ([502b0cc](https://gitlab.com/biomedit/sett-rs/commit/502b0cccd2273f8421608818bed53fecd3841f63)), Closes #114

### 🐞 Bug Fixes

- **crypt:** Improve error message when wrong password to decrypt data ([29e0d51](https://gitlab.com/biomedit/sett-rs/commit/29e0d51f7ff341034958cff3cf32a37427c6439c)), Closes #193
- **keys tab:** Rename Private Key badge to Private ([a475e6c](https://gitlab.com/biomedit/sett-rs/commit/a475e6c7d885a3ecb2f5c8bb109639090f925c87))
- **Toast:** Keep size of dismissal button constant ([18090ce](https://gitlab.com/biomedit/sett-rs/commit/18090ce8c0174281569aa4b3f5922491d903d925)), Closes #194
- **Chip:** Keep size of dismissal button constant ([edcc7e7](https://gitlab.com/biomedit/sett-rs/commit/edcc7e749627a8e8a9290a45c42f7ca4f6dc7248))
- Improve error message when parsing data packages ([b144daa](https://gitlab.com/biomedit/sett-rs/commit/b144daa69d50a8cb0bea0b2853122a44c394d254)), Closes #164
- **decrypt:** Fix bug in decryption with multiple recipients in GUI ([8db333d](https://gitlab.com/biomedit/sett-rs/commit/8db333df247c3aedacee77851277355848aeb154)), Closes #173
- `encrypt` does no longer work for S3 transfers ([fd9690e](https://gitlab.com/biomedit/sett-rs/commit/fd9690ec30b29188cdc1c31cf7d13d697e65517f)), Closes #179
- `upload` method not working for S3 destination ([ec4c75e](https://gitlab.com/biomedit/sett-rs/commit/ec4c75e7c9ef3cb4ebaaefe60d03ba2c73de9c63)), Closes #179
- **settings:** Correct async loading of settings in GUI ([ba32a53](https://gitlab.com/biomedit/sett-rs/commit/ba32a53339e8c14069631a77d4cf242e6bb7f238))
- Update styling of main workflow buttons ([479966e](https://gitlab.com/biomedit/sett-rs/commit/479966e297060b355882c49c9f348e38df3fae40)), Closes #174
- Correctly load default settings ([266299a](https://gitlab.com/biomedit/sett-rs/commit/266299a3c849684cfc648b41a75b5b79e14fac08))
- Make selecting destination compulsory in GUI ([8643689](https://gitlab.com/biomedit/sett-rs/commit/864368940c6353da11fd15341316400bb8599be5)), Closes #171
- Make DTR ID field more `numeric` and nicer to use ([3f4bb84](https://gitlab.com/biomedit/sett-rs/commit/3f4bb84f2d5c4b624f1fbf6c86c5d279e5290dc6))
- The whole key line is now clickable to open the details ([64f4810](https://gitlab.com/biomedit/sett-rs/commit/64f48106d916ce6eecb57f6412ff3ca930fcbd2e))
- **encrypt:** List both private and public certs as recipient choice ([58b33f7](https://gitlab.com/biomedit/sett-rs/commit/58b33f782cb74bd559b21d024c1354c4d11e0697)), Closes #163

### 🧱 Build system and dependencies

- Select build features in Cargo.toml ([f5e7ab5](https://gitlab.com/biomedit/sett-rs/commit/f5e7ab53cc3120445c9ee4f1b809310c1b16137f))
- Remove `serde_json` as it's only relevant as transitive dependency ([1a4e880](https://gitlab.com/biomedit/sett-rs/commit/1a4e880c25e29a003203f096fd7675db72fe6170))
- Update lock files ([8a8dd7e](https://gitlab.com/biomedit/sett-rs/commit/8a8dd7eb8cff0f2c4e833d604495ab2428fbf8dd))
- Fix openssl-sys compilation issue ([26ff364](https://gitlab.com/biomedit/sett-rs/commit/26ff36489cab8d2fc97da60ea38c4150f81e64c9))
- Bump MSRV to 1.74 ([f0c5743](https://gitlab.com/biomedit/sett-rs/commit/f0c574305a87e2fc74ec085d9a3c929c52463831))
- **lockfile:** Remove unused dependency ([d18ef21](https://gitlab.com/biomedit/sett-rs/commit/d18ef211accd6a41501bfb3ec3c3c0ced716f244))
- Increase MSRV from 1.70 to 1.72 ([694f68a](https://gitlab.com/biomedit/sett-rs/commit/694f68ab85d29b7256f2a378afa5c36b901d025f))

### 🧹 Refactoring

- Refactor DropZone component ([272a84b](https://gitlab.com/biomedit/sett-rs/commit/272a84b9f6c1ee08c9be097b3f11ea936ed2dfdd))
- `sessionToken` is NOT required ([7646bf6](https://gitlab.com/biomedit/sett-rs/commit/7646bf63b99436793d1ec8d8b57251e2df1d438f))
- Sort string-unions and import-members, add eslint rules ([90421c8](https://gitlab.com/biomedit/sett-rs/commit/90421c890290a174741e973869e4d3939fef3737))
- **PasswordDialog:** Change prop name from "value" to "password" ([53a8d05](https://gitlab.com/biomedit/sett-rs/commit/53a8d0535559e6748524204f810108b54f7d8559))
- Add ClearableItem component ([5f69352](https://gitlab.com/biomedit/sett-rs/commit/5f69352e645ca0a06a78ea7c7ca8b2f6527e288c))
- `CheckboxInput` and `RadioInput` added ([95b1f44](https://gitlab.com/biomedit/sett-rs/commit/95b1f4469124230e002c2f6997697adb11b8a0b5))
- Simplify the code for loading settings ([d2ad0ac](https://gitlab.com/biomedit/sett-rs/commit/d2ad0ac2c1a9a6e701bc74649d06d88b002ff9ac))
- Use already existing global CSS definition `nowrap` ([d73fb7a](https://gitlab.com/biomedit/sett-rs/commit/d73fb7aadfcd3b910438459fa55262ebb8967631))
- `event.target` can not be null ([7e72f8b](https://gitlab.com/biomedit/sett-rs/commit/7e72f8bb53ae4e268fef75912bacc5037aaceeb8))
- Improve tooltip for private badge ([81c286c](https://gitlab.com/biomedit/sett-rs/commit/81c286cea210039dc7ccefdd18be4812aebff00a)), Closes #172
- Use `cog` instead of `adjustments` icon for settings ([5f04ccf](https://gitlab.com/biomedit/sett-rs/commit/5f04ccf7ffc491551b1548ee88781479409b2aa3)), Closes #172
- Button `Paste from clipboard` and helper text were shifted ([58503e1](https://gitlab.com/biomedit/sett-rs/commit/58503e1e198aff38d6741bddc2d553100971907c))
- `local` destination no longer occupies so much space ([8e6b64d](https://gitlab.com/biomedit/sett-rs/commit/8e6b64d1a35246b9a544c70db75e72285997b1e8))
- **encrypt:** Use Button component to fix unwanted form submission ([f719f11](https://gitlab.com/biomedit/sett-rs/commit/f719f1184a54df317a59314d5ea1b09acd2361ea))
- Comply with eslint standards ([920bc2a](https://gitlab.com/biomedit/sett-rs/commit/920bc2aaedade9c2c82b4660cad1499623b2d343))

## [5.0.0-rc.1](https://gitlab.com/biomedit/sett-rs/-/releases/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1) - 2024-02-02

[See all changes since the last release](https://gitlab.com/biomedit/sett-rs/compare/sett%2Dgui%2F5%2E0%2E0%2Drc%2E1...sett%2Dgui%2F5%2E0%2E0%2Drc%2E1)


### ✨ Features

- **error:** Display all unhandled errors as toasts ([db5307a](https://gitlab.com/biomedit/sett-rs/commit/db5307ac5c0d0b33909619ca1c187929e786157c)), Close #136
- Display an error when an invalid package is selected ([f474c62](https://gitlab.com/biomedit/sett-rs/commit/f474c6217d168cb0085e37cf4b7d391d024bce43)), Close #129
- **updater:** Set up auto-updater ([f3739a8](https://gitlab.com/biomedit/sett-rs/commit/f3739a89c3f84c5eda6b1e445c8a84e76eaf2d18)), Close #155
- Display all revocation reasons in GUI ([907b9e8](https://gitlab.com/biomedit/sett-rs/commit/907b9e8a0beed6d0bb023e29414b2945e3018666))
- **certificates tab:** Remove User ID info from certificate details drop-down table ([c939591](https://gitlab.com/biomedit/sett-rs/commit/c939591b2e6681183a018a7a2d6cebbd07766546))
- Display certificate revocation and expiry date ([c1d8b11](https://gitlab.com/biomedit/sett-rs/commit/c1d8b11f5428392f2e2a08eee7c7738369edd247)), Closes #130
- Add close button to ImportFromGnupgDialog ([b86934a](https://gitlab.com/biomedit/sett-rs/commit/b86934a6bd1ea1dd5673a36333ff609be3508e21))
- **settings:** Add settings page ([6d0f1f0](https://gitlab.com/biomedit/sett-rs/commit/6d0f1f00a1be969225ac84004ade6fc08eaf09ab)), close #84
- Improve GUI after new certificate generation ([ab1a102](https://gitlab.com/biomedit/sett-rs/commit/ab1a102c90133c2a2c7ad1f53cc499a8b111f010))
- Do not close dialog when click on backdrop ([786e19c](https://gitlab.com/biomedit/sett-rs/commit/786e19c9d8779e0800ede107eda8d869a048e581))
- Merge private and public certificates ([6c5bc61](https://gitlab.com/biomedit/sett-rs/commit/6c5bc61056b549de1347d1d1009b452573ca5895)), Closes #134
- Add action for uploading a certificate to keyserver ([9aa01a5](https://gitlab.com/biomedit/sett-rs/commit/9aa01a5bf7a941c35322e31ac26d666fd54deac3)), Closes #106
- **cert:** Add certificate revocation dialog ([666d381](https://gitlab.com/biomedit/sett-rs/commit/666d38109f35bc7c587409e6de942cabb2280b1e)), Close #111
- **cert:** Add create revocation certificate dialog ([a6fa608](https://gitlab.com/biomedit/sett-rs/commit/a6fa608420452d25e65f6d71e1cce06edf366056)), Close #110
- Add certificate deletion dialog ([603ec01](https://gitlab.com/biomedit/sett-rs/commit/603ec010b93f0f16ea40dc86f4ede6e1e02467db)), closes #112
- **cert:** Add certificate export functionality ([d82bed9](https://gitlab.com/biomedit/sett-rs/commit/d82bed9838ab279fde69971ee4e09c13b95cf9e1)), Close #108, #109
- Add general structure of cert management tab ([c7728d3](https://gitlab.com/biomedit/sett-rs/commit/c7728d32d41bb89a232a04a651b3406f8d7800e6)), Closes #81
- **cert.rs:** Add functions to retrieve cert approval and path ([1941d78](https://gitlab.com/biomedit/sett-rs/commit/1941d78f0dff81d1ce53e6855afea724d487639e))
- **certificate:** Add "Add" certificate button ([d5af9f8](https://gitlab.com/biomedit/sett-rs/commit/d5af9f8925bace144bfbeaa51b97180c251f5df4)), Close #95
- Add encryption page ([4a669b0](https://gitlab.com/biomedit/sett-rs/commit/4a669b0a4f27dfc224b93a62822df61df94aad46)), Close #82
- **transfer:** Add transfer page ([e65e832](https://gitlab.com/biomedit/sett-rs/commit/e65e8327d8c56ade0b481d3e435c6f83f49d6699)), Closes #83
- Add `Page.svelte` and `Section.svelte` ([2eb21ae](https://gitlab.com/biomedit/sett-rs/commit/2eb21ae3d08a346ba5b71a40e3c4f91925291387))
- **ui:** Make styling consistent ([49181ce](https://gitlab.com/biomedit/sett-rs/commit/49181ce2d913285454e58d18ca5bcb92d36cf5ad)), Close #88
- Add decrypt and tasks pages ([d95a638](https://gitlab.com/biomedit/sett-rs/commit/d95a638ad9401aba45636e0161231e6a6e3e6e79)), Close #80
- Add (skeleton) GUI ([f7e765c](https://gitlab.com/biomedit/sett-rs/commit/f7e765c39e3b8435961788e9fd0881bc2e147b43)), Close #79

### 🐞 Bug Fixes

- **task:** Send error events for all possible backend errors ([b73c52f](https://gitlab.com/biomedit/sett-rs/commit/b73c52f8268947f9a6587d750bec29f3ccefe245))
- `region` and `endpoint` are now optional ([07b8fe2](https://gitlab.com/biomedit/sett-rs/commit/07b8fe2322ac4d4714601b11f7052d1c4ce0d98b))
- Make certificate revocation revoke both public and secret certs ([7a4eb26](https://gitlab.com/biomedit/sett-rs/commit/7a4eb2603a9574b2003b89d1ac932fbbbdb994cf)), Closes #152
- Micro-typo ([28eca1a](https://gitlab.com/biomedit/sett-rs/commit/28eca1a86d045c33862ed43b2ff1d43e3f26fcc6))
- Make reveal password button non-tabable ([cf3bf65](https://gitlab.com/biomedit/sett-rs/commit/cf3bf654a5c62fddbd35284c1de79ff3c78af756))
- The imported file decides which type ([9682946](https://gitlab.com/biomedit/sett-rs/commit/968294626be2fff52ef447893b49a5f835411f76)), Closes #126
- **cert:** Make getCertPath return a placeholder string instead of Error ([4a8ed85](https://gitlab.com/biomedit/sett-rs/commit/4a8ed85acda2da36e60f15e2ca5b51e20a5ee4f7))
- Secret key input field of type password ([1c766f7](https://gitlab.com/biomedit/sett-rs/commit/1c766f772617a74da39c88a2678bf7660b1256e7)), closes #104
- **security:** Export only selected TAURI variables to Vite ([d35d447](https://gitlab.com/biomedit/sett-rs/commit/d35d447584453b22c2574e7b349c999df10a7915))
- **destination:** Fix "attempted to assign to readonly property" ([224bd7b](https://gitlab.com/biomedit/sett-rs/commit/224bd7b93da715dfbbd1dbe8ea0284e7c7f44d03))
- Improve the top-level UI layout ([34f67b9](https://gitlab.com/biomedit/sett-rs/commit/34f67b9611629c260cdeb39187a7ad921fe1855a))
- Show correct job names in notifications ([53d3b68](https://gitlab.com/biomedit/sett-rs/commit/53d3b68d89864c7e8040da2aff72e0ca774a100d))

### 🧱 Build system and dependencies

- **windows:** Use NSIS installer ([ae3c4b0](https://gitlab.com/biomedit/sett-rs/commit/ae3c4b07ef2d97b06874913f763a2e259115e877)), Close #168
- **windows:** Use crypto-cng backend ([5812a8a](https://gitlab.com/biomedit/sett-rs/commit/5812a8a0e52244a070364868d127dbf3a226ad1c))
- Add CHANGELOG ([7563cc1](https://gitlab.com/biomedit/sett-rs/commit/7563cc12239080bce389583826cb644ab5826e87))
- Bump MSRV to 1.70 ([94705b8](https://gitlab.com/biomedit/sett-rs/commit/94705b818bba589df4796a586acfdeab5cc9e22e))
- Fix minimal dependency versions ([2048c8c](https://gitlab.com/biomedit/sett-rs/commit/2048c8cb0f56a86a0c3fed88053090be78e0957a)), Close #131
- Update frontend dependencies ([383c605](https://gitlab.com/biomedit/sett-rs/commit/383c605cc55d236928f6c4eb0626582b6923038d))
- Update Vite to v5 ([8f14bf1](https://gitlab.com/biomedit/sett-rs/commit/8f14bf10d7e64c2ca28156ba89d3ebc138773b20))
- Add 'eslin:fix' script shortcut to package.json ([452b4fd](https://gitlab.com/biomedit/sett-rs/commit/452b4fd00d6bbbf06c7ed5f75a5031417c61aa7d))
- Remove duplicate app version ([03c837c](https://gitlab.com/biomedit/sett-rs/commit/03c837cc8ec70f8f54a2d4e27fc6d63a17c68656))
- **lint:** Add `rules` to linter ([50296b2](https://gitlab.com/biomedit/sett-rs/commit/50296b23a0b5cffe0b7bf664d9b7b906b3dfa865))
- **postcss:** Use postcss-preset-env ([4307e81](https://gitlab.com/biomedit/sett-rs/commit/4307e815fe2c89c0885b340933fd5e616bc2cd21))
- **sett-gui:** Pin exact dependency versions in package.json ([c132032](https://gitlab.com/biomedit/sett-rs/commit/c13203240353e854631af23f4264695aaedd3dff))

### 👷 CI

- **prettier:** Ignore CHANGELOG ([cb92883](https://gitlab.com/biomedit/sett-rs/commit/cb9288344bec8441fcbceea23daeccd3743789eb))

### 📝 Documentation

- Update dev installation instructions ([3317566](https://gitlab.com/biomedit/sett-rs/commit/33175660144dd1f13c756c4c6b5c3fee1887a046))
- Complete the authors list with additional contributors ([b884770](https://gitlab.com/biomedit/sett-rs/commit/b8847704b710c56462517a6d2a978c4d4bf11b84))

### 🧹 Refactoring

- Change the terms certificate to key and secret to private ([c77fed5](https://gitlab.com/biomedit/sett-rs/commit/c77fed50f5e4a1f9f0d0cc73c5e0fc918429e458)), Closes #156
- Style for radio and checkboxes ([560b051](https://gitlab.com/biomedit/sett-rs/commit/560b0511fe26d45296a3baba01e37d9fef154e22))
- Improve consistency ([22ac645](https://gitlab.com/biomedit/sett-rs/commit/22ac6457f2321838bd4cf4baf8db8c61b1e7d897))
- Improve button consitency ([1dd8345](https://gitlab.com/biomedit/sett-rs/commit/1dd83458f9399b7318d62465652547314c2e86f2))
- **certificates tab:** Change status icons to solid icons ([34ca101](https://gitlab.com/biomedit/sett-rs/commit/34ca1015e23954e9c9cae64065abb7ebc6f44a63))
- Use `sett::dpgk::Package` for data package verification ([298ce2d](https://gitlab.com/biomedit/sett-rs/commit/298ce2d8a418a2ea6b26e0abf1aa46816833fc82))
- Replace button with Button component in Cert component ([d6e4bbf](https://gitlab.com/biomedit/sett-rs/commit/d6e4bbfc4157b83a3b91742d8fdc3ddb5ab7cd18))
- Add drag-and-drop component ([6f2fecc](https://gitlab.com/biomedit/sett-rs/commit/6f2fecc07ec48b4f01c1858c521e80c61169a20b)), Closes #99
- **backend:** Exclude public certs that have a private counterpart ([1fc6ea5](https://gitlab.com/biomedit/sett-rs/commit/1fc6ea545cf1d0ca3568df55033fcbce8d6fcc4f))
- Set `height` for `ApprovalIcon` ([14240f1](https://gitlab.com/biomedit/sett-rs/commit/14240f1e37f7ed0fce6ed7510f4e2b7af2a7c687))
- Define a generic and default class for badges ([601989e](https://gitlab.com/biomedit/sett-rs/commit/601989e26eb3688632bc87ace4c1fb17b5ebdec7))
- Make success and error colors brighter in dark mode ([aadb9c7](https://gitlab.com/biomedit/sett-rs/commit/aadb9c782ed73fef84ea512a0401904e8a9fa9ca))
- Get rid of section numbering and make section headers a bit bolder ([983a7a7](https://gitlab.com/biomedit/sett-rs/commit/983a7a7ff8b44b4a5aedf91651784bdc658b12ce))
- Apply some UI improvements to import dialog window ([1a2eb5f](https://gitlab.com/biomedit/sett-rs/commit/1a2eb5f1b42d5ff0df736b59aaeccc20729ecc94))
- Add `direction` to dropdown ([b0525f6](https://gitlab.com/biomedit/sett-rs/commit/b0525f6406ac832f8ff8d7e4596eb70dd40bd01d))
- Keep the gap between the buttons the same (`0.5rem`) ([34ea299](https://gitlab.com/biomedit/sett-rs/commit/34ea299b307538ae50998bfe89f978e56331708d))
- Making all the `small` buttons the same height ([cac541c](https://gitlab.com/biomedit/sett-rs/commit/cac541c3b7dea9951c41ad2640e59d2edae5aeb2))
- Remove `certificate(s)` from button labels ([518db7b](https://gitlab.com/biomedit/sett-rs/commit/518db7bcc76505d20126ee43a64907c1de961998))
- Create component for SecretInput ([eed5e0d](https://gitlab.com/biomedit/sett-rs/commit/eed5e0d969ddf152908c7219c85c5cd23a613cd0))
- Use CopyToClipboard component accross entire app ([8f5382e](https://gitlab.com/biomedit/sett-rs/commit/8f5382ece088649c9f96285f320e0b5357d2b8e0)), Closes #124
- Use custom error type ([9025f5f](https://gitlab.com/biomedit/sett-rs/commit/9025f5f14a1b8d094435a6a87c4cd05381ca28a2)), Close #123
- Move copy-to-clipboard functionality to its own component ([5a19c1e](https://gitlab.com/biomedit/sett-rs/commit/5a19c1e6be5d7494993cf7058ac014f3eae9b7bc))
- Add copy-to-clipboard icons to Button component ([a4d1672](https://gitlab.com/biomedit/sett-rs/commit/a4d16726fd229710ca0f09f1ede7f97645658ef2))
- **gnupug:** Use CertType instead of String ([40537bb](https://gitlab.com/biomedit/sett-rs/commit/40537bbd5a4e3d669daa1a521a2bc66a19deace5))
- **destination:** Make all the labels bold ([f514fba](https://gitlab.com/biomedit/sett-rs/commit/f514fbac8bfbfaee91e162121f15fd774f066c0f))
- Improve responsiveness for `Destination` panel ([952ce40](https://gitlab.com/biomedit/sett-rs/commit/952ce40a2827c3a033afe0cf87f691a090ea713b))
- Remove `secure TLS connection` input field ([b5e55cd](https://gitlab.com/biomedit/sett-rs/commit/b5e55cd1990221c3821509c114a915831aa23d75))
- Create a dedicated password dialog component ([e319ed4](https://gitlab.com/biomedit/sett-rs/commit/e319ed4a1d70281c2ecefda4aa73d181029b8286))
- Make `selectPackage` and `processOpts` available to other widgets/contexts ([ec765b5](https://gitlab.com/biomedit/sett-rs/commit/ec765b52f3dfda3a782bb7ca5ab52c46f9435642))
- Use `CloudArrowUp` as transfer icon ([01a502d](https://gitlab.com/biomedit/sett-rs/commit/01a502d54bc9a3d036f8581430ddbab2cce1d465))
