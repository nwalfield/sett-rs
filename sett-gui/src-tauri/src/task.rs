use std::{
    path::{Path, PathBuf},
    str::FromStr,
    sync::atomic::{AtomicU64, Ordering},
};

use chrono::Utc;
use serde::{Deserialize, Serialize};
use sett::{certstore, task::Mode};

use crate::error::Error;

#[derive(Clone, Serialize)]
#[serde(tag = "type", rename_all(serialize = "camelCase"))]
enum TaskStatus {
    Running { progress: f32 },
    Finished { destination: String },
    Error { message: String },
}

#[derive(Clone, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub(super) struct TaskEvent {
    task_id: u32,
    status: TaskStatus,
}

#[derive(Debug, Deserialize)]
pub(super) struct Task {
    id: u32,
    kind: TaskKind,
}

#[derive(Debug, Deserialize)]
pub(super) enum TaskKind {
    Encrypt(EncryptOpts),
    Decrypt(DecryptOpts),
    Transfer(TransferOpts),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub(super) struct DecryptOpts {
    package: String,
    destination: String,
    signer: String,
    recipients: Vec<String>,
    password: String,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(super) struct TransferOpts {
    package: String,
    destination: DestinationProps,
    verify: bool,
    portal_url: String,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
enum DestinationProps {
    Local(LocalProps),
    S3(S3Props),
    Sftp(SftpProps),
}

impl From<DestinationProps> for sett::destination::Destination<'_> {
    fn from(val: DestinationProps) -> Self {
        match val {
            DestinationProps::Local(opts) => sett::destination::local::LocalOpts::from(opts).into(),
            DestinationProps::S3(opts) => sett::destination::s3::S3Opts::from(opts).into(),
            DestinationProps::Sftp(opts) => sett::destination::sftp::SftpOpts::from(opts).into(),
        }
    }
}

#[derive(Debug, Deserialize)]
struct LocalProps(String);

impl From<LocalProps> for sett::destination::local::LocalOpts {
    fn from(val: LocalProps) -> Self {
        Self {
            path: val.0.into(),
            name: None,
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
struct SftpProps {
    username: String,
    host: String,
    base_path: String,
    ssh_key: SSHKey,
}

impl From<SftpProps> for sett::destination::sftp::SftpOpts<'_> {
    fn from(val: SftpProps) -> Self {
        let mut host = val.host.split(':');
        Self {
            host: String::from(
                host.next()
                    .expect("the split iterator should always have at least one element"),
            )
            .into(),
            port: host
                .next()
                .and_then(|s| u16::from_str(s).ok())
                .unwrap_or(22),
            username: val.username.into(),
            base_path: PathBuf::from(&val.base_path).into(),
            key_path: val.ssh_key.location.map(Into::into),
            key_password: val.ssh_key.password.map(Into::into),
            ..Default::default()
        }
    }
}

#[derive(Debug, Deserialize)]
struct SSHKey {
    location: Option<String>,
    password: Option<String>,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
struct S3Props {
    url: String,
    bucket: String,
    access_key: String,
    secret_key: String,
    session_token: String,
}

fn add_prefix_to_url_if_needed(url: String) -> String {
    let prefix = if !url.starts_with("https://") && !url.starts_with("http://") {
        "https://"
    } else {
        ""
    };
    format!("{}{}", prefix, url)
}

impl<'a> From<S3Props> for sett::destination::s3::S3Opts<'a> {
    fn from(val: S3Props) -> Self {
        sett::destination::s3::S3Opts {
            endpoint: Some(add_prefix_to_url_if_needed(val.url).into()),
            bucket: val.bucket.into(),
            region: None,
            profile: None,
            access_key: Some(val.access_key.into()),
            secret_key: Some(val.secret_key.into()),
            session_token: Some(val.session_token.into()),
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(super) struct EncryptOpts {
    files: Vec<String>,
    signer: String,
    recipients: Vec<String>,
    password: String,
    transfer_id: Option<u32>,
    destination: DestinationProps,
    verify: bool,
    portal_url: String,
}

struct Progress<Cb: Fn(f32)> {
    current: AtomicU64,
    total: AtomicU64,
    fraction: AtomicU64,
    cb: Cb,
}

impl<Cb: Fn(f32)> Progress<Cb> {
    fn new(cb: Cb) -> Self {
        Self {
            current: AtomicU64::new(0),
            total: AtomicU64::new(0),
            fraction: AtomicU64::new(0),
            cb,
        }
    }
    pub fn update(&self) {
        let fraction = (100 * self.current.load(Ordering::Acquire))
            .checked_div(self.total.load(Ordering::Acquire))
            .unwrap_or(100);
        let previous = self.fraction.fetch_max(fraction, Ordering::Release);
        if fraction > previous {
            (self.cb)(fraction as f32);
        }
    }
}

impl<Cb: Fn(f32)> sett::utils::Progress for Progress<Cb> {
    fn set_length(&self, len: u64) {
        self.total.store(len, Ordering::Release);
    }
    fn inc(&self, delta: u64) {
        self.current.fetch_add(delta, Ordering::Release);
        self.update();
    }
    fn finish(&self) {
        self.current
            .store(self.total.load(Ordering::Acquire), Ordering::Release);
        self.update();
    }
}

fn load_cert(
    fp: &str,
    store: &certstore::CertStore,
    cert_type: certstore::CertType,
) -> Result<Vec<u8>, Error> {
    Ok(store
        .get_cert(&certstore::QueryTerm::from_str(fp)?, cert_type)?
        .pop()
        .expect("there should be one cert when queried by fingerprint")
        .serialize(cert_type, certstore::SerializationFormat::Binary)?)
}

enum CertsFor {
    Encrypt,
    Decrypt,
}

fn load_certs(
    signer: &str,
    recipients: &[String],
    task: CertsFor,
) -> Result<(Vec<u8>, Vec<Vec<u8>>), Error> {
    let store = certstore::CertStore::open(Default::default())?;
    let signer = load_cert(
        signer,
        &store,
        match task {
            CertsFor::Encrypt => certstore::CertType::Secret,
            CertsFor::Decrypt => certstore::CertType::Public,
        },
    )?;
    let recipients = match task {
        CertsFor::Encrypt => recipients
            .iter()
            .map(|fp| load_cert(fp, &store, certstore::CertType::Public))
            .collect::<Result<_, _>>()?,
        CertsFor::Decrypt => recipients
            .iter()
            .filter_map(|fp| load_cert(fp, &store, certstore::CertType::Secret).ok())
            .collect::<Vec<_>>(),
    };
    Ok((signer, recipients))
}

#[tauri::command]
pub(super) async fn check_task(task: TaskKind) -> Result<(), Error> {
    let progress = None::<Progress<fn(f32)>>;
    match task {
        TaskKind::Decrypt(opts) => decrypt(opts, Mode::Check, progress).await?,
        TaskKind::Transfer(opts) => transfer(opts, Mode::Check, progress).await?,
        TaskKind::Encrypt(opts) => encrypt(opts, Mode::Check, progress).await?,
    };
    Ok(())
}

#[tauri::command]
pub(super) async fn run_task(window: tauri::Window, task: Task) -> Result<(), Error> {
    macro_rules! emit_task_event {
        ($payload:expr, $window:expr) => {
            if $window.emit("task", $payload).is_err() {
                log::error!("Failed to emit task event");
            }
        };
    }
    macro_rules! emit_error {
        ($f:literal) => {
            |e| {
                emit_task_event!(
                    TaskEvent {
                        task_id: task.id,
                        status: TaskStatus::Error {
                            message: format!(concat!("{:", $f, "}"), e),
                        },
                    },
                    window
                );
                e
            }
        };
    }
    macro_rules! emit_finished {
        ($status:expr) => {
            emit_task_event!(
                TaskEvent {
                    task_id: task.id,
                    status: TaskStatus::Finished {
                        destination: match $status {
                            sett::task::Status::Completed { destination, .. } => destination,
                            sett::task::Status::Checked { .. } => unreachable!(),
                        }
                    },
                },
                window
            );
        };
    }

    let window_clone = window.clone();
    let progress = Some(Progress::new(move |progress| {
        emit_task_event!(
            TaskEvent {
                task_id: task.id,
                status: TaskStatus::Running { progress },
            },
            window_clone
        )
    }));
    match task.kind {
        TaskKind::Decrypt(opts) => {
            let status = decrypt(opts, Mode::Run, progress)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
        TaskKind::Transfer(opts) => {
            let status = transfer(opts, Mode::Run, progress)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
        TaskKind::Encrypt(opts) => {
            let status = encrypt(opts, Mode::Run, progress)
                .await
                .map_err(emit_error!("?"))?;
            emit_finished!(status);
            Ok(())
        }
    }
}

async fn encrypt(
    opts: EncryptOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + 'static>>,
) -> Result<sett::task::Status, Error> {
    let (signer, recipients) = load_certs(&opts.signer, &opts.recipients, CertsFor::Encrypt)?;
    let timestamp = Utc::now();
    let mut prefix = None;
    if opts.verify {
        let metadata = sett::dpkg::PkgMetadata {
            sender: opts.signer,
            recipients: opts.recipients,
            timestamp,
            transfer_id: opts.transfer_id,
            ..Default::default()
        };
        prefix = Some(verify_package(&metadata, "missing", &opts.portal_url).await?);
    }
    let encrypt_opts = sett::encrypt::EncryptOpts {
        files: opts.files.iter().map(PathBuf::from).collect(),
        recipients,
        signer: Some(signer),
        max_cpu: None,
        mode,
        purpose: None,
        transfer_id: opts.transfer_id,
        compression_algorithm: Default::default(),
        password: Some(opts.password.into()),
        progress,
        timestamp,
        prefix,
    };
    Ok(sett::encrypt::encrypt(
        encrypt_opts,
        sett::destination::Destination::from(opts.destination),
    )
    .await?)
}

async fn transfer(
    opts: TransferOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + 'static>>,
) -> Result<sett::task::Status, Error> {
    if opts.verify {
        let metadata = crate::cert::read_pkg_metadata(opts.package.clone()).await?;
        let file_name = Path::new(&opts.package)
            .file_name()
            .ok_or("Failed to get file name")?
            .to_string_lossy();
        let project_code = verify_package(&metadata, &file_name, &opts.portal_url).await?;
        let file_name_expected = format!(
            "{}_{}.zip",
            project_code,
            metadata.timestamp.format(sett::dpkg::DATETIME_FORMAT)
        );
        if file_name != file_name_expected {
            return Err(Error::from(format!(
                "Package verification failed: invalid file name '{}' (expected '{}')",
                file_name, file_name_expected
            )));
        }
    }
    match opts.destination {
        DestinationProps::Local(_) => Err(Error::from("Local destination is not supported")),
        DestinationProps::Sftp(props) => Ok(sett::destination::sftp::upload(
            &opts.package,
            &props.into(),
            progress.as_ref(),
            None,
            mode,
        )?),
        DestinationProps::S3(props) => {
            Ok(sett::destination::s3::upload(&props.into(), &opts.package, mode, progress).await?)
        }
    }
}

async fn decrypt(
    opts: DecryptOpts,
    mode: Mode,
    progress: Option<Progress<impl Fn(f32) + Send + Sync + 'static>>,
) -> Result<sett::task::Status, Error> {
    let (signer, recipients) = load_certs(&opts.signer, &opts.recipients, CertsFor::Decrypt)?;
    Ok(sett::decrypt::decrypt(sett::decrypt::DecryptOpts {
        package: sett::dpkg::Package::open(&opts.package)?,
        recipients,
        signer,
        password: Some(opts.password.into()),
        output: Some(PathBuf::from(opts.destination)),
        decrypt_only: false,
        mode,
        progress,
    })
    .await?)
}

async fn verify_package(
    metadata: &sett::dpkg::PkgMetadata,
    package_name: &str,
    portal_url: &str,
) -> Result<String, Error> {
    let portal = sett::portal::Portal::new(portal_url)?;
    let response = portal.check_package(metadata, package_name).await?;
    Ok(response.project_code)
}
