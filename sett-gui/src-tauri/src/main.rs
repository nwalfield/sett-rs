// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

mod cert;
mod error;
mod fs;
mod task;

fn main() {
    #[cfg(not(windows))]
    {
        let _ = fix_path_env::fix();
    }
    tauri::Builder::default()
        .plugin(
            tauri_plugin_log::Builder::default()
                .targets([tauri_plugin_log::LogTarget::LogDir])
                .level(log::LevelFilter::Debug)
                .build(),
        )
        .plugin(tauri_plugin_store::Builder::default().build())
        .invoke_handler(tauri::generate_handler![
            cert::export_cert,
            cert::generate_cert,
            cert::generate_rev_sig,
            cert::get_cert_approval_status_from_portal,
            cert::get_certstore_path,
            cert::import_cert_from_file,
            cert::import_cert_from_string,
            cert::import_cert_from_gnupg,
            cert::import_cert_from_keyserver,
            cert::list_certs,
            cert::list_gnupg_certs,
            cert::read_pkg_metadata,
            cert::revoke_cert,
            cert::upload_cert_to_keyserver,
            task::check_task,
            task::run_task,
            fs::is_dir,
            fs::is_writeable,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
