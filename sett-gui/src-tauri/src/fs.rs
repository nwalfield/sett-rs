use crate::error::Error;
use std::path::PathBuf;
use tempfile::NamedTempFile;

#[tauri::command]
pub(super) async fn is_dir(path: String) -> bool {
    PathBuf::from(path).is_dir()
}

#[tauri::command]
pub(super) fn is_writeable(path: String) -> Result<bool, Error> {
    match NamedTempFile::new_in(PathBuf::from(path)) {
        Ok(marker_file) => {
            marker_file.close()?;
            Ok(true)
        }
        Err(_) => Ok(false),
    }
}
