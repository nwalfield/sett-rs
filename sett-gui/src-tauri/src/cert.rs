use std::str::FromStr;

use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use sett::{
    certstore::{self, Cert},
    keyserver::{Keyserver, KeyserverEmailStatus},
    portal,
};

use crate::error::Error;

#[derive(Serialize, Deserialize, Copy, Clone)]
pub(crate) enum CertType {
    Public,
    Secret,
}

impl From<certstore::CertType> for CertType {
    fn from(val: certstore::CertType) -> Self {
        match val {
            certstore::CertType::Public => Self::Public,
            certstore::CertType::Secret => Self::Secret,
        }
    }
}

impl From<CertType> for certstore::CertType {
    fn from(val: CertType) -> Self {
        match val {
            CertType::Public => Self::Public,
            CertType::Secret => Self::Secret,
        }
    }
}

#[derive(Serialize, Deserialize)]
enum ValidityStatus {
    Valid,
    Revoked,
}

#[derive(Serialize, Deserialize, Copy, Clone)]
pub(super) enum ReasonForRevocation {
    Unspecified,
    Superseded,
    Compromised,
    Retired,
}

impl From<certstore::ReasonForRevocation> for ReasonForRevocation {
    fn from(value: certstore::ReasonForRevocation) -> Self {
        match value {
            certstore::ReasonForRevocation::KeySuperseded => Self::Superseded,
            certstore::ReasonForRevocation::KeyRetired => Self::Retired,
            certstore::ReasonForRevocation::UIDRetired => Self::Retired,
            certstore::ReasonForRevocation::KeyCompromised => Self::Compromised,
            certstore::ReasonForRevocation::Unspecified => Self::Unspecified,
            _ => Self::Unspecified,
        }
    }
}

impl From<ReasonForRevocation> for certstore::ReasonForRevocation {
    fn from(value: ReasonForRevocation) -> Self {
        match value {
            ReasonForRevocation::Unspecified => Self::Unspecified,
            ReasonForRevocation::Superseded => Self::KeySuperseded,
            ReasonForRevocation::Compromised => Self::KeyCompromised,
            ReasonForRevocation::Retired => Self::KeyRetired,
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Validity {
    status: ValidityStatus,
    reasons: Option<Vec<ReasonForRevocation>>,
    messages: Option<Vec<String>>,
}

impl From<certstore::RevocationStatus> for Validity {
    fn from(revocation_status: certstore::RevocationStatus) -> Self {
        match revocation_status {
            certstore::RevocationStatus::Revoked(signatures) => {
                let (reasons, messages) = signatures
                    .into_iter()
                    .map(|sig| {
                        sig.reason_for_revocation().map_or(
                            (ReasonForRevocation::Unspecified, String::from("")),
                            |(r, m)| (r.into(), String::from_utf8_lossy(m).to_string()),
                        )
                    })
                    .unzip();
                Self {
                    status: ValidityStatus::Revoked,
                    reasons: Some(reasons),
                    messages: Some(messages),
                }
            }
            _ => Self {
                status: ValidityStatus::Valid,
                reasons: None,
                messages: None,
            },
        }
    }
}

#[derive(Serialize, Deserialize)]
enum ExpirationDateStatus {
    Valid,
    Expired,
    ReadError, // Error occurred while trying to read the expiration date.
}

#[derive(Serialize, Deserialize)]
struct ExpirationDate {
    status: ExpirationDateStatus,
    details: String,
}

impl From<Cert> for ExpirationDate {
    fn from(cert: Cert) -> Self {
        match cert.expiration_time() {
            Ok(option_time) => match option_time {
                Some(expiration_time) => Self {
                    status: if expiration_time > std::time::SystemTime::now() {
                        ExpirationDateStatus::Valid
                    } else {
                        ExpirationDateStatus::Expired
                    },
                    details: DateTime::<Local>::from(expiration_time)
                        .format("%d.%m.%Y %H:%M")
                        .to_string(),
                },
                None => Self {
                    status: ExpirationDateStatus::Valid,
                    details: String::from("does not expire"),
                },
            },
            Err(msg) => Self {
                status: ExpirationDateStatus::ReadError,
                details: msg.to_string(),
            },
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub(super) struct CertInfo {
    fingerprint: String,
    userid: Option<String>,
    cert_type: CertType,
    validity: Validity,
    expiration_date: ExpirationDate,
}

impl From<certstore::Cert> for CertInfo {
    fn from(cert: certstore::Cert) -> Self {
        Self {
            fingerprint: cert.fingerprint().to_string(),
            userid: cert.userids().into_iter().next(),
            cert_type: cert.cert_type().into(),
            validity: cert.revocation_status().into(),
            expiration_date: cert.into(),
        }
    }
}

impl From<sett::gnupg::CertInfo> for CertInfo {
    fn from(cert: sett::gnupg::CertInfo) -> Self {
        Self {
            fingerprint: cert.fingerprint,
            userid: cert.userid,
            cert_type: cert.cert_type.into(),
            validity: Validity {
                status: ValidityStatus::Valid,
                reasons: None,
                messages: None,
            },
            expiration_date: ExpirationDate {
                status: ExpirationDateStatus::ReadError,
                details: String::from("not needed"),
            },
        }
    }
}

#[tauri::command]
pub(super) async fn list_certs() -> Result<Vec<CertInfo>, Error> {
    let store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: true,
        ..Default::default()
    })?;
    let mut secret_certs = store.get_all_certs(certstore::CertType::Secret)?;
    let secret_fingerprints = secret_certs
        .iter()
        .map(|cert| cert.fingerprint().to_string())
        .collect::<Vec<String>>();
    let mut public_certs = store
        .get_all_certs(certstore::CertType::Public)?
        .into_iter()
        .filter(|cert| !secret_fingerprints.contains(&cert.fingerprint().to_string()))
        .collect::<Vec<Cert>>();

    let get_user_id = |cert: &Cert| cert.userids().first().map(|x| x.to_lowercase());
    secret_certs.sort_unstable_by_key(get_user_id);
    public_certs.sort_unstable_by_key(get_user_id);

    Ok(secret_certs
        .into_iter()
        .chain(public_certs.into_iter())
        .map(Into::into)
        .collect())
}

#[tauri::command]
pub(super) async fn generate_cert(
    userid: String,
    password: String,
) -> Result<(CertInfo, String), Error> {
    let (cert, rev) = certstore::generate_cert(
        &userid,
        password.as_bytes(),
        certstore::CipherSuite::Cv25519,
    )?;
    certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: false,
        ..Default::default()
    })?
    .add_cert(&cert, certstore::CertType::Secret)?;
    Ok((cert.into(), String::from_utf8(rev)?))
}

fn import_cert_from_bytes<R: std::io::Read + Send + Sync>(data: R) -> Result<Vec<CertInfo>, Error> {
    let mut store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: false,
        ..Default::default()
    })?;
    certstore::read_cert(data)?
        .iter()
        .map(|cert| Ok(store.add_cert(cert, cert.cert_type())?.into()))
        .collect()
}

#[tauri::command]
pub(super) async fn import_cert_from_string(data: String) -> Result<Vec<CertInfo>, Error> {
    import_cert_from_bytes(std::io::Cursor::new(&data))
}

#[tauri::command]
pub(super) async fn import_cert_from_file(path: String) -> Result<Vec<CertInfo>, Error> {
    import_cert_from_bytes(std::fs::File::open(path)?)
}

#[tauri::command]
pub(super) async fn upload_cert_to_keyserver(
    fingerprint: String,
    verify: bool,
) -> Result<String, Error> {
    let cert = load_cert(fingerprint, CertType::Public)?;
    if let Some(email) = certstore::get_primary_email(&cert)? {
        let keyserver = Keyserver::new()?;
        let response = keyserver.upload_cert(&cert).await?;
        if verify {
            return match response.status.get(&email) {
                Some(KeyserverEmailStatus::Unpublished) | Some(KeyserverEmailStatus::Pending) => {
                    keyserver.verify_cert(&response.token, &email).await?;
                    Ok("Key uploaded and verification requested.".into())
                }
                Some(KeyserverEmailStatus::Revoked) => {
                    Err("This key is revoked and cannot be used.".into())
                }
                Some(KeyserverEmailStatus::Published) => {
                    Err("This key is already verified.".into())
                }
                None => Err(format!("Couldn't find '{}' in keyserver's response.", email).into()),
            };
        }
        Ok("OpenPGP key successfully uploaded.".into())
    } else {
        Err("This key does not contain an email.".into())
    }
}

#[tauri::command]
pub(super) async fn list_gnupg_certs(cert_type: CertType) -> Result<Vec<CertInfo>, Error> {
    Ok(
        sett::gnupg::list_keys(cert_type.into(), None::<&std::path::Path>)?
            .into_iter()
            .map(Into::into)
            .collect(),
    )
}

#[tauri::command]
pub(super) async fn import_cert_from_gnupg(
    identifier: String,
    cert_type: CertType,
    password: Option<String>,
) -> Result<(), Error> {
    let cert_type = cert_type.into();
    let mut store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: false,
        ..Default::default()
    })?;
    store.add_cert(
        &sett::certstore::Cert::from_bytes(sett::gnupg::export_key(
            &identifier,
            cert_type,
            password.map(|p| p.into_bytes()).as_deref(),
            None::<&std::path::Path>,
        )?)?,
        cert_type,
    )?;
    Ok(())
}

#[tauri::command]
pub(super) fn import_cert_from_keyserver(identifier: String) -> Result<Vec<CertInfo>, Error> {
    let query = certstore::QueryTerm::from_str(&identifier)?;
    let store = certstore::CertStore::open(certstore::CertStoreOpts {
        use_keyserver: true,
        ..Default::default()
    })?;
    Ok(store
        .get_cert(&query, certstore::CertType::Public)?
        .into_iter()
        .map(Into::into)
        .collect())
}

#[derive(Serialize)]
pub(crate) enum ApprovalStatus {
    Approved,
    Revoked,
    CertificateDeleted,
    Pending,
    CertificateRevoked,
    CertificateUnknown,
    Rejected,
}

#[derive(Serialize)]
pub(super) struct CertApprovalStatus {
    fingerprint: String,
    status: ApprovalStatus,
}

/// Path of local OpenPGP certificate store on disk.
#[derive(Serialize)]
pub(super) struct CertStorePath {
    secret: String,
    public: String,
}

impl From<portal::ApprovalStatus> for ApprovalStatus {
    fn from(val: portal::ApprovalStatus) -> Self {
        match val {
            portal::ApprovalStatus::Approved => Self::Approved,
            portal::ApprovalStatus::ApprovalRevoked => Self::Revoked,
            portal::ApprovalStatus::Deleted => Self::CertificateDeleted,
            portal::ApprovalStatus::Pending => Self::Pending,
            portal::ApprovalStatus::KeyRevoked => Self::CertificateRevoked,
            portal::ApprovalStatus::UnknownKey => Self::CertificateUnknown,
            portal::ApprovalStatus::Rejected => Self::Rejected,
        }
    }
}

impl From<portal::KeyStatus> for CertApprovalStatus {
    fn from(data: portal::KeyStatus) -> Self {
        Self {
            fingerprint: data.fingerprint,
            status: data.status.into(),
        }
    }
}

/// Retrieve the approval status of OpenPGP certificates specified via their
/// `fingerprints` from the portal.
#[tauri::command]
pub(super) async fn get_cert_approval_status_from_portal(
    portal_url: String,
    fingerprints: Vec<String>,
) -> Result<Vec<CertApprovalStatus>, Error> {
    Ok(portal::Portal::new(&portal_url)?
        .get_key_status(&fingerprints)
        .await?
        .into_iter()
        .map(Into::into)
        .collect())
}

/// Return the paths of the secret and public certificate stores on disk.
#[tauri::command]
pub(super) async fn get_certstore_path() -> Result<CertStorePath, Error> {
    let store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: true,
        ..Default::default()
    })?;
    Ok(CertStorePath {
        secret: store
            .get_store_path(certstore::CertType::Secret)
            .to_string_lossy()
            .to_string(),
        public: store
            .get_store_path(certstore::CertType::Public)
            .to_string_lossy()
            .to_string(),
    })
}

fn load_cert(fingerprint: String, cert_type: CertType) -> Result<certstore::Cert, Error> {
    let query = certstore::QueryTerm::from_str(&fingerprint)?;
    let store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: true,
        allow_create: false,
        ..Default::default()
    })?;
    Ok(store
        .get_cert(&query, cert_type.into())?
        .pop()
        .expect("`certs` should contain exactly 1 element at this point."))
}

#[tauri::command]
pub(super) async fn export_cert(fingerprint: String, cert_type: CertType) -> Result<String, Error> {
    let serialized = load_cert(fingerprint, cert_type)?.serialize(
        cert_type.into(),
        certstore::SerializationFormat::AsciiArmored,
    )?;
    Ok(String::from_utf8(serialized)?)
}

#[tauri::command]
pub(super) async fn generate_rev_sig(
    fingerprint: String,
    reason: ReasonForRevocation,
    message: String,
    password: String,
) -> Result<String, Error> {
    let cert = load_cert(fingerprint, CertType::Secret)?;
    let rev =
        cert.generate_rev_sig(reason.into(), message.as_bytes(), Some(password.as_bytes()))?;
    Ok(String::from_utf8(rev)?)
}

#[tauri::command]
pub(super) async fn revoke_cert(fingerprint: String, rev: String) -> Result<(), Error> {
    let cert = load_cert(fingerprint, CertType::Secret)?;
    let revoked = cert.revoke(std::io::Cursor::new(&rev))?;
    let mut store = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: false,
        allow_create: false,
        ..Default::default()
    })?;
    store.add_cert(&revoked, certstore::CertType::Public)?;
    store.add_cert(&revoked, certstore::CertType::Secret)?;
    Ok(())
}

#[tauri::command]
pub(crate) async fn read_pkg_metadata(
    path: String,
) -> Result<sett::dpkg::PkgMetadata, crate::error::Error> {
    let certstore = certstore::CertStore::open(certstore::CertStoreOpts {
        read_only: true,
        ..Default::default()
    })?;

    // Verify the file has the correct structure for a data package
    // and that the metadata signature is valid. Then extract the metadata.
    Ok(sett::dpkg::Package::open(path)?
        .verify(&certstore)
        .map_err(|e| format!("error verifying package - {e}"))?
        .metadata()
        .map_err(|e| format!("error reading package metadata - {e}"))?)
}
