import ArrowDownTray from '../icon/ArrowDownTray.svelte';
import ArrowPath from '../icon/ArrowPath.svelte';
import ArrowUpTray from '../icon/ArrowUpTray.svelte';
import ArrowUturnLeft from '../icon/ArrowUturnLeft.svelte';
import ChevronDown from '../icon/ChevronDown.svelte';
import ChevronUp from '../icon/ChevronUp.svelte';
import ClipboardDocumentCheck from '../icon/ClipboardDocumentCheck.svelte';
import ClipboardDocumentList from '../icon/ClipboardDocumentList.svelte';
import Cog8Tooth from '../icon/Cog8Tooth.svelte';
import type { ComponentType } from 'svelte';
import Cube from '../icon/Cube.svelte';
import DocumentArrowDown from '../icon/DocumentArrowDown.svelte';
import DocumentArrowUp from '../icon/DocumentArrowUp.svelte';
import DocumentPlus from '../icon/DocumentPlus.svelte';
import EllipsisHorizontal from '../icon/EllipsisHorizontal.svelte';
import Eye from '../icon/Eye.svelte';
import EyeSlash from '../icon/EyeSlash.svelte';
import Folder from '../icon/Folder.svelte';
import FolderPlus from '../icon/FolderPlus.svelte';
import Lock from '../icon/Lock.svelte';
import PlusCircle from '../icon/PlusCircle.svelte';
import Send from '../icon/Send.svelte';
import Trash from '../icon/Trash.svelte';
import Unlock from '../icon/Unlock.svelte';
import Warning from '../icon/Warning.svelte';
import XMark from '../icon/XMark.svelte';

export type TableRow = {
  key: string;
  value: string;
  tooltip?: string;
};

// Icon to use on a Button component.
export type ButtonIcon =
  | 'ArrowDownTray'
  | 'ArrowPath'
  | 'ArrowUpTray'
  | 'ArrowUturnLeft'
  | 'ChevronDown'
  | 'ChevronUp'
  | 'ClipboardDocumentCheck'
  | 'ClipboardDocumentList'
  | 'Cog8Tooth'
  | 'Cube'
  | 'DocumentArrowDown'
  | 'DocumentArrowUp'
  | 'DocumentPlus'
  | 'EllipsisHorizontal'
  | 'Eye'
  | 'EyeSlash'
  | 'Folder'
  | 'FolderPlus'
  | 'Lock'
  | 'PlusCircle'
  | 'Send'
  | 'Trash'
  | 'Unlock'
  | 'Warning'
  | 'XMark'
  | null;

export const iconMap: {
  [key: string]: ComponentType;
} = {
  ArrowDownTray,
  ArrowPath,
  ArrowUpTray,
  ArrowUturnLeft,
  ChevronDown,
  ChevronUp,
  ClipboardDocumentCheck,
  ClipboardDocumentList,
  Cog8Tooth,
  Cube,
  DocumentArrowDown,
  DocumentArrowUp,
  DocumentPlus,
  EllipsisHorizontal,
  Eye,
  EyeSlash,
  Folder,
  FolderPlus,
  Lock,
  PlusCircle,
  Send,
  Trash,
  Unlock,
  Warning,
  XMark,
};

// Icon to use on a NavItem component.
export type NavItemIcon =
  | 'Cog6Tooth'
  | 'Info'
  | 'Key'
  | 'List'
  | 'Lock'
  | 'Send'
  | 'Unlock';

// Location where tooltip gets displayed.
export type TooltipPosition = 'bottom' | 'right' | 'top';

// Direction of tooltip text.
export type TooltipDirection = 'center' | 'to-left' | 'to-right';
