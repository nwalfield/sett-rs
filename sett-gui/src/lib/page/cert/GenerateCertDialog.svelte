<script lang="ts">
  import { invoke } from '@tauri-apps/api';

  import { type CertInfo } from '../../store/cert';
  import { NotificationLevel } from '../../store/event';

  import Alert from '../../component/Alert.svelte';
  import Button from '../../component/Button.svelte';
  import CopyToClipboard from '../../component/CopyToClipboard.svelte';
  import Dialog from '../../component/Dialog.svelte';
  import SecretInput from '../../component/SecretInput.svelte';
  import UploadCertDialog from './UploadCertDialog.svelte';

  let name = '';
  let comment = '';
  let email = '';
  let password = '';
  let passwordRepeat = '';
  let validationError = '';

  let state: 'confirm' | 'error' | 'input' | 'run' | 'success' | 'uploadCert' =
    'input';
  let cert: CertInfo;
  let revocationSignature = '';
  let errorMsg = '';
  let show = true;
  let showPassword = false;
  let invalidEmail = false;
  $: userid =
    name + (comment ? ` (${comment})` : '') + (email ? ` <${email}>` : '');

  export let onClose: () => void;
  export let refreshCertStore: () => void;

  $: if (!show) onClose();

  async function generateKey() {
    state = 'run';
    try {
      [cert, revocationSignature] = await invoke<[CertInfo, string]>(
        'generate_cert',
        {
          userid: userid,
          password: password,
        },
      );

      state = 'success';
    } catch (e) {
      errorMsg = e as string;
      state = 'error';
    }
    refreshCertStore();
  }

  function validateEmail() {
    invalidEmail = !/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(
      email.toLowerCase(),
    );
    if (invalidEmail) {
      validationError = 'Invalid email address syntax';
    }
  }

  function validatePassword() {
    if (passwordRepeat && password !== passwordRepeat) {
      validationError = 'Passwords do not match';
    }
    if (!passwordRepeat) {
      validationError = 'Please confirm your password';
    }
    if (password.length < 10) {
      validationError = 'Password must be at least 10 characters long';
    }
  }

  function isEmpty(varLabel: string, varValue: string) {
    if (varValue.length == 0) {
      validationError = varLabel + ' is missing';
    }
  }

  function validateInput() {
    validationError = '';
    validatePassword();
    isEmpty('Password', password);
    validateEmail();
    isEmpty('Email address', email);
    isEmpty('Name of key owner', name);

    if (!validationError) {
      return 'confirm';
    }
    return 'input';
  }
</script>

<Dialog
  bind:show
  title="Generate OpenPGP key pair"
  closeOnBackdropClick={false}
>
  {#if state === 'input'}
    {#if validationError}
      <div class="alert">
        <Alert level={NotificationLevel.Warning}>{validationError}</Alert>
      </div>
    {/if}
    <div class="grid-layout">
      <label for="name" class="required">Name</label>
      <!-- svelte-ignore a11y-autofocus -->
      <input
        bind:value={name}
        type="text"
        id="name"
        name="name"
        required
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        autofocus
        placeholder="First and last name of key owner"
      />
      <label for="comment">Comment</label>
      <input
        bind:value={comment}
        type="text"
        id="comment"
        name="comment"
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        placeholder="Optional: additional info about key owner"
      />
      <label for="email" class="required">Email</label>
      <input
        bind:value={email}
        type="email"
        id="email"
        name="email"
        required
        autocapitalize="none"
        autocomplete="off"
        autocorrect="off"
        on:blur={validateEmail}
        placeholder="Email of key owner"
      />
      <label for="password" class="required">Password</label>
      <SecretInput
        bind:userInput={password}
        bind:showPassword
        required
        id="password"
        minlength={10}
        placeholder="Password for private key - min 10 chars"
      />
      <label for="password-repeat" class="required">Confirm password</label>
      <SecretInput
        bind:userInput={passwordRepeat}
        required
        id="password-repeat"
        bind:showPassword
      />
    </div>
    <div class="button-layout">
      <Button
        color="secondary"
        text="Cancel"
        tooltip="Close window without generating new key"
        on:click={onClose}
      />
      <Button
        color={'primary'}
        text="Next >"
        tooltip="Proceed with new key generation"
        on:click={() => (state = validateInput())}
      />
    </div>
  {:else if state === 'confirm'}
    <p>
      Please <strong>verify the name and email</strong> to be associated with the
      new key. Key expiry dates cannot be specified in this version of the application.
    </p>
    <div class="grid-layout-confirm">
      <span><strong>Key user ID:</strong></span>
      <span>{userid}</span>
      <span><strong>Expiry date:</strong></span>
      <span>never expires</span>
    </div>
    <div class="button-layout">
      <Button
        text="Back"
        color="secondary"
        tooltip="Go back to data input form"
        tooltipDirection="to-right"
        on:click={() => (state = 'input')}
      />
      <Button
        color="secondary"
        text="Cancel"
        on:click={onClose}
        tooltip="Close window without generating new key"
        tooltipDirection="center"
      />
      <Button
        color="primary"
        text="Generate key"
        icon="Cog8Tooth"
        tooltip="Generate a new OpenPGP key with the provided data"
        on:click={generateKey}
      />
    </div>
  {:else if state === 'run'}
    <p>Generating OpenPGP key pair...</p>
  {:else if state === 'success'}
    <Alert level={NotificationLevel.Success}
      >Successfully created OpenPGP key pair</Alert
    >
    <p>
      In addition to your new public and private key pair, a revocation
      signature has been created (see below). A revocation signature allows you
      to invalidate your key at any time, i.e. to let other people know that you
      key has been compromised or is no longer in use.
    </p>
    <p>
      You should store your revocation signature in a safe location, e.g. a
      password manager. While it is possible to generate a revocation signature
      at a later point in time, this will need your private key as well as its
      password.
    </p>
    <pre class="signature">{revocationSignature}</pre>
    <span class="button-layout">
      <CopyToClipboard
        toCopy={revocationSignature}
        successMsg="Revocation signature copied to clipboard"
        tooltip="Copy revocation signature to clipboard"
      />
      <Button
        color="primary"
        text="Next >"
        tooltip="Proceed to key upload dialog"
        on:click={() => (state = 'uploadCert')}
      />
    </span>
  {:else if state === 'uploadCert'}
    <UploadCertDialog {onClose} {cert} />
  {:else if state === 'error'}
    <Alert level={NotificationLevel.Error}
      >Failed to generate OpenPGP key pair: {errorMsg}</Alert
    >
  {/if}
</Dialog>

<style>
  label {
    font-weight: bold;
  }

  p {
    margin: 1em 0;
  }

  .signature {
    padding: 1em;
    margin: 1em 0;
    background-color: var(--color-component-bg);
    overflow-x: auto;
    font-size: smaller;
    line-height: 1.2em;
  }

  .grid-layout {
    display: grid;
    align-items: center;
    grid-template-columns: 1fr 2.4fr;
    gap: 1em;
  }

  .grid-layout-confirm {
    display: grid;
    grid-template-columns: 6rem 2fr;
    gap: 0.2rem;
    margin: 0 0 2rem 0.5rem;
  }

  .alert {
    margin-bottom: 1em;
  }

  .button-layout {
    display: flex;
    justify-content: flex-end;
    gap: 0.5rem;
    margin-top: 2rem;
  }
</style>
