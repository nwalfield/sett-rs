<script lang="ts">
  import { type CertInfo, certStore, CertType } from '../store/cert';
  import {
    destinationKind,
    type EncryptOpts,
    paths,
    recipients,
    signer,
    transferId,
  } from '../store/encrypt';
  import {
    encryptLocalProps,
    encryptS3Props,
    encryptSftpProps,
  } from '../store/destination';
  import { NotificationLevel, toasts } from '../store/event';
  import { setContext } from 'svelte';
  import { settingsStore } from '../store/settings';
  import { tasks } from '../store/task';

  import ButtonWorkflow from '../component/ButtonWorkflow.svelte';
  import Chip from '../component/Chip.svelte';
  import Destination from '../component/Destination.svelte';
  import DropZone from '../component/DropZone.svelte';
  import ElementWithTooltip from '../component/ElementWithTooltip.svelte';
  import Page from '../component/Page.svelte';
  import PasswordDialog from '../component/PasswordDialog.svelte';
  import QuickSearch from '../component/QuickSearch.svelte';
  import Section from '../component/Section.svelte';

  let showPasswordDialog = false;
  let password = '';
  let submitDisabled = false;

  setContext('store', {
    localProps: encryptLocalProps,
    s3Props: encryptS3Props,
    sftpProps: encryptSftpProps,
  });

  async function handleSubmit() {
    showPasswordDialog = false;
    submitDisabled = true;
    let destination_props;
    switch ($destinationKind) {
      case 'sftp':
        destination_props = $encryptSftpProps;
        break;
      case 's3':
        destination_props = $encryptS3Props;
        break;
      case 'local':
        destination_props = $encryptLocalProps;
        break;
    }
    const opts: EncryptOpts = {
      kind: 'encrypt',
      files: $paths.map((p) => p.path),
      signer: $signer!.fingerprint,
      recipients: $recipients.map((r) => r.fingerprint),
      transferId: $transferId,
      password,
      destination: { kind: $destinationKind, props: destination_props },
      verifyPackage: $settingsStore.verifyPackage,
      portalUrl: $settingsStore.portalURL,
    };
    try {
      await tasks.check(opts);
    } catch (e) {
      toasts.add({
        level: NotificationLevel.Error,
        message: e as string,
        timeout: 5000,
        dismissible: true,
      });
      submitDisabled = false;
      return;
    }
    tasks.add(opts);
    toasts.add({
      level: NotificationLevel.Info,
      message: 'Encryption job submitted',
      timeout: 5000,
      dismissible: true,
    });
    // reset the form.
    password = '';
    paths.clear();
    recipients.clear();
    $signer = null;
    $transferId = null;
    $encryptLocalProps = '';
    encryptS3Props.reset();
    encryptSftpProps.reset();
    submitDisabled = false;
  }

  // Return the list of certificates in the certStore a list of objects with
  // a `label` and `value` attribute.
  // Optional arguments:
  // * `certType`: filter the certificates to keep only those matching the
  //   specified certificate type.
  // * `exclude`: filter the certificates to exclude those in the `exclude`
  //   list. This is used to exclude certificates that are already selected.
  function certsAsQuickSearchChoice(
    certStoreContent: CertInfo[],
    certType: CertType | null = null,
    exclude: CertInfo[] = [],
  ) {
    // Note: the content of the store is an argument of the function for
    // reactivity reasons (so that Svelte runs the function after the store
    // is populated).
    return certStoreContent
      .filter(
        (certInfo) =>
          (certType ? certInfo.certType === certType : true) &&
          !exclude.includes(certInfo),
      )
      .map((certInfo) => ({
        label: `${certInfo.userid || ''} (${certInfo.fingerprint})`,
        value: certInfo,
      }));
  }

  const removeNonNumeric = (n: number | null): number | null => {
    if (n === null) {
      return null;
    }
    const cleaned = n.toString().replace(/[^0-9]/g, '');
    return cleaned === '' ? null : Number(cleaned);
  };

  $: hideSender = $paths.length == 0;
  $: hideRecipients = hideSender || $signer == null;
  $: hideAdditionalInfo = hideRecipients || $recipients.length == 0;
  $: hideDestination =
    hideRecipients ||
    $recipients.length == 0 ||
    ($transferId === null && $settingsStore.verifyPackage);
  $: hideEncryptButton =
    $destinationKind === 'local' // eslint-disable-line no-nested-ternary
      ? $encryptLocalProps
        ? hideDestination
        : true
      : hideDestination;
  $: $transferId = removeNonNumeric($transferId);
  $: encryptButtonTooltip = `Create ${
    $destinationKind === 'local' ? '' : 'and send '
  }an encrypted data package`;

  function removePath(path?: string) {
    if (path) {
      paths.remove(path);
    }
  }
</script>

<Page title="Encrypt">
  <form on:submit|preventDefault={() => (showPasswordDialog = true)}>
    <Section title="Select files and directories to encrypt">
      <DropZone
        selectedItems={$paths}
        clearItem={removePath}
        selectItemLabel={'Add file'}
        selectDirLabel={'Add directory'}
        helpText={'Drag and drop a file or directory, or click the buttons to select one.'}
      />
    </Section>

    <Section title="Select sender" hidden={hideSender}>
      {#if $signer === null}
        <QuickSearch
          items={certsAsQuickSearchChoice($certStore, CertType.Secret)}
          on:selected={(selected) => ($signer = selected.detail.value)}
        />
      {:else}
        <Chip
          tooltip={`fingerprint: ${$signer.fingerprint}`}
          onClear={() => ($signer = null)}
        >
          {$signer.userid}
        </Chip>
      {/if}
    </Section>

    <Section title="Select recipients" hidden={hideRecipients}>
      <QuickSearch
        items={certsAsQuickSearchChoice($certStore, null, $recipients)}
        on:selected={(selected) => recipients.add(selected.detail.value)}
      />
      {#if $recipients.length > 0}
        <div class="recipients">
          {#each $recipients as recipient}
            <Chip
              tooltip={`fingerprint: ${recipient.fingerprint}`}
              onClear={() => recipients.remove(recipient)}
            >
              {recipient.userid}
            </Chip>
          {/each}
        </div>
      {/if}
    </Section>

    <Section title="Data Transfer Request ID" hidden={hideAdditionalInfo}>
      <div class="dtr-field">
        <ElementWithTooltip
          tooltip="Data Transfer Request ID - according to Portal"
          tooltipPosition="right"
        >
          <div class="dtr-field">
            <label for="dtr-id"> Transfer ID (DTR)</label>
            <input
              type="text"
              pattern="[0-9]*"
              inputmode="numeric"
              bind:value={$transferId}
              autocapitalize="none"
              autocomplete="off"
              autocorrect="off"
              required={$settingsStore.verifyPackage}
              id="dtr-id"
            />
          </div>
        </ElementWithTooltip>
      </div>
    </Section>

    <Section title="Select destination" hidden={hideDestination}>
      <Destination
        bind:destinationKind={$destinationKind}
        destinations={['local', 's3', 'sftp']}
      />
    </Section>

    <ButtonWorkflow
      bind:disabled={submitDisabled}
      hidden={hideEncryptButton}
      icon="Lock"
      text={$destinationKind === 'local' ? 'Encrypt package' : 'Send package'}
      tooltip={encryptButtonTooltip}
    />
  </form>
</Page>

<PasswordDialog
  bind:show={showPasswordDialog}
  bind:password
  on:submit={handleSubmit}
  label="Sender OpenPGP key password"
  buttonLabel={$destinationKind === 'local'
    ? 'Encrypt'
    : 'Encrypt and transfer'}
  buttonIcon="Lock"
/>

<style>
  .dtr-field {
    display: flex;
    flex-direction: row;
    align-items: center;
    gap: 0.8rem;
  }

  .recipients {
    display: flex;
    flex-wrap: wrap;
    gap: 0.2em;
    margin-top: 0.7em;
  }
</style>
