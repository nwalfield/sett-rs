import { type Event, listen } from '@tauri-apps/api/event';
import { type FilePath, paths } from './store/encrypt';

import { NotificationLevel, toasts } from './store/event';
import { route, Route } from './store/route';
import { runNextTask, type Task, tasks, TaskState } from './store/task';
import { decryptOpts } from './store/workflow';
import { fileDragStatus } from './store/event';
import { get } from 'svelte/store';
import { invoke } from '@tauri-apps/api';
import { transferOpts } from './store/transfer';

const NOTIFICATION_TIMEOUT = 10000;

export async function listenFileDrop() {
  await listen('tauri://file-drop', async (event: Event<[string]>) => {
    const currentRoute = get(route);
    fileDragStatus.set(false);
    switch (currentRoute) {
      case Route.Encrypt: {
        const filePaths = await Promise.all(
          event.payload.map(async (path) => {
            const directory: boolean = await invoke('is_dir', {
              path: event.payload[0],
            });
            return { path, directory } as FilePath;
          }),
        );
        paths.add(filePaths);
        break;
      }
      case Route.Decrypt:
      case Route.Transfer: {
        const files = event.payload.filter((f) => f.endsWith('.zip'));
        if (files.length > 0) {
          const packageOpts =
            currentRoute === Route.Decrypt ? decryptOpts : transferOpts;
          packageOpts.updatePackage(files[0]);
          if (files.length > 1) {
            toasts.add({
              level: NotificationLevel.Warning,
              message: `Adding multiple packages not allowed: only the first one was kept.`,
              timeout: NOTIFICATION_TIMEOUT,
              dismissible: true,
            });
          }
        } else {
          event.payload
            .filter((f) => !f.endsWith('.zip'))
            .forEach((f) => {
              toasts.add({
                level: NotificationLevel.Error,
                message: `File ${
                  (f.split('\\').pop() || f).split('/').pop() || f
                } is not a data package (ZIP file)`,
                timeout: NOTIFICATION_TIMEOUT,
                dismissible: true,
              });
            });
        }
        break;
      }
    }
  });
}

type TaskStatusType = 'error' | 'finished' | 'running';

interface TaskEvent {
  taskId: number;
  status: {
    type: TaskStatusType;
    message?: string;
    progress?: number;
    destination?: string;
  };
}

export async function listenTaskEvent() {
  await listen('task', (event: Event<TaskEvent>) => {
    const taskKindToName = {
      encrypt: 'Encryption',
      decrypt: 'Decryption',
      transfer: 'Transfer',
    };
    const statusToName = {
      running: 'running',
      finished: 'finished',
      error: 'failed',
    };
    const statusToLevel = {
      running: NotificationLevel.Info,
      finished: NotificationLevel.Success,
      error: NotificationLevel.Error,
    };

    const addToast = (task: Task, status: TaskStatusType) => {
      toasts.add({
        level: statusToLevel[status],
        message: `${taskKindToName[task.opts.kind]} job ${
          statusToName[status]
        }`,
        timeout: NOTIFICATION_TIMEOUT,
        dismissible: true,
      });
    };

    const task = event.payload;
    switch (task.status.type) {
      case 'running':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            progress: task.status.progress!,
            state: TaskState.Running,
          };
          return current;
        });
        break;
      case 'finished':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            progress: 100,
            state: TaskState.Finished,
            destination: task.status.destination!,
          };
          addToast(current[idx], task.status.type);
          return current;
        });
        runNextTask();
        break;
      case 'error':
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.taskId);
          current[idx] = {
            ...current[idx],
            updated: new Date(),
            state: TaskState.Error,
            message: task.status.message,
          };
          addToast(current[idx], task.status.type);
          return current;
        });
        runNextTask();
        break;
    }
  });
}

export function listenThemeChange() {
  const theme = window.matchMedia('(prefers-color-scheme: dark)');
  if (theme.matches) {
    document.documentElement.setAttribute('data-theme', 'dark');
  }
  const toggle = (e: MediaQueryListEvent) => {
    if (e.matches) {
      document.documentElement.setAttribute('data-theme', 'dark');
    } else {
      document.documentElement.removeAttribute('data-theme');
    }
  };
  theme.addEventListener('change', toggle);
}

export function listenUnhandledRejection() {
  window.onunhandledrejection = (event) => {
    toasts.add({
      level: NotificationLevel.Error,
      message: `${event.reason}`,
      dismissible: true,
    });
  };
}
