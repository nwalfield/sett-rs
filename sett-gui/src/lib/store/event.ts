import { writable } from 'svelte/store';

export interface Toast {
  id?: number;
  level: NotificationLevel;
  message: string;
  timeout?: number;
  dismissible: boolean;
}

export enum NotificationLevel {
  Info = 'INFO',
  Success = 'SUCCESS',
  Warning = 'WARNING',
  Error = 'ERROR',
}

let toastId = 0;

function createToastStore() {
  const { subscribe, update } = writable<Toast[]>([]);

  function add(toast: Toast) {
    const thisToastId = toastId++;
    update((s) => [...s, { ...toast, id: thisToastId }]);

    if (toast.timeout) {
      setTimeout(() => dismiss(thisToastId), toast.timeout);
    }
  }

  function dismiss(id: number) {
    update((s) => s.filter((t) => t.id !== id));
  }

  return {
    subscribe,
    add,
    dismiss,
  };
}

export const toasts = createToastStore();

// Store to track whether a file is currently being dragged over a drop-zone.
export const fileDragStatus = writable<boolean>(false);
