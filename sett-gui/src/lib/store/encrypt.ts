import { homeDir } from '@tauri-apps/api/path';
import { open } from '@tauri-apps/api/dialog';
import { writable } from 'svelte/store';

import type { Destination, DestinationKind } from './destination';
import type { CertInfo } from './cert';

export interface EncryptOpts {
  kind: 'encrypt';
  files: string[];
  signer: string;
  recipients: string[];
  transferId: number | null;
  password: string;
  destination: Destination | null;
  verifyPackage: boolean;
  portalUrl: string;
}

export type FilePath = {
  path: string;
  directory: boolean;
};

function createPathsStore() {
  const { subscribe, update } = writable<FilePath[]>([]);

  function add(newFiles: FilePath[]) {
    update((files) => {
      const newPaths = [
        ...files,
        ...newFiles.filter((file) => !files.some((f) => f.path === file.path)),
      ];
      newPaths.sort(
        (a, b) =>
          // Directories first, then sort by path
          Number(b.directory) - Number(a.directory) ||
          a.path.toLowerCase().localeCompare(b.path.toLowerCase()),
      );
      return newPaths;
    });
  }

  function remove(path: string) {
    update((files) => files.filter((file) => file.path !== path));
  }

  function clear() {
    update(() => []);
  }

  return {
    subscribe,
    add,
    remove,
    clear,
  };
}

function createRecipientsStore() {
  const { subscribe, update } = writable<CertInfo[]>([]);

  function add(recipient: CertInfo) {
    update((s) => [...new Set([...s, recipient])]);
  }

  function remove(recipient: CertInfo) {
    update((s) => s.filter((r) => r.fingerprint !== recipient.fingerprint));
  }

  function clear() {
    update(() => []);
  }

  return {
    subscribe,
    add,
    remove,
    clear,
  };
}

// NOTE: we are using more granular stores here to limit the unwanted
// reactivity caused by updating a larger store object.
export const paths = createPathsStore();
export const signer = writable<CertInfo | null>(null);
export const recipients = createRecipientsStore();
export const transferId = writable<number | null>(null);
export const destinationKind = writable<DestinationKind>('local');

export async function selectPaths(directory: boolean) {
  const selected = await open({
    multiple: true,
    directory,
    defaultPath: await homeDir(),
  });
  if (selected !== null && Array.isArray(selected)) {
    paths.add(selected.map((path) => ({ path, directory }) as FilePath));
  }
}
