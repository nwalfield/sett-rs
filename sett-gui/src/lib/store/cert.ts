import { get, readable, writable } from 'svelte/store';
import { invoke } from '@tauri-apps/api';

import { settingsStore } from './settings';

export enum CertType {
  Public = 'Public',
  Secret = 'Secret',
}

export enum ValidityStatus {
  Valid = 'Valid',
  Revoked = 'Revoked',
}

export enum ReasonForRevocation {
  Unspecified = 'Unspecified',
  Superseded = 'Superseded',
  Compromised = 'Compromised',
  Retired = 'Retired',
}

export interface Validity {
  status: ValidityStatus;
  reasons: ReasonForRevocation[] | null;
  messages: string[] | null;
}

export enum ExpirationDateStatus {
  Valid = 'Valid',
  Expired = 'Expired',
  ReadError = 'ReadError',
}

export interface ExpirationDate {
  status: ExpirationDateStatus;
  details: string;
}

export interface CertInfo {
  fingerprint: string;
  userid: string | null;
  certType: CertType;
  validity: Validity;
  expirationDate: ExpirationDate;
}

export enum ApprovalStatus {
  Approved = 'Approved',
  Revoked = 'Revoked',
  CertificateDeleted = 'CertificateDeleted',
  Pending = 'Pending',
  CertificateRevoked = 'CertificateRevoked',
  CertificateUnknown = 'CertificateUnknown',
  Rejected = 'Rejected',
  PortalUnreachable = 'PortalUnreachable',
  Unknown = 'Unknown',
}

export interface CertApprovalStatus {
  fingerprint: string;
  status: ApprovalStatus;
}

// Location of OpenPGP certificate store on disk.
export interface CertStorePath {
  secret: string | null;
  public: string | null;
}

// Svelte store for OpenPGP certificate infos.
function createCertStore() {
  const { subscribe, set } = writable<CertInfo[]>([]);

  async function refresh() {
    set(await invoke<CertInfo[]>('list_certs'));
  }

  return {
    subscribe,
    refresh,
  };
}

export const certStore = createCertStore();

// Svelte store for certificate approval values.
function createApprovalStore() {
  const { subscribe, set } = writable<Map<string, ApprovalStatus>>(new Map());

  async function refresh(certs: CertInfo[]) {
    const uniqueFingerprints = [
      ...new Set(certs.map((cert) => cert.fingerprint)),
    ];
    try {
      const approvals = await invoke<CertApprovalStatus[]>(
        'get_cert_approval_status_from_portal',
        {
          portalUrl: get(settingsStore).portalURL,
          fingerprints: uniqueFingerprints,
        },
      );
      set(new Map(approvals.map((a) => [a.fingerprint, a.status])));
    } catch (e) {
      set(
        new Map(
          uniqueFingerprints.map((f) => [f, ApprovalStatus.PortalUnreachable]),
        ),
      );
      throw new Error(e as string);
    }
  }

  return {
    subscribe,
    refresh,
  };
}

export const approvalStore = createApprovalStore();

// Svelte store for location of certificate store on disk.
export const certStorePath = readable<CertStorePath>(
  { secret: null, public: null },
  (set) => {
    invoke<CertStorePath>('get_certstore_path').then((storePath) =>
      set(storePath),
    );
  },
);

// Return the path of disk of an OpenPGP certificate specified via its
// fingerprint.
export function getCertPath(
  fingerprint: string,
  certType: CertType,
  certStorePath: CertStorePath,
) {
  fingerprint = fingerprint.toLowerCase();
  const storePath =
    certType === CertType.Public ? certStorePath.public : certStorePath.secret;
  if (storePath) {
    const sep = storePath.startsWith('/') ? '/' : '\\';
    return [storePath, fingerprint.slice(0, 2), fingerprint.slice(2)].join(sep);
  }
  return 'Store path not set';
}

// Status that combines certificate ValidityStatus, ExpirationDateStatus and
// ApprovalStatus.
export enum CertStatus {
  Approved = 'Key is valid and approved',
  Revoked = 'Key approval was revoked',
  CertificateDeleted = 'Key was removed from keyserver by its owner',
  Pending = 'Key is valid, but not approved on Portal yet',
  CertificateRevoked = 'Key was revoked by its owner',
  CertificateUnknown = 'Key is not registered on Portal',
  Rejected = 'Key approval request was rejected',
  PortalUnreachable = 'Key is valid, but approval status cannot be retrieved at this time because Portal website is unreachable',
  Unknown = 'Key is valid but has unknown approval status',
  Expired = 'Key has expired',
  ReadError = 'Expiration date of this key could not be retrieved',
}
