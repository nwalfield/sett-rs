import { get, writable } from 'svelte/store';
import { invoke } from '@tauri-apps/api';

import { type WorkflowOpts } from './workflow';

export enum TaskState {
  Pending = 'PENDING',
  Running = 'RUNNING',
  Finished = 'FINISHED',
  Error = 'ERROR',
}

export interface Task {
  id: number;
  started: Date | null;
  updated: Date | null;
  opts: WorkflowOpts;
  state: TaskState;
  progress: number;
  message?: string;
  destination: string | null;
}

let taskId = 0;

function createTasks() {
  const { subscribe, update } = writable<Task[]>([]);

  async function check(opts: WorkflowOpts) {
    switch (opts.kind) {
      case 'encrypt':
        await invoke('check_task', {
          task: {
            Encrypt: {
              files: opts.files,
              signer: opts.signer,
              recipients: opts.recipients,
              transferId: opts.transferId,
              password: opts.password,
              destination: opts.destination?.props,
              verify: opts.verifyPackage,
              portalUrl: opts.portalUrl,
            },
          },
        });
        break;
      case 'decrypt':
        await invoke('check_task', {
          task: {
            Decrypt: {
              package: opts.package,
              destination: opts.destination,
              signer: opts.metadata!.sender,
              recipients: opts.metadata!.recipients,
              password: opts.password,
            },
          },
        });
        break;
      case 'transfer':
        await invoke('check_task', {
          task: {
            Transfer: {
              package: opts.package,
              destination: opts.destination?.props,
              verify: opts.verifyPackage,
              portalUrl: opts.portalUrl,
            },
          },
        });
        break;
    }
  }

  function add(opts: WorkflowOpts) {
    const newTask = {
      id: taskId++,
      started: null,
      updated: null,
      opts,
      state: TaskState.Pending,
      progress: 0,
      destination: null,
    };
    // Prepend the new task to the list
    update((s) => [newTask, ...s]);
    runNextTask();
  }

  function remove(id: number) {
    update((s) => s.filter((t) => t.id !== id));
  }

  return {
    subscribe,
    update,
    add,
    remove,
    check,
  };
}

export const tasks = createTasks();

export function runNextTask() {
  const taskList = get(tasks);
  // We only run one task at a time
  if (!taskList.find((t) => t.state === TaskState.Running)) {
    // We take the pending task with the lowest id (first in, first served)
    const pending = taskList
      .filter((t) => t.state === TaskState.Pending)
      .sort((a, b) => a.id - b.id);
    if (pending.length) {
      const task = pending[0];

      tasks.update((current) => {
        const idx = current.findIndex((t) => t.id === task.id);
        current[idx] = {
          ...current[idx],
          started: new Date(),
        };
        return current;
      });

      const forgetPassword = () => {
        tasks.update((current) => {
          const idx = current.findIndex((t) => t.id === task.id);
          const old = current[idx];
          // NOTE: this check is necessary to avoid Svelte linter errors
          if (old.opts.kind === 'encrypt' || old.opts.kind === 'decrypt') {
            current[idx] = {
              ...old,
              opts: { ...old.opts, password: '' },
            };
          }
          return current;
        });
      };

      switch (task.opts.kind) {
        case 'encrypt':
          invoke('run_task', {
            task: {
              id: task.id,
              kind: {
                Encrypt: {
                  files: task.opts.files,
                  signer: task.opts.signer,
                  recipients: task.opts.recipients,
                  transferId: task.opts.transferId,
                  password: task.opts.password,
                  destination: task.opts.destination?.props,
                  verify: task.opts.verifyPackage,
                  portalUrl: task.opts.portalUrl,
                },
              },
            },
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
          }).catch((_) => {}); // error handled in a listener
          forgetPassword();
          break;
        case 'decrypt':
          invoke('run_task', {
            task: {
              id: task.id,
              kind: {
                Decrypt: {
                  package: task.opts.package,
                  destination: task.opts.destination,
                  signer: task.opts.metadata!.sender,
                  recipients: task.opts.metadata!.recipients,
                  password: task.opts.password,
                },
              },
            },
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
          }).catch((_) => {}); // error handled in a listener
          forgetPassword();
          break;
        case 'transfer':
          invoke('run_task', {
            task: {
              id: task.id,
              kind: {
                Transfer: {
                  package: task.opts.package,
                  destination: task.opts.destination?.props,
                  verify: task.opts.verifyPackage,
                  portalUrl: task.opts.portalUrl,
                },
              },
            },
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
          }).catch((_) => {}); // error handled in a listener
          break;
      }
    }
  }
}
