import { writable, type Writable } from 'svelte/store';

export type S3Props = {
  url: string;
  bucket: string;
  accessKey: string;
  secretKey: string;
  sessionToken?: string;
};

export type SftpProps = {
  username: string;
  host: string;
  basePath: string;
  sshKey: SSHKey;
};

type SSHKey = {
  location: string;
  password: string;
};

export type LocalProps = string;
export type DestinationProps = LocalProps | S3Props | SftpProps;
export type DestinationKind = 'local' | 's3' | 'sftp';
export type Destination = {
  kind: DestinationKind;
  props: DestinationProps;
};
export type ResetProps = { reset: () => void };

const reset = (update: CallableFunction, empty: S3Props | SftpProps) => () =>
  update((s: S3Props) => ({
    ...s,
    ...empty,
  }));

const createS3Props = (): ResetProps & Writable<S3Props> => {
  const createEmpty = () => ({
    url: '',
    bucket: '',
    accessKey: '',
    secretKey: '',
    sessionToken: '',
  });
  const { subscribe, set, update } = writable<S3Props>(createEmpty());

  return {
    subscribe,
    set,
    update,
    reset: reset(update, createEmpty()),
  };
};

const createSftpProps = (): ResetProps & Writable<SftpProps> => {
  const createEmpty = () => ({
    username: '',
    host: '',
    basePath: '',
    sshKey: {
      location: '',
      password: '',
    },
  });
  const { subscribe, set, update } = writable<SftpProps>(createEmpty());
  return {
    subscribe,
    update,
    set,
    reset: reset(update, createEmpty()),
  };
};

export const transferS3Props = createS3Props();
export const transferSftpProps = createSftpProps();

export const encryptLocalProps = writable<LocalProps>('');
export const encryptS3Props = createS3Props();
export const encryptSftpProps = createSftpProps();
