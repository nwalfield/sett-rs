import { writable, type Writable } from 'svelte/store';
import { invoke } from '@tauri-apps/api/tauri';

import { NotificationLevel, toasts } from './event';
import type { EncryptOpts } from './encrypt';
import type { TransferOpts } from './transfer';

interface PkgMetadata {
  checksum: string;
  checksum_algorithm: string;
  purpose: string | null;
  recipients: string[];
  sender: string;
  timestamp: string;
  transfer_id: number | null;
  version: string;
}

export type PkgOpts = {
  package: string;
  metadata: PkgMetadata | null;
};

export type UpdatePackageOpts = { updatePackage: (path: string) => void };

export interface DecryptOpts extends PkgOpts {
  kind: 'decrypt';
  destination: string;
  password: string | null;
}

export type WorkflowOpts = DecryptOpts | EncryptOpts | TransferOpts;

export function updatePackage(update: CallableFunction) {
  return async (path: string) => {
    if (path) {
      try {
        const metadata: PkgMetadata = await invoke('read_pkg_metadata', {
          path,
        });
        update((s: PkgOpts) => ({
          ...s,
          package: path,
          metadata,
        }));
      } catch (e) {
        toasts.add({
          level: NotificationLevel.Error,
          message: `Failed to load data package (${
            (path.split('\\').pop() || path).split('/').pop() || path
          }): ${e}`,
          dismissible: true,
        });
      }

      // Case where an empty sting is passed: package values are reset.
    } else {
      update((s: PkgOpts) => ({
        ...s,
        package: '',
        metadata: null,
      }));
    }
  };
}

function createDecryptOpts(): UpdatePackageOpts & Writable<DecryptOpts> {
  const { subscribe, set, update } = writable<DecryptOpts>({
    kind: 'decrypt',
    package: '',
    destination: '',
    metadata: null,
    password: null,
  });

  return {
    subscribe,
    set,
    update,
    updatePackage: updatePackage(update),
  };
}

export const decryptOpts = createDecryptOpts();
