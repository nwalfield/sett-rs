import { writable } from 'svelte/store';

export enum Route {
  About = 'about',
  Certs = 'certs',
  Decrypt = 'decrypt',
  Encrypt = 'encrypt',
  Tasks = 'tasks',
  Settings = 'settings',
  Transfer = 'transfer',
}

export const route = writable(Route.Encrypt);
