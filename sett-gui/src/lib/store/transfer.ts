import type { Destination, DestinationKind } from './destination';
import type { PkgOpts, UpdatePackageOpts } from './workflow';
import { writable, type Writable } from 'svelte/store';
import { updatePackage } from './workflow';

export interface TransferOpts extends PkgOpts {
  kind: 'transfer';
  destination: Destination | null;
  verifyPackage: boolean;
  portalUrl: string;
}

export type UpdateDestination = { updateDestination: CallableFunction };

const updateDestination =
  (update: CallableFunction) => (destination: Destination) =>
    update((s: TransferOpts) => ({
      ...s,
      destination,
    }));

function createTransferOpts(): UpdateDestination &
  UpdatePackageOpts &
  Writable<TransferOpts> {
  const { subscribe, set, update } = writable<TransferOpts>({
    kind: 'transfer',
    package: '',
    metadata: null,
    destination: null,
    verifyPackage: true,
    portalUrl: '',
  });

  return {
    subscribe,
    set,
    update,
    updatePackage: updatePackage(update),
    updateDestination: updateDestination(update),
  };
}

export const transferOpts = createTransferOpts();
export const destinationKind = writable<DestinationKind>('s3');
