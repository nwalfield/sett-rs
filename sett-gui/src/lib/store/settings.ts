import { appLogDir, homeDir } from '@tauri-apps/api/path';
import { getName } from '@tauri-apps/api/app';
import { Store } from 'tauri-plugin-store-api';
import { writable } from 'svelte/store';

import { removeTrailingPathSep } from '../page/utils';

export const lastSelectedDir = writable<string>('');

export type Settings = {
  defaultOutputDir: string;
  portalURL: string;
  keyserverURL: string;
  verifyPackage: boolean;
  logFile: string;
};

const applicationStore = new Store('.settings.dat');

async function logFilePath(): Promise<string> {
  try {
    const appLogDirPath = await appLogDir();
    const appName = await getName();
    return appLogDirPath + appName + '.log';
  } catch {
    return 'error retrieving log file path';
  }
}

function initialSettings(): Settings {
  return {
    defaultOutputDir: '',
    portalURL: 'https://portal.dcc.sib.swiss',
    keyserverURL: 'https://keys.openpgp.org',
    verifyPackage: true,
    logFile: '',
  };
}

async function defaultSettings(): Promise<Settings> {
  return {
    ...initialSettings(),
    defaultOutputDir: removeTrailingPathSep(await homeDir()),
    logFile: await logFilePath(),
  };
}

function createSettingsStore() {
  const { subscribe, set } = writable<Settings>(initialSettings());

  async function load() {
    set(
      await Promise.all([
        defaultSettings(),
        applicationStore.get<Settings>('settings'),
      ]).then(([defaults, fromStore]) => {
        return { ...defaults, ...fromStore };
      }),
    );
  }

  async function save(settings: Settings) {
    await applicationStore.set('settings', settings);
    await applicationStore.save();
    set(settings);
  }

  async function reset() {
    save(await defaultSettings());
  }

  return {
    subscribe,
    load,
    set: save,
    reset,
  };
}

export const settingsStore = createSettingsStore();

// App name.
export const appName = 'sett';
