# sett GUI - developer instructions

## Prerequisites

See the official
[Tauri documentation](https://tauri.app/v1/guides/getting-started/prerequisites)
for initial setup.

After installing prerequisites, install `tauri-cli`:

```shell
cargo install tauri-cli
```

You also need a recent version (_>=18.0.0_) of `node` to build
the **frontend**. Download the official installer from the [node](https://nodejs.org/) homepage or use [nvm](https://github.com/nvm-sh/nvm)
if you want to manage multiple **node** versions.

## Development

**Tauri** allows running an application in **development mode**. This is a special mode which automatically refreshes the application when changes are made to the code (which is very handy).

To start the application in dev mode, run the commands in the current directory
(`sett-gui`):

```shell
npm install
cargo tauri dev
```

For details kindly refer to [Tauri docs](https://tauri.app/v1/guides/development/development-cycle/).

### VS Code users - recommended IDE Setup

When using [VS Code](https://code.visualstudio.com/), it is recommended to install and use following extensions:

- [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode)
- [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode)
- [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

### GUI Icons

Icons for the GUI are taken from [heroicons.com](https://heroicons.com).

## Release builds

```shell
npm install
cargo tauri build
```

For details see [Tauri docs](https://tauri.app/v1/guides/building/).

NOTE: **Linux** bundles should be built on the oldest possible system (for more information see [Tauri docs](https://tauri.app/v1/guides/building/linux#limitations)).
